<?php

if(!function_exists('cortex_mikado_register_sidebars')) {
    /**
     * Function that registers theme's sidebars
     */
    function cortex_mikado_register_sidebars() {

        register_sidebar(array(
            'name' => esc_html__('Sidebar', 'cortex'),
            'id' => 'sidebar',
            'description' => esc_html__('Default Sidebar', 'cortex'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="mkdf-widget-title">',
            'after_title' => '</h5>'
        ));

    }

    add_action('widgets_init', 'cortex_mikado_register_sidebars');
}

if(!function_exists('cortex_mikado_add_support_custom_sidebar')) {
    /**
     * Function that adds theme support for custom sidebars. It also creates CortexMikadoSidebar object
     */
    function cortex_mikado_add_support_custom_sidebar() {
        add_theme_support('CortexMikadoSidebar');
        if (get_theme_support('CortexMikadoSidebar')) new CortexMikadoSidebar();
    }

    add_action('after_setup_theme', 'cortex_mikado_add_support_custom_sidebar');
}
