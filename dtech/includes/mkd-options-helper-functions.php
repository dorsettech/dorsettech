<?php

if(!function_exists('cortex_mikado_is_responsive_on')) {
    /**
     * Checks whether responsive mode is enabled in theme options
     * @return bool
     */
    function cortex_mikado_is_responsive_on() {
        return cortex_mikado_options()->getOptionValue('responsiveness') !== 'no';
    }
}