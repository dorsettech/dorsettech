(function($) {
    'use strict';

    var portfolio = {};
    mkdf.modules.portfolio = portfolio;

    portfolio.mkdfOnDocumentReady = mkdfOnDocumentReady;
    portfolio.mkdfOnWindowLoad = mkdfOnWindowLoad;
    portfolio.mkdfOnWindowResize = mkdfOnWindowResize;
    portfolio.mkdfOnWindowScroll = mkdfOnWindowScroll;

    portfolio.mkdfPortfolioSingleMasonry = mkdfPortfolioSingleMasonry;
    portfolio.mkdfPortfolioWideSlider = mkdfPortfolioWideSlider;
    portfolio.mkdfPortfolioRelatedProducts = mkdfPortfolioRelatedProducts;

    $(document).ready(mkdfOnDocumentReady);
    $(window).load(mkdfOnWindowLoad);
    $(window).resize(mkdfOnWindowResize);
    $(window).scroll(mkdfOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function mkdfOnDocumentReady() {
        mkdfPortfolioSingleMasonry();
        mkdfPortfolioWideSlider();
        mkdfPortfolioFullScreenSlider().init();
        mkdfPortfolioRelatedProducts();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function mkdfOnWindowLoad() {
        mkdfPortfolioSingleFollow().init();
        mkdfPortfolioSingleStick().init();
    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function mkdfOnWindowResize() {

    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function mkdfOnWindowScroll() {

    }



    var mkdfPortfolioSingleFollow = function() {

        var info = $('.mkdf-follow-portfolio-info .small-images.mkdf-portfolio-single-holder .mkdf-portfolio-info-holder, ' +
            '.mkdf-follow-portfolio-info .small-slider.mkdf-portfolio-single-holder .mkdf-portfolio-info-holder, ' +
            '.mkdf-follow-portfolio-info .small-masonry.mkdf-portfolio-single-holder .mkdf-portfolio-info-holder, ' +
            '.mkdf-follow-portfolio-info .wide-images.mkdf-portfolio-single-holder .mkdf-portfolio-info-holder, ' +
            '.mkdf-follow-portfolio-info .gallery.mkdf-portfolio-single-holder .mkdf-portfolio-info-holder');

        if (info.length) {
            var infoHolder = info.parent(),
                infoHolderOffset = infoHolder.offset().top,
                infoHolderHeight = infoHolder.height(),
                mediaHolder = $('.mkdf-portfolio-media'),
                mediaHolderHeight = mediaHolder.height(),
                header = $('.header-appear, .mkdf-fixed-wrapper'),
                headerHeight = (header.length) ? header.height() : 0;
        }

        var infoHolderPosition = function() {

            if(info.length && mkdf.windowWidth > 1024) {

                if (mediaHolderHeight > infoHolderHeight) {
                    if(mkdf.scroll > infoHolderOffset) {
                        var marginTop = mkdf.scroll - infoHolderOffset + mkdfGlobalVars.vars.mkdfAddForAdminBar + headerHeight + 20; //20 px is for styling, spacing between header and info holder
                        // if scroll is initially positioned below mediaHolderHeight
                        if(marginTop + infoHolderHeight > mediaHolderHeight){
                            marginTop = mediaHolderHeight - infoHolderHeight;
                        }
                        info.animate({
                            marginTop: marginTop
                        });
                    }
                }

            }
        };

        var recalculateInfoHolderPosition = function() {

            if (info.length && mkdf.windowWidth > 1024) {
                if(mediaHolderHeight > infoHolderHeight) {
                    if(mkdf.scroll > infoHolderOffset) {

                        if(mkdf.scroll + headerHeight + mkdfGlobalVars.vars.mkdfAddForAdminBar + infoHolderHeight + 100 < infoHolderOffset + mediaHolderHeight) {    //70 to prevent mispositioning

                            //Calculate header height if header appears
                            if ($('.header-appear, .mkdf-fixed-wrapper').length) {
                                headerHeight = $('.header-appear, .mkdf-fixed-wrapper').height();
                            }
                            info.stop().animate({
                                marginTop: (mkdf.scroll - infoHolderOffset + mkdfGlobalVars.vars.mkdfAddForAdminBar + headerHeight + 20) //20 px is for styling, spacing between header and info holder
                            });
                            //Reset header height
                            headerHeight = 0;
                        }
                        else{
                            info.stop().animate({
                                marginTop: mediaHolderHeight - infoHolderHeight
                            });
                        }
                    } else {
                        info.stop().animate({
                            marginTop: 0
                        });
                    }
                }
            }
        };

        return {

            init : function() {

                infoHolderPosition();
                $(window).scroll(function(){
                    recalculateInfoHolderPosition();
                });

            }

        };

    };



    /**
     * Init Portfolio Single Masonry
     */
    function mkdfPortfolioSingleMasonry(){
        var gallery = $('.mkdf-portfolio-single-holder.small-masonry .mkdf-portfolio-media, .mkdf-portfolio-single-holder.big-masonry .mkdf-portfolio-media');

        if(gallery.length) {
            gallery.each(function () {
                var thisGallery = $(this);
                thisGallery.waitForImages(function () {
                    var size = thisGallery.find('.mkdf-single-masonry-grid-sizer').width();
                    mkdfPortfolioSingleResizeMasonry(size,thisGallery);
                    mkdfInitSingleMasonry(thisGallery);

                });
                $(window).resize(function(){
                    var size = thisGallery.find('.mkdf-single-masonry-grid-sizer').width();
                    mkdfPortfolioSingleResizeMasonry(size,thisGallery);
                    mkdfInitSingleMasonry(thisGallery);
                });
            });
        }
    }

    function mkdfInitSingleMasonry(container){
        container.animate({opacity: 1});
        container.isotope({
            itemSelector: '.mkdf-portfolio-single-media',
            masonry: {
                columnWidth: '.mkdf-single-masonry-grid-sizer'
            }
        });
    }


    function mkdfPortfolioSingleResizeMasonry(size,container){

        var defaultMasonryItem = container.find('.mkdf-default-masonry-item');
        var largeWidthMasonryItem = container.find('.mkdf-large-width-masonry-item');
        var largeHeightMasonryItem = container.find('.mkdf-large-height-masonry-item');
        var largeWidthHeightMasonryItem = container.find('.mkdf-large-width-height-masonry-item');

        defaultMasonryItem.css('height', size);
        largeHeightMasonryItem.css('height', Math.round(2*size));

        if(mkdf.windowWidth > 600){
            largeWidthHeightMasonryItem.css('height', Math.round(2*size));
            largeWidthMasonryItem.css('height', size);
        }else{
            largeWidthHeightMasonryItem.css('height', size);
            largeWidthMasonryItem.css('height', Math.round(size/2));
        }
    }

    function mkdfPortfolioRelatedProducts(){
		var relatedProducts = $('.mkdf-portfolio-related-holder');

		if (relatedProducts.length){
			var prevArrow = relatedProducts.find('.mkdf-related-prev'),
				nextArrow = relatedProducts.find('.mkdf-related-next'),
				prevSlick = relatedProducts.find('.mkdf-slick-prev'),
				nextSlick = relatedProducts.find('.mkdf-slick-next');

			prevArrow.click(function(){
				prevSlick.trigger("click");
			});

			nextArrow.click(function(){
				nextSlick.trigger("click");
			});
		}
    }


    function mkdfPortfolioWideSlider(){
    	var wideSlider = $('.mkdf-ptf-wide-slider');

    	if (wideSlider.length){

    		wideSlider.slick({
				infinite: true,
				autoplay: true,
				slidesToShow : 3,
				centerMode: true,
				arrows: true,
				dots: false,
				adaptiveHeight: true,
				prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="lnr lnr-chevron-left"></span></span>',
				nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"><span class="lnr lnr-chevron-right"></span></span>'
			});
    	}
    }
        /* Portfolio Single Split*/
    var mkdfPortfolioSingleStick = function(){
    	var portfolioSplit = $('.mkdf-portfolio-single-holder.split-screen');
        var info = $('.mkdf-follow-portfolio-info .split-screen.mkdf-portfolio-single-holder .mkdf-portfolio-info-holder');
        if (info.length && mkdf.htmlEl.hasClass('no-touch')) {
            var infoHolder = info.parent(),
                infoHolderOffset = infoHolder.offset().top,
                infoHolderHeight = info.outerHeight() + 100, //30 is some default margin
                mediaHolder = $('.mkdf-portfolio-media'),
                mediaHolderHeight = mediaHolder.height(),
                header = $('.mkdf-page-header'),
                fixedHeader = header.find('.mkdf-fixed-wrapper'),
                headerHeight = (header.length) ? header.height() : 0,
                fixedHeaderHeight = (fixedHeader.length) ? fixedHeader.height() : 0,
                infoHolderOffsetAfterScroll = headerHeight + 15; //15 is some default margin

        }

        var infoWidth = function() {
			if(info.length && mkdf.htmlEl.hasClass('no-touch')){
				info.css('width',info.width());
			}
        };


        var initInfoHolder = function(){
            if(info.length && mkdf.htmlEl.hasClass('no-touch')){
				var stickyActive = header.find('.mkdf-sticky-header');
				if (stickyActive.length){
					if (!stickyActive.hasClass('header-appear')){
						var headerVisible = (headerHeight - mkdf.scroll) > 0 ? true : false;
						if (headerVisible){
							infoHolderOffsetAfterScroll = mkdfGlobalVars.vars.mkdfAddForAdminBar + headerHeight - 5; // 5 is designer wishes
						}
						else{
							infoHolderOffsetAfterScroll = 24;
						}
					}
					else{
						infoHolderOffsetAfterScroll = mkdfGlobalVars.vars.mkdfStickyHeaderTransparencyHeight + mkdfGlobalVars.vars.mkdfAddForAdminBar + 15;
					}
				}
				else if (fixedHeader.length){
					infoHolderOffsetAfterScroll = mkdfGlobalVars.vars.mkdfAddForAdminBar + fixedHeaderHeight + 15; // 5 is designer wishes
				}
				if(info.length && mediaHolderHeight > infoHolderHeight && mkdf.htmlEl.hasClass('no-touch')) {
					info.css('top',infoHolderOffsetAfterScroll+'px');
				}
			}
        };

        var calcInfoHolderPosition = function(){
            if(info.length && mkdf.htmlEl.hasClass('no-touch')){
                infoHolderHeight = info.outerHeight() + 30;
                mediaHolderHeight = mediaHolder.height();

                if(mediaHolderHeight > infoHolderHeight && mkdf.htmlEl.hasClass('no-touch')) {
                	if (fixedHeader.length){
                		headerHeight = fixedHeaderHeight;
                	}
            		if(mkdf.scroll >= infoHolderOffset - headerHeight - mkdfGlobalVars.vars.mkdfAddForAdminBar){
            			if (info.css('position') !== 'fixed'){
							info.css('position','fixed');
							if (mkdf.scroll > 0) {
								info.addClass('mkdf-animating');
								info.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
									info.removeClass('mkdf-animating');
								});
							}
						}
                    }else{
                        info.css('position','static');
                    }

                    if(infoHolderOffset+mediaHolderHeight<=mkdf.scroll+infoHolderHeight + infoHolderOffsetAfterScroll){
                        info.stop().css('margin-top',infoHolderOffset + mediaHolderHeight - mkdf.scroll - infoHolderHeight - infoHolderOffsetAfterScroll+'px');
                    }else{
                        info.css('margin-top','0');
                    }
                }
				if (!info.hasClass('mkdf-appeared')){
					info.addClass('mkdf-appeared');
				}
            }
            else if (mkdf.htmlEl.hasClass('touch')){
				if (!info.hasClass('mkdf-appeared')){
					info.addClass('mkdf-appeared');
				}
            }
        };

        return {
            init: function(){
				if (portfolioSplit.length){
					infoWidth();
					calcInfoHolderPosition();
					initInfoHolder();
					$(window).scroll(function(){
						calcInfoHolderPosition();
						initInfoHolder();
					});
					$(window).resize(function(){
						initInfoHolder();
						calcInfoHolderPosition();
					});
				}
            }
        };
    };
    /**
     * Init Full Screen Slider
     */
    var mkdfPortfolioFullScreenSlider = function() {

        var sliderHolder = $('.mkdf-full-screen-slider-holder');
        var content = $('.mkdf-wrapper .mkdf-content');

        var sliders = $('.mkdf-portfolio-full-screen-slider');
        var fullScreenSliderHolder = $('.full-screen-slider');

        var mkdfFullScreenSliderHeight = function() {
            if (sliderHolder.length) {

                var contentMargin = parseInt(content.css('margin-top')),
                	imageHolder = sliderHolder.find('.mkdf-portfolio-single-media'),
                	title = $('.mkdf-title'),
                	paspartuHeight = 0,
                	sliderHeight = mkdf.windowHeight;


                if (mkdf.body.hasClass('mkdf-passepartout')){
                	var paspartu = $('.mkdf-passepartout-top');

                	paspartuHeight = paspartu.outerHeight() * 2;
                	sliderHeight -= paspartuHeight;
                }

                if (title.length){
                	sliderHeight -= title.height();
                }

                if(mkdf.windowWidth > 1024) {
                    if(contentMargin >= 0) {
                        sliderHeight -= mkdfGlobalVars.vars.mkdfMenuAreaHeight;
                    }
                }
                else {
                    sliderHeight -= mkdfGlobalVars.vars.mkdfMobileHeaderHeight;
                }

                fullScreenSliderHolder.css("height", sliderHeight);
                sliderHolder.css("height", sliderHeight);
                imageHolder.css("height", sliderHeight);
            }
        };

        var mkdfFullScreenSlider = function() {

            if (sliderHolder.length) {
                sliders.each(function () {
                    var slider = $(this);


                    slider.on('init', function(slick){
						var activeSlide = slider.find('.slick-active.mkdf-portfolio-single-media');
                        if(activeSlide.hasClass('mkdf-slide-dark-skin')){
                            slider.removeClass('mkdf-slide-light-skin').addClass('mkdf-slide-dark-skin');
                        }else{
                            slider.removeClass('mkdf-slide-dark-skin').addClass('mkdf-slide-light-skin');
                        }
					});

                    slider.on('afterChange', function(slick, currentSlide){
						var activeSlide = slider.find('.slick-active.mkdf-portfolio-single-media');
                        if(activeSlide.hasClass('mkdf-slide-dark-skin')){
                            slider.removeClass('mkdf-slide-light-skin').addClass('mkdf-slide-dark-skin');
                        }else{
                            slider.removeClass('mkdf-slide-dark-skin').addClass('mkdf-slide-light-skin');
                        }
					});

                    slider.slick({
						vertical: true,
						verticalSwiping: true,
						infinite: true,
						slidesToShow : 1,
						arrows: true,
						dots: false,
	                    easing: 'easeOutQuart',
						dotsClass: 'mkdf-slick-dots',
						prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="arrow_up"></span></span>',
						nextArrow: '<span class="mkdf-slick-next mkdf-prev-icon"><span class="arrow_down"></span></span>',
						customPaging: function(slider, i) {
							return '<span class="mkdf-slick-dot-inner"></span>';
						}
                    }).animate({'opacity': 1}, 600);
                });
            }

        };

        var mkdfFullScreenSliderInfo = function() {

            if (sliderHolder.length) {

                var sliderContent = $('.mkdf-portfolio-slider-content');
                var close = $('.mkdf-control.mkdf-close');
                var description = $('.mkdf-description');
                var info = $('.mkdf-portfolio-slider-content-info');

                sliderContent.on('click',function(e){
                    e.preventDefault();
                    if (!sliderContent.hasClass('opened')) {
                        description.fadeOut(400, function() {
                            sliderContent.addClass('opened');
                            setTimeout(function(){
                                info.fadeIn(400);
                            }, 400);
                            setTimeout(function(){
                                $(".mkdf-portfolio-slider-content-info").niceScroll({
                                    scrollspeed: 60,
                                    mousescrollstep: 40,
                                    cursorwidth: 0,
                                    cursorborder: 0,
                                    cursorborderradius: 0,
                                    cursorcolor: "transparent",
                                    autohidemode: false,
                                    horizrailenabled: false
                                });
                            }, 800);
                        });
                    }
                });

                close.on('click',function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    info.fadeOut( 400, function() {
                        sliderContent.removeClass('opened');
                        setTimeout(function() {
                            description.fadeIn(400);
                        }, 400);
                    });
                });

            }

        };
        return {
            init : function() {
                mkdfFullScreenSliderHeight();
                mkdfFullScreenSlider();
                mkdfFullScreenSliderInfo();

                $(window).resize(function(){
                    mkdfFullScreenSliderHeight();
                });
            }
        };
    };
    
})(jQuery);