(function($) {
    'use strict';

    var shortcodes = {};

    mkdf.modules.shortcodes = shortcodes;

    shortcodes.mkdfInitCounter = mkdfInitCounter;
    shortcodes.mkdfInitProgressBars = mkdfInitProgressBars;
    shortcodes.mkdfInitCountdown = mkdfInitCountdown;
    shortcodes.mkdfInitMessages = mkdfInitMessages;
    shortcodes.mkdfInitMessageHeight = mkdfInitMessageHeight;
    shortcodes.mkdfInitTestimonials = mkdfInitTestimonials;
    shortcodes.mkdfInitCarousels = mkdfInitCarousels;
    shortcodes.mkdfInitPieChart = mkdfInitPieChart;
    shortcodes.mkdfInitPieChartDoughnut = mkdfInitPieChartDoughnut;
    shortcodes.mkdfInitTabs = mkdfInitTabs;
    shortcodes.mkdfInitTabIcons = mkdfInitTabIcons;
    shortcodes.mkdfInitBlogListMasonry = mkdfInitBlogListMasonry;
    shortcodes.mkdfInitBlogListBoxes = mkdfInitBlogListBoxes;
    shortcodes.mkdfCustomFontResize = mkdfCustomFontResize;
    shortcodes.mkdfInitImageGallery = mkdfInitImageGallery;
    shortcodes.mkdfInitAccordions = mkdfInitAccordions;
    shortcodes.mkdfShowGoogleMap = mkdfShowGoogleMap;
    shortcodes.mkdfInitPortfolioListMasonry = mkdfInitPortfolioListMasonry;
    shortcodes.mkdfInitPortfolioListPinterest = mkdfInitPortfolioListPinterest;
    shortcodes.mkdfInitPortfolio = mkdfInitPortfolio;
    shortcodes.mkdfInitPortfolioMasonryFilter = mkdfInitPortfolioMasonryFilter;
    shortcodes.mkdfInitPortfolioSlider = mkdfInitPortfolioSlider;
    shortcodes.mkdfInitPortfolioLoadMore = mkdfInitPortfolioLoadMore;
    shortcodes.mkdfCheckSliderForHeaderStyle = mkdfCheckSliderForHeaderStyle;
    shortcodes.mkdfCustomFontTypeOut = mkdfCustomFontTypeOut;
    shortcodes.mkdfItemShowcase = mkdfItemShowcase;
    shortcodes.mkdfSectionTitleTypeOut = mkdfSectionTitleTypeOut;
    shortcodes.mkdfInitSectionHolder = mkdfInitSectionHolder;
    shortcodes.mkdfInitImageGalleryMasonry = mkdfInitImageGalleryMasonry;
    shortcodes.mkdfInitTextSlider = mkdfInitTextSlider;
    shortcodes.mkdfInitInteractiveItems = mkdfInitInteractiveItems;
    shortcodes.mkdfParallaxCTA = mkdfParallaxCTA;
    shortcodes.mkdfScrollToContent = mkdfScrollToContent;

    shortcodes.mkdfOnDocumentReady = mkdfOnDocumentReady;
    shortcodes.mkdfOnWindowLoad = mkdfOnWindowLoad;
    shortcodes.mkdfOnWindowResize = mkdfOnWindowResize;
    shortcodes.mkdfOnWindowScroll = mkdfOnWindowScroll;

    $(document).ready(mkdfOnDocumentReady);
    $(window).load(mkdfOnWindowLoad);
    $(window).resize(mkdfOnWindowResize);
    $(window).scroll(mkdfOnWindowScroll);

    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function mkdfOnDocumentReady() {
        mkdfInitCounter();
        mkdfInitProgressBars();
        mkdfInitCountdown();
        mkdfIcon().init();
        mkdfInitMessages();
        mkdfInitMessageHeight();
        mkdfInitTestimonials();
        mkdfInitCarousels();
        mkdfInitPieChart();
        mkdfInitPieChartDoughnut();
        mkdfInitTabs();
        mkdfInitTabIcons();
        mkdfButton().init();
        mkdfInitBlogListMasonry();
        mkdfInitBlogListBoxes();
		mkdfInitBlogSlider();
        mkdfCustomFontResize();
        mkdfInitImageGallery();
        mkdfInitAccordions();
        mkdfShowGoogleMap();
        mkdfInitPortfolioListMasonry();
        mkdfInitPortfolioListPinterest();
        mkdfInitPortfolio();
        mkdfInitPortfolioMasonryFilter();
        mkdfInitPortfolioSlider();
        mkdfInitPortfolioLoadMore();
        mkdfSlider().init();
        mkdfSocialIconWidget().init();
        mkdfInitIconList().init();
	    mkdfInitVerticalSplitSlider();
	    mkdfCustomFontTypeOut();
	    mkdfItemShowcase();
	    mkdfSectionTitleTypeOut();
	    mkdfInitImageGalleryMasonry();
	    mkdfInitTextSlider();
	    mkdfInitInteractiveItems();
        mkdfParallaxCTA();
        mkdfScrollToContent();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function mkdfOnWindowLoad() {
    	mkdfInitSectionHolder();
    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function mkdfOnWindowResize() {
        mkdfCircleGoogleMapResize();
        mkdfInitBlogListMasonry();
        mkdfCustomFontResize();
        mkdfInitPortfolioListMasonry();
        mkdfInitPortfolioListPinterest();
    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function mkdfOnWindowScroll() {
        
    }

    

    /**
     * Counter Shortcode
     */
    function mkdfInitCounter() {

        var counters = $('.mkdf-counter');


        if (counters.length) {
            counters.each(function() {
                var counter = $(this);
                counter.appear(function() {
                    counter.parent().addClass('mkdf-counter-holder-show');

                    //Counter zero type
                    if (counter.hasClass('zero')) {
                        var max = parseFloat(counter.text());
                        counter.countTo({
                            from: 0,
                            to: max,
                            speed: 1500,
                            refreshInterval: 100
                        });
                    } else {
                        counter.absoluteCounter({
                            speed: 2000,
                            fadeInDelay: 1000
                        });
                    }

                },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
            });
        }

    }
    
    /*
    **	Horizontal progress bars shortcode
    */
    function mkdfInitProgressBars(){
        
        var progressBar = $('.mkdf-progress-bar');
        
        if(progressBar.length){
            
            progressBar.each(function() {
                
                var thisBar = $(this);
                
                thisBar.appear(function() {
                    mkdfInitToCounterProgressBar(thisBar);
                    if(thisBar.find('.mkdf-floating.mkdf-floating-inside') !== 0){
                        var floatingInsideMargin = thisBar.find('.mkdf-progress-content').height();
                        floatingInsideMargin += parseFloat(thisBar.find('.mkdf-progress-title-holder').css('padding-bottom'));
                        floatingInsideMargin += parseFloat(thisBar.find('.mkdf-progress-title-holder').css('margin-bottom'));
                        thisBar.find('.mkdf-floating-inside').css('margin-bottom',-(floatingInsideMargin)+'px');
                    }
                    var percentage = thisBar.find('.mkdf-progress-content').data('percentage'),
                        progressContent = thisBar.find('.mkdf-progress-content'),
                        progressNumber = thisBar.find('.mkdf-progress-number');

                    progressContent.css('width', '0%');
                    progressContent.animate({'width': percentage+'%'}, 1500);
                    progressNumber.css('left', '0%');
                    progressNumber.animate({'left': percentage+'%'}, 1500);

                });
            });
        }
    }

    /*
    **	Counter for horizontal progress bars percent from zero to defined percent
    */
    function mkdfInitToCounterProgressBar(progressBar){
        var percentage = parseFloat(progressBar.find('.mkdf-progress-content').data('percentage'));
        var percent = progressBar.find('.mkdf-progress-number .mkdf-percent');
        if(percent.length) {
            percent.each(function() {
                var thisPercent = $(this);
                thisPercent.parents('.mkdf-progress-number-wrapper').css('opacity', '1');
                thisPercent.countTo({
                    from: 0,
                    to: percentage,
                    speed: 1500,
                    refreshInterval: 50
                });
            });
        }
    }
    
    /*
    **	Function to close message shortcode
    */
    function mkdfInitMessages(){
        var message = $('.mkdf-message');
        if(message.length){
            message.each(function(){
                var thisMessage = $(this);
                thisMessage.find('.mkdf-close').click(function(e){
                    e.preventDefault();
                    $(this).parent().parent().fadeOut(500);
                });
            });
        }
    }
    
    /*
    **	Init message height
    */
    function mkdfInitMessageHeight(){
       var message = $('.mkdf-message.mkdf-with-icon');
       if(message.length){
           message.each(function(){
               var thisMessage = $(this);
               var textHolderHeight = thisMessage.find('.mkdf-message-text-holder').height();
               var iconHolderHeight = thisMessage.find('.mkdf-message-icon-holder').height();
               
               if(textHolderHeight > iconHolderHeight) {
                   thisMessage.find('.mkdf-message-icon-holder').height(textHolderHeight);
               } else {
                   thisMessage.find('.mkdf-message-text-holder').height(iconHolderHeight);
               }
           });
       }
    }

    /**
     * Countdown Shortcode
     */
    function mkdfInitCountdown() {

        var countdowns = $('.mkdf-countdown'),
            year,
            month,
            day,
            hour,
            minute,
            timezone,
            monthLabel,
            dayLabel,
            hourLabel,
            minuteLabel,
            secondLabel;

        if (countdowns.length) {

            countdowns.each(function(){

                //Find countdown elements by id-s
                var countdownId = $(this).attr('id'),
                    countdown = $('#'+countdownId),
                    digitFontSize,
                    labelFontSize,
                    digitColor,
                    labelColor;

                //Get data for countdown
                year = countdown.data('year');
                month = countdown.data('month');
                day = countdown.data('day');
                hour = countdown.data('hour');
                minute = countdown.data('minute');
                timezone = countdown.data('timezone');
                monthLabel = countdown.data('month-label');
                dayLabel = countdown.data('day-label');
                hourLabel = countdown.data('hour-label');
                minuteLabel = countdown.data('minute-label');
                secondLabel = countdown.data('second-label');
                digitFontSize = countdown.data('digit-size');
                labelFontSize = countdown.data('label-size');
                digitColor = countdown.data('digit-color');
                labelColor = countdown.data('label-color');

                //Initialize countdown
                countdown.countdown({
                    until: new Date(year, month - 1, day, hour, minute, 44),
                    labels: ['Years', monthLabel, 'Weeks', dayLabel, hourLabel, minuteLabel, secondLabel],
                    format: 'ODHMS',
                    timezone: timezone,
                    padZeroes: true,
                    onTick: setCountdownStyle
                });

                function setCountdownStyle() {
                    countdown.find('.countdown-amount').css({
                        'font-size' : digitFontSize+'px',
                        'color' : digitColor,
                        'line-height' : digitFontSize+'px'
                    });
                    countdown.find('.countdown-period').css({
                        'font-size' : labelFontSize+'px',
                        'color' : labelColor
                    });
                }

            });

        }

    }

    /**
     * Object that represents icon shortcode
     * @returns {{init: Function}} function that initializes icon's functionality
     */
    var mkdfIcon = mkdf.modules.shortcodes.mkdfIcon = function() {
        //get all icons on page
        var icons = $('.mkdf-icon-shortcode');

        /**
         * Function that triggers icon animation and icon animation delay
         */
        var iconAnimation = function(icon) {
            if(icon.hasClass('mkdf-icon-animation')) {
                icon.appear(function() {
                    icon.parent('.mkdf-icon-animation-holder').addClass('mkdf-icon-animation-show');
                }, {accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
            }
        };

        /**
         * Function that triggers icon hover color functionality
         */
        var iconHoverColor = function(icon) {
            if(typeof icon.data('hover-color') !== 'undefined') {
                var changeIconColor = function(event) {
                    event.data.icon.css('color', event.data.color);
                };

                var iconElement = icon.find('.mkdf-icon-element');
                var hoverColor = icon.data('hover-color');
                var originalColor = iconElement.css('color');

                if(hoverColor !== '') {
                    icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
                    icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
                }
            }
        };

        /**
         * Function that triggers icon holder background color hover functionality
         */
        var iconHolderBackgroundHover = function(icon) {
            if(typeof icon.data('hover-background-color') !== 'undefined') {
                var changeIconBgColor = function(event) {
                    event.data.icon.css('background-color', event.data.color);
                };

                var hoverBackgroundColor = icon.data('hover-background-color');
                var originalBackgroundColor = icon.css('background-color');

                if(hoverBackgroundColor !== '') {
                    icon.on('mouseenter', {icon: icon, color: hoverBackgroundColor}, changeIconBgColor);
                    icon.on('mouseleave', {icon: icon, color: originalBackgroundColor}, changeIconBgColor);
                }
            }
        };

        /**
         * Function that initializes icon holder border hover functionality
         */
        var iconHolderBorderHover = function(icon) {
            if(typeof icon.data('hover-border-color') !== 'undefined') {
                var changeIconBorder = function(event) {
                    event.data.icon.css('border-color', event.data.color);
                };

                var hoverBorderColor = icon.data('hover-border-color');
                var originalBorderColor = icon.css('border-color');

                if(hoverBorderColor !== '') {
                    icon.on('mouseenter', {icon: icon, color: hoverBorderColor}, changeIconBorder);
                    icon.on('mouseleave', {icon: icon, color: originalBorderColor}, changeIconBorder);
                }
            }
        };

        return {
            init: function() {
                if(icons.length) {
                    icons.each(function() {
                        iconAnimation($(this));
                        iconHoverColor($(this));
                        iconHolderBackgroundHover($(this));
                        iconHolderBorderHover($(this));
                    });

                }
            }
        };
    };

    /**
     * Object that represents social icon widget
     * @returns {{init: Function}} function that initializes icon's functionality
     */
    var mkdfSocialIconWidget = mkdf.modules.shortcodes.mkdfSocialIconWidget = function() {
        //get all social icons on page
        var icons = $('.mkdf-social-icon-widget-holder');

        /**
         * Function that triggers icon hover color functionality
         */
        var socialIconHoverColor = function(icon) {
            if(typeof icon.data('hover-color') !== 'undefined') {
                var changeIconColor = function(event) {
                    event.data.icon.css('color', event.data.color);
                };

                var iconElement = icon;
                var hoverColor = icon.data('hover-color');
                var originalColor = iconElement.css('color');

                if(hoverColor !== '') {
                    icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
                    icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
                }
            }
        };

        return {
            init: function() {
                if(icons.length) {
                    icons.each(function() {
                        socialIconHoverColor($(this));
                    });

                }
            }
        };
    };

    /**
     * Init testimonials shortcode
     */
    function mkdfInitTestimonials(){

        var testimonial = $('.mkdf-testimonials');
        if(testimonial.length){
            testimonial.each(function(){

                var thisTestimonial = $(this);

                thisTestimonial.appear(function() {
                    thisTestimonial.css('visibility','visible');
                },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});

                var auto = false;
                var controlNav = true;
                var directionNav = true;
                var animationSpeed = 600;
				var responsive;
				var slidesToShow = 1;

                if(typeof thisTestimonial.data('animation-speed') !== 'undefined' && thisTestimonial.data('animation-speed') !== false) {
                    animationSpeed = thisTestimonial.data('animation-speed');
                }

				if(typeof thisTestimonial.data('dots-navigation') !== 'undefined') {
					controlNav = thisTestimonial.data('dots-navigation');
				}
				if(typeof thisTestimonial.data('arrows-navigation') !== 'undefined') {
					directionNav = thisTestimonial.data('arrows-navigation');
				}

                var fadeSlides = function () {
                    var slides = thisTestimonial.find('.slick-slide');

                    slides.removeClass('mkdf-fade-in mkdf-fade-out');
                    slides.each(function () {
                        var currentSlide = $(this),
                            sliderWindowOffsetLeft = thisTestimonial.find('.slick-list').offset().left,
                            sliderWindowWidth = thisTestimonial.find('.slick-list').outerWidth(),
                            currentSlideOffsetLeft = currentSlide.offset().left,
                            currentSlideWidth = currentSlide.outerWidth();

                        if (currentSlideOffsetLeft >= sliderWindowOffsetLeft && currentSlideOffsetLeft + currentSlideWidth <= sliderWindowOffsetLeft + sliderWindowWidth) {
                            currentSlide.addClass('mkdf-fade-out');
                        } else  {
                            currentSlide.addClass('mkdf-fade-in');
                        }
                    });
                }

                thisTestimonial.on('beforeChange', function () {
                    fadeSlides();
                });

                thisTestimonial.on('init', function(){
                    thisTestimonial.find('.mkdf-testimonial-content.slick-active').addClass('mkdf-fade-in');
                });

				thisTestimonial.slick({
					infinite: true,
					autoplay: auto,
					slidesToShow : slidesToShow,
					arrows: directionNav,
					dots: controlNav,
					dotsClass: 'mkdf-slick-dots',
					adaptiveHeight: true,
                    easing: 'easeInOutQuint',
                    speed: 1200,
					prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="arrow_left"></span></span>',
					nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"><span class="arrow_right"></span></span>',
					customPaging: function(slider, i) {
						return '<span class="mkdf-slick-dot-inner"></span>';
					},
					responsive: responsive
				});

            });

        }

    }

    /**
     * Init Carousel shortcode
     */
    function mkdfInitCarousels() {

        var carouselHolders = $('.mkdf-carousel-holder'),
            carousel,
            numberOfItems,
			arrowsNavigation,
			dotsNavigation;

        if (carouselHolders.length) {
            carouselHolders.each(function(){
                carousel = $(this).children('.mkdf-carousel');
                numberOfItems = carousel.data('items');
                arrowsNavigation = (carousel.data('arrows-navigation') == 'yes') ? true : false;
                dotsNavigation = (carousel.data('dots-navigation') == 'yes') ? true : false;

                //Responsive breakpoints

                var laptopNumberOfItems;

                if((numberOfItems == 8) || (numberOfItems == 7)) {
                    laptopNumberOfItems = 6;
                } else {
                    laptopNumberOfItems = numberOfItems;
                }

                carousel.on('init', function(slick){
                    carousel.css('visibility','visible');
                });

      			carousel.slick({
					infinite: true,
					autoplay: true,
					slidesToShow : numberOfItems,
					arrows: arrowsNavigation,
					dots: dotsNavigation,
					dotsClass: 'mkdf-slick-dots',
					adaptiveHeight: true,
					prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="arrow_left"></span></span>',
					nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"><span class="arrow_right"></span></span>',
                    easing: 'easeInOutQuint',
                    speed: 1000,
					customPaging: function(slider, i) {
						return '<span class="mkdf-slick-dot-inner"></span>';
					},
					responsive: [
                        {
                            breakpoint: 1281,
                            settings: {
                                slidesToShow: laptopNumberOfItems,
                                slidesToScroll: 1,
                                infinite: true,
                            }
                        },
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 1,
								infinite: true,
							}
						},
						{
							breakpoint: 600,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
					]
				});


			});
        }

    }

    /**
     * Init Pie Chart and Pie Chart With Icon shortcode
     */
    function mkdfInitPieChart() {

        var pieCharts = $('.mkdf-pie-chart-holder, .mkdf-pie-chart-with-icon-holder');

        if (pieCharts.length) {

            pieCharts.each(function () {

                var pieChart = $(this),
                    percentageHolder = pieChart.children('.mkdf-percentage, .mkdf-percentage-with-icon'),
                    barColor = mkdfGlobalVars.vars.mkdfFirstColor,
                    trackColor = '#f6f6f6',
                    lineWidth = '15',
                    size = 200;

                if(typeof percentageHolder.data('bar-color') !== 'undefined' && percentageHolder.data('bar-color') !== '') {
                    barColor = percentageHolder.data('bar-color');
                }

                if(typeof percentageHolder.data('track-color') !== 'undefined' && percentageHolder.data('track-color') !== '') {
                    trackColor = percentageHolder.data('track-color');
                }

                if(typeof percentageHolder.data('size') !== 'undefined' && percentageHolder.data('size') !== '') {
                    size = parseInt(percentageHolder.data('size'));
                }

                percentageHolder.appear(function() {
                    initToCounterPieChart(pieChart);
                    percentageHolder.css('opacity', '1');
                    percentageHolder.easyPieChart({
                        barColor: barColor,
                        trackColor: trackColor,
                        scaleColor: false,
                        lineCap: 'butt',
                        lineWidth: lineWidth,
                        animate: 1500,
                        size: size
                    });
                },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});

            });

        }

    }

    /*
     **	Counter for pie chart number from zero to defined number
     */
    function initToCounterPieChart( pieChart ){

        pieChart.css('opacity', '1');
        var counter = pieChart.find('.mkdf-to-counter'),
            max = parseFloat(counter.text());
        counter.countTo({
            from: 0,
            to: max,
            speed: 1500,
            refreshInterval: 50
        });

    }

    /**
     * Init Pie Chart shortcode
     */
    function mkdfInitPieChartDoughnut() {

        var pieCharts = $('.mkdf-pie-chart-doughnut-holder, .mkdf-pie-chart-pie-holder');

        pieCharts.each(function(){

            var pieChart = $(this),
                canvas = pieChart.find('canvas'),
                chartID = canvas.attr('id'),
                chart = document.getElementById(chartID).getContext('2d'),
                data = [],
                jqChart = $(chart.canvas); //Convert canvas to JQuery object and get data parameters

            for (var i = 1; i<=10; i++) {

                var chartItem,
                    value = jqChart.data('value-' + i),
                    color = jqChart.data('color-' + i);
                
                if (typeof value !== 'undefined' && typeof color !== 'undefined' ) {
                    chartItem = {
                        value : value,
                        color : color
                    };
                    data.push(chartItem);
                }

            }

            if (canvas.hasClass('mkdf-pie')) {
                new Chart(chart).Pie(data,
                    {segmentStrokeColor : 'transparent'}
                );
            } else {
                new Chart(chart).Doughnut(data,
                    {segmentStrokeColor : 'transparent'}
                );
            }

        });

    }

    /*
    **	Init tabs shortcode
    */
    function mkdfInitTabs(){

       var tabs = $('.mkdf-tabs');
        if(tabs.length){
            tabs.each(function(){
                var thisTabs = $(this);

                thisTabs.children('.mkdf-tab-container').each(function(index){
                    index = index + 1;
                    var that = $(this),
                        link = that.attr('id'),
                        navItem = that.parent().find('.mkdf-tabs-nav li:nth-child('+index+') a'),
                        navLink = navItem.attr('href');

                        link = '#'+link;

                        if(link.indexOf(navLink) > -1) {
                            navItem.attr('href',link);
                        }
                });

                if(thisTabs.hasClass('mkdf-horizontal-tab')){
                    thisTabs.tabs();
                } else if(thisTabs.hasClass('mkdf-vertical-tab')){
                    thisTabs.tabs().addClass( 'ui-tabs-vertical ui-helper-clearfix' );
                    thisTabs.find('.mkdf-tabs-nav > ul >li').removeClass( 'ui-corner-top' ).addClass( 'ui-corner-left' );
                }
            });
        }
    }

    /*
    **	Generate icons in tabs navigation
    */
    function mkdfInitTabIcons(){

        var tabContent = $('.mkdf-tab-container');
        if(tabContent.length){

            tabContent.each(function(){
                var thisTabContent = $(this);

                var id = thisTabContent.attr('id');
                var icon = '';
                if(typeof thisTabContent.data('icon-html') !== 'undefined' || thisTabContent.data('icon-html') !== 'false') {
                    icon = thisTabContent.data('icon-html');
                }

                var tabNav = thisTabContent.parents('.mkdf-tabs').find('.mkdf-tabs-nav > li > a[href="#'+id+'"]');

                if(typeof(tabNav) !== 'undefined') {
                    tabNav.children('.mkdf-icon-frame').append(icon);
                }
            });
        }
    }

    /**
     * Button object that initializes whole button functionality
     * @type {Function}
     */
    var mkdfButton = mkdf.modules.shortcodes.mkdfButton = function() {
        //all buttons on the page
        var buttons = $('.mkdf-btn');

        /**
         * Initializes button hover color
         * @param button current button
         */
        var buttonHoverColor = function(button) {
            if(typeof button.data('hover-color') !== 'undefined') {
                var changeButtonColor = function(event) {
                    event.data.button.css('color', event.data.color);
                };

                var originalColor = button.css('color');
                var hoverColor = button.data('hover-color');

                button.on('mouseenter', { button: button, color: hoverColor }, changeButtonColor);
                button.on('mouseleave', { button: button, color: originalColor }, changeButtonColor);
            }
        };



        /**
         * Initializes button hover background color
         * @param button current button
         */
        var buttonHoverBgColor = function(button) {
            if(typeof button.data('hover-bg-color') !== 'undefined') {
                var changeButtonBg = function(event) {
                    event.data.button.css('background-color', event.data.color);
                };

                var originalBgColor = button.css('background-color');
                var hoverBgColor = button.data('hover-bg-color');

                button.on('mouseenter', { button: button, color: hoverBgColor }, changeButtonBg);
                button.on('mouseleave', { button: button, color: originalBgColor }, changeButtonBg);
            }
        };

        /**
         * Initializes button border color
         * @param button
         */
        var buttonHoverBorderColor = function(button) {
            if(typeof button.data('hover-border-color') !== 'undefined') {
                var changeBorderColor = function(event) {
                    event.data.button.css('border-color', event.data.color);
                };

                var originalBorderColor = button.css('borderTopColor'); //take one of the four sides
                var hoverBorderColor = button.data('hover-border-color');

                button.on('mouseenter', { button: button, color: hoverBorderColor }, changeBorderColor);
                button.on('mouseleave', { button: button, color: originalBorderColor }, changeBorderColor);
            }
        };
        
        return {
            init: function() {
                if(buttons.length) {
                    buttons.each(function() {
                        buttonHoverColor($(this));
                        buttonHoverBgColor($(this));
                        buttonHoverBorderColor($(this));
                    });
                }
            }
        };
    };
    
    /*
    **	Init blog list masonry type
    */
    function mkdfInitBlogListMasonry(){
        var blogList = $('.mkdf-blog-list-holder.mkdf-masonry .mkdf-blog-list');
        if(blogList.length) {
            blogList.each(function() {
                var thisBlogList = $(this);
                blogList.waitForImages(function() {
                    thisBlogList.isotope({
                        itemSelector: '.mkdf-blog-list-masonry-item',
                        masonry: {
                            columnWidth: '.mkdf-blog-list-masonry-grid-sizer',
                            gutter: '.mkdf-blog-list-masonry-grid-gutter'
                        }
                    });
                    thisBlogList.addClass('mkdf-appeared');
                });
            });

        }
    }

    
    /*
    **	Init blog list boxes type
    */
	function mkdfInitBlogListBoxes(){
		var blogList = $('.mkdf-blog-list-holder.mkdf-boxes .mkdf-blog-list');
		if (blogList.length){
			blogList.each(function(){
				var thisList = $(this),
					items = thisList.find('.mkdf-blog-list-item'),
					height = items.first().outerHeight();

					items.each(function(){
						var thisItem = $(this);

						if (height < thisItem.outerHeight()){
							height = thisItem.outerHeight();
						}
					});

					items.each(function(){
						var thisItem = $(this);
						
						thisItem.css('height',height);
					});

					thisList.addClass('mkdf-appeared');
			});
		}
	}

	/**
	 * Initializes portfolio slider
	 */

	function mkdfInitBlogSlider(){
		var blogSlider = $('.mkdf-blog-slider');
		if(blogSlider.length){
			blogSlider.each(function(){
				var thisBlogSlider = $(this);
				var navigation = false;
				var responsive;
				var slides = 1;

				if (typeof thisBlogSlider.data('type') !== 'undefined' && thisBlogSlider.data('type') !== false && thisBlogSlider.data('type') == 'carousel') {

					responsive = [
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1,
								infinite: true,
								dots: true
							}
						},
						{
							breakpoint: 600,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
					];
					slides = 3;
				}

				thisBlogSlider.slick({
					infinite: true,
					autoplay: false,
					slidesToShow : slides,
					arrows: navigation,
					dots: true,
					dotsClass: 'mkdf-slick-dots',
					adaptiveHeight: true,
					prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="arrow_left"></span></span>',
					nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"><span class="arrow_right"></span></span>',
                    easing: 'easeInOutQuint',
                    speed: 1000,
					customPaging: function(slider, i) {
						return '<span class="mkdf-slick-dot-inner"></span>';
					},
					responsive: responsive
				});
			});
		}
	}

	/*
	**	Custom Font resizing
	*/
	function mkdfCustomFontResize(){
		var customFont = $('.mkdf-custom-font-holder');
		if (customFont.length){
			customFont.each(function(){
				var thisCustomFont = $(this);
				var fontSize;
				var lineHeight;
				var coef1 = 1;
				var coef2 = 1;

				if (mkdf.windowWidth < 1200){
					coef1 = 0.8;
				}

				if (mkdf.windowWidth < 1000){
					coef1 = 0.7;
				}

				if (mkdf.windowWidth < 768){
					coef1 = 0.6;
					coef2 = 0.7;
				}

				if (mkdf.windowWidth < 600){
					coef1 = 0.5;
					coef2 = 0.6;
				}

				if (mkdf.windowWidth < 480){
					coef1 = 0.4;
					coef2 = 0.5;
				}

				if (typeof thisCustomFont.data('font-size') !== 'undefined' && thisCustomFont.data('font-size') !== false) {
					fontSize = parseInt(thisCustomFont.data('font-size'));

					if (fontSize > 70) {
						fontSize = Math.round(fontSize*coef1);
					}
					else if (fontSize > 35) {
						fontSize = Math.round(fontSize*coef2);
					}

					thisCustomFont.css('font-size',fontSize + 'px');
				}

				if (typeof thisCustomFont.data('line-height') !== 'undefined' && thisCustomFont.data('line-height') !== false) {
					lineHeight = parseInt(thisCustomFont.data('line-height'));

					if (lineHeight > 70 && mkdf.windowWidth < 1200) {
						lineHeight = '1.2em';
					}
					else if (lineHeight > 35 && mkdf.windowWidth < 768) {
						lineHeight = '1.2em';
					}
					else{
						lineHeight += 'px';
					}

					thisCustomFont.css('line-height', lineHeight);
				}
			});
		}
	}

    /*
     **	Show Google Map
     */
    function mkdfShowGoogleMap(){

        if($('.mkdf-google-map').length){
            $('.mkdf-google-map').each(function(){

                var element = $(this);

                var predefinedStyle = false;
                if(typeof element.data('predefined-style') !== 'undefined' && element.data('predefined-style') === 'yes') {
                    predefinedStyle = true;
                }

                var customMapStyle;
                if(typeof element.data('custom-map-style') !== 'undefined') {
                    customMapStyle = element.data('custom-map-style');
                }

                var colorOverlay;
                if(typeof element.data('color-overlay') !== 'undefined' && element.data('color-overlay') !== false) {
                    colorOverlay = element.data('color-overlay');
                }

                var saturation;
                if(typeof element.data('saturation') !== 'undefined' && element.data('saturation') !== false) {
                    saturation = element.data('saturation');
                }

                var lightness;
                if(typeof element.data('lightness') !== 'undefined' && element.data('lightness') !== false) {
                    lightness = element.data('lightness');
                }

                var zoom;
                if(typeof element.data('zoom') !== 'undefined' && element.data('zoom') !== false) {
                    zoom = element.data('zoom');
                }

                var pin;
                if(typeof element.data('pin') !== 'undefined' && element.data('pin') !== false) {
                    pin = element.data('pin');
                }

                var mapHeight;
                if(typeof element.data('height') !== 'undefined' && element.data('height') !== false) {
                    mapHeight = element.data('height');
                }

                var uniqueId;
                if(typeof element.data('unique-id') !== 'undefined' && element.data('unique-id') !== false) {
                    uniqueId = element.data('unique-id');
                }

                var scrollWheel;
                if(typeof element.data('scroll-wheel') !== 'undefined') {
                    scrollWheel = element.data('scroll-wheel');
                }
                var addresses;
                if(typeof element.data('addresses') !== 'undefined' && element.data('addresses') !== false) {
                    addresses = element.data('addresses');
                }

                var map = "map_"+ uniqueId;
                var geocoder = "geocoder_"+ uniqueId;
                var holderId = "mkdf-map-"+ uniqueId;

                mkdfInitializeGoogleMap(predefinedStyle, customMapStyle, colorOverlay, saturation, lightness, scrollWheel, zoom, holderId, mapHeight, pin,  map, geocoder, addresses);
            });
        }

    }

    function mkdfCircleGoogleMap(holderId, height) {

        var mapId = $('#' + holderId);

        if(mapId.parent().hasClass( "mkdf-circle-map" )) {

            var width;
            width = mapId.width();

            if( width > parseInt(height)) {
                height = width;
            } else if ( width < parseInt(height)) {
                height = width;
            }

        }

        return height;
    }

    function mkdfCircleGoogleMapResize() {

        if($('.mkdf-google-map').length){
            $('.mkdf-google-map').each(function(){

                var element = $(this);

                var uniqueId;
                if(typeof element.data('unique-id') !== 'undefined' && element.data('unique-id') !== false) {
                    uniqueId = element.data('unique-id');
                }

                var mapHeight;
                if(typeof element.data('height') !== 'undefined' && element.data('height') !== false) {
                    mapHeight = element.data('height');
                }

                var holderId = "mkdf-map-"+ uniqueId;

                var height = mkdfCircleGoogleMap(holderId,mapHeight);

                var holderElement = document.getElementById(holderId);
                holderElement.style.height = height + 'px';
            });
        }

    }


    /*
     **	Init Google Map
     */
    function mkdfInitializeGoogleMap(predefinedStyle, customMapStyle, color, saturation, lightness, wheel, zoom, holderId, height, pin,  map, geocoder, data){

        var mapStyles = [];
        if(predefinedStyle) {
            mapStyles = [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "color": "#ff6a6a"
                        },
                        {
                            "lightness": "0"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ee3123"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#ee3123"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ee3123"
                        },
                        {
                            "lightness": "62"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "lightness": "75"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.bus",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "weight": "0.01"
                        },
                        {
                            "hue": "#ff0028"
                        },
                        {
                            "lightness": "0"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#80e4d8"
                        },
                        {
                            "lightness": "25"
                        },
                        {
                            "saturation": "-23"
                        }
                    ]
                }
            ];
        } else {
            mapStyles = [
                {
                    stylers: [
                        {hue: color },
                        {saturation: saturation},
                        {lightness: lightness},
                        {gamma: 1}
                    ]
                }
            ];
        }

        var googleMapStyleId;

        if(customMapStyle || predefinedStyle){
            googleMapStyleId = 'mkdf-style';
        } else {
            googleMapStyleId = google.maps.MapTypeId.ROADMAP;
        }

        var qoogleMapType = new google.maps.StyledMapType(mapStyles,
            {name: "Mikado Google Map"});

        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);

        if (!isNaN(height)){
            height = height + 'px';
        }

        var myOptions = {

            zoom: zoom,
            scrollwheel: wheel,
            center: latlng,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            scaleControl: false,
            scaleControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            streetViewControl: false,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            panControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            mapTypeControl: false,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'mkdf-style'],
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            mapTypeId: googleMapStyleId
        };

        map = new google.maps.Map(document.getElementById(holderId), myOptions);
        map.mapTypes.set('mkdf-style', qoogleMapType);

        var index;

        for (index = 0; index < data.length; ++index) {
            mkdfInitializeGoogleAddress(data[index], pin, map, geocoder);
        }

        height = mkdfCircleGoogleMap(holderId,height);

        height = parseInt(height) + 'px';

        var holderElement = document.getElementById(holderId);
        holderElement.style.height = height;
    }
    /*
     **	Init Google Map Addresses
     */
    function mkdfInitializeGoogleAddress(data, pin,  map, geocoder){
        if (data === '')
            return;
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<div id="bodyContent">'+
            '<p>'+data+'</p>'+
            '</div>'+
            '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        geocoder.geocode( { 'address': data}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    icon:  pin,
                    title: data['store_title']
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });

                google.maps.event.addDomListener(window, 'resize', function() {
                    map.setCenter(results[0].geometry.location);
                });

            }
        });
    }

    function mkdfInitAccordions(){
        var accordion = $('.mkdf-accordion-holder');
        if(accordion.length){
            accordion.each(function(){

               var thisAccordion = $(this);

				if(thisAccordion.hasClass('mkdf-accordion')){

					thisAccordion.accordion({
						animate: "swing",
						collapsible: true,
						active: 0,
						icons: "",
						heightStyle: "content"
					});
				}

				if(thisAccordion.hasClass('mkdf-toggle')){

					var toggleAccordion = $(this);
					var toggleAccordionTitle = toggleAccordion.find('.mkdf-title-holder');
					var toggleAccordionContent = toggleAccordionTitle.next();

					toggleAccordion.addClass("accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset");
					toggleAccordionTitle.addClass("ui-accordion-header ui-state-default ui-corner-top ui-corner-bottom");
					toggleAccordionContent.addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide();

					toggleAccordionTitle.each(function(){
						var thisTitle = $(this);
						thisTitle.hover(function(){
							thisTitle.toggleClass("ui-state-hover");
						});

						thisTitle.on('click',function(){
							thisTitle.toggleClass('ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom');
							thisTitle.next().toggleClass('ui-accordion-content-active').slideToggle(400);
						});
					});
				}
            });
        }
    }

    function mkdfInitImageGallery() {

        var galleries = $('.mkdf-image-gallery');

        if (galleries.length) {
            galleries.each(function () {
                var gallery = $(this).children('.mkdf-image-gallery-sliding'),
                    autoplay = gallery.data('autoplay'),
                    animation = (gallery.data('animation') == 'fade'),
                    arrows = (gallery.data('navigation') == 'yes'),
                    dots = (gallery.data('pagination') == 'yes'),
                    slidesToShow = 1,
                    variableWidth = false,
                    centerMode = false,
                    autoplaySpeed;


                if (autoplay == 'disable') {
                    autoplay = false;
                } else {
                    autoplaySpeed = autoplay * 1000;
                    autoplay = true;
                }

                if (gallery.hasClass('mkdf-gallery-image-carousel')){
                	variableWidth = true;
                    centerMode = true;
                }

                gallery.on('init', function(slick){
                    var element = gallery.find('.slick-slide');

                    element.each(function(){
                        var thisElement = $(this),
                            flag = 0,
                            mousedownFlag = 0,
                            moved = false;

                        thisElement.on("mousedown", function(){
                            flag = 0;
                            mousedownFlag = 1;
                            moved = false;
                        });

                        thisElement.on("mousemove", function(){
                            if (mousedownFlag == 1){
                                if (moved){
                                    flag = 1;
                                }
                                moved = true;
                            }
                        });

                        thisElement.on("mouseleave", function(){
                            flag = 0;
                        });

                        thisElement.on("mouseup", function(e){
                            if(flag === 1){
                                thisElement.find('a[data-rel^="prettyPhoto"]').unbind('click');
                            }
                            else{
                                mkdf.modules.common.mkdfPrettyPhoto();
                            }
                            flag = 0;
                            mousedownFlag = 0;
                        });
                    });

                });
                
                gallery.slick({
					infinite: true,
					autoplay: autoplay,
					autoplaySpeed: autoplaySpeed,
                    easing: 'easeInOutQuint',
                    speed: 1000,
					slidesToShow : 1,
					fade: animation,
					arrows: arrows,
					dots: dots,
					dotsClass: 'mkdf-slick-dots',
					adaptiveHeight: false,
					variableWidth: variableWidth,
					centerMode: centerMode,
                    prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="arrow_left"></span></span>',
                    nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"><span class="arrow_right"></span></span>',
					customPaging: function(slider, i) {
						return '<span class="mkdf-slick-dot-inner"></span>';
					}
                });
            });
        }

    }

    /**
     * Init Image Masonry Gallery
     */
    function mkdfInitImageGalleryMasonry(){
        var masonryGallery = $('.mkdf-image-gallery-masonry');

        if(masonryGallery.length) {
            masonryGallery.each(function () {
                var thisGallery = $(this);
                thisGallery.waitForImages(function () {
                    var size = thisGallery.find('.mkdf-image-masonry-grid-sizer').width();
                    mkdfImageResizeMasonry(size,thisGallery);
                    mkdfInitImageMasonry(thisGallery);

                });
                $(window).resize(function(){
                    var size = thisGallery.find('.mkdf-image-masonry-grid-sizer').width();
                    mkdfImageResizeMasonry(size,thisGallery);
                    mkdfInitImageMasonry(thisGallery);
                });
            });
        }
    }

    function mkdfInitImageMasonry(container){
        container.animate({opacity: 1});
        container.isotope({
            itemSelector: '.mkdf-gallery-image',
            masonry: {
                columnWidth: '.mkdf-image-masonry-grid-sizer'
            }
        });
    }


    function mkdfImageResizeMasonry(size,container){

        var defaultMasonryItem = container.find('.mkdf-size-square');
        var largeWidthMasonryItem = container.find('.mkdf-size-landscape');
        var largeHeightMasonryItem = container.find('.mkdf-size-portrait');
        var largeWidthHeightMasonryItem = container.find('.mkdf-size-big-square');

        defaultMasonryItem.css('height', size);
        largeHeightMasonryItem.css('height', Math.round(2*size));

        if(mkdf.windowWidth > 600){
            largeWidthHeightMasonryItem.css('height', Math.round(2*size));
            largeWidthMasonryItem.css('height', size);
        }else{
            largeWidthHeightMasonryItem.css('height', size);
            largeWidthMasonryItem.css('height', Math.round(size/2));
        }
    }

    /**
     * Initializes portfolio list
     */
    function mkdfInitPortfolio(){
        var portList = $('.mkdf-portfolio-list-holder-outer.mkdf-ptf-standard, .mkdf-portfolio-list-holder-outer.mkdf-ptf-gallery, .mkdf-portfolio-list-holder-outer.mkdf-ptf-gallery-with-space');
        if(portList.length){            
            portList.each(function(){
                var thisPortList = $(this);
                thisPortList.appear(function(){
                    mkdfInitPortMixItUp(thisPortList);
                });
            });
        }
    }
    /**
     * Initializes mixItUp function for specific container
     */
    function mkdfInitPortMixItUp(container){
        var filterClass = '';
        if(container.hasClass('mkdf-ptf-has-filter')){
            filterClass = container.find('.mkdf-portfolio-filter-holder-inner ul li').data('class');
            filterClass = '.'+filterClass;
        }
        
        var holderInner = container.find('.mkdf-portfolio-list-holder');
        holderInner.mixItUp({
            callbacks: {
                onMixLoad: function(){
                	setTimeout(function () {
                		holderInner.addClass('mkdf-appeared');
                		mkdf.modules.common.mkdfInitParallax();
                	},100);
                    holderInner.find('article').css('visibility','visible');
                    holderInner.find('article').css('dislay','inline-block');
                },
                onMixStart: function(){
                    holderInner.find('article').css('visibility','visible');
                    holderInner.find('article').css('dislay','inline-block');
                },
                onMixBusy: function(){
                    holderInner.find('article').css('visibility','visible');
                } 
            },           
            selectors: {
                filter: filterClass
            },
            animation: {
                effects: 'fade',
                duration: 600
            }
            
        });
        
    }
     /*
    **	Init portfolio list masonry type
    */
    function mkdfInitPortfolioListMasonry(){
        var portList = $('.mkdf-portfolio-list-holder-outer.mkdf-ptf-masonry');
        if(portList.length) {
            portList.each(function() {
                var thisPortList = $(this).children('.mkdf-portfolio-list-holder');
                var size = thisPortList.find('.mkdf-portfolio-list-masonry-grid-sizer').width();
                mkdfResizeMasonry(size,thisPortList);
                
                mkdfInitMasonry(thisPortList);
                $(window).resize(function(){
                    mkdfResizeMasonry(size,thisPortList);
                    mkdfInitMasonry(thisPortList);
                });
            });
        }
    }
    
    function mkdfInitMasonry(container){
        container.waitForImages(function() {
            container.isotope({
                itemSelector: '.mkdf-portfolio-item',
                masonry: {
                    columnWidth: '.mkdf-portfolio-list-masonry-grid-sizer'
                }
            });
            container.addClass('mkdf-appeared');
        });
    }
    
    function mkdfResizeMasonry(size,container){
        
        var defaultMasonryItem = container.find('.mkdf-default-masonry-item');
        var largeWidthMasonryItem = container.find('.mkdf-large-width-masonry-item');
        var largeHeightMasonryItem = container.find('.mkdf-large-height-masonry-item');
        var largeWidthHeightMasonryItem = container.find('.mkdf-large-width-height-masonry-item');

        defaultMasonryItem.css('height', size);
        largeHeightMasonryItem.css('height', Math.round(2*size));

        if(mkdf.windowWidth > 600){
            largeWidthHeightMasonryItem.css('height', Math.round(2*size));
            largeWidthMasonryItem.css('height', size);
        }else{
            largeWidthHeightMasonryItem.css('height', size);
            largeWidthMasonryItem.css('height', Math.round(size/2));

        }
    }
    /**
     * Initializes portfolio pinterest 
     */
    function mkdfInitPortfolioListPinterest(){
        
        var portList = $('.mkdf-portfolio-list-holder-outer.mkdf-ptf-pinterest');
        if(portList.length) {
            portList.each(function() {
                var thisPortList = $(this).children('.mkdf-portfolio-list-holder');
                mkdfInitPinterest(thisPortList);
                $(window).resize(function(){
                     mkdfInitPinterest(thisPortList);
                });
            });
            
        }
    }
    
    function mkdfInitPinterest(container){
        container.waitForImages(function() {
            container.isotope({
                itemSelector: '.mkdf-portfolio-item',
                masonry: {
                    columnWidth: '.mkdf-portfolio-list-masonry-grid-sizer'
                }
            });
        });
        container.addClass('mkdf-appeared');
        
    }
    /**
     * Initializes portfolio masonry filter
     */
    function mkdfInitPortfolioMasonryFilter(){
        
        var filterHolder = $('.mkdf-portfolio-filter-holder.mkdf-masonry-filter');
        
        if(filterHolder.length){
            filterHolder.each(function(){
               
                var thisFilterHolder = $(this);
                
                var portfolioIsotopeAnimation = null;
                
                var filter = thisFilterHolder.find('ul li').data('class');
                
                thisFilterHolder.find('.filter:first').addClass('current');
                
                thisFilterHolder.find('.filter').click(function(){

                    var currentFilter = $(this);
                    clearTimeout(portfolioIsotopeAnimation);

                    $('.isotope, .isotope .isotope-item').css('transition-duration','0.8s');

                    portfolioIsotopeAnimation = setTimeout(function(){
                        $('.isotope, .isotope .isotope-item').css('transition-duration','0s'); 
                    },700);

                    var selector = $(this).attr('data-filter');
                    thisFilterHolder.siblings('.mkdf-portfolio-list-holder-outer').find('.mkdf-portfolio-list-holder').isotope({ filter: selector });

                    thisFilterHolder.find('.filter').removeClass('current');
                    currentFilter.addClass('current');

                    return false;

                });
                
            });
        }
    }
    /**
     * Initializes portfolio slider
     */
    
    function mkdfInitPortfolioSlider(){
        var portSlider = $('.mkdf-portfolio-list-holder-outer.mkdf-portfolio-slider-holder');
        if(portSlider.length){
            portSlider.each(function(){
                var thisPortSlider = $(this);
                var sliderWrapper = thisPortSlider.children('.mkdf-portfolio-list-holder');
                var numberOfItems = thisPortSlider.data('items');
                var navigation = true;
                var navResponsive = false;
                var dots = true;
                var element;

                if (thisPortSlider.hasClass('mkdf-portfolio-related-holder')){
                	dots = false;
                	navResponsive = true;
                }

                //Responsive breakpoints
                var responsive =[
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 3,
						}
					},
					{
						breakpoint: 769,
						settings: {
							slidesToShow: 2,
                            arrows: navResponsive
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
                            arrows: navResponsive
						}
					}
				];

    			sliderWrapper.on('init', function(slick){
					element = sliderWrapper.find('.slick-slide');

					element.each(function(){
						var thisElement = $(this),
							flag = 0,
							mousedownFlag = 0,
							moved = false;
							
						thisElement.on("mousedown", function(){
							flag = 0;
							mousedownFlag = 1;
							moved = false;
						});

						thisElement.on("mousemove", function(){
							if (mousedownFlag == 1){
								if (moved){
									flag = 1;
								}
								moved = true;
							}
						});

						thisElement.on("mouseleave", function(){
							flag = 0;
						});

						thisElement.on("mouseup", function(e){
							if(flag === 1){
								thisElement.find('a[data-rel^="prettyPhoto"]').unbind('click');
							}
							else{
								mkdf.modules.common.mkdfPrettyPhoto();
							}
							flag = 0;
							mousedownFlag = 0;
						});
					});

				});

                sliderWrapper.slick({
					infinite: true,
					autoplay: true,
					autoplaySpeed: 5000,
                    speed: 600,
					slidesToShow : numberOfItems,
					arrows: navigation,
					dots: dots,
                    easing: 'easeOutQuart',
					dotsClass: 'mkdf-slick-dots',
					adaptiveHeight: true,
					prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="arrow_left"></span></span>',
					nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"><span class="arrow_right"></span></span>',
					customPaging: function(slider, i) {
						return '<span class="mkdf-slick-dot-inner"></span>';
					},
					responsive: responsive
                });
            });
        }
    }
    /**
     * Initializes portfolio load more function
     */
    function mkdfInitPortfolioLoadMore(){
        var portList = $('.mkdf-portfolio-list-holder-outer.mkdf-ptf-show-more');

        if(portList.length){

            portList.each(function(){
                
                var thisPortList = $(this);
                var thisPortListInner = thisPortList.find('.mkdf-portfolio-list-holder');
                var size = thisPortList.find('.mkdf-portfolio-list-masonry-grid-sizer').width();
                var nextPage; 
                var maxNumPages;
                var loadMoreButton = thisPortList.find('.mkdf-ptf-list-load-more a');
                var buttonText = loadMoreButton.children(".mkdf-btn-text");
                
                if (typeof thisPortList.data('max-num-pages') !== 'undefined' && thisPortList.data('max-num-pages') !== false) {  
                    maxNumPages = thisPortList.data('max-num-pages');
                }

                if (thisPortList.hasClass('mkdf-ptf-load-more')) {

                    loadMoreButton.on('click', function (e) {
                        var loadMoreDatta = mkdfGetPortfolioAjaxData(thisPortList);
                        nextPage = loadMoreDatta.nextPage;
                        e.preventDefault();
                        e.stopPropagation();
                        if (nextPage <= maxNumPages) {
                            var ajaxData = mkdfSetPortfolioAjaxData(loadMoreDatta);
                            buttonText.text(mkdfGlobalVars.vars.mkdfLoadingMoreText);
                            $.ajax({
                                type: 'POST',
                                data: ajaxData,
                                url: mkdCoreAjaxUrl,
                                success: function (data) {
                                    nextPage++;
                                    thisPortList.data('next-page', nextPage);
                                    var response = $.parseJSON(data);
                                    var responseHtml = mkdfConvertHTML(response.html); //convert response html into jQuery collection that Mixitup can work with
                                    thisPortList.waitForImages(function () {
                                        setTimeout(function () {
                                            if (thisPortList.hasClass('mkdf-ptf-masonry') || thisPortList.hasClass('mkdf-ptf-pinterest')) {
                                                thisPortListInner.isotope().append(responseHtml).isotope('appended', responseHtml).isotope('reloadItems');
                                                mkdfResizeMasonry(size, thisPortList);
                                                mkdfInitMasonry(thisPortList);
                                            } else {
                                                thisPortListInner.mixItUp('append', responseHtml);
                                            }

                                            buttonText.text(mkdfGlobalVars.vars.mkdfLoadMoreText);
                                            mkdf.modules.common.mkdfPrettyPhoto();

                                            if(nextPage > maxNumPages){
                                                loadMoreButton.hide();
                                            }
                                        }, 400);
                                    });
                                }
                            });
                        }
                    });

                } else if (thisPortList.hasClass('mkdf-ptf-infinite-scroll')) {
                    loadMoreButton.appear(function(e) {
                        var loadMoreDatta = mkdfGetPortfolioAjaxData(thisPortList);
                        nextPage = loadMoreDatta.nextPage;
                        e.preventDefault();
                        e.stopPropagation();
                        loadMoreButton.css('visibility', 'visible');
                        if(nextPage <= maxNumPages){
                            var ajaxData = mkdfSetPortfolioAjaxData(loadMoreDatta);
                            $.ajax({
                                type: 'POST',
                                data: ajaxData,
                                url: mkdCoreAjaxUrl,
                                success: function (data) {
                                    nextPage++;
                                    thisPortList.data('next-page', nextPage);
                                    var response = $.parseJSON(data);
                                    var responseHtml = mkdfConvertHTML(response.html); //convert response html into jQuery collection that Mixitup can work with
                                    thisPortList.waitForImages(function(){
                                        setTimeout(function() {
                                            if(thisPortList.hasClass('mkdf-ptf-masonry') || thisPortList.hasClass('mkdf-ptf-pinterest') ){
                                                thisPortListInner.isotope().append( responseHtml ).isotope( 'appended', responseHtml).isotope('reloadItems');
                                            } else {
                                                thisPortListInner.mixItUp('append',responseHtml);
                                            }
                                            loadMoreButton.css('visibility','hidden');
                                            mkdf.modules.common.mkdfPrettyPhoto();
                                        },400);
                                    });
                                }
                            });
                        }
                        if(nextPage === maxNumPages){
                            setTimeout(function() {
                                loadMoreButton.fadeOut(400);
                            }, 400);
                        }

                    },{ one: false, accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
                }
                
            });
        }
    }
    
    function mkdfConvertHTML ( html ) {
        var newHtml = $.trim( html ),
                $html = $(newHtml ),
                $empty = $();

        $html.each(function ( index, value ) {
            if ( value.nodeType === 1) {
                $empty = $empty.add ( this );
            }
        });

        return $empty;
    }

    /**
     * Initializes portfolio load more data params
     * @param portfolio list container with defined data params
     * return array
     */
    function mkdfGetPortfolioAjaxData(container){
        var returnValue = {};
        
        returnValue.type = '';
        returnValue.columns = '';
        returnValue.gridSize = '';
        returnValue.orderBy = '';
        returnValue.order = '';
        returnValue.number = '';
        returnValue.imageSize = '';
        returnValue.hoverType = '';
        returnValue.filter = '';
        returnValue.filterOrderBy = '';
        returnValue.category = '';
        returnValue.selectedProjectes = '';
        returnValue.showMore = '';
        returnValue.titleTag = '';
        returnValue.nextPage = '';
        returnValue.maxNumPages = '';
        
        if (typeof container.data('type') !== 'undefined' && container.data('type') !== false) {
            returnValue.type = container.data('type');
        }
        if (typeof container.data('grid-size') !== 'undefined' && container.data('grid-size') !== false) {                    
            returnValue.gridSize = container.data('grid-size');
        }
        if (typeof container.data('columns') !== 'undefined' && container.data('columns') !== false) {                    
            returnValue.columns = container.data('columns');
        }
        if (typeof container.data('order-by') !== 'undefined' && container.data('order-by') !== false) {                    
            returnValue.orderBy = container.data('order-by');
        }
        if (typeof container.data('order') !== 'undefined' && container.data('order') !== false) {                    
            returnValue.order = container.data('order');
        }
        if (typeof container.data('number') !== 'undefined' && container.data('number') !== false) {                    
            returnValue.number = container.data('number');
        }
        if (typeof container.data('image-size') !== 'undefined' && container.data('image-size') !== false) {                    
            returnValue.imageSize = container.data('image-size');
        }
        if (typeof container.data('hover-type') !== 'undefined' && container.data('hover-type') !== false) {                    
            returnValue.hoverType = container.data('hover-type');
        }
        if (typeof container.data('filter') !== 'undefined' && container.data('filter') !== false) {                    
            returnValue.filter = container.data('filter');
        }
        if (typeof container.data('filter-order-by') !== 'undefined' && container.data('filter-order-by') !== false) {                    
            returnValue.filterOrderBy = container.data('filter-order-by');
        }
        if (typeof container.data('category') !== 'undefined' && container.data('category') !== false) {                    
            returnValue.category = container.data('category');
        }
        if (typeof container.data('selected-projects') !== 'undefined' && container.data('selected-projects') !== false) {                    
            returnValue.selectedProjectes = container.data('selected-projects');
        }
        if (typeof container.data('show-more') !== 'undefined' && container.data('show-more') !== false) {                    
            returnValue.showMore = container.data('show-more');
        }
        if (typeof container.data('title-tag') !== 'undefined' && container.data('title-tag') !== false) {                    
            returnValue.titleTag = container.data('title-tag');
        }
        if (typeof container.data('next-page') !== 'undefined' && container.data('next-page') !== false) {                    
            returnValue.nextPage = container.data('next-page');
        }
        if (typeof container.data('max-num-pages') !== 'undefined' && container.data('max-num-pages') !== false) {                    
            returnValue.maxNumPages = container.data('max-num-pages');
        }
        return returnValue;
    }
     /**
     * Sets portfolio load more data params for ajax function
     * @param portfolio list container with defined data params
     * return array
     */
    function mkdfSetPortfolioAjaxData(container){
        var returnValue = {
            action: 'mkd_core_portfolio_ajax_load_more',
            type: container.type,
            columns: container.columns,
            gridSize: container.gridSize,
            orderBy: container.orderBy,
            order: container.order,
            number: container.number,
            imageSize: container.imageSize,
            hoverType: container.hoverType,
            filter: container.filter,
            filterOrderBy: container.filterOrderBy,
            category: container.category,
            selectedProjectes: container.selectedProjectes,
            showMore: container.showMore,
            titleTag: container.titleTag,
            nextPage: container.nextPage
        };
        return returnValue;
    }

	/**
	 * Slider object that initializes whole slider functionality
	 * @type {Function}
	 */
	var mkdfSlider = mkdf.modules.shortcodes.mkdfSlider = function() {

		//all sliders on the page
		var sliders = $('.mkdf-slider .carousel');
		//image regex used to extract img source
		var imageRegex = /url\(["']?([^'")]+)['"]?\)/;

		/*** Functionality for translating image in slide - START ***/

		var matrixArray = { zoom_center : '1.2, 0, 0, 1.2, 0, 0', zoom_top_left: '1.2, 0, 0, 1.2, -150, -150', zoom_top_right : '1.2, 0, 0, 1.2, 150, -150', zoom_bottom_left: '1.2, 0, 0, 1.2, -150, 150', zoom_bottom_right: '1.2, 0, 0, 1.2, 150, 150'};

		// regular expression for parsing out the matrix components from the matrix string
		var matrixRE = /\([0-9epx\.\, \t\-]+/gi;

		// parses a matrix string of the form "matrix(n1,n2,n3,n4,n5,n6)" and
		// returns an array with the matrix components
		var parseMatrix = function (val) {
			return val.match(matrixRE)[0].substr(1).
			split(",").map(function (s) {
				return parseFloat(s);
			});
		};

		// transform css property names with vendor prefixes;
		// the plugin will check for values in the order the names are listed here and return as soon as there
		// is a value; so listing the W3 std name for the transform results in that being used if its available
		var transformPropNames = [
			"transform",
			"-webkit-transform"
		];

		var getTransformMatrix = function (el) {
			// iterate through the css3 identifiers till we hit one that yields a value
			var matrix = null;
			transformPropNames.some(function (prop) {
				matrix = el.css(prop);
				return (matrix !== null && matrix !== "");
			});

			// if "none" then we supplant it with an identity matrix so that our parsing code below doesn't break
			matrix = (!matrix || matrix === "none") ?
				"matrix(1,0,0,1,0,0)" : matrix;
			return parseMatrix(matrix);
		};

		// set the given matrix transform on the element; note that we apply the css transforms in reverse order of how its given
		// in "transformPropName" to ensure that the std compliant prop name shows up last
		var setTransformMatrix = function (el, matrix) {
			var m = "matrix(" + matrix.join(",") + ")";
			for (var i = transformPropNames.length - 1; i >= 0; --i) {
				el.css(transformPropNames[i], m + ' rotate(0.01deg)');
			}
		};

		// interpolates a value between a range given a percent
		var interpolate = function (from, to, percent) {
			return from + ((to - from) * (percent / 100));
		};

		$.fn.transformAnimate = function (opt) {
			// extend the options passed in by caller
			var options = {
				transform: "matrix(1,0,0,1,0,0)"
			};
			$.extend(options, opt);

			// initialize our custom property on the element to track animation progress
			this.css("percentAnim", 0);

			// supplant "options.step" if it exists with our own routine
			var sourceTransform = getTransformMatrix(this);
			var targetTransform = parseMatrix(options.transform);
			options.step = function (percentAnim, fx) {
				// compute the interpolated transform matrix for the current animation progress
				var $this = $(this);
				var matrix = sourceTransform.map(function (c, i) {
					return interpolate(c, targetTransform[i],
						percentAnim);
				});

				// apply the new matrix
				setTransformMatrix($this, matrix);

				// invoke caller's version of "step" if one was supplied;
				if (opt.step) {
					opt.step.apply(this, [matrix, fx]);
				}
			};

			// animate!
			return this.stop().animate({ percentAnim: 100 }, options);
		};

		/*** Functionality for translating image in slide - END ***/


		/**
		 * Calculate heights for slider holder and slide item, depending on window width, but only if slider is set to be responsive
		 * @param slider, current slider
		 * @param defaultHeight, default height of slider, set in shortcode
		 * @param responsive_breakpoint_set, breakpoints set for slider responsiveness
		 * @param reset, boolean for reseting heights
		 */
		var setSliderHeight = function(slider, defaultHeight, responsive_breakpoint_set, reset) {
			var sliderHeight = defaultHeight;
			if(!reset) {
				if(mkdf.windowWidth > responsive_breakpoint_set[0]) {
					sliderHeight = defaultHeight;
				} else if(mkdf.windowWidth > responsive_breakpoint_set[1]) {
					sliderHeight = defaultHeight * 0.75;
				} else if(mkdf.windowWidth > responsive_breakpoint_set[2]) {
					sliderHeight = defaultHeight * 0.6;
				} else if(mkdf.windowWidth > responsive_breakpoint_set[3]) {
					sliderHeight = defaultHeight * 0.55;
				} else if(mkdf.windowWidth <= responsive_breakpoint_set[3]) {
					sliderHeight = defaultHeight * 0.45;
				}
			}

			slider.css({'height': (sliderHeight) + 'px'});
			slider.find('.mkdf-slider-preloader').css({'height': (sliderHeight) + 'px'});
			slider.find('.mkdf-slider-preloader .mkdf-ajax-loader').css({'display': 'block'});
			slider.find('.item').css({'height': (sliderHeight) + 'px'});
			if(mkdfPerPageVars.vars.mkdfStickyScrollAmount === 0) {
				mkdf.modules.header.stickyAppearAmount = sliderHeight; //set sticky header appear amount if slider there is no amount entered on page itself
			}
		};

		/**
		 * Calculate heights for slider holder and slide item, depending on window size, but only if slider is set to be full height
		 * @param slider, current slider
		 */
		var setSliderFullHeight = function(slider) {
			var mobileHeaderHeight = mkdf.windowWidth < 1000 ? mkdfGlobalVars.vars.mkdfMobileHeaderHeight + $('.mkdf-top-bar').height() : 0;
			slider.css({'height': (mkdf.windowHeight - mobileHeaderHeight) + 'px'});
			slider.find('.mkdf-slider-preloader').css({'height': (mkdf.windowHeight - mobileHeaderHeight) + 'px'});
			slider.find('.mkd-slider-preloader .mkdf-ajax-loader').css({'display': 'block'});
			slider.find('.item').css({'height': (mkdf.windowHeight - mobileHeaderHeight) + 'px'});
			if(mkdfPerPageVars.vars.mkdfStickyScrollAmount === 0) {
				mkdf.modules.header.stickyAppearAmount = mkdf.windowHeight; //set sticky header appear amount if slider there is no amount entered on page itself
			}
		};

		var setElementsResponsiveness = function(slider) {
			// Basic text styles responsiveness
			slider
				.find('.mkdf-slide-element-text-small, .mkdf-slide-element-text-normal, .mkdf-slide-element-text-large, .mkdf-slide-element-text-extra-large')
				.each(function() {
					var element = $(this);
					if (typeof element.data('default-font-size') === 'undefined') { element.data('default-font-size', parseInt(element.css('font-size'),10)); }
					if (typeof element.data('default-line-height') === 'undefined') { element.data('default-line-height', parseInt(element.css('line-height'),10)); }
					if (typeof element.data('default-letter-spacing') === 'undefined') { element.data('default-letter-spacing', parseInt(element.css('letter-spacing'),10)); }
				});
			// Advanced text styles responsiveness
			slider.find('.mkdf-slide-element-responsive-text').each(function() {
				if (typeof $(this).data('default-font-size') === 'undefined') { $(this).data('default-font-size', parseInt($(this).css('font-size'),10)); }
				if (typeof $(this).data('default-line-height') === 'undefined') { $(this).data('default-line-height', parseInt($(this).css('line-height'),10)); }
				if (typeof $(this).data('default-letter-spacing') === 'undefined') { $(this).data('default-letter-spacing', parseInt($(this).css('letter-spacing'),10)); }
			});
			// Button responsiveness
			slider.find('.mkdf-slide-element-responsive-button').each(function() {
				if (typeof $(this).data('default-font-size') === 'undefined') { $(this).data('default-font-size', parseInt($(this).find('a').css('font-size'),10)); }
				if (typeof $(this).data('default-line-height') === 'undefined') { $(this).data('default-line-height', parseInt($(this).find('a').css('line-height'),10)); }
				if (typeof $(this).data('default-letter-spacing') === 'undefined') { $(this).data('default-letter-spacing', parseInt($(this).find('a').css('letter-spacing'),10)); }
				if (typeof $(this).data('default-ver-padding') === 'undefined') { $(this).data('default-ver-padding', parseInt($(this).find('a').css('padding-top'),10)); }
				if (typeof $(this).data('default-hor-padding') === 'undefined') { $(this).data('default-hor-padding', parseInt($(this).find('a').css('padding-left'),10)); }
			});
			// Margins for non-custom layouts
			slider.find('.mkdf-slide-element').each(function() {
				var element = $(this);
				if (typeof element.data('default-margin-top') === 'undefined') { element.data('default-margin-top', parseInt(element.css('margin-top'),10)); }
				if (typeof element.data('default-margin-bottom') === 'undefined') { element.data('default-margin-bottom', parseInt(element.css('margin-bottom'),10)); }
				if (typeof element.data('default-margin-left') === 'undefined') { element.data('default-margin-left', parseInt(element.css('margin-left'),10)); }
				if (typeof element.data('default-margin-right') === 'undefined') { element.data('default-margin-right', parseInt(element.css('margin-right'),10)); }
			});
			adjustElementsSizes(slider);
		};

		var adjustElementsSizes = function(slider) {
			var boundaries = {
				// These values must match those in map.php (for slider), slider.php and mkd.layout.php
				mobile: 600,
				tabletp: 800,
				tabletl: 1024,
				laptop: 1440
			};
			slider.find('.mkdf-slider-elements-container').each(function() {
				var container = $(this);
				var target = container.filter('.mkdf-custom-elements').add(container.not('.mkdf-custom-elements').find('.mkdf-slider-elements-holder-frame')).not('.mkdf-grid');
				if (target.length) {
					if (boundaries.mobile >= mkdf.windowWidth && container.attr('data-width-mobile').length) {
						target.css('width', container.data('width-mobile') + '%');
					}
					else if (boundaries.tabletp >= mkdf.windowWidth && container.attr('data-width-tablet-p').length) {
						target.css('width', container.data('width-tablet-p') + '%');
					}
					else if (boundaries.tabletl >= mkdf.windowWidth && container.attr('data-width-tablet-l').length) {
						target.css('width', container.data('width-tablet-l') + '%');
					}
					else if (boundaries.laptop >= mkdf.windowWidth && container.attr('data-width-laptop').length) {
						target.css('width', container.data('width-laptop') + '%');
					}
					else if (container.attr('data-width-desktop').length){
						target.css('width', container.data('width-desktop') + '%');
					}
				}
			});
			slider.find('.item').each(function() {
				var slide = $(this);
				var def_w = slide.find('.mkdf-slider-elements-holder-frame').data('default-width');
				var elements = slide.find('.mkdf-slide-element');

				// Adjusting margins for all elements
				elements.each(function() {
					var element = $(this);
					var def_m_top = element.data('default-margin-top'),
						def_m_bot = element.data('default-margin-bottom'),
						def_m_l = element.data('default-margin-left'),
						def_m_r = element.data('default-margin-right');
					var scale_data = (typeof element.data('resp-scale') !== 'undefined') ? element.data('resp-scale') : undefined;
					var factor;

					if (boundaries.mobile >= mkdf.windowWidth) {
						factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.mobile);
					}
					else if (boundaries.tabletp >= mkdf.windowWidth) {
						factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.tabletp);
					}
					else if (boundaries.tabletl >= mkdf.windowWidth) {
						factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.tabletl);
					}
					else if (boundaries.laptop >= mkdf.windowWidth) {
						factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.laptop);
					}
					else {
						factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.desktop);
					}

					element.css({
						'margin-top': Math.round(factor * def_m_top )+ 'px',
						'margin-bottom': Math.round(factor * def_m_bot )+ 'px',
						'margin-left': Math.round(factor * def_m_l )+ 'px',
						'margin-right': Math.round(factor * def_m_r) + 'px'
					});
				});

				// Adjusting responsiveness
				elements
					.filter('.mkdf-slide-element-responsive-text, .mkdf-slide-element-responsive-button, .mkdf-slide-element-responsive-image')
					.add(elements.find('a.mkdf-slide-element-responsive-text, span.mkdf-slide-element-responsive-text'))
					.each(function() {
						var element = $(this);
						var scale_data = (typeof element.data('resp-scale') !== 'undefined') ? element.data('resp-scale') : undefined,
							left_data = (typeof element.data('resp-left') !== 'undefined') ? element.data('resp-left') : undefined,
							top_data = (typeof element.data('resp-top') !== 'undefined') ? element.data('resp-top') : undefined;
						var factor, new_left, new_top;

						if (boundaries.mobile >= mkdf.windowWidth) {
							factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.mobile);
							new_left = (typeof left_data === 'undefined') ? (typeof element.data('left') !== 'undefined' ? element.data('left')+'%' : '') : (left_data.mobile != '' ? left_data.mobile+'%' : element.data('left')+'%');
							new_top = (typeof top_data === 'undefined') ? (typeof element.data('top') !== 'undefined' ? element.data('top')+'%' : '') : (top_data.mobile != '' ? top_data.mobile+'%' : element.data('top')+'%');
						}
						else if (boundaries.tabletp >= mkdf.windowWidth) {
							factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.tabletp);
							new_left = (typeof left_data === 'undefined') ? (typeof element.data('left') !== 'undefined' ? element.data('left')+'%' : '') : (left_data.tabletp != '' ? left_data.tabletp+'%' : element.data('left')+'%');
							new_top = (typeof top_data === 'undefined') ? (typeof element.data('top') !== 'undefined' ? element.data('top')+'%' : '') : (top_data.tabletp != '' ? top_data.tabletp+'%' : element.data('top')+'%');
						}
						else if (boundaries.tabletl >= mkdf.windowWidth) {
							factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.tabletl);
							new_left = (typeof left_data === 'undefined') ? (typeof element.data('left') !== 'undefined' ? element.data('left')+'%' : '') : (left_data.tabletl != '' ? left_data.tabletl+'%' : element.data('left')+'%');
							new_top = (typeof top_data === 'undefined') ? (typeof element.data('top') !== 'undefined' ? element.data('top')+'%' : '') : (top_data.tabletl != '' ? top_data.tabletl+'%' : element.data('top')+'%');
						}
						else if (boundaries.laptop >= mkdf.windowWidth) {
							factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.laptop);
							new_left = (typeof left_data === 'undefined') ? (typeof element.data('left') !== 'undefined' ? element.data('left')+'%' : '') : (left_data.laptop != '' ? left_data.laptop+'%' : element.data('left')+'%');
							new_top = (typeof top_data === 'undefined') ? (typeof element.data('top') !== 'undefined' ? element.data('top')+'%' : '') : (top_data.laptop != '' ? top_data.laptop+'%' : element.data('top')+'%');
						}
						else {
							factor = (typeof scale_data === 'undefined') ? mkdf.windowWidth / def_w : parseFloat(scale_data.desktop);
							new_left = (typeof left_data === 'undefined') ? (typeof element.data('left') !== 'undefined' ? element.data('left')+'%' : '') : (left_data.desktop != '' ? left_data.desktop+'%' : element.data('left')+'%');
							new_top = (typeof top_data === 'undefined') ? (typeof element.data('top') !== 'undefined' ? element.data('top')+'%' : '') : (top_data.desktop != '' ? top_data.desktop+'%' : element.data('top')+'%');
						}

						if (!factor) {
							element.hide();
						}
						else {
							element.show();
							var def_font_size,
								def_line_h,
								def_let_spac,
								def_ver_pad,
								def_hor_pad;

							if (element.is('.mkdf-slide-element-responsive-button')) {
								def_font_size = element.data('default-font-size');
								def_line_h = element.data('default-line-height');
								def_let_spac = element.data('default-letter-spacing');
								def_ver_pad = element.data('default-ver-padding');
								def_hor_pad = element.data('default-hor-padding');

								element.css({
										'left': new_left,
										'top': new_top
									})
									.find('.mkdf-btn').css({
									'font-size': Math.round(factor * def_font_size) + 'px',
									'line-height': Math.round(factor * def_line_h) + 'px',
									'letter-spacing': Math.round(factor * def_let_spac) + 'px',
									'padding-left': Math.round(factor * def_hor_pad) + 'px',
									'padding-right': Math.round(factor * def_hor_pad) + 'px',
									'padding-top': Math.round(factor * def_ver_pad) + 'px',
									'padding-bottom': Math.round(factor * def_ver_pad) + 'px'
								});
							}
							else if (element.is('.mkdf-slide-element-responsive-image')) {
								if (factor != mkdf.windowWidth / def_w) { // if custom factor has been set for this screen width
									var up_w = element.data('upload-width'),
										up_h = element.data('upload-height');

									element.filter('.custom-image').css({
											'left': new_left,
											'top': new_top
										})
										.add(element.not('.custom-image').find('img'))
										.css({
											'width': Math.round(factor * up_w) + 'px',
											'height': Math.round(factor * up_h) + 'px'
										});
								}
								else {
									var w = element.data('width');

									element.filter('.custom-image').css({
											'left': new_left,
											'top': new_top
										})
										.add(element.not('.custom-image').find('img'))
										.css({
											'width': w + '%',
											'height': ''
										});
								}
							}
							else {
								def_font_size = element.data('default-font-size');
								def_line_h = element.data('default-line-height');
								def_let_spac = element.data('default-letter-spacing');

								element.css({
									'left': new_left,
									'top': new_top,
									'font-size': Math.round(factor * def_font_size) + 'px',
									'line-height': Math.round(factor * def_line_h) + 'px',
									'letter-spacing': Math.round(factor * def_let_spac) + 'px'
								});
							}
						}
					});
			});
			var nav = slider.find('.carousel-indicators');
			slider.find('.mkdf-slide-element-section-link').css('bottom', nav.length ? parseInt(nav.css('bottom'),10) + nav.outerHeight() + 10 + 'px' : '20px');
		};

		var checkButtonsAlignment = function(slider) {
			slider.find('.item').each(function() {
				var inline_buttons = $(this).find('.mkdf-slide-element-button-inline');
				inline_buttons.css('display', 'inline-block').wrapAll('<div class="mkdf-slide-elements-buttons-wrapper" style="text-align: ' + inline_buttons.eq(0).css('text-align') + ';"/>');
			});
		};

		/**
		 * Set heights for slider and elemnts depending on slider settings (full height, responsive height od set height)
		 * @param slider, current slider
		 */
		var setHeights =  function(slider) {

			var responsiveBreakpointSet = [1600,1200,900,650,500,320];

			setElementsResponsiveness(slider);

			if(slider.hasClass('mkdf-full-screen')){

				setSliderFullHeight(slider);

				$(window).resize(function() {
					setSliderFullHeight(slider);
					adjustElementsSizes(slider);
				});

			}else if(slider.hasClass('mkdf-responsive-height')){

				var defaultHeight = slider.data('height');
				setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false);

				$(window).resize(function() {
					setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false);
					adjustElementsSizes(slider);
				});

			}else {
				var defaultHeight = slider.data('height');

				slider.find('.mkdf-slider-preloader').css({'height': (slider.height()) + 'px'});
				slider.find('.mkdf-slider-preloader .mkdf-ajax-loader').css({'display': 'block'});

				mkdf.windowWidth < 1000 ? setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false) : setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, true);

				$(window).resize(function() {
					if(mkdf.windowWidth < 1000){
						setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false);
					}else{
						setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, true);
					}
					adjustElementsSizes(slider);
				});
			}
		};

		/**
		 * Set prev/next numbers on navigation arrows
		 * @param slider, current slider
		 * @param currentItem, current slide item index
		 * @param totalItemCount, total number of slide items
		 */
		var setPrevNextNumbers = function(slider, currentItem, totalItemCount) {
			if(currentItem == 1){
				slider.find('.left.carousel-control .prev').html(totalItemCount);
				slider.find('.right.carousel-control .next').html(currentItem + 1);
			}else if(currentItem == totalItemCount){
				slider.find('.left.carousel-control .prev').html(currentItem - 1);
				slider.find('.right.carousel-control .next').html(1);
			}else{
				slider.find('.left.carousel-control .prev').html(currentItem - 1);
				slider.find('.right.carousel-control .next').html(currentItem + 1);
			}
		};

		/**
		 * Set video background size
		 * @param slider, current slider
		 */
		var initVideoBackgroundSize = function(slider){
			var min_w = 1500; // minimum video width allowed
			var video_width_original = 1920;  // original video dimensions
			var video_height_original = 1080;
			var vid_ratio = 1920/1080;

			slider.find('.item .mkdf-video .mkdf-video-wrap').each(function(){

				var slideWidth = mkdf.windowWidth;
				var slideHeight = $(this).closest('.carousel').height();

				$(this).width(slideWidth);

				min_w = vid_ratio * (slideHeight+20);
				$(this).height(slideHeight);

				var scale_h = slideWidth / video_width_original;
				var scale_v = (slideHeight - mkdfGlobalVars.vars.mkdfMenuAreaHeight) / video_height_original;
				var scale =  scale_v;
				if (scale_h > scale_v)
					scale =  scale_h;
				if (scale * video_width_original < min_w) {scale = min_w / video_width_original;}

				$(this).find('video, .mejs-overlay, .mejs-poster').width(Math.ceil(scale * video_width_original +2));
				$(this).find('video, .mejs-overlay, .mejs-poster').height(Math.ceil(scale * video_height_original +2));
				$(this).scrollLeft(($(this).find('video').width() - slideWidth) / 2);
				$(this).find('.mejs-overlay, .mejs-poster').scrollTop(($(this).find('video').height() - slideHeight) / 2);
				$(this).scrollTop(($(this).find('video').height() - slideHeight) / 2);
			});
		};

		/**
		 * Init video background
		 * @param slider, current slider
		 */
		var initVideoBackground = function(slider) {
			$('.item .mkdf-video-wrap .mkdf-video-element').mediaelementplayer({
				enableKeyboard: false,
				iPadUseNativeControls: false,
				pauseOtherPlayers: false,
				// force iPhone's native controls
				iPhoneUseNativeControls: false,
				// force Android's native controls
				AndroidUseNativeControls: false
			});

			initVideoBackgroundSize(slider);
			$(window).resize(function() {
				initVideoBackgroundSize(slider);
			});

			//mobile check
			if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/)){
				$('.mkdf-slider .mkdf-mobile-video-image').show();
				$('.mkdf-slider .mkdf-video-wrap').remove();
			}
		};

		var initPeek = function(slider) {
			if (slider.hasClass('mkdf-slide-peek')) {

				var navArrowHover = function(arrow, entered) {
					var dir = arrow.is('.left') ? 'left' : 'right';
					var targ_peeker = peekers.filter('.'+dir);
					if (entered) {
						arrow.addClass('hovered');
						var targ_item = (items.index(items.filter('.active')) + (dir=='left' ? -1 : 1) + items.length) % items.length;
						targ_peeker.find('.mkdf-slider-peeker-inner').css({
							'background-image': items.eq(targ_item).find('.mkdf-image, .mkdf-mobile-video-image').css('background-image'),
							'width': itemWidth + 'px'
						});
						targ_peeker.addClass('shown');
					}
					else {
						arrow.removeClass('hovered');
						peekers.removeClass('shown');
					}
				};

				var navBulletHover = function(bullet, entered) {
					if (entered) {
						bullet.addClass('hovered');

						var targ_item = bullet.data('slide-to');
						var cur_item = items.index(items.filter('.active'));
						if (cur_item != targ_item) {
							var dir = (targ_item < cur_item) ? 'left' : 'right';
							var targ_peeker = peekers.filter('.'+dir);
							targ_peeker.find('.mkdf-slider-peeker-inner').css({
								'background-image': items.eq(targ_item).find('.mkdf-image, .mkdf-mobile-video-image').css('background-image'),
								'width': itemWidth + 'px'
							});
							targ_peeker.addClass('shown');
						}
					}
					else {
						bullet.removeClass('hovered');
						peekers.removeClass('shown');
					}
				};

				var handleResize = function() {
					itemWidth = items.filter('.active').width();
					itemWidth += (itemWidth % 2) ? 1 : 0; // To make it even
					items.children('.mkdf-image, .mkdf-video').css({
						'position': 'absolute',
						'width': itemWidth + 'px',
						'height': '110%',
						'left': '50%',
						'transform': 'translateX(-50%)'
					});
				};

				var items = slider.find('.item');
				var itemWidth;
				handleResize();
				$(window).resize(handleResize);

				slider.find('.carousel-inner').append('<div class="mkdf-slider-peeker left"><div class="mkdf-slider-peeker-inner"></div></div><div class="mkdf-slider-peeker right"><div class="mkdf-slider-peeker-inner"></div></div>');
				var peekers = slider.find('.mkdf-slider-peeker');
				var nav_arrows = slider.find('.carousel-control');
				var nav_bullets = slider.find('.carousel-indicators > li');

				nav_arrows
					.hover(
						function() {
							navArrowHover($(this), true);
						},
						function() {
							navArrowHover($(this), false);
						}
					);

				nav_bullets
					.hover(
						function() {
							navBulletHover($(this), true);
						},
						function() {
							navBulletHover($(this), false);
						}
					);

				slider.on('slide.bs.carousel', function() {
					setTimeout(function() {
						peekers.addClass('mkdf-slide-peek-in-progress').removeClass('shown');
					}, 500);
				});

				slider.on('slid.bs.carousel', function() {
					nav_arrows.filter('.hovered').each(function() {
						navArrowHover($(this), true);
					});
					setTimeout(function() {
						nav_bullets.filter('.hovered').each(function() {
							navBulletHover($(this), true);
						});
					}, 200);
					peekers.removeClass('mkdf-slide-peek-in-progress');
				});
			}
		};

		var updateNavigationThumbs = function(slider) {
			if (slider.hasClass('mkdf-slider-thumbs')) {
				var src, prev_image, next_image;
				var all_items_count = slider.find('.item').length;
				var curr_item = slider.find('.item').index($('.item.active')[0]) + 1;
				setPrevNextNumbers(slider, curr_item, all_items_count);

				// prev thumb
				if(slider.find('.item.active').prev('.item').length){
					if(slider.find('.item.active').prev('div').find('.mkdf-image').length){
						src = imageRegex.exec(slider.find('.active').prev('div').find('.mkdf-image').attr('style'));
						prev_image = new Image();
						prev_image.src = src[1];
						//prev_image = '<div class="thumb-image" style="background-image: url('+src[1]+')"></div>';
					}else{
						prev_image = slider.find('.active').prev('div').find('> .mkdf-video').clone();
						prev_image.find('.mkdf-video-overlay, .mejs-offscreen').remove();
						prev_image.find('.mkdf-video-wrap').width(150).height(84);
						prev_image.find('.mejs-container').width(150).height(84);
						prev_image.find('video').width(150).height(84);
					}
					slider.find('.left.carousel-control .img .old').fadeOut(300,function(){
						$(this).remove();
					});
					slider.find('.left.carousel-control .img').append(prev_image).find('div.thumb-image, > img, div.mkdf-video').fadeIn(300).addClass('old');

				}else{
					if(slider.find('.carousel-inner .item:last-child .mkdf-image').length){
						src = imageRegex.exec(slider.find('.carousel-inner .item:last-child .mkdf-image').attr('style'));
						prev_image = new Image();
						prev_image.src = src[1];
						//prev_image = '<div class="thumb-image" style="background-image: url('+src[1]+')"></div>';
					}else{
						prev_image = slider.find('.carousel-inner .item:last-child > .mkdf-video').clone();
						prev_image.find('.mkdf-video-overlay, .mejs-offscreen').remove();
						prev_image.find('.mkdf-video-wrap').width(150).height(84);
						prev_image.find('.mejs-container').width(150).height(84);
						prev_image.find('video').width(150).height(84);
					}
					slider.find('.left.carousel-control .img .old').fadeOut(300,function(){
						$(this).remove();
					});
					slider.find('.left.carousel-control .img').append(prev_image).find('div.thumb-image, > img, div.mkdf-video').fadeIn(300).addClass('old');
				}

				// next thumb
				if(slider.find('.active').next('div.item').length){
					if(slider.find('.active').next('div').find('.mkdf-image').length){
						src = imageRegex.exec(slider.find('.active').next('div').find('.mkdf-image').attr('style'));
						next_image = new Image();
						next_image.src = src[1];
						//next_image = '<div class="thumb-image" style="background-image: url('+src[1]+')"></div>';
					}else{
						next_image = slider.find('.active').next('div').find('> .mkdf-video').clone();
						next_image.find('.mkdf-video-overlay, .mejs-offscreen').remove();
						next_image.find('.mkdf-video-wrap').width(150).height(84);
						next_image.find('.mejs-container').width(150).height(84);
						next_image.find('video').width(150).height(84);
					}

					slider.find('.right.carousel-control .img .old').fadeOut(300,function(){
						$(this).remove();
					});
					slider.find('.right.carousel-control .img').append(next_image).find('div.thumb-image, > img, div.mkdf-video').fadeIn(300).addClass('old');

				}else{
					if(slider.find('.carousel-inner .item:first-child .mkdf-image').length){
						src = imageRegex.exec(slider.find('.carousel-inner .item:first-child .mkdf-image').attr('style'));
						next_image = new Image();
						next_image.src = src[1];
						//next_image = '<div class="thumb-image" style="background-image: url('+src[1]+')"></div>';
					}else{
						next_image = slider.find('.carousel-inner .item:first-child > .mkdf-video').clone();
						next_image.find('.mkdf-video-overlay, .mejs-offscreen').remove();
						next_image.find('.mkdf-video-wrap').width(150).height(84);
						next_image.find('.mejs-container').width(150).height(84);
						next_image.find('video').width(150).height(84);
					}
					slider.find('.right.carousel-control .img .old').fadeOut(300,function(){
						$(this).remove();
					});
					slider.find('.right.carousel-control .img').append(next_image).find('div.thumb-image, > img, div.mkdf-video').fadeIn(300).addClass('old');
				}
			}
		};

		/**
		 * initiate slider
		 * @param slider, current slider
		 * @param currentItem, current slide item index
		 * @param totalItemCount, total number of slide items
		 * @param slideAnimationTimeout, timeout for slide change
		 */
		var initiateSlider = function(slider, totalItemCount, slideAnimationTimeout) {

			//set active class on first item
			slider.find('.carousel-inner .item:first-child').addClass('active');
			//check for header style
			mkdfCheckSliderForHeaderStyle($('.carousel .active'), slider.hasClass('mkdf-header-effect'));
			// setting numbers on carousel controls
			if(slider.hasClass('mkdf-slider-numbers')) {
				setPrevNextNumbers(slider, 1, totalItemCount);
			}
			// set video background if there is video slide
			if(slider.find('.item video').length){
				//initVideoBackgroundSize(slider);
				initVideoBackground(slider);
			}

			// update thumbs
			updateNavigationThumbs(slider);

			// initiate peek
			initPeek(slider);

			// enable link hover color for slide elements with links
			slider.find('.mkdf-slide-element-wrapper-link')
				.mouseenter(function() {
					$(this).removeClass('inheriting');
				})
				.mouseleave(function() {
					$(this).addClass('inheriting');
				})
			;

			//init slider
			if(slider.hasClass('mkdf-auto-start')){
				slider.carousel({
					interval: slideAnimationTimeout,
					pause: false
				});

				//pause slider when hover slider button
				slider.find('.slide_buttons_holder .qbutton')
					.mouseenter(function() {
						slider.carousel('pause');
					})
					.mouseleave(function() {
						slider.carousel('cycle');
					});
			} else {
				slider.carousel({
					interval: 0,
					pause: false
				});
			}

			$(window).scroll(function() {
				if(slider.hasClass('mkdf-full-screen') && mkdf.scroll > mkdf.windowHeight && mkdf.windowWidth > 1000){
					slider.carousel('pause');
				}else if(!slider.hasClass('mkdf-full-screen') && mkdf.scroll > slider.height() && mkdf.windowWidth > 1000){
					slider.carousel('pause');
				}else{
					slider.carousel('cycle');
				}
			});


			//initiate image animation
			if($('.carousel-inner .item:first-child').hasClass('mkdf-animate-image') && mkdf.windowWidth > 1000){
				slider.find('.carousel-inner .item.mkdf-animate-image:first-child .mkdf-image').transformAnimate({
					transform: "matrix("+matrixArray[$('.carousel-inner .item:first-child').data('mkdf_animate_image')]+")",
					duration: 30000
				});
			}
		};

		return {
			init: function() {
				if(sliders.length) {
					sliders.each(function() {
						var $this = $(this);
						var slideAnimationTimeout = $this.data('slide_animation_timeout');
						var totalItemCount = $this.find('.item').length;

						checkButtonsAlignment($this);

						setHeights($this);

						/*** wait until first video or image is loaded and than initiate slider - start ***/
						if(mkdf.htmlEl.hasClass('touch')){
							if($this.find('.item:first-child .mkdf-mobile-video-image').length > 0){
								var src = imageRegex.exec($this.find('.item:first-child .mkdf-mobile-video-image').attr('style'));
							}else{
								var src = imageRegex.exec($this.find('.item:first-child .mkdf-image').attr('style'));
							}
							if(src) {
								var backImg = new Image();
								backImg.src = src[1];
								$(backImg).load(function(){
									$('.mkdf-slider-preloader').fadeOut(500);
									initiateSlider($this,totalItemCount,slideAnimationTimeout);
								});
							}
						} else {
							if($this.find('.item:first-child video').length > 0){
								$this.find('.item:first-child video').eq(0).one('loadeddata',function(){
									$('.mkdf-slider-preloader').fadeOut(500);
									initiateSlider($this,totalItemCount,slideAnimationTimeout);
								});
							}else{
								var src = imageRegex.exec($this.find('.item:first-child .mkdf-image').attr('style'));
								if (src) {
									var backImg = new Image();
									backImg.src = src[1];
									$(backImg).load(function(){
										$('.mkdf-slider-preloader').fadeOut(500);
										initiateSlider($this,totalItemCount,slideAnimationTimeout);
									});
								}
							}
						}
						/*** wait until first video or image is loaded and than initiate slider - end ***/

						/* before slide transition - start */
						$this.on('slide.bs.carousel', function () {
							$this.addClass('mkdf-in-progress');
							$this.find('.active .mkdf-slider-elements-holder-frame, .active .mkdf-slide-element-section-link').fadeTo(250,0);
						});
						/* before slide transition - end */

						/* after slide transition - start */
						$this.on('slid.bs.carousel', function () {
							$this.removeClass('mkdf-in-progress');
							$this.find('.active .mkdf-slider-elements-holder-frame, .active .mkdf-slide-element-section-link').fadeTo(0,1);

							// setting numbers on carousel controls
							if($this.hasClass('mkdf-slider-numbers')) {
								var currentItem = $('.item').index($('.item.active')[0]) + 1;
								setPrevNextNumbers($this, currentItem, totalItemCount);
							}

							// initiate image animation on active slide and reset all others
							$('.item.mkdf-animate-image .mkdf-image').stop().css({'transform':'', '-webkit-transform':''});
							if($('.item.active').hasClass('mkdf-animate-image') && mkdf.windowWidth > 1000){
								$('.item.mkdf-animate-image.active .mkdf-image').transformAnimate({
									transform: "matrix("+matrixArray[$('.item.mkdf-animate-image.active').data('mkdf_animate_image')]+")",
									duration: 30000
								});
							}

							// setting thumbnails on navigation controls
							if($this.hasClass('mkdf-slider-thumbs')) {
								updateNavigationThumbs($this);
							}
						});
						/* after slide transition - end */

						/* swipe functionality - start */
						$this.swipe( {
							swipeLeft: function(){ $this.carousel('next'); },
							swipeRight: function(){ $this.carousel('prev'); },
							threshold:20
						});
						/* swipe functionality - end */

					});

					//adding parallax functionality on slider
					if($('.no-touch .carousel').length){
						var skrollr_slider = skrollr.init({
							smoothScrolling: false,
							forceHeight: false
						});
						skrollr_slider.refresh();
					}

					$(window).scroll(function(){
						//set control class for slider in order to change header style
						if($('.mkdf-slider .carousel').height() < mkdf.scroll){
							$('.mkdf-slider .carousel').addClass('mkdf-disable-slider-header-style-changing');
						}else{
							$('.mkdf-slider .carousel').removeClass('mkdf-disable-slider-header-style-changing');
							mkdfCheckSliderForHeaderStyle($('.mkdf-slider .carousel .active'),$('.mkdf-slider .carousel').hasClass('mkdf-header-effect'));
						}

						//hide slider when it is out of viewport
						if($('.mkdf-slider .carousel').hasClass('mkdf-full-screen') && mkdf.scroll > mkdf.windowHeight && mkdf.windowWidth > 1000){
							$('.mkdf-slider .carousel').find('.carousel-inner, .carousel-indicators').hide();
						}else if(!$('.mkdf-slider .carousel').hasClass('mkdf-full-screen') && mkdf.scroll > $('.mkdf-slider .carousel').height() && mkdf.windowWidth > 1000){
							$('.mkdf-slider .carousel').find('.carousel-inner, .carousel-indicators').hide();
						}else{
							$('.mkdf-slider .carousel').find('.carousel-inner, .carousel-indicators').show();
						}
					});
				}
			}
		};
	};

    /**
     * Check if slide effect on header style changing
     * @param slide, current slide
     * @param headerEffect, flag if slide
     */

    function mkdfCheckSliderForHeaderStyle(slide, headerEffect) {

        if($('.mkdf-slider .carousel').not('.mkdf-disable-slider-header-style-changing').length > 0) {

            var slideHeaderStyle = "";
            if (slide.hasClass('light')) { slideHeaderStyle = 'mkdf-light-header'; }
            if (slide.hasClass('dark')) { slideHeaderStyle = 'mkdf-dark-header'; }

            if (slideHeaderStyle !== "") {
                if (headerEffect) {
                    mkdf.body.removeClass('mkdf-dark-header mkdf-light-header').addClass(slideHeaderStyle);
                }
            } else {
                if (headerEffect) {
                    mkdf.body.removeClass('mkdf-dark-header mkdf-light-header').addClass(mkdf.defaultHeaderStyle);
                }

            }
        }
    }

    /**
     * List object that initializes list with animation
     * @type {Function}
     */
    var mkdfInitIconList = mkdf.modules.shortcodes.mkdfInitIconList = function() {
        var iconList = $('.mkdf-animate-list');

        /**
         * Initializes icon list animation
         * @param list current list shortcode
         */
        var iconListInit = function(list) {
            setTimeout(function(){
                list.appear(function(){
                    list.addClass('mkdf-appeared');
                },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
            },30);
        };

        return {
            init: function() {
                if(iconList.length) {
                    iconList.each(function() {
                        iconListInit($(this));
                    });
                }
            }
        };
    };


	/*
	 **	Vertical Split Slider
	 */

	function mkdfInitVerticalSplitSlider(){

		var body = $('body');

		if(body.hasClass('mkdf-vertical-split-screen-initialized')){
			body.removeClass('mkdf-vertical-split-screen-initialized');
			$.fn.multiscroll.destroy();
		}

		if($('.mkdf-vertical-split-slider').length) {

			var slider = $('.mkdf-vertical-split-slider');

			slider.height(mkdf.windowHeight).animate({opacity:1},300);
			slider.multiscroll({
				scrollingSpeed: 500,
				navigation: true,
				useAnchorsOnLoad: false,
				sectionSelector: '.mkdf-vss-ms-section',
				leftSelector: '.mkdf-vss-ms-left',
				rightSelector: '.mkdf-vss-ms-right',
				afterRender: function(){

					body.addClass('mkdf-vertical-split-screen-initialized');

					//prepare html for smaller screens - start //
					var verticalSplitSliderResponsive = $("<div class='mkdf-vertical-split-slider-responsive' />");
					slider.after(verticalSplitSliderResponsive);
					var leftSide    = $('.mkdf-vertical-split-slider .mkdf-vss-ms-left > div');
					var rightSide   = $('.mkdf-vertical-split-slider .mkdf-vss-ms-right > div');

					for(var i = 0; i < leftSide.length; i++){
						verticalSplitSliderResponsive.append($(leftSide[i]).clone(true));
						verticalSplitSliderResponsive.append($(rightSide[leftSide.length-1-i]).clone(true));
					}

					//prepare google maps clones
					if($('.mkdf-vertical-split-slider-responsive .mkdf-google-map').length){
						$('.mkdf-vertical-split-slider-responsive .mkdf-google-map').each(function(){
							var map = $(this);
							map.empty();
							var num = Math.floor((Math.random() * 100000) + 1);
							map.attr('id','mkdf-map-' + num);
							map.data('unique-id', num);
						});
					}

					mkdfInitPortfolioListMasonry();
					mkdfInitPortfolioListPinterest();
					mkdfInitPortfolio();
					mkdfShowGoogleMap();
				}
			});


			if(mkdf.windowWidth <= 1024){
				$.fn.multiscroll.destroy();
			}else{
				$.fn.multiscroll.build();
			}
			
			$(window).resize(function() {
				if(mkdf.windowWidth <= 1024){
					$.fn.multiscroll.destroy();
				}else{
					$.fn.multiscroll.build();
				}
				
			});
		}
	}

	/*
	 * Type out functionality for Custom Font
	 */
	function mkdfCustomFontTypeOut() {

		var mkdfTyped = $('.mkdf-typed');

		if (mkdfTyped.length) {
			mkdfTyped.each(function(){

				//vars
				var thisTyped = $(this),
					typedWrap = thisTyped.parents('.mkdf-typed-wrap'),
					customFontHolder = typedWrap.parents('.mkdf-custom-font-holder'),
					originalText = customFontHolder.find('.mkdf-custom-font-original'),
					str,
					string_1 = thisTyped.find('.mkdf-typed-1').text(),
					string_2 = thisTyped.find('.mkdf-typed-2').text(),
					string_3 = thisTyped.find('.mkdf-typed-3').text();

				//show only the strings that are entered in
				if (!string_2.trim() || !string_3.trim() ) {
					str = [string_1];
				}
				if (!string_3.trim() && string_2.length) {
					str = [string_1,string_2];
				}
				if (string_1.length && string_2.length && string_3.length) {
					str = [string_1,string_2,string_3];
				}

				//ampersand
				if(originalText.text().indexOf('&') != -1) {
					originalText.html(originalText.text().replace('&', '<span class="mkdf-amp">&</span>'));
				}

				//typeout
				setTimeout(function(){
					customFontHolder.appear(function() {
						thisTyped.typed({
							strings: str,
							typeSpeed: 90,
							backDelay: 700,
							loop: true,
							contentType: 'text',
							loopCount: false,
							cursorChar: "_",
						});
					},{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
				}, 100);

			});
		}
	}

	/*
	 * Type out functionality for Section Title
	 */
	function mkdfSectionTitleTypeOut() {

		var mkdfTyped = $('.mkdf-section-type-out');

		if (mkdfTyped.length) {
			mkdfTyped.each(function(){

				//vars
				var thisTyped = $(this),
					typed = thisTyped.find('.mkdf-section-highlighted'),
					str = typed.text();

				//typeout
				setTimeout(function(){
					thisTyped.appear(function() {
						typed.typed({
							strings: [str],
							typeSpeed: 90,
							backDelay: 700,
							loop: true,
							contentType: 'text',
							loopCount: false,
							cursorChar: "_",
						});
					},{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
				}, 100);

			});
		}
	}

    /**
     * Check if slide effect on header style changing
     */
    function mkdfItemShowcase() {
        var itemShowcase = $('.mkdf-item-showcase');
        if (itemShowcase.length) {
            itemShowcase.each(function(){
                var thisItemShowcase = $(this),
                    leftItems = thisItemShowcase.find('.mkdf-item-left'),
                    rightItems = thisItemShowcase.find('.mkdf-item-right'),
                    itemImage = thisItemShowcase.find('.mkdf-item-image');

                //logic
                leftItems.wrapAll( "<div class='mkdf-item-showcase-holder mkdf-holder-left' />");
                rightItems.wrapAll( "<div class='mkdf-item-showcase-holder mkdf-holder-right' />");
                thisItemShowcase.animate({opacity:1},200);
                setTimeout(function(){
                    thisItemShowcase.appear(function(){
                        itemImage.addClass('mkdf-appeared');
                        thisItemShowcase.on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
                            function(e) {
                                if(mkdf.windowWidth > 1200) {
                                    itemAppear('.mkdf-holder-left .mkdf-item');
                                    itemAppear('.mkdf-holder-right .mkdf-item');
                                } else {
                                    itemAppear('.mkdf-item');
                                }
                            });
                    },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
                },100);

                //appear animation trigger
                function itemAppear(itemCSSClass) {
                    thisItemShowcase.find(itemCSSClass).each(function(i){
                        var thisListItem = $(this);
                        setTimeout(function(){
                            thisListItem.addClass('mkdf-appeared');
                        }, i*150);
                    });
                }
            });

        }
    }

    /*
    **	Init Section Holder
    */
	function mkdfInitSectionHolder(){
		var sectionHolder = $('.mkdf-section-holder');
		if (sectionHolder.length){
			sectionHolder.each(function(){
				var thisHolder = $(this),
					items = thisHolder.find('.mkdf-section-item'),
					height = items.first().outerHeight();

					items.each(function(){
						var thisItem = $(this);

						if (height < thisItem.outerHeight()){
							height = thisItem.outerHeight();
						}
					});

					items.each(function(){
						var thisItem = $(this);
						
						thisItem.css('height',height);
					});

					thisHolder.addClass('mkdf-appeared');
			});
		}
	}

    /**
     * Init text slider shortcode
     */
    function mkdfInitTextSlider(){

        var textSlider = $('.mkdf-text-slider');
        if(textSlider.length){
            textSlider.each(function(){

                var thisSlider = $(this),
					auto = true,
					controlNav = true,
					animationSpeed = 600,
					slidesToShow = 1;

				if(typeof thisSlider.data('bullets') !== 'undefined') {
					controlNav = (thisSlider.data('bullets') == 'yes') ? true : false;
				}

                thisSlider.on('init', function(){
                    thisSlider.css('visibility','visible');
                });

				thisSlider.slick({
					infinite: true,
					autoplay: auto,
					slidesToShow : slidesToShow,
					arrows: false,
					dots: controlNav,
					dotsClass: 'mkdf-slick-numbers',
					adaptiveHeight: true,
					prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"><span class="arrow_left"></span></span>',
					nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"><span class="arrow_right"></span></span>',
					customPaging: function(slider, i) {
						return '<span class="mkdf-slick-numbers-inner">' + (i + 1) + '</span>';
					},
                    easing: 'easeInOutQuint',
                    speed: 1000,
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
				});

            });

        }

    }

    /*
    * Interactive Images
    */
    function mkdfInitInteractiveItems() {
        var interactiveItemsHolder = $('.mkdf-interactive-items-holder');

        if (interactiveItemsHolder.length){
	        interactiveItemsHolder.each(function () {
	        	var thisHolder = $(this),
	        		items = thisHolder.find('.mkdf-int-item'),
	        		height = items.first().outerHeight();

				items.each(function(){
					var thisItem = $(this),
						thisItemHeight = thisItem.outerHeight();

					if (height < thisItemHeight){
						height = thisItemHeight;
					}
				});

				items.each(function(){
					var thisItem = $(this);
					
					thisItem.css('height',height);
				});

				if (thisHolder.hasClass('mkdf-tile-hover-effect') && !mkdf.html.hasClass('touch')){
            
		            items.each(function(){
		                var currentImage = $(this),
		                    flag = false,
		                    enter,
		                    leave;

		                currentImage.on('mouseenter', function(){
		                    if (!flag) {
		                        currentImage.addClass('mkdf-hovered');
		                        clearTimeout(leave);
		                        enter = setTimeout(function(){
		                            flag = true;
		                        }, 400);
		                    }
		                });

		                currentImage.on('mouseleave', function(){
		                    if (flag) {
		                        currentImage.removeClass('mkdf-hovered');
		                        flag = false;
		                    } else {
		                        clearTimeout(enter);
		                        leave = setTimeout(function(){
		                            currentImage.removeClass('mkdf-hovered');
		                            flag = false;
		                        },400);
		                    }
		                });
		            });
				}


				if (thisHolder.hasClass('mkdf-appear-effect') && !mkdf.html.hasClass('touch')){
					if (thisHolder.hasClass('mkdf-one-by-one')){
		                var cycle = 0,
		                    n = 0;
		                
		                items.each(function(){
		                    if ($(this).parent().offset().top == thisHolder.offset().top) {
		                        cycle ++;
		                    }
		                });

		                items.appear(function(){
		                    var currentImage = $(this);

		                    if (n == cycle) {
		                        n = 0;
		                    }

		                    setTimeout(function(){
		                        currentImage.addClass('mkdf-appeared');
		                    }, n * 200);

		                    n++;
		                },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
					}
					else if (thisHolder.hasClass('mkdf-randomize')){
		                thisHolder.appear(function(){

		                    var randomize = function(n) {
		                        var queue = new Array();

		                        for (var i = 0; i < numberOfItems; i++) {
		                            var queueElement = Math.floor(Math.random()*numberOfItems);

		                            if( jQuery.inArray(queueElement, queue) > 0 ) { 
		                                --i;
		                            } else {
		                                queue.push(queueElement);
		                            }
		                        }

		                        return queue;
		                    };

		                    var numberOfItems = items.length,
		                        r = randomize(numberOfItems);

		                    items.each(function(i) {
		                        var currentImage = $(this);

		                        //ease fx
		                        //n = Math.floor(Math.random() * (i + 1)/i*5) + 1; 
		                        //setTimeout(function(){
		                        //     currentImage.addClass('mkdf-appeared')
		                        //}, n * 150);

		                        //linear
		                        setTimeout(function(){
		                            currentImage.addClass('mkdf-appeared');
		                        },  r[i]*70);
		                    });
		                },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
					}
				}


	        });
	    }

    }

    /*
    * Parallax Call to Action shortcode init
    */
    function mkdfParallaxCTA() {
        var parallaxCTAs = $('.mkdf-parallax-call-to-action');

        if (parallaxCTAs.length) {
            parallaxCTAs.each(function(){
                var parallaxCTA = $(this),
                    contentHolder = parallaxCTA.find('.mkdf-pcta-content-holder'),
                    imagesHolder = parallaxCTA.find('.mkdf-pcta-images-holder'),
                    mainImage = imagesHolder.find('.mkdf-main-image'),
                    additionalImage = imagesHolder.find('.mkdf-additional-image');

                var parallaxInstance = function(element,offset) {
                    element.attr('data-parallax', '{"y":'+offset+', "smoothness":20}');
                }

                var ctaParallax = function() {
                    if (!mkdf.htmlEl.hasClass('touch')) {
                        parallaxInstance(contentHolder, -120);
                        parallaxInstance(mainImage, 100);
                        parallaxInstance(additionalImage, -40);

                        setTimeout(function(){
                            ParallaxScroll.init(); //initialzation removed from plugin js file to have it run only on non-touch devices
                        }, 100); //wait for calcs
                    }
                }

                parallaxCTA.waitForImages({
                    finished: function() {
                        ctaParallax();
                    },
                    waitForAll: true
                });
            });
        }
    }

    /*
    * Slide to content on scroll - one scroll to page content
    */
    function mkdfScrollToContent() {
        if (mkdf.body.hasClass('mkdf-scroll-to-content') && !mkdf.htmlEl.hasClass('touch')) {
            var sliderHolder = $('.mkdf-slider'),
                sliderHolderHeight = sliderHolder.height(),
                sliderHolderOffset = sliderHolder.offset().top,
                sliderArea = sliderHolderHeight - sliderHolderOffset,
                revSlider = sliderHolder.find('.rev_slider'),
                pageJump = false,
                normalScroll = true,
                set = false;

            var mkdfScrollHandler = function() {
                if  ($(window).scrollTop() < sliderArea) {
                    normalScroll = false;
                }

                function mkdfScrollTo() {
                    pageJump = true;
                    $('html, body').animate({
                        scrollTop: sliderArea
                    }, 1000, 'easeInOutQuint', function() {
                        pageJump = false;
                        normalScroll = true;
                    });
                }

                window.addEventListener('wheel', function(event) {
                    var scroll = event.deltaY,
                        scrollingForward = false,
                        reInitOneScroll = false;

                    if (scroll > 0) {
                        scrollingForward = true;
                    } else {
                        scrollingForward = false;
                    }

                    if ($(window).scrollTop() - sliderHolderOffset <=  Math.round(sliderHolderHeight * 0.5)) {
                        reInitOneScroll = true;
                    }

                    if (!pageJump && !normalScroll) {
                        if (scrollingForward && ($(window).scrollTop() < sliderArea)) {
                            event.preventDefault();
                            mkdfScrollTo();
                        }
                    } else {
                        if (!normalScroll) {
                            event.preventDefault();
                        }

                        if (normalScroll && !scrollingForward && reInitOneScroll) {
                            pageJump = false;
                            normalScroll = false;
                            event.preventDefault();
                        }
                    }
                });

                //scrollbar click
                $(document).on('mousedown', function(event){
                    if( $(window).outerWidth() <= event.pageX ){
                        if ($(window).scrollTop() == sliderHolderOffset) {
                            event.preventDefault();
                            mkdfScrollTo();
                        }
                    }
                });
            }

            //prevent mousewheel scroll
            window.addEventListener('wheel', function (event) {
                if (!set) {
                    event.preventDefault();
                }
            });

            //prevent scrollbar scroll
            window.addEventListener('scroll', function () {
                if (!set) {
                    $(window).scrollTop(sliderHolderOffset);
                }
            })  

            //init
            if (revSlider.length) {
                revSlider.bind('revolution.slide.onchange', function(e, data) {
                    set = true;
                    mkdfScrollHandler();
                });
            } else {
                $(window).load(function(){
                    set = true;
                    mkdfScrollHandler();
                });   
            }
        }
    }
})(jQuery);