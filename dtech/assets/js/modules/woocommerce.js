(function($) {
    'use strict';

    var woocommerce = {};
    mkdf.modules.woocommerce = woocommerce;

    woocommerce.mkdfInitQuantityButtons = mkdfInitQuantityButtons;
    woocommerce.mkdfInitSelect2 = mkdfInitSelect2;

    woocommerce.mkdfOnDocumentReady = mkdfOnDocumentReady;
    woocommerce.mkdfOnWindowLoad = mkdfOnWindowLoad;
    woocommerce.mkdfOnWindowResize = mkdfOnWindowResize;
    woocommerce.mkdfOnWindowScroll = mkdfOnWindowScroll;

    $(document).ready(mkdfOnDocumentReady);
    $(window).load(mkdfOnWindowLoad);
    $(window).resize(mkdfOnWindowResize);
    $(window).scroll(mkdfOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function mkdfOnDocumentReady() {
        mkdfInitQuantityButtons();
        mkdfInitButtonLoading();
        mkdfInitSelect2();
        mkdfInitSingleProductImageSwitch();
        mkdfInitRelatedProducts();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function mkdfOnWindowLoad() {

    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function mkdfOnWindowResize() {

    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function mkdfOnWindowScroll() {

    }
    

    function mkdfInitQuantityButtons() {

        $(document).on('click', '.mkdf-quantity-minus, .mkdf-quantity-plus', function(e) {
            e.stopPropagation();

            var button = $(this),
                inputField = button.parent().siblings('.mkdf-quantity-input'),
                step = parseFloat(inputField.data('step')),
                max = parseFloat(inputField.data('max')),
                minus = false,
                inputValue = parseFloat(inputField.val()),
                newInputValue;

            if (button.hasClass('mkdf-quantity-minus')) {
                minus = true;
            }

            if (minus) {
                newInputValue = inputValue - step;
                if (newInputValue >= 1) {
                    inputField.val(newInputValue);
                } else {
                    inputField.val(0);
                }
            } else {
                newInputValue = inputValue + step;
                if ( max === undefined ) {
                    inputField.val(newInputValue);
                } else {
                    if ( newInputValue >= max ) {
                        inputField.val(max);
                    } else {
                        inputField.val(newInputValue);
                    }
                }
            }
            inputField.trigger('change');

        });

    }

    function mkdfInitButtonLoading() {

        $(".add_to_cart_button").click(function(){
            $(this).children(".mkdf-btn-text").text(mkdfGlobalVars.vars.mkdfAddingToCart);
        });

    }

    function mkdfInitSelect2() {

        if ($('.woocommerce-ordering .orderby').length ||  $('#calc_shipping_country').length ) {

            $('.woocommerce-ordering .orderby').select2({
                minimumResultsForSearch: Infinity
            });

            $('#calc_shipping_country, .dropdown_product_cat, .dropdown_layered_nav_color').select2();

        }

    }

    /*
    ** Init switch image logic for thumbnail and featured images on product single page
    */
    function mkdfInitSingleProductImageSwitch() {
            
        var thumbnailImage = $('.mkdf-single-product-wrapper-top .images .thumbnails a'),
            mainImageLink = $('.mkdf-single-product-wrapper-top .images .wp-post-image').parent(),
            mainImage = mainImageLink.parent();

        if(mainImage.length) {
            mainImage.on('click', function(e) {
                e.preventDefault();
                if(mainImage.children('.mkdf-fake-featured-image').length){
                    $('.mkdf-fake-featured-image').stop().animate({'opacity': '0'}, 100, function() {
                        $(this).remove();
                    });
                    mainImage.find('a img').css('opacity', '1');
                }             
            });
        }

        console.log(thumbnailImage);

        if(thumbnailImage.length) {
            thumbnailImage.each(function(){
                var thisThumbnailImage = $(this),
                    thisThumbnailImageSrc = thisThumbnailImage.attr('href');

                console.log('dvojka');

                thisThumbnailImage.on('click', function(e) {
                    console.log(thisThumbnailImageSrc);
                    console.log(mainImage);
                    e.preventDefault();
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    if(thisThumbnailImageSrc !== '' && mainImage !== '') {
                        mainImage.append('<img itemprop="image" class="mkdf-fake-featured-image" src="'+thisThumbnailImageSrc+'" />');
                        if(mainImage.children('.mkdf-fake-featured-image').length > 1){
                            $('.mkdf-fake-featured-image').first().remove();
                        }
                        mainImage.find('a img').css('opacity', '0');
                    }
                });
            });
        }
    }

	function mkdfInitRelatedProducts() {
		var relatedProducts = $('.related.products');

		if (relatedProducts.length){
			var productList = relatedProducts.find('ul.products'),
				prevArrow = relatedProducts.find('.mkdf-related-prev'),
				nextArrow = relatedProducts.find('.mkdf-related-next'),
				prevSlick, nextSlick;

			var responsive = [
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						infinite: true
					}
				},
				{
					breakpoint: 601,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			];

			productList.slick({
				infinite: true,
				slidesToShow : 3,
				arrows: true,
				dots: false,
				dotsClass: 'mkdf-slick-dots',
				adaptiveHeight: true,
				prevArrow: '<span class="mkdf-slick-prev mkdf-prev-icon"></span>',
				nextArrow: '<span class="mkdf-slick-next mkdf-next-icon"></span>',
				responsive: responsive
			});

			prevSlick = relatedProducts.find('.mkdf-slick-prev');
			nextSlick = relatedProducts.find('.mkdf-slick-next');


			prevArrow.click(function(){
				prevSlick.trigger("click");
			});

			nextArrow.click(function(){
				nextSlick.trigger("click");
			});
		}
	}

})(jQuery);