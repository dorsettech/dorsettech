<?php
if(!function_exists('cortex_mikado_design_styles')) {
    /**
     * Generates general custom styles
     */
    function cortex_mikado_design_styles() {

        $preload_background_styles = array();

        if(cortex_mikado_options()->getOptionValue('preload_pattern_image') !== ""){
            $preload_background_styles['background-image'] = 'url('.cortex_mikado_options()->getOptionValue('preload_pattern_image').') !important';
        }else{
            $preload_background_styles['background-image'] = 'url('.esc_url(MIKADO_ASSETS_ROOT."/img/preload_pattern.png").') !important';
        }

        echo cortex_mikado_dynamic_css('.mkdf-preload-background', $preload_background_styles);

		if (cortex_mikado_options()->getOptionValue('google_fonts')){
			$font_family = cortex_mikado_options()->getOptionValue('google_fonts');
			if(cortex_mikado_is_font_option_valid($font_family)) {
				echo cortex_mikado_dynamic_css('body', array('font-family' => cortex_mikado_get_font_option_val($font_family)));
			}
		}

		if (cortex_mikado_options()->getOptionValue('google_fonts_second')){
			$second_font_family = cortex_mikado_options()->getOptionValue('google_fonts_second');
			$second_font_selector = array(
				'h1',
				'h2',
				'h3',
				'h4',
				'h5',
				'h6',
				'.mkdf-comment-holder .mkdf-comment-text .replay',
				'.mkdf-comment-holder .mkdf-comment-text .comment-reply-link',
				'.mkdf-comment-holder .mkdf-comment-text .comment-edit-link',
				'.mkdf-comment-holder .mkdf-comment-text .mkdf-comment-date',
				'.wpcf7-form-control.wpcf7-text',
				'.wpcf7-form-control.wpcf7-number',
				'.wpcf7-form-control.wpcf7-date',
				'.wpcf7-form-control.wpcf7-textarea',
				'.wpcf7-form-control.wpcf7-select',
				'.wpcf7-form-control.wpcf7-quiz',
				'.select2-container--default .select2-search--inline .select2-search__field',
				'#respond textarea',
				'#respond input[type="text"]',
				'.post-password-form input[type="password"]',
				'.slick-slider .mkdf-slick-numbers li',
				'.mkdf-404-text',
				'.mkdf-main-menu > ul > li > a',
				'.mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner>ul>li>a',
				'.mkdf-header-vertical .mkdf-vertical-menu > ul > li > a',
				'.mkdf-mobile-header .mkdf-mobile-nav a',
				'.mkdf-mobile-header .mkdf-mobile-nav h4',
				'.mkdf-title .mkdf-title-holder h1',
				'nav.mkdf-fullscreen-menu ul li a',
				'.mkdf-search-cover input',
				'.mkdf-search-cover input:focus',
				'.mkdf-counter-holder .mkdf-counter',
				'.countdown-section',
				'.mkdf-message .mkdf-message-inner .mkdf-message-text .mkdf-message-text-inner',
				'.mkdf-testimonials.mkdf-testimonials-type-cortex .mkdf-testimonial-text',
				'.mkdf-testimonials.mkdf-testimonials-type-cortex .mkdf-testimonials-quotes',
				'.mkdf-price-table .mkdf-price-table-inner ul li.mkdf-table-prices .mkdf-price-holder',
				'.mkdf-pie-chart-holder .mkdf-to-counter',
				'.mkdf-pie-chart-holder .mkdf-percent-sign',
				'.mkdf-pie-chart-doughnut-holder .mkdf-pie-legend ul li p',
				'.mkdf-process-holder .mkdf-process-item-holder .mkdf-pi-number-holder .mkdf-pi-number',
				'.mkdf-tabs .mkdf-tabs-nav li a',
				'.mkdf-blog-list-holder .mkdf-item-info-section .mkdf-post-info-date',
				'.mkdf-blog-slider .mkdf-item-info-section .mkdf-post-info-date',
				'.mkdf-btn',
				'blockquote .mkdf-blockquote-text',
				'.mkdf-portfolio-filter-holder .mkdf-portfolio-filter-holder-inner ul li span',
				'.mkdf-social-share-holder.mkdf-list .mkdf-social-share-title',
				'.mkdf-section-title',
				'.mkdf-image-with-text span.mkdf-iwt-title',
				'.mkdf-sidebar .widget.widget_categories li',
				'.mkdf-sidebar .widget.widget_archive li',
				'.mkdf-sidebar .widget a',
				'.mkdf-blog-holder article .mkdf-post-info-bottom .mkdf-post-info-bottom-left > div',
				'.mkdf-blog-holder article .mkdf-post-info-bottom .mkdf-post-info-bottom-right > div',
				'.mkdf-blog-holder article .mkdf-post-mark .mkdf-quote-mark-inner',
				'.mkdf-blog-single-navigation .mkdf-nav-holder .mkdf-nav-text',
				'.woocommerce .amount',
				'.mkdf-woocommerce-page .amount',
				'.woocommerce .woocommerce-result-count',
				'.mkdf-woocommerce-page .woocommerce-result-count',
				'.woocommerce .added_to_cart',
				'.mkdf-woocommerce-page .added_to_cart',
				'.mkdf-single-product-summary .product_meta > span .mkdf-info-meta-title',
				'.woocommerce-account input[type=submit]',
				'.woocommerce-checkout input[type=submit]',
				'.mkdf-woocommerce-page input[type="text"]',
				'.mkdf-woocommerce-page input[type="email"]',
				'.mkdf-woocommerce-page input[type="tel"]',
				'.mkdf-woocommerce-page input[type="password"]',
				'.mkdf-woocommerce-page textarea',
				'.mkdf-shopping-cart-outer .mkdf-cart-amount',
				'.mkdf-shopping-cart-dropdown',
				'.mkdf-shopping-cart-dropdown .mkdf-cart-bottom .mkdf-subtotal-holder',
				'.mkdf-woocommerce-page .select2-container .select2-choice',
				'.woocommerce.widget input[type=submit]',
				'.woocommerce.widget button',
				'.woocommerce.widget.widget_product_categories li',
				'.mkdf-top-bar .widget'
			);
			
			if(cortex_mikado_is_font_option_valid($second_font_family)) {
				echo cortex_mikado_dynamic_css($second_font_selector, array('font-family' => cortex_mikado_get_font_option_val($second_font_family)));
			}
		}

        if(cortex_mikado_options()->getOptionValue('first_color') !== "") {
            $color_selector = array(
                'a',
                'h1 a:hover',
                'h2 a:hover',
                'h3 a:hover',
                'h4 a:hover',
                'h5 a:hover',
                'h6 a:hover',
                'p a',
                '.mkdf-comment-holder .mkdf-comment-text .comment-edit-link:hover',
                '.mkdf-comment-holder .mkdf-comment-text .comment-reply-link:hover',
                '.mkdf-comment-holder .mkdf-comment-text .replay:hover',
                '.mkdf-pagination-holder .mkdf-pagination li a',
                '.mkdf-pagination-holder .mkdf-pagination li.active span',
                '.slick-slider .mkdf-slick-numbers li.slick-active',
                '.mkdf-404-text',
                '.mkdf-mobile-header .mkdf-mobile-nav a:hover',
                '.mkdf-mobile-header .mkdf-mobile-nav h4:hover',
                '.mkdf-mobile-header .mkdf-mobile-menu-opener a:hover',
                '.mkdf-page-header .mkdf-sticky-header .mkdf-fullscreen-menu-opener .mkdf-fullscreen-icon',
                'footer .widget.widget_calendar #today',
                '.mkdf-title .mkdf-title-holder .mkdf-breadcrumbs a',
                '.mkdf-title .mkdf-title-holder .mkdf-breadcrumbs span',
                '.mkdf-side-menu-button-opener',
                '.mkdf-side-menu .widget a:hover',
                '.mkdf-side-menu .widget_calendar a',
                '.mkdf-side-menu a.mkdf-close-side-menu span',
                '.mkdf-fullscreen-menu-opener .mkdf-fullscreen-icon',
                '.mkdf-fullscreen-menu-opener.opened .mkdf-fullscreen-icon-close',
                '.mkdf-search-opener',
                '.mkdf-search-cover .mkdf-search-close a:hover',
                '.mkdf-fullscreen-search-holder .mkdf-search-submit',
                '.mkdf-portfolio-single-holder .mkdf-portfolio-related-holder .mkdf-portfolio-back-btn a:hover',
                '.mkdf-team .mkdf-team-social .mkdf-icon-shortcode:hover',
                '.mkdf-counter-holder .mkdf-counter-icon .mkdf-icon-shortcode',
                '.mkdf-message .mkdf-message-inner a.mkdf-close',
                '.mkdf-ordered-list ol>li:before',
                '.mkdf-testimonials.mkdf-testimonials-type-standard .mkdf-testimonial-author-text',
                '.mkdf-testimonials.mkdf-testimonials-type-cortex .mkdf-testimonial-author',
                '.mkdf-testimonials.mkdf-testimonials-type-cortex .mkdf-testimonials-quotes',
                '.mkdf-process-holder .mkdf-process-item-holder .mkdf-pi-number-holder .mkdf-pi-arrow',
                '.mkdf-process-holder .mkdf-process-item-holder:hover .mkdf-pi-number',
                '.mkdf-blog-list-holder .mkdf-item-info-section>div',
                '.mkdf-blog-list-holder .mkdf-item-info-section>div a:hover',
                '.mkdf-blog-list-holder .mkdf-post-info-author',
                '.mkdf-blog-list-holder.mkdf-boxes .mkdf-blog-list-item-table-cell .mkdf-section-title',
                '.mkdf-blog-slider .mkdf-item-info-section>div',
                '.mkdf-blog-slider .mkdf-item-info-section>div a:hover',
                '.mkdf-blog-slider .mkdf-post-info-author',
                'blockquote .mkdf-icon-quotations-holder',
                '.mkdf-video-button-play .mkdf-video-button-wrapper',
                '.mkdf-dropcaps',
                '.mkdf-portfolio-list-holder-outer.mkdf-ptf-hover-zoom-lightbox article .mkdf-item-text-holder span',
                '.mkdf-iwt .mkdf-icon-shortcode',
                '.mkdf-portfolio-filter-holder .mkdf-portfolio-filter-holder-inner ul li.active span',
                '.mkdf-portfolio-filter-holder .mkdf-portfolio-filter-holder-inner ul li.current span',
                '.mkdf-portfolio-filter-holder .mkdf-portfolio-filter-holder-inner ul li:hover span',
                '.mkdf-social-share-holder.mkdf-list li a',
                '.mkdf-section-title .mkdf-section-highlighted',
                '.mkdf-twitter-feed .mkdf-tweet-info-holder .mkdf-tweeter-username',
                '.mkdf-banner:hover .mkdf-banner-info .mkdf-banner-title',
                '.mkdf-parallax-call-to-action',
                '.widget .mkdf-search-wrapper input[type=submit]',
                '.widget .tagcloud a:hover',
                '.widget.widget_calendar #next a',
                '.widget.widget_calendar #prev a',
                '.widget.widget_calendar td a',
                '.mkdf-sidebar .mkdf-widget-title',
                '.mkdf-sidebar .widget.widget_search input[type=text]',
                '.mkdf-sidebar .widget.widget_search input[type=text]:-moz-placeholder',
                '.mkdf-sidebar .widget.widget_search input[type=text]::-moz-placeholder',
                '.mkdf-sidebar .widget.widget_search input[type=text]:-ms-input-placeholder',
                '.mkdf-sidebar .widget.widget_search input[type=text]::-webkit-input-placeholder',
                '.mkdf-sidebar .widget.widget_categories li a:hover',
                '.mkdf-sidebar .widget.widget_rss .mkdf-widget-title a',
                '.mkdf-sidebar .widget.widget_archive li a:hover',
                '.mkdf-woocommerce-page .mkdf-product-cat a:hover',
                '.woocommerce .mkdf-product-cat a:hover',
                '.woocommerce-pagination .page-numbers',
                '.mkdf-single-product-related-products-holder .products .mkdf-related-glob:hover a',
                '.mkdf-single-product-wrapper-top .out-of-stock',
                '.mkdf-single-product-summary .product_meta>span a:hover',
                '.summary .group_table td.label a',
                '.woocommerce-account .woocommerce-MyAccount-navigation .woocommerce-MyAccount-navigation-link.is-active a',
                '.woocommerce-account .woocommerce-MyAccount-navigation .woocommerce-MyAccount-navigation-link:hover a',
                '.mkdf-shopping-cart-outer .mkdf-shopping-cart-header .mkdf-cart-icon',
                '.mkdf-shopping-cart-dropdown ul li a:hover',
                '.mkdf-shopping-cart-dropdown .mkdf-item-info-holder .mkdf-item-left:hover',
                '.mkdf-shopping-cart-dropdown .mkdf-cart-bottom .checkout span',
                '.mkdf-shopping-cart-dropdown .mkdf-cart-bottom .view-cart span',
                '.mkdf-sticky-header .mkdf-shopping-cart-outer .mkdf-shopping-cart-header .mkdf-cart-icon',
                '.woocommerce.widget.widget_product_categories li a:hover',
                '.product_list_widget li .product-title',
            );

            $color_important_selector = array(
                '.mkdf-dark-header .mkdf-page-header>div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon',
                '.mkdf-dark-header .mkdf-page-header>div:not(.mkdf-sticky-header) .mkdf-side-menu-button-opener',
                '.mkdf-dark-header .mkdf-top-bar .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon',
                '.mkdf-dark-header .mkdf-top-bar .mkdf-side-menu-button-opener',
                '.mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon',
                '.mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-side-menu-button-opener',
            );

            $background_color_selector = array(
                '.mkdf-st-loader .pulse',
                '.mkdf-st-loader .double_pulse .double-bounce1',
                '.mkdf-st-loader .double_pulse .double-bounce2',
                '.mkdf-st-loader .cube',
                '.mkdf-st-loader .rotating_cubes .cube1',
                '.mkdf-st-loader .rotating_cubes .cube2',
                '.mkdf-st-loader .stripes>div',
                '.mkdf-st-loader .wave>div',
                '.mkdf-st-loader .two_rotating_circles .dot1',
                '.mkdf-st-loader .two_rotating_circles .dot2',
                '.mkdf-st-loader .five_rotating_circles .container1>div',
                '.mkdf-st-loader .five_rotating_circles .container2>div',
                '.mkdf-st-loader .five_rotating_circles .container3>div',
                '.mkdf-st-loader .atom .ball-1:before',
                '.mkdf-st-loader .atom .ball-2:before',
                '.mkdf-st-loader .atom .ball-3:before',
                '.mkdf-st-loader .atom .ball-4:before',
                '.mkdf-st-loader .clock .ball:before',
                '.mkdf-st-loader .mitosis .ball',
                '.mkdf-st-loader .lines .line1',
                '.mkdf-st-loader .lines .line2',
                '.mkdf-st-loader .lines .line3',
                '.mkdf-st-loader .lines .line4',
                '.mkdf-st-loader .fussion .ball',
                '.mkdf-st-loader .fussion .ball-1',
                '.mkdf-st-loader .fussion .ball-2',
                '.mkdf-st-loader .fussion .ball-3',
                '.mkdf-st-loader .fussion .ball-4',
                '.mkdf-st-loader .wave_circles .ball',
                '.mkdf-st-loader .pulse_circles .ball',
                '#submit_comment:hover',
                '.post-password-form input[type=submit]:hover',
                'input.wpcf7-form-control.wpcf7-submit:hover',
                '.slick-slider .mkdf-slick-dots li.slick-active',
                '.flex-control-paging.flex-control-nav li a .flex-active',
                '.flex-control-paging.flex-control-nav li a:hover',
                '#mkdf-back-to-top>.mkdf-btt-top-shadow',
                '.mkdf-main-menu>ul>li>a span.mkdf-item-inner .mkdf-item-text:before',
                '.mkdf-light-header .mkdf-page-header>div:not(.mkdf-sticky-header) .mkdf-main-menu>ul>li>a span.mkdf-item-inner .mkdf-item-text:before',
                '.mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu>ul>li>a span.mkdf-item-inner .mkdf-item-text:before',
                '.mkdf-header-vertical .mkdf-vertical-menu>ul>li>a:after',
                'footer .mkdf-footer-ingrid-border-holder-outer',
                '.mkdf-team.main-info-on-hover .mkdf-team-title-holder-inner .mkdf-team-name:after',
                '.mkdf-icon-shortcode.circle',
                '.mkdf-icon-shortcode.square',
                '.mkdf-message',
                '.mkdf-progress-bar .mkdf-progress-content-outer .mkdf-progress-content',
                '.mkdf-progress-bar.mkdf-progress-bar-light .mkdf-progress-content-outer .mkdf-progress-content',
                '.mkdf-price-table .mkdf-price-table-inner ul li.mkdf-table-title',
                '.mkdf-pie-chart-doughnut-holder .mkdf-pie-legend ul li .mkdf-pie-color-holder',
                '.mkdf-pie-chart-pie-holder .mkdf-pie-legend ul li .mkdf-pie-color-holder',
                '.mkdf-tabs .mkdf-tabs-nav li.ui-state-active:after',
                '.mkdf-tabs .mkdf-tabs-nav li.ui-state-hover:after',
                '.mkdf-tabs.mkdf-tab-boxed .mkdf-tabs-nav li.ui-state-active a',
                '.mkdf-tabs.mkdf-tab-boxed .mkdf-tabs-nav li.ui-state-hover a',
                '.mkdf-tabs.mkdf-tab-boxed.mkdf-style-grey .mkdf-tabs-nav li.ui-state-active a',
                '.mkdf-tabs.mkdf-tab-boxed.mkdf-style-grey .mkdf-tabs-nav li.ui-state-hover a',
                '.mkdf-tabs.mkdf-tab-boxed.mkdf-style-white .mkdf-tabs-nav li.ui-state-active a',
                '.mkdf-tabs.mkdf-tab-boxed.mkdf-style-white .mkdf-tabs-nav li.ui-state-hover a',
                '.mkdf-accordion-holder.mkdf-style-grey .mkdf-title-holder.ui-state-active',
                '.mkdf-accordion-holder.mkdf-style-grey .mkdf-title-holder.ui-state-hover',
                '.mkdf-accordion-holder.mkdf-style-white .mkdf-title-holder.ui-state-active',
                '.mkdf-accordion-holder.mkdf-style-white .mkdf-title-holder.ui-state-hover',
                '.mkdf-btn.mkdf-btn-solid .mkdf-btn-top-shadow',
                '.mkdf-btn.mkdf-btn-solid.mkdf-btn-frst-clr',
                '.mkdf-btn.mkdf-btn-solid.mkdf-btn-solid-no-shadow:hover',
                '.mkdf-video-button-play .mkdf-video-button-wrapper:hover',
                '.mkdf-dropcaps.mkdf-circle',
                '.mkdf-dropcaps.mkdf-square',
                '.mkdf-portfolio-list-holder-outer.mkdf-ptf-hover-sweep-left article .mkdf-item-text-holder-inner .mkdf-item-title a:after',
                '.mkdf-woocommerce-page .product .mkdf-product-badge',
                '.woocommerce .product .mkdf-product-badge',
                '.woocommerce-account input[type=submit]:hover',
                '.woocommerce-checkout input[type=submit]:hover',
                '.mkdf-shopping-cart-outer .mkdf-cart-amount',
                '.mkdf-sticky-header .mkdf-shopping-cart-outer .mkdf-cart-amount',
                '.woocommerce.widget button',
                '.woocommerce.widget input[type=submit]',
                '.widget_price_filter .ui-slider .ui-slider-handle',
                '.mkdf-image-with-text.mkdf-iwt-layout-text-on-hover .mkdf-iwt-text .mkdf-iwt-title:after'
            );

            $background_color_important_selector = array(
                'input[type=submit].mkdf-btn.mkdf-btn-bckg-hover:hover',
            );


            $border_color_selector = array(
                '.mkdf-st-loader .pulse_circles .ball',
                '#submit_comment:hover',
                '.post-password-form input[type=submit]:hover',
                'input.wpcf7-form-control.wpcf7-submit:hover',
                '.slick-slider .mkdf-slick-dots li.slick-active',
                '.flex-control-paging.flex-control-nav li a',
                '.mkdf-message',
                '.mkdf-process-holder .mkdf-process-item-holder .mkdf-pi-number-holder:after',
                '.mkdf-video-button-play .mkdf-video-button-wrapper',
                '.woocommerce-account input[type=submit]:hover',
                '.woocommerce-checkout input[type=submit]:hover',
                '.woocommerce.widget button',
                '.woocommerce.widget input[type=submit]',
            );

            echo cortex_mikado_dynamic_css($color_selector, array('color' => cortex_mikado_options()->getOptionValue('first_color')));
            echo cortex_mikado_dynamic_css($color_important_selector, array('color' => cortex_mikado_options()->getOptionValue('first_color').'!important'));
            echo cortex_mikado_dynamic_css('::selection', array('background' => cortex_mikado_options()->getOptionValue('first_color')));
            echo cortex_mikado_dynamic_css('::-moz-selection', array('background' => cortex_mikado_options()->getOptionValue('first_color')));
            echo cortex_mikado_dynamic_css($background_color_selector, array('background-color' => cortex_mikado_options()->getOptionValue('first_color')));
            echo cortex_mikado_dynamic_css($background_color_important_selector, array('background-color' => cortex_mikado_options()->getOptionValue('first_color').'!important'));
            echo cortex_mikado_dynamic_css($border_color_selector, array('border-color' => cortex_mikado_options()->getOptionValue('first_color')));
        }

        if(cortex_mikado_options()->getOptionValue('second_color') !== "") {
            $color_selector = array(
                '.mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li a:not(.mkdf-image-widget-link):hover',
                '.mkdf-header-vertical .mkdf-vertical-dropdown-float .mkdf-menu-second .mkdf-menu-inner ul li a:hover',
                '.mkdf-side-menu h4',
                '.mkdf-side-menu h5',
                '.mkdf-side-menu h6',
                '.mkdf-side-menu .widget .mkdf-sidearea-widget-title',
                'nav.mkdf-fullscreen-menu ul li a:hover',
                'nav.mkdf-fullscreen-menu ul li ul li a:hover',
                '.mkdf-custom-font-holder .mkdf-highlighted',
                '.mkdf-unordered-list ul>li:before',
                '.mkdf-btn.mkdf-btn-transparent .mkdf-btn-icon-holder',
                '.mkdf-carousel-holder .mkdf-slick-next',
                '.mkdf-carousel-holder .mkdf-slick-prev',
                '.mkdf-sidebar .widget.widget_recent_entries ul li a:before',
                '.mkdf-twitter-widget .mkdf-tweet-icon',
            );

            $background_color_selector = array(
                '.slick-slider .mkdf-slick-dots li',
                '#mkdf-back-to-top>.mkdf-btt-bottom-shadow',
                '.mkdf-dark-header .mkdf-page-header>div:not(.mkdf-sticky-header) .mkdf-main-menu>ul>li>a span.mkdf-item-inner .mkdf-item-text:before',
                '.mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu>ul>li>a span.mkdf-item-inner .mkdf-item-text:before',
                '.mkdf-header-vertical .mkdf-vertical-menu>ul>li>a .mkdf-item-text:before',
                '.mkdf-icon-shortcode.circle:hover',
                '.mkdf-icon-shortcode.square:hover',
                '.mkdf-accordion-holder .mkdf-title-holder',
                '.mkdf-btn.mkdf-btn-solid .mkdf-btn-bottom-shadow',
            );

            $background_color_important_selector = array(
                '.mkdf-btn.mkdf-btn-solid.mkdf-btn-frst-clr.mkdf-btn-bckg-hover:hover',
            );

            $border_color_selector = array(
                '.slick-slider .mkdf-slick-dots li',
                'footer .widget.widget_nav_menu ul li a:before',
                '.mkdf-side-menu .widget_nav_menu ul li a:before',
            );

            echo cortex_mikado_dynamic_css($color_selector, array('color' => cortex_mikado_options()->getOptionValue('second_color')));
            echo cortex_mikado_dynamic_css($background_color_selector, array('background-color' => cortex_mikado_options()->getOptionValue('second_color')));
            echo cortex_mikado_dynamic_css($background_color_important_selector, array('background-color' => cortex_mikado_options()->getOptionValue('second_color').'!important'));
            echo cortex_mikado_dynamic_css($border_color_selector, array('border-color' => cortex_mikado_options()->getOptionValue('second_color')));
        }

        if(cortex_mikado_options()->getOptionValue('third_color') !== "") {
            $color_selector = array(
                'body',
                'h1',
                'h2',
                'h3',
                'h4',
                'h5',
                'h6',
                '.mkdf-comment-holder .mkdf-comment-text .comment-edit-link',
                '.mkdf-comment-holder .mkdf-comment-text .comment-reply-link',
                '.mkdf-comment-holder .mkdf-comment-text .replay',
                '#respond input[type=text]',
                '#respond textarea',
                '.post-password-form input[type=password]',
                '.wpcf7-form-control.wpcf7-date',
                '.wpcf7-form-control.wpcf7-number',
                '.wpcf7-form-control.wpcf7-quiz',
                '.wpcf7-form-control.wpcf7-select',
                '.wpcf7-form-control.wpcf7-text',
                '.wpcf7-form-control.wpcf7-textarea',
                '#respond input[type=text]:-moz-placeholder',
                '#respond textarea:-moz-placeholder',
                '.post-password-form input[type=password]:-moz-placeholder',
                '.wpcf7-form-control.wpcf7-date:-moz-placeholder',
                '.wpcf7-form-control.wpcf7-number:-moz-placeholder',
                '.wpcf7-form-control.wpcf7-quiz:-moz-placeholder',
                '.wpcf7-form-control.wpcf7-select:-moz-placeholder',
                '.wpcf7-form-control.wpcf7-text:-moz-placeholder',
                '.wpcf7-form-control.wpcf7-textarea:-moz-placeholder',
                '#respond input[type=text]::-moz-placeholder',
                '#respond textarea::-moz-placeholder',
                '.post-password-form input[type=password]::-moz-placeholder',
                '.wpcf7-form-control.wpcf7-date::-moz-placeholder',
                '.wpcf7-form-control.wpcf7-number::-moz-placeholder',
                '.wpcf7-form-control.wpcf7-quiz::-moz-placeholder',
                '.wpcf7-form-control.wpcf7-select::-moz-placeholder',
                '.wpcf7-form-control.wpcf7-text::-moz-placeholder',
                '.wpcf7-form-control.wpcf7-textarea::-moz-placeholder',
                '#respond input[type=text]:-ms-input-placeholder',
                '#respond textarea:-ms-input-placeholder',
                '.post-password-form input[type=password]:-ms-input-placeholder',
                '.wpcf7-form-control.wpcf7-date:-ms-input-placeholder',
                '.wpcf7-form-control.wpcf7-number:-ms-input-placeholder',
                '.wpcf7-form-control.wpcf7-quiz:-ms-input-placeholder',
                '.wpcf7-form-control.wpcf7-select:-ms-input-placeholder',
                '.wpcf7-form-control.wpcf7-text:-ms-input-placeholder',
                '.wpcf7-form-control.wpcf7-textarea:-ms-input-placeholder',
                '#respond input[type=text]::-webkit-input-placeholder',
                '#respond textarea::-webkit-input-placeholder',
                '.post-password-form input[type=password]::-webkit-input-placeholder',
                '.wpcf7-form-control.wpcf7-date::-webkit-input-placeholder',
                '.wpcf7-form-control.wpcf7-number::-webkit-input-placeholder',
                '.wpcf7-form-control.wpcf7-quiz::-webkit-input-placeholder',
                '.wpcf7-form-control.wpcf7-select::-webkit-input-placeholder',
                '.wpcf7-form-control.wpcf7-text::-webkit-input-placeholder',
                '.wpcf7-form-control.wpcf7-textarea::-webkit-input-placeholder',
                '.mkdf-main-menu ul li a',
                '.mkdf-main-menu.mkdf-sticky-nav>ul>li>a',
                '.mkdf-header-vertical .mkdf-vertical-menu>ul>li>a',
                'footer .widget input[type=text]',
                'footer .widget select',
                '.mkdf-portfolio-single-holder .mkdf-portfolio-related-holder .mkdf-portfolio-back-btn a',
                '.mkdf-team .mkdf-team-social .mkdf-icon-shortcode',
                '.mkdf-team.main-info-below-image .mkdf-team-text p',
                '.mkdf-icon-list-item .mkdf-icon-list-icon-holder-inner i',
                '.mkdf-icon-list-item .mkdf-icon-list-icon-holder-inner span',
                '.mkdf-testimonials .mkdf-slick-next',
                '.mkdf-testimonials .mkdf-slick-prev',
                '.mkdf-testimonials.mkdf-testimonials-type-cortex .mkdf-testimonial-text',
                '.mkdf-price-table .mkdf-price-table-inner ul li.mkdf-table-prices .mkdf-price-holder',
                '.mkdf-pie-chart-holder .mkdf-percent-sign',
                '.mkdf-pie-chart-holder .mkdf-to-counter',
                '.mkdf-pie-chart-with-icon-holder .mkdf-percentage-with-icon i',
                '.mkdf-pie-chart-with-icon-holder .mkdf-percentage-with-icon span',
                '.mkdf-process-holder .mkdf-process-item-holder .mkdf-pi-number-holder .mkdf-pi-number',
                '.mkdf-tabs .mkdf-tabs-nav li.ui-state-active a',
                '.mkdf-tabs .mkdf-tabs-nav li.ui-state-hover a',
                '.mkdf-portfolio-filter-holder .mkdf-portfolio-filter-holder-inner ul li span',
                '.mkdf-social-share-holder.mkdf-list li a:hover',
                '.mkdf-section-subtitle',
                '.mkdf-section-title',
                '.widget .mkdf-search-wrapper input[type=text]',
                '.widget .mkdf-search-wrapper input[type=text]:-moz-placeholder',
                '.widget .mkdf-search-wrapper input[type=text]::-moz-placeholder',
                '.widget .mkdf-search-wrapper input[type=text]:-ms-input-placeholder',
                '.widget .mkdf-search-wrapper input[type=text]::-webkit-input-placeholder',
                '.widget .tagcloud a',
                '.mkdf-woocommerce-page .mkdf-product-list-title-holder .price',
                '.woocommerce .mkdf-product-list-title-holder .price',
                '.mkdf-single-product-summary .mkdf-social-share-holder .mkdf-social-share-title',
                '.mkdf-woocommerce-page .woocommerce-error',
                '.mkdf-woocommerce-page .woocommerce-info',
                '.mkdf-woocommerce-page .woocommerce-message',
                '.mkdf-woocommerce-page input[type=email]',
                '.mkdf-woocommerce-page input[type=tel]',
                '.mkdf-woocommerce-page input[type=password]',
                '.mkdf-woocommerce-page input[type=text]',
                '.mkdf-woocommerce-page textarea',
                '.mkdf-woocommerce-page input[type=email]:-moz-placeholder',
                '.mkdf-woocommerce-page input[type=tel]:-moz-placeholder',
                '.mkdf-woocommerce-page input[type=password]:-moz-placeholder',
                '.mkdf-woocommerce-page input[type=text]:-moz-placeholder',
                '.mkdf-woocommerce-page textarea:-moz-placeholder',
                '.mkdf-woocommerce-page input[type=email]::-moz-placeholder',
                '.mkdf-woocommerce-page input[type=tel]::-moz-placeholder',
                '.mkdf-woocommerce-page input[type=password]::-moz-placeholder',
                '.mkdf-woocommerce-page input[type=text]::-moz-placeholder',
                '.mkdf-woocommerce-page textarea::-moz-placeholder',
                '.mkdf-woocommerce-page input[type=email]:-ms-input-placeholder',
                '.mkdf-woocommerce-page input[type=tel]:-ms-input-placeholder',
                '.mkdf-woocommerce-page input[type=password]:-ms-input-placeholder',
                '.mkdf-woocommerce-page input[type=text]:-ms-input-placeholder',
                '.mkdf-woocommerce-page textarea:-ms-input-placeholder',
                '.mkdf-woocommerce-page input[type=email]::-webkit-input-placeholder',
                '.mkdf-woocommerce-page input[type=tel]::-webkit-input-placeholder',
                '.mkdf-woocommerce-page input[type=password]::-webkit-input-placeholder',
                '.mkdf-woocommerce-page input[type=text]::-webkit-input-placeholder',
                '.mkdf-woocommerce-page textarea::-webkit-input-placeholder',
                '.select2-container--default.select2-container--open .select2-selection--single',
                '.select2-container--default .select2-results__option[aria-disabled=true]',
                '.select2-container--default .select2-results__option[aria-selected=true]',
                '.select2-container--default .select2-results__option--highlighted[aria-selected]',
                '.mkdf-woocommerce-page .select2-container .select2-choice',
                '.mkdf-woocommerce-page .select2-container .select2-choice:-moz-placeholder',
                '.mkdf-woocommerce-page .select2-container .select2-choice::-moz-placeholder',
                '.mkdf-woocommerce-page .select2-container .select2-choice:-ms-input-placeholder',
                '.mkdf-woocommerce-page .select2-container .select2-choice::-webkit-input-placeholder',
                '.mkdf-woocommerce-page .select2.select2-container--default',
                '.mkdf-woocommerce-page .select2.select2-container--default:-moz-placeholder',
                '.mkdf-woocommerce-page .select2.select2-container--default::-moz-placeholder',
                '.mkdf-woocommerce-page .select2.select2-container--default:-ms-input-placeholder',
                '.mkdf-woocommerce-page .select2.select2-container--default::-webkit-input-placeholder',
                '.woocommerce.widget input[type=search]',
                '.woocommerce.widget input[type=search]:-moz-placeholder',
                '.woocommerce.widget input[type=search]::-moz-placeholder',
                '.woocommerce.widget input[type=search]:-ms-input-placeholder',
                '.woocommerce.widget input[type=search]::-webkit-input-placeholder',
                '.product_list_widget li .star-rating span',
            );

            $background_color_selector = array(
                '#submit_comment',
                '.post-password-form input[type=submit]',
                'input.wpcf7-form-control.wpcf7-submit',
                '#mkdf-back-to-top>.mkdf-icon-stack',
                '.mkdf-drop-down .mkdf-menu-narrow .mkdf-menu-second .mkdf-menu-inner ul',
                '.mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner>ul',
                '.mkdf-drop-down .mkdf-menu-wide.mkdf-wide-background .mkdf-menu-second',
                '.mkdf-top-bar',
                '.mkdf-header-vertical .mkdf-vertical-dropdown-float .menu-item .mkdf-menu-second',
                '.mkdf-header-vertical .mkdf-vertical-dropdown-float .mkdf-menu-second .mkdf-menu-inner ul ul',
                'footer .mkdf-footer-top-holder',
                'footer .mkdf-footer-bottom-holder',
                '.mkdf-accordion-holder .mkdf-title-holder.ui-state-active',
                '.mkdf-accordion-holder .mkdf-title-holder.ui-state-hover',
                '.mkdf-btn.mkdf-btn-solid',
                'input[type=submit].mkdf-btn',
                '.mkdf-woocommerce-page .add_to_cart_button',
                '.mkdf-woocommerce-page .mkdf-view-product',
                '.woocommerce .add_to_cart_button',
                '.woocommerce .mkdf-view-product',
                '.mkdf-woocommerce-page .added_to_cart',
                '.woocommerce .added_to_cart',
                '.woocommerce-account input[type=submit]',
                '.woocommerce-checkout input[type=submit]',
                '.select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-selection__choice',
                '.woocommerce.widget button:hover',
                '.woocommerce.widget input[type=submit]:hover',
            );

            $border_color_selector = array(
                '#submit_comment',
                '.post-password-form input[type=submit]',
                'input.wpcf7-form-control.wpcf7-submit',
                '.woocommerce-account input[type=submit]',
                '.woocommerce-checkout input[type=submit]',
                '.woocommerce.widget button:hover',
                '.woocommerce.widget input[type=submit]:hover',
            );

            echo cortex_mikado_dynamic_css($color_selector, array('color' => cortex_mikado_options()->getOptionValue('third_color')));
            echo cortex_mikado_dynamic_css($background_color_selector, array('background-color' => cortex_mikado_options()->getOptionValue('third_color')));
            echo cortex_mikado_dynamic_css($border_color_selector, array('border-color' => cortex_mikado_options()->getOptionValue('third_color')));
        }

		if (cortex_mikado_options()->getOptionValue('page_background_color')) {
			$background_color_selector = array(
				'.mkdf-wrapper-inner',
				'.mkdf-content',
				'.mkdf-content-inner > .mkdf-container'
			);
			echo cortex_mikado_dynamic_css($background_color_selector, array('background-color' => cortex_mikado_options()->getOptionValue('page_background_color')));
		}

		if (cortex_mikado_options()->getOptionValue('selection_color')) {
			echo cortex_mikado_dynamic_css('::selection', array('background' => cortex_mikado_options()->getOptionValue('selection_color')));
			echo cortex_mikado_dynamic_css('::-moz-selection', array('background' => cortex_mikado_options()->getOptionValue('selection_color')));
		}

		$boxed_background_style = array();
		if (cortex_mikado_options()->getOptionValue('page_background_color_in_box')) {
			$boxed_background_style['background-color'] = cortex_mikado_options()->getOptionValue('page_background_color_in_box');
		}

		if (cortex_mikado_options()->getOptionValue('boxed_background_image')) {
			$boxed_background_style['background-image'] = 'url('.esc_url(cortex_mikado_options()->getOptionValue('boxed_background_image')).')';
			if(cortex_mikado_options()->getOptionValue('boxed_background_image_repeating') == 'yes') {
				$boxed_background_style['background-position'] = '0px 0px';
				$boxed_background_style['background-repeat'] = 'repeat';
			} else {
				$boxed_background_style['background-position'] = 'center 0px';
				$boxed_background_style['background-repeat'] = 'no-repeat';
			}
		}


		if (cortex_mikado_options()->getOptionValue('boxed_background_image_attachment')) {
			$boxed_background_style['background-attachment'] = (cortex_mikado_options()->getOptionValue('boxed_background_image_attachment'));
		}

		echo cortex_mikado_dynamic_css('.mkdf-boxed .mkdf-wrapper', $boxed_background_style);
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_design_styles');
}

if (!function_exists('cortex_mikado_h1_styles')) {

    function cortex_mikado_h1_styles() {

        $h1_styles = array();

        if(cortex_mikado_options()->getOptionValue('h1_color') !== '') {
            $h1_styles['color'] = cortex_mikado_options()->getOptionValue('h1_color');
        }
        if(cortex_mikado_options()->getOptionValue('h1_google_fonts') !== '-1') {
            $h1_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('h1_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('h1_fontsize') !== '') {
            $h1_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h1_fontsize')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h1_lineheight') !== '') {
            $h1_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h1_lineheight')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h1_texttransform') !== '') {
            $h1_styles['text-transform'] = cortex_mikado_options()->getOptionValue('h1_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('h1_fontstyle') !== '') {
            $h1_styles['font-style'] = cortex_mikado_options()->getOptionValue('h1_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('h1_fontweight') !== '') {
            $h1_styles['font-weight'] = cortex_mikado_options()->getOptionValue('h1_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('h1_letterspacing') !== '') {
            $h1_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h1_letterspacing')).'px';
        }

        $h1_selector = array(
            'h1'
        );

        if (!empty($h1_styles)) {
            echo cortex_mikado_dynamic_css($h1_selector, $h1_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_h1_styles');
}

if (!function_exists('cortex_mikado_h2_styles')) {

    function cortex_mikado_h2_styles() {

        $h2_styles = array();

        if(cortex_mikado_options()->getOptionValue('h2_color') !== '') {
            $h2_styles['color'] = cortex_mikado_options()->getOptionValue('h2_color');
        }
        if(cortex_mikado_options()->getOptionValue('h2_google_fonts') !== '-1') {
            $h2_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('h2_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('h2_fontsize') !== '') {
            $h2_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h2_fontsize')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h2_lineheight') !== '') {
            $h2_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h2_lineheight')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h2_texttransform') !== '') {
            $h2_styles['text-transform'] = cortex_mikado_options()->getOptionValue('h2_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('h2_fontstyle') !== '') {
            $h2_styles['font-style'] = cortex_mikado_options()->getOptionValue('h2_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('h2_fontweight') !== '') {
            $h2_styles['font-weight'] = cortex_mikado_options()->getOptionValue('h2_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('h2_letterspacing') !== '') {
            $h2_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h2_letterspacing')).'px';
        }

        $h2_selector = array(
            'h2'
        );

        if (!empty($h2_styles)) {
            echo cortex_mikado_dynamic_css($h2_selector, $h2_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_h2_styles');
}

if (!function_exists('cortex_mikado_h3_styles')) {

    function cortex_mikado_h3_styles() {

        $h3_styles = array();

        if(cortex_mikado_options()->getOptionValue('h3_color') !== '') {
            $h3_styles['color'] = cortex_mikado_options()->getOptionValue('h3_color');
        }
        if(cortex_mikado_options()->getOptionValue('h3_google_fonts') !== '-1') {
            $h3_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('h3_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('h3_fontsize') !== '') {
            $h3_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h3_fontsize')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h3_lineheight') !== '') {
            $h3_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h3_lineheight')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h3_texttransform') !== '') {
            $h3_styles['text-transform'] = cortex_mikado_options()->getOptionValue('h3_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('h3_fontstyle') !== '') {
            $h3_styles['font-style'] = cortex_mikado_options()->getOptionValue('h3_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('h3_fontweight') !== '') {
            $h3_styles['font-weight'] = cortex_mikado_options()->getOptionValue('h3_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('h3_letterspacing') !== '') {
            $h3_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h3_letterspacing')).'px';
        }

        $h3_selector = array(
            'h3'
        );

        if (!empty($h3_styles)) {
            echo cortex_mikado_dynamic_css($h3_selector, $h3_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_h3_styles');
}

if (!function_exists('cortex_mikado_h4_styles')) {

    function cortex_mikado_h4_styles() {

        $h4_styles = array();

        if(cortex_mikado_options()->getOptionValue('h4_color') !== '') {
            $h4_styles['color'] = cortex_mikado_options()->getOptionValue('h4_color');
        }
        if(cortex_mikado_options()->getOptionValue('h4_google_fonts') !== '-1') {
            $h4_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('h4_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('h4_fontsize') !== '') {
            $h4_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h4_fontsize')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h4_lineheight') !== '') {
            $h4_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h4_lineheight')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h4_texttransform') !== '') {
            $h4_styles['text-transform'] = cortex_mikado_options()->getOptionValue('h4_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('h4_fontstyle') !== '') {
            $h4_styles['font-style'] = cortex_mikado_options()->getOptionValue('h4_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('h4_fontweight') !== '') {
            $h4_styles['font-weight'] = cortex_mikado_options()->getOptionValue('h4_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('h4_letterspacing') !== '') {
            $h4_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h4_letterspacing')).'px';
        }

        $h4_selector = array(
            'h4'
        );

        if (!empty($h4_styles)) {
            echo cortex_mikado_dynamic_css($h4_selector, $h4_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_h4_styles');
}

if (!function_exists('cortex_mikado_h5_styles')) {

    function cortex_mikado_h5_styles() {

        $h5_styles = array();

        if(cortex_mikado_options()->getOptionValue('h5_color') !== '') {
            $h5_styles['color'] = cortex_mikado_options()->getOptionValue('h5_color');
        }
        if(cortex_mikado_options()->getOptionValue('h5_google_fonts') !== '-1') {
            $h5_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('h5_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('h5_fontsize') !== '') {
            $h5_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h5_fontsize')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h5_lineheight') !== '') {
            $h5_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h5_lineheight')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h5_texttransform') !== '') {
            $h5_styles['text-transform'] = cortex_mikado_options()->getOptionValue('h5_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('h5_fontstyle') !== '') {
            $h5_styles['font-style'] = cortex_mikado_options()->getOptionValue('h5_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('h5_fontweight') !== '') {
            $h5_styles['font-weight'] = cortex_mikado_options()->getOptionValue('h5_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('h5_letterspacing') !== '') {
            $h5_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h5_letterspacing')).'px';
        }

        $h5_selector = array(
            'h5'
        );

        if (!empty($h5_styles)) {
            echo cortex_mikado_dynamic_css($h5_selector, $h5_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_h5_styles');
}

if (!function_exists('cortex_mikado_h6_styles')) {

    function cortex_mikado_h6_styles() {

        $h6_styles = array();

        if(cortex_mikado_options()->getOptionValue('h6_color') !== '') {
            $h6_styles['color'] = cortex_mikado_options()->getOptionValue('h6_color');
        }
        if(cortex_mikado_options()->getOptionValue('h6_google_fonts') !== '-1') {
            $h6_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('h6_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('h6_fontsize') !== '') {
            $h6_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h6_fontsize')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h6_lineheight') !== '') {
            $h6_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h6_lineheight')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('h6_texttransform') !== '') {
            $h6_styles['text-transform'] = cortex_mikado_options()->getOptionValue('h6_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('h6_fontstyle') !== '') {
            $h6_styles['font-style'] = cortex_mikado_options()->getOptionValue('h6_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('h6_fontweight') !== '') {
            $h6_styles['font-weight'] = cortex_mikado_options()->getOptionValue('h6_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('h6_letterspacing') !== '') {
            $h6_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('h6_letterspacing')).'px';
        }

        $h6_selector = array(
            'h6'
        );

        if (!empty($h6_styles)) {
            echo cortex_mikado_dynamic_css($h6_selector, $h6_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_h6_styles');
}

if (!function_exists('cortex_mikado_text_styles')) {

    function cortex_mikado_text_styles() {

        $text_styles = array();

        if(cortex_mikado_options()->getOptionValue('text_color') !== '') {
            $text_styles['color'] = cortex_mikado_options()->getOptionValue('text_color');
        }
        if(cortex_mikado_options()->getOptionValue('text_google_fonts') !== '-1') {
            $text_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('text_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('text_fontsize') !== '') {
            $text_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('text_fontsize')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('text_lineheight') !== '') {
            $text_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('text_lineheight')).'px';
        }
        if(cortex_mikado_options()->getOptionValue('text_texttransform') !== '') {
            $text_styles['text-transform'] = cortex_mikado_options()->getOptionValue('text_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('text_fontstyle') !== '') {
            $text_styles['font-style'] = cortex_mikado_options()->getOptionValue('text_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('text_fontweight') !== '') {
            $text_styles['font-weight'] = cortex_mikado_options()->getOptionValue('text_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('text_letterspacing') !== '') {
            $text_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('text_letterspacing')).'px';
        }

        $text_selector = array(
            'p'
        );

        if (!empty($text_styles)) {
            echo cortex_mikado_dynamic_css($text_selector, $text_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_text_styles');
}

if (!function_exists('cortex_mikado_link_styles')) {

    function cortex_mikado_link_styles() {

        $link_styles = array();

        if(cortex_mikado_options()->getOptionValue('link_color') !== '') {
            $link_styles['color'] = cortex_mikado_options()->getOptionValue('link_color');
        }
        if(cortex_mikado_options()->getOptionValue('link_fontstyle') !== '') {
            $link_styles['font-style'] = cortex_mikado_options()->getOptionValue('link_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('link_fontweight') !== '') {
            $link_styles['font-weight'] = cortex_mikado_options()->getOptionValue('link_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('link_fontdecoration') !== '') {
            $link_styles['text-decoration'] = cortex_mikado_options()->getOptionValue('link_fontdecoration');
        }

        $link_selector = array(
            'a',
            'p a'
        );

        if (!empty($link_styles)) {
            echo cortex_mikado_dynamic_css($link_selector, $link_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_link_styles');
}

if (!function_exists('cortex_mikado_link_hover_styles')) {

    function cortex_mikado_link_hover_styles() {

        $link_hover_styles = array();

        if(cortex_mikado_options()->getOptionValue('link_hovercolor') !== '') {
            $link_hover_styles['color'] = cortex_mikado_options()->getOptionValue('link_hovercolor');
        }
        if(cortex_mikado_options()->getOptionValue('link_hover_fontdecoration') !== '') {
            $link_hover_styles['text-decoration'] = cortex_mikado_options()->getOptionValue('link_hover_fontdecoration');
        }

        $link_hover_selector = array(
            'a:hover',
            'p a:hover'
        );

        if (!empty($link_hover_styles)) {
            echo cortex_mikado_dynamic_css($link_hover_selector, $link_hover_styles);
        }

        $link_heading_hover_styles = array();

        if(cortex_mikado_options()->getOptionValue('link_hovercolor') !== '') {
            $link_heading_hover_styles['color'] = cortex_mikado_options()->getOptionValue('link_hovercolor');
        }

        $link_heading_hover_selector = array(
            'h1 a:hover',
            'h2 a:hover',
            'h3 a:hover',
            'h4 a:hover',
            'h5 a:hover',
            'h6 a:hover'
        );

        if (!empty($link_heading_hover_styles)) {
            echo cortex_mikado_dynamic_css($link_heading_hover_selector, $link_heading_hover_styles);
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_link_hover_styles');
}

if (!function_exists('cortex_mikado_smooth_page_transition_styles')) {

    function cortex_mikado_smooth_page_transition_styles() {
        
        $loader_style = array();

        if(cortex_mikado_options()->getOptionValue('smooth_pt_bgnd_color') !== '') {
            $loader_style['background-color'] = cortex_mikado_options()->getOptionValue('smooth_pt_bgnd_color');
        }

        $loader_selector = array('.mkdf-smooth-transition-loader');

        if (!empty($loader_style)) {
            echo cortex_mikado_dynamic_css($loader_selector, $loader_style);
        }

        $spinner_style = array();

        if(cortex_mikado_options()->getOptionValue('smooth_pt_spinner_color') !== '') {
            $spinner_style['background-color'] = cortex_mikado_options()->getOptionValue('smooth_pt_spinner_color');
        }

        $spinner_selectors = array(
            '.mkdf-st-loader .pulse',
            '.mkdf-st-loader .mkdf-triple-bounce .mkdf-triple-bounce1',
            '.mkdf-st-loader .cube',
            '.mkdf-st-loader .rotating_cubes .cube1',
            '.mkdf-st-loader .rotating_cubes .cube2',
            '.mkdf-st-loader .stripes > div',
            '.mkdf-st-loader .wave > div',
            '.mkdf-st-loader .two_rotating_circles .dot1',
            '.mkdf-st-loader .two_rotating_circles .dot2',
            '.mkdf-st-loader .five_rotating_circles .container1 > div',
            '.mkdf-st-loader .five_rotating_circles .container2 > div',
            '.mkdf-st-loader .five_rotating_circles .container3 > div',
            '.mkdf-st-loader .atom .ball-1:before',
            '.mkdf-st-loader .atom .ball-2:before',
            '.mkdf-st-loader .atom .ball-3:before',
            '.mkdf-st-loader .atom .ball-4:before',
            '.mkdf-st-loader .clock .ball:before',
            '.mkdf-st-loader .mitosis .ball',
            '.mkdf-st-loader .lines .line1',
            '.mkdf-st-loader .lines .line2',
            '.mkdf-st-loader .lines .line3',
            '.mkdf-st-loader .lines .line4',
            '.mkdf-st-loader .fussion .ball',
            '.mkdf-st-loader .fussion .ball-1',
            '.mkdf-st-loader .fussion .ball-2',
            '.mkdf-st-loader .fussion .ball-3',
            '.mkdf-st-loader .fussion .ball-4',
            '.mkdf-st-loader .wave_circles .ball',
            '.mkdf-st-loader .pulse_circles .ball'
        );

        if (!empty($spinner_style)) {
            echo cortex_mikado_dynamic_css($spinner_selectors, $spinner_style);
        }

        if (cortex_mikado_options()->getOptionValue('smooth_pt_spinner_type') === 'triple_bounce') {
            $additional_spinner_styles1 = array();

            if(cortex_mikado_options()->getOptionValue('additional_color_1') !== '') {
                $additional_spinner_styles1['background-color'] = cortex_mikado_options()->getOptionValue('additional_color_1');
            }

            if (!empty($additional_spinner_styles1)) {
                echo cortex_mikado_dynamic_css('.mkdf-st-loader .mkdf-triple-bounce .mkdf-triple-bounce2', $additional_spinner_styles1);
            }

            $additional_spinner_styles2 = array();

            if(cortex_mikado_options()->getOptionValue('additional_color_2') !== '') {
                $additional_spinner_styles2['background-color'] = cortex_mikado_options()->getOptionValue('additional_color_2');
            }

            if (!empty($additional_spinner_styles2)) {
                echo cortex_mikado_dynamic_css('.mkdf-st-loader .mkdf-triple-bounce .mkdf-triple-bounce3', $additional_spinner_styles2);
            }
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_smooth_page_transition_styles');
}