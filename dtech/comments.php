<?php
if ( post_password_required() ) {
	return;
}

if ( comments_open() || get_comments_number()) : ?>
	<div class="mkdf-comment-holder clearfix" id="comments">
		<div class="mkdf-comment-number">
			<div class="mkdf-comment-number-inner">
				<h4><?php comments_number( esc_html__('No Comments','cortex'), esc_html__('Comment: ','cortex').' 1', esc_html__('Comments: ','cortex').' %'); ?></h4>
			</div>
		</div>
		<div class="mkdf-comments">
			<?php if ( have_comments() ) : ?>
				<ul class="mkdf-comment-list">
					<?php wp_list_comments(array( 'callback' => 'cortex_mikado_comment')); ?>
				</ul>
			<?php endif; ?>
			<?php if( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' )) : ?>
				<p><?php esc_html_e('Sorry, the comment form is closed at this time.', 'cortex'); ?></p>
			<?php endif; ?>
		</div>
	</div>
	<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );

		$args = array(
			'id_form' => 'commentform',
			'id_submit' => 'submit_comment',
			'title_reply_before' => '<h4 id="reply-title" class="comment-reply-title">',
			'title_reply_after' => '</h4>',
			'title_reply'=> esc_html__( 'Post a Comment','cortex' ),
			'title_reply_to' => esc_html__( 'Post a Reply to %s','cortex' ),
			'cancel_reply_link' => esc_html__( 'Cancel Reply','cortex' ),
			'label_submit' => esc_html__( 'Submit','cortex' ),
			'comment_field' => '<textarea id="comment" placeholder="'.esc_html__( 'TYPE COMMENT...','cortex' ).'" name="comment" cols="45" rows="8" aria-required="true"></textarea>',
			'comment_notes_before' => '',
			'comment_notes_after' => '',
			'fields' => apply_filters( 'comment_form_default_fields', array(
				'author' => '<div class="mkdf-two-columns-50-50 clearfix"><div class="mkdf-two-columns-50-50-inner clearfix"><div class="mkdf-column"><div class="mkdf-column-inner"><input id="author" name="author" placeholder="'. esc_html__( 'NAME','cortex' ) .'" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"' . $aria_req . ' /></div></div>',
				'url' => '<div class="mkdf-column"><div class="mkdf-column-inner"><input id="email" name="email" placeholder="'. esc_html__( 'EMAIL','cortex' ) .'" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '"' . $aria_req . ' /></div></div></div></div>'
				 ) )
			);

		if(is_user_logged_in()){
			$args['class_form'] = 'mkdf-comment-registered-user';
			$args['title_reply_before'] = '<h4 id="reply-title" class="comment-reply-title mkdf-comment-reply-title-registered">';
			$args['title_reply_after'] = '</h4>';
		}
	?>
	<?php if(get_comment_pages_count() > 1 ){ ?>
		<div class="mkdf-comment-pager">
			<p><?php paginate_comments_links(); ?></p>
		</div>
	<?php } ?>
	 <div class="mkdf-comment-form">
		<?php comment_form($args); ?>
	</div>
<?php endif; ?>


