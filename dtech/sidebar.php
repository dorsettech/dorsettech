<?php
$cortex_mikado_sidebar = cortex_mikado_get_sidebar();
?>
<div class="mkdf-column-inner">
    <aside class="mkdf-sidebar">
       <?php if(is_product_category()){
				 dynamic_sidebar( 'custom-product-category-sidebar' ); 
			} else {
				if (is_active_sidebar($cortex_mikado_sidebar)) {
					dynamic_sidebar($cortex_mikado_sidebar);
				}
			}
		?>
    </aside>
</div>
<style type="text/css">
	.woocommerce.widget input[type="submit"].mkdf-product-search-form{font-family: Linearicons-Free !important}
	a.mkdf-social-icon-widget-holder{margin-right: 5px !important}
	.widget_price_filter .price_slider_amount .price_label{    width: 100% !important;    margin-top: 10px !important;}
</style>