<?php
$blog_archive_pages_classes = cortex_mikado_blog_archive_pages_classes(cortex_mikado_get_default_blog_list());
?>
<?php get_header(); ?>
<?php cortex_mikado_get_title(); ?>
<div class="<?php echo esc_attr($blog_archive_pages_classes['holder']); ?>">
<?php do_action('cortex_mikado_after_container_open'); ?>
	<div class="<?php echo esc_attr($blog_archive_pages_classes['inner']); ?>">
		<?php cortex_mikado_get_blog(cortex_mikado_get_default_blog_list()); ?>
	</div>
<?php do_action('cortex_mikado_before_container_close'); ?>
</div>
<?php do_action('cortex_mikado_after_container_close'); ?>
<?php get_footer(); ?>