<?php
/*
Template Name: Blog: Masonry
*/
?>
<?php get_header(); ?>
<?php cortex_mikado_get_title(); ?>
<?php get_template_part('slider'); ?>
	<div class="mkdf-container">
		<?php do_action('cortex_mikado_after_container_open'); ?>
		<div class="mkdf-container-inner">
			<?php cortex_mikado_get_blog('masonry'); ?>
		</div>
		<?php do_action('cortex_mikado_before_container_close'); ?>
	</div>
<?php do_action('cortex_mikado_after_container_close'); ?>
<?php get_footer(); ?>