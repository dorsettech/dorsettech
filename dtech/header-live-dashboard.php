<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php
    /**
     * @see cortex_mikado_header_meta() - hooked with 10
     * @see mkd_user_scalable - hooked with 10
     */
    ?>
	<?php do_action('cortex_mikado_header_meta'); ?>
<?php echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8" />'; ?>
	<?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
	<meta name="msvalidate.01" content="70B9B3654E229E815C139E6F43B58DB2" />
	
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '2994311953918609');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=2994311953918609&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<script src="https://cdn-eu.pagesense.io/js/dorsettech/00d5513881d143948a034cd80c6ac02b.js"></script>
	<script type="text/javascript">
	window.pagesense = window.pagesense || [];
window.pagesense.push(['trackEvent', 'Contact Page Views']);
	</script>

  <script>
    function gtag_report_conversion(url) {
      var callback = function () {
        if (typeof(url) != 'undefined') {
          window.location = url;
        }
      };
      gtag('event', 'conversion', {
          'send_to': 'AW-739393748/WVTzCKj7hbMBENSByeAC',
          'event_callback': callback
      });
      return false;
    }

    jQuery(document).ready(function(){
      jQuery('.footer-email').click(function(){
        gtag_report_conversion();
      });
    });
  </script>
	
</head>

<body <?php body_class();?>>
	<script type="text/javascript" src="https://secure.rear9axis.com/js/198436.js" ></script>
<noscript><img alt="" src="https://secure.rear9axis.com/198436.png" style="display:none;" /></noscript>
<?php cortex_mikado_get_side_area(); ?>


<?php
/*if(cortex_mikado_options()->getOptionValue('smooth_page_transitions') == "yes") {
    $cortex_mikado_ajax_class = 'mkdf-mimic-ajax';
?>
<div class="mkdf-smooth-transition-loader <?php echo esc_attr($cortex_mikado_ajax_class); ?>">
    <div class="mkdf-st-loader">
        <div class="mkdf-st-loader1">
            <img src="/wp-content/themes/dtech/assets/img/superloading.gif" id="superloader"/>
            <?php //cortex_mikado_loading_spinners(); ?>
        </div>
    </div>
</div>
<?php }*/ ?>

<div class="mkdf-wrapper">
    <div class="mkdf-wrapper-inner">
        <?php// cortex_mikado_get_header(); ?>

        <?php if (cortex_mikado_options()->getOptionValue('show_back_button') == "yes") { ?>
            <a id='mkdf-back-to-top'  href='#'>
                <span class="mkdf-btt-top-shadow"></span>
                <span class="mkdf-icon-stack">
                     <?php
                        cortex_mikado_icon_collections()->getBackToTopIcon('font_elegant');
                    ?>
                </span>
                <span class="mkdf-btt-bottom-shadow"></span>
            </a>
        <?php } ?>
        <?php cortex_mikado_get_full_screen_menu(); ?>

        <div class="mkdf-content" <?php cortex_mikado_content_elem_style_attr(); ?>>

            <div class="mkdf-content-inner">
