<?php
$cortex_mikado_sidebar = cortex_mikado_sidebar_layout();?>

<?php get_header(); ?>
<?php //cortex_mikado_get_title(); ?>

<div class="mkdf-title portfolio-arch-title">
	<div class="mkdf-title-holder">
		<div class="mkdf-container clearfix">
			<div class="mkdf-vertical-align-containers">
				<div class="mkdf-title-subtitle-holder">
					<div class="mkdf-title-subtitle-holder-inner">
						<h1><span><?php echo get_queried_object()->name;?></span></h1>
						<p><?php echo get_queried_object()->description;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="mkdf-full-width">
	<div class="mkdf-full-width-inner">
		<div class="mkdf-vertical-align-containers archive-portfolio-list-holder">
			<div class="mkdf-ptf-gallery mkdf-ptf-with-space mkdf-ptf-four-columns">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article class="mkdf-portfolio-item mix <?php echo esc_attr($categories)?>">
						<div class="mkdf-item-image-holder">
							<a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>">
								<?php
									echo get_the_post_thumbnail(get_the_ID(),'cortex_mikado_square');
								?>
							</a>
						</div>
						<div class="mkdf-item-text-holder">
							<a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>">
								<h2><?php echo esc_attr(get_the_title()); ?></h2>
							</a>
							<div class="portfolio-excerpt">
								<?php
									the_excerpt();
								?>
							</div>
						</div>
					</article>

					<?php do_action('cortex_mikado_page_after_content'); ?>
			<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
