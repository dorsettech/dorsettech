<?php

get_header();
cortex_mikado_get_title();
get_template_part('slider');
cortex_mikado_single_portfolio();
do_action('cortex_mikado_after_container_close');
get_footer();

?>