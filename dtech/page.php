<?php $cortex_mikado_sidebar = cortex_mikado_sidebar_layout(); ?>
<?php get_header(); ?>
	<?php cortex_mikado_get_title(); ?>
	<?php get_template_part('slider'); ?>
	<div class="mkdf-container">
		<?php do_action('cortex_mikado_after_container_open'); ?>
		<div class="mkdf-container-inner clearfix">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php if(($cortex_mikado_sidebar == 'default')||($cortex_mikado_sidebar == '')) : ?>
					<?php the_content(); ?>
					<?php do_action('cortex_mikado_page_after_content'); ?>
				<?php elseif($cortex_mikado_sidebar == 'sidebar-33-right' || $cortex_mikado_sidebar == 'sidebar-25-right'): ?>
					<div <?php echo cortex_mikado_sidebar_columns_class(); ?>>
						<div class="mkdf-column1 mkdf-content-left-from-sidebar">
							<div class="mkdf-column-inner">
								<?php the_content(); ?>
								<?php do_action('cortex_mikado_page_after_content'); ?>
							</div>
						</div>
						<div class="mkdf-column2">
							<?php get_sidebar(); ?>
						</div>
					</div>
				<?php elseif($cortex_mikado_sidebar == 'sidebar-33-left' || $cortex_mikado_sidebar == 'sidebar-25-left'): ?>
					<div <?php echo cortex_mikado_sidebar_columns_class(); ?>>
						<div class="mkdf-column1">
							<?php get_sidebar(); ?>
						</div>
						<div class="mkdf-column2 mkdf-content-right-from-sidebar">
							<div class="mkdf-column-inner">
								<?php the_content(); ?>
								<?php do_action('cortex_mikado_page_after_content'); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<?php do_action('cortex_mikado_before_container_close'); ?>
	</div>
	<?php do_action('cortex_mikado_after_container_close'); ?>
<?php get_footer(); ?>