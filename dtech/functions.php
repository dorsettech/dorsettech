<?php
include_once get_template_directory().'/theme-includes.php';

if(!function_exists('cortex_mikado_styles')) {
    /**
     * Function that includes theme's core styles
     */
    function cortex_mikado_styles() {
        wp_register_style('cortex_mikado_blog', MIKADO_ASSETS_ROOT.'/css/blog.min.css');

        //include theme's core styles
        wp_enqueue_style('cortex_mikado_default_style', MIKADO_ROOT.'/style.css');
        wp_enqueue_style('cortex_mikado_modules_plugins', MIKADO_ASSETS_ROOT.'/css/plugins.min.css');
        wp_enqueue_style('cortex_mikado_modules', MIKADO_ASSETS_ROOT.'/css/modules.min.css');

        cortex_mikado_icon_collections()->enqueueStyles();

        if(cortex_mikado_load_blog_assets()) {
            wp_enqueue_style('cortex_mikado_blog');
        }

        if(cortex_mikado_load_blog_assets() || is_singular('portfolio-item')) {
            wp_enqueue_style('wp-mediaelement');
        }

        //define files afer which style dynamic needs to be included. It should be included last so it can override other files
        $style_dynamic_deps_array = array();

        //is woocommerce installed?
        if(cortex_mikado_is_woocommerce_installed()) {
            if(cortex_mikado_load_woo_assets()) {

                $style_dynamic_deps_array[] = 'cortex_mikado_woocommerce';

                //include theme's woocommerce styles
                wp_enqueue_style('cortex_mikado_woocommerce', MIKADO_ASSETS_ROOT.'/css/woocommerce.min.css');


                //is responsive option turned on?
                if(cortex_mikado_options()->getOptionValue('responsiveness') == 'yes') {
                    $style_dynamic_deps_array[] = 'cortex_mikado_woocommerce_responsive';
                    //include theme's woocommerce responsive styles
                    wp_enqueue_style('cortex_mikado_woocommerce_responsive', MIKADO_ASSETS_ROOT.'/css/woocommerce-responsive.min.css');
                }
            }
        }




        //is responsive option turned on?
        if(cortex_mikado_is_responsive_on()) {
            wp_enqueue_style('cortex_mikado_modules_responsive', MIKADO_ASSETS_ROOT.'/css/modules-responsive.min.css');
            wp_enqueue_style('cortex_mikado_blog_responsive', MIKADO_ASSETS_ROOT.'/css/blog-responsive.min.css');

            //include proper styles
            if(file_exists(MIKADO_ROOT_DIR.'/assets/css/style_dynamic_responsive.css') && cortex_mikado_is_css_folder_writable() && !is_multisite()) {
                wp_enqueue_style('cortex_mikado_style_dynamic_responsive', MIKADO_ASSETS_ROOT.'/css/style_dynamic_responsive.css', array(), filemtime(MIKADO_ROOT_DIR.'/assets/css/style_dynamic_responsive.css'));
            } elseif(file_exists(MIKADO_ROOT_DIR.'/assets/css/style_dynamic_responsive_ms_id_'. cortex_mikado_get_multisite_blog_id() .'.css') && cortex_mikado_is_css_folder_writable() && is_multisite()) {
                wp_enqueue_style('cortex_mikado_style_dynamic_responsive', MIKADO_ASSETS_ROOT.'/css/style_dynamic_responsive_ms_id_'. cortex_mikado_get_multisite_blog_id() .'.css', array(), filemtime(MIKADO_ROOT_DIR.'/assets/css/style_dynamic_responsive_ms_id_'. cortex_mikado_get_multisite_blog_id() .'.css'));
            }
        }

        if(file_exists(MIKADO_ROOT_DIR.'/assets/css/style_dynamic.css') && cortex_mikado_is_css_folder_writable() && !is_multisite()) {
            wp_enqueue_style('cortex_mikado_style_dynamic', MIKADO_ASSETS_ROOT.'/css/style_dynamic.css', $style_dynamic_deps_array, filemtime(MIKADO_ROOT_DIR.'/assets/css/style_dynamic.css')); //it must be included after woocommerce styles so it can override it
        } else if(file_exists(MIKADO_ROOT_DIR.'/assets/css/style_dynamic_ms_id_'. cortex_mikado_get_multisite_blog_id() .'.css') && cortex_mikado_is_css_folder_writable() && is_multisite()) {
            wp_enqueue_style('cortex_mikado_style_dynamic', MIKADO_ASSETS_ROOT.'/css/style_dynamic_ms_id_'. cortex_mikado_get_multisite_blog_id() .'.css', $style_dynamic_deps_array, filemtime(MIKADO_ROOT_DIR.'/assets/css/style_dynamic_ms_id_'. cortex_mikado_get_multisite_blog_id() .'.css')); //it must be included after woocommerce styles so it can override it
        }

        //include Visual Composer styles
        if(class_exists('WPBakeryVisualComposerAbstract')) {
            wp_enqueue_style('js_composer_front');
        }
    }

    add_action('wp_enqueue_scripts', 'cortex_mikado_styles');
}

if(!function_exists('cortex_mikado_google_fonts_styles')) {
	/**
	 * Function that includes google fonts defined anywhere in the theme
	 */
    function cortex_mikado_google_fonts_styles() {
        $font_simple_field_array = cortex_mikado_options()->getOptionsByType('fontsimple');
        if(!(is_array($font_simple_field_array) && count($font_simple_field_array) > 0)) {
            $font_simple_field_array = array();
        }

        $font_field_array = cortex_mikado_options()->getOptionsByType('font');
        if(!(is_array($font_field_array) && count($font_field_array) > 0)) {
            $font_field_array = array();
        }

        $available_font_options = array_merge($font_simple_field_array, $font_field_array);
        $font_weight_str        = '100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic';

        //define available font options array
        $fonts_array = array();
        foreach($available_font_options as $font_option) {
            //is font set and not set to default and not empty?
            $font_option_value = cortex_mikado_options()->getOptionValue($font_option);
            if(cortex_mikado_is_font_option_valid($font_option_value) && !cortex_mikado_is_native_font($font_option_value)) {
                $font_option_string = $font_option_value.':'.$font_weight_str;
                if(!in_array($font_option_string, $fonts_array)) {
                    $fonts_array[] = $font_option_string;
                }
            }
        }

        $fonts_array         = array_diff($fonts_array, array('-1:'.$font_weight_str));
        $google_fonts_string = implode('|', $fonts_array);

        //default fonts should be separated with %7C because of HTML validation
        $default_font_string = 'Montserrat:'.$font_weight_str;
        $protocol = is_ssl() ? 'https:' : 'http:';

        //is google font option checked anywhere in theme?
        if (count($fonts_array) > 0) {

            //include all checked fonts
            $fonts_full_list = $default_font_string . '|' . str_replace('+', ' ', $google_fonts_string);
            $fonts_full_list_args = array(
                'family' => urlencode($fonts_full_list),
                'subset' => urlencode('latin,latin-ext'),
            );

            $cortex_mikado_fonts = add_query_arg( $fonts_full_list_args, $protocol.'//fonts.googleapis.com/css' );
            wp_enqueue_style( 'cortex_mikado_google_fonts', esc_url_raw($cortex_mikado_fonts), array(), '1.0.0' );

        } else {
            //include default google font that theme is using
            $default_fonts_args = array(
                'family' => urlencode($default_font_string),
                'subset' => urlencode('latin,latin-ext'),
            );
            $cortex_mikado_fonts = add_query_arg( $default_fonts_args, $protocol.'//fonts.googleapis.com/css' );
            wp_enqueue_style( 'cortex_mikado_google_fonts', esc_url_raw($cortex_mikado_fonts), array(), '1.0.0' );
        }

    }

	add_action('wp_enqueue_scripts', 'cortex_mikado_google_fonts_styles');
}

if(!function_exists('cortex_mikado_scripts')) {
    /**
     * Function that includes all necessary scripts
     */
    function cortex_mikado_scripts() {
        global $wp_scripts;

        //init theme core scripts
		wp_enqueue_script( 'jquery-ui-core');
		wp_enqueue_script( 'jquery-ui-tabs');
		wp_enqueue_script( 'jquery-ui-accordion');
		wp_enqueue_script( 'wp-mediaelement');

        wp_enqueue_script('cortex_mikado_third_party', MIKADO_ASSETS_ROOT.'/js/third-party.min.js', array('jquery'), false, true);
        wp_enqueue_script('isotope', MIKADO_ASSETS_ROOT.'/js/jquery.isotope.min.js', array('jquery'), false, true);

		if(cortex_mikado_is_smoth_scroll_enabled()) {
			wp_enqueue_script("cortex_mikado_smooth_page_scroll", MIKADO_ASSETS_ROOT . "/js/smoothPageScroll.js", array(), false, true);
		}

        //include google map api script
		if(cortex_mikado_options()->getOptionValue('google_maps_api_key') != '') {
			$google_maps_api_key = cortex_mikado_options()->getOptionValue('google_maps_api_key');
			wp_enqueue_script('google_map_api', '//maps.googleapis.com/maps/api/js?key=' . $google_maps_api_key, array(), false, true);
		}

        wp_enqueue_script('cortex_mikado_modules', MIKADO_ASSETS_ROOT.'/js/modules.js', array('jquery'), false, true);

        if(cortex_mikado_load_blog_assets()) {
            wp_enqueue_script('cortex_mikado_blog', MIKADO_ASSETS_ROOT.'/js/blog.min.js', array('jquery'), false, true);
        }

        //include comment reply script
        $wp_scripts->add_data('comment-reply', 'group', 1);
        if(is_singular() && comments_open() && get_option( 'thread_comments' )) {
            wp_enqueue_script("comment-reply");
        }

        //include Visual Composer script
        if(class_exists('WPBakeryVisualComposerAbstract')) {
            wp_enqueue_script('wpb_composer_front_js');
        }
    }

    add_action('wp_enqueue_scripts', 'cortex_mikado_scripts');
}

//defined content width variable
if (!isset( $content_width )) $content_width = 1060;

if(!function_exists('cortex_mikado_theme_setup')) {
    /**
     * Function that adds various features to theme. Also defines image sizes that are used in a theme
     */
    function cortex_mikado_theme_setup() {
        //add support for feed links
        add_theme_support('automatic-feed-links');

        //add support for post formats
        add_theme_support('post-formats', array('gallery', 'link', 'quote', 'video', 'audio'));

        //add theme support for post thumbnails
        add_theme_support('post-thumbnails');

        //add theme support for title tag
		add_theme_support('title-tag');

        //define thumbnail sizes
        add_image_size('cortex_mikado_square', 550, 550, true);
        add_image_size('cortex_mikado_landscape', 800, 600, true);
        add_image_size('cortex_mikado_portrait', 600, 800, true);
        add_image_size('cortex_mikado_large_width', 1100, 550, true);
        add_image_size('cortex_mikado_large_height', 550, 1100, true);
        add_image_size('cortex_mikado_large_width_height', 1100, 1100, true);

        load_theme_textdomain( 'cortex', get_template_directory().'/languages' );
    }

    add_action('after_setup_theme', 'cortex_mikado_theme_setup');
}


if(!function_exists('cortex_mikado_rgba_color')) {
    /**
     * Function that generates rgba part of css color property
     *
     * @param $color string hex color
     * @param $transparency float transparency value between 0 and 1
     *
     * @return string generated rgba string
     */
    function cortex_mikado_rgba_color($color, $transparency) {
        if($color !== '' && $transparency !== '') {
            $rgba_color = '';

            $rgb_color_array = cortex_mikado_hex2rgb($color);
            $rgba_color .= 'rgba('.implode(', ', $rgb_color_array).', '.$transparency.')';

            return $rgba_color;
        }
    }
}


if(!function_exists('cortex_mikado_header_meta')) {
    /**
     * Function that echoes meta data if our seo is enabled
     */
    function cortex_mikado_header_meta() { ?>

        <meta charset="<?php bloginfo('charset'); ?>"/>
        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <?php }

    add_action('cortex_mikado_header_meta', 'cortex_mikado_header_meta');
}

if(!function_exists('cortex_mikado_user_scalable_meta')) {
    /**
     * Function that outputs user scalable meta if responsiveness is turned on
     * Hooked to cortex_mikado_header_meta action
     */
    function cortex_mikado_user_scalable_meta() {
        //is responsiveness option is chosen?
        if(cortex_mikado_is_responsive_on()) { ?>
            <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <?php } else { ?>
            <meta name="viewport" content="width=1200,user-scalable=yes">
        <?php }
    }

    add_action('cortex_mikado_header_meta', 'cortex_mikado_user_scalable_meta');
}

if(!function_exists('cortex_mikado_get_page_id')) {
	/**
	 * Function that returns current page / post id.
	 * Checks if current page is woocommerce page and returns that id if it is.
	 * Checks if current page is any archive page (category, tag, date, author etc.) and returns -1 because that isn't
	 * page that is created in WP admin.
	 *
	 * @return int
	 *
	 * @version 0.1
	 *
	 * @see cortex_mikado_is_woocommerce_installed()
	 * @see cortex_mikado_is_woocommerce_shop()
	 */
	function cortex_mikado_get_page_id() {
		if(cortex_mikado_is_woocommerce_installed() && cortex_mikado_is_woocommerce_shop()) {
			return cortex_mikado_get_woo_shop_page_id();
		}

		if(is_archive() || is_search() || is_404() || (is_home() && is_front_page())) {
			return -1;
		}

		return get_queried_object_id();
	}
}


if(!function_exists('cortex_mikado_is_default_wp_template')) {
    /**
     * Function that checks if current page archive page, search, 404 or default home blog page
     * @return bool
     *
     * @see is_archive()
     * @see is_search()
     * @see is_404()
     * @see is_front_page()
     * @see is_home()
     */
    function cortex_mikado_is_default_wp_template() {
        return is_archive() || is_search() || is_404() || (is_front_page() && is_home());
    }
}

if(!function_exists('cortex_mikado_get_page_template_name')) {
    /**
     * Returns current template file name without extension
     * @return string name of current template file
     */
    function cortex_mikado_get_page_template_name() {
        $file_name = '';

        if(!cortex_mikado_is_default_wp_template()) {
            $file_name_without_ext = preg_replace('/\\.[^.\\s]{3,4}$/', '', basename(get_page_template()));

            if($file_name_without_ext !== '') {
                $file_name = $file_name_without_ext;
            }
        }

        return $file_name;
    }
}

if(!function_exists('cortex_mikado_has_shortcode')) {
    /**
     * Function that checks whether shortcode exists on current page / post
     *
     * @param string shortcode to find
     * @param string content to check. If isn't passed current post content will be used
     *
     * @return bool whether content has shortcode or not
     */
    function cortex_mikado_has_shortcode($shortcode, $content = '') {
        $has_shortcode = false;

        if($shortcode) {
            //if content variable isn't past
            if($content == '') {
                //take content from current post
                $page_id = cortex_mikado_get_page_id();
                if(!empty($page_id)) {
                    $current_post = get_post($page_id);

                    if(is_object($current_post) && property_exists($current_post, 'post_content')) {
                        $content = $current_post->post_content;
                    }
                }
            }

            //does content has shortcode added?
            if(stripos($content, '['.$shortcode) !== false) {
                $has_shortcode = true;
            }
        }

        return $has_shortcode;
    }
}

if(!function_exists('cortex_mikado_get_dynamic_sidebar')) {
    /**
     * Return Custom Widget Area content
     *
     * @return string
     */
    function cortex_mikado_get_dynamic_sidebar($index = 1) {
        ob_start();
        dynamic_sidebar($index);
        $sidebar_contents = ob_get_clean();

        return $sidebar_contents;
    }
}

add_action( 'pre_get_posts', 'njengah_hide_out_of_stock_products' );

function njengah_hide_out_of_stock_products( $query ) {

  if ( ! $query->is_main_query() || is_admin() ) {
    return;
  }

     if ( $outofstock_term = get_term_by( 'name', 'outofstock', 'product_visibility' ) ) {

     $tax_query = (array) $query->get('tax_query');

      $tax_query[] = array(
      'taxonomy' => 'product_visibility',
      'field' => 'term_taxonomy_id',
      'terms' => array( $outofstock_term->term_taxonomy_id ),
      'operator' => 'NOT IN'
   );

  $query->set( 'tax_query', $tax_query );

}

  remove_action( 'pre_get_posts', 'njengah_hide_out_of_stock_products' );

}

if(!function_exists('cortex_mikado_get_sidebar')) {
    /**
     * Return Sidebar
     *
     * @return string
     */
    function cortex_mikado_get_sidebar() {

        $id = cortex_mikado_get_page_id();

        $sidebar = "sidebar";

        if (get_post_meta($id, 'mkdf_custom_sidebar_meta', true) != '') {
            $sidebar = get_post_meta($id, 'mkdf_custom_sidebar_meta', true);
        } else {
            if (is_single() && cortex_mikado_options()->getOptionValue('blog_single_custom_sidebar') != '') {
                $sidebar = esc_attr(cortex_mikado_options()->getOptionValue('blog_single_custom_sidebar'));
            } elseif ((is_archive() || (is_home() && is_front_page())) && cortex_mikado_options()->getOptionValue('blog_custom_sidebar') != '') {
                $sidebar = esc_attr(cortex_mikado_options()->getOptionValue('blog_custom_sidebar'));
            } elseif (is_page() && cortex_mikado_options()->getOptionValue('page_custom_sidebar') != '') {
                $sidebar = esc_attr(cortex_mikado_options()->getOptionValue('page_custom_sidebar'));
            }
        }

        return $sidebar;
    }
}



if( !function_exists('cortex_mikado_sidebar_columns_class') ) {

    /**
     * Return classes for columns holder when sidebar is active
     *
     * @return array
     */

    function cortex_mikado_sidebar_columns_class() {

        $sidebar_class = array();
        $sidebar_layout = cortex_mikado_sidebar_layout();

        switch($sidebar_layout):
            case 'sidebar-33-right':
                $sidebar_class[] = 'mkdf-two-columns-66-33';
                break;
            case 'sidebar-25-right':
                $sidebar_class[] = 'mkdf-two-columns-75-25';
                break;
            case 'sidebar-33-left':
                $sidebar_class[] = 'mkdf-two-columns-33-66';
                break;
            case 'sidebar-25-left':
                $sidebar_class[] = 'mkdf-two-columns-25-75';
                break;

        endswitch;

        $sidebar_class[] = 'clearfix';

        return cortex_mikado_class_attribute($sidebar_class);

    }

}


if( !function_exists('cortex_mikado_sidebar_layout') ) {

    /**
     * Function that check is sidebar is enabled and return type of sidebar layout
     */

    function cortex_mikado_sidebar_layout() {

        $sidebar_layout = '';
        $page_id        = cortex_mikado_get_page_id();

        $page_sidebar_meta = get_post_meta($page_id, 'mkdf_sidebar_meta', true);

        if(($page_sidebar_meta !== '') && $page_id !== -1) {
            if($page_sidebar_meta == 'no-sidebar') {
                $sidebar_layout = '';
            } else {
                $sidebar_layout = $page_sidebar_meta;
            }
        } else {
            if(is_single() && cortex_mikado_options()->getOptionValue('blog_single_sidebar_layout')) {
                $sidebar_layout = esc_attr(cortex_mikado_options()->getOptionValue('blog_single_sidebar_layout'));
            } elseif((is_archive() || (is_home() && is_front_page())) && cortex_mikado_options()->getOptionValue('archive_sidebar_layout')) {
                $sidebar_layout = esc_attr(cortex_mikado_options()->getOptionValue('archive_sidebar_layout'));
            } elseif(is_page() && cortex_mikado_options()->getOptionValue('page_sidebar_layout')) {
                $sidebar_layout = esc_attr(cortex_mikado_options()->getOptionValue('page_sidebar_layout'));
            }
        }

        return $sidebar_layout;

    }

}


if( !function_exists('cortex_mikado_page_custom_style') ) {

    /**
     * Function that print custom page style
     */

    function cortex_mikado_page_custom_style() {
       $style = array();
       $style = apply_filters('cortex_mikado_add_page_custom_style', $style);

		if($style !== '') {
			wp_add_inline_style( 'cortex_mikado_modules', implode(' ', $style));
		}
    }
	add_action('wp_enqueue_scripts', 'cortex_mikado_page_custom_style');

}


if(!function_exists('cortex_mikado_get_unique_page_class')) {
	/**
	 * Returns unique page class based on post type and page id
	 *
	 * @return string
	 */
	function cortex_mikado_get_unique_page_class() {
		$id = cortex_mikado_get_page_id();
		$page_class = '';

		if(is_single()) {
			$page_class = '.postid-'.$id;
		} elseif($id === cortex_mikado_get_woo_shop_page_id()) {
			$page_class = '.archive';
        } elseif(is_home()) {
            $page_class .= '.home';
		} else {
			$page_class .= '.page-id-'.$id;
		}

		return $page_class;
	}
}

if( !function_exists('cortex_mikado_container_style') ) {

    /**
     * Function that return container style
     */

    function cortex_mikado_container_style($style) {
        $id = cortex_mikado_get_page_id();
        $class_prefix = cortex_mikado_get_unique_page_class();

        $container_selector = array(
            $class_prefix.' .mkdf-content .mkdf-content-inner > .mkdf-container',
            $class_prefix.' .mkdf-content .mkdf-content-inner > .mkdf-full-width',
        );

        $container_class = array();
        $page_background_color = get_post_meta($id, "mkdf_page_background_color_meta", true);

        if($page_background_color){
            $container_class['background-color'] = $page_background_color;
        }

        $current_style = cortex_mikado_dynamic_css($container_selector, $container_class);
		$style[]       = $current_style;

        return $style;

    }
    add_filter('cortex_mikado_add_page_custom_style', 'cortex_mikado_container_style');
}

if( !function_exists('cortex_mikado_page_padding') ) {

    /**
     * Function that return container style
     */

    function cortex_mikado_page_padding( $style ) {

		$id = cortex_mikado_get_page_id();
		$class_prefix = cortex_mikado_get_unique_page_class();


        $page_selector = array(
			$class_prefix . ' .mkdf-content .mkdf-content-inner > .mkdf-container > .mkdf-container-inner',
			$class_prefix . ' .mkdf-content .mkdf-content-inner > .mkdf-full-width > .mkdf-full-width-inner'
        );
        $page_css = array();

        $page_padding = get_post_meta($id, 'mkdf_page_padding_meta', true);

        if($page_padding !== ''){
            $page_css['padding'] = $page_padding;
        }

        $current_style = cortex_mikado_dynamic_css($page_selector, $page_css);

		$style[]       = $current_style;

        return $style;

    }
    add_filter('cortex_mikado_add_page_custom_style', 'cortex_mikado_page_padding');
}

if( !function_exists('cortex_mikado_page_boxed_style') ) {

	/**
	 * Function that return container style
	 */

	function cortex_mikado_page_boxed_style( $style ) {

		$id = cortex_mikado_get_page_id();
		$class_prefix = cortex_mikado_get_unique_page_class();

		$page_selector = array(
			$class_prefix . '.mkdf-boxed .mkdf-wrapper'
		);
		$page_css = array();

		$page_background_color 				= get_post_meta($id, 'mkdf_page_background_color_in_box_meta', true);
		$page_background_image				= get_post_meta($id, 'mkdf_boxed_background_image_meta', true);
		$page_background_image_repeating	= get_post_meta($id, 'mkdf_boxed_background_image_repeating_meta', true);

		if($page_background_color !== ''){
			$page_css['background-color'] = $page_background_color;
		}
		if($page_background_image !== '' && $page_background_image_repeating != ''){
			$page_css['background-image'] = 'url(' .$page_background_image . ')';
			$page_css['background-repeat'] = $page_background_image_repeating;

			if($page_background_image_repeating == 'no') {
				$page_css['background-position']	= 'center 0';
				$page_css['background-repeat'] 		= 'no-repeat';
			} else {
				$page_css['background-position'] 	= '0 0';
				$page_css['background-repeat'] 		= 'repeat';
			}
		}

		$current_style = cortex_mikado_dynamic_css($page_selector, $page_css);

		$style[]       = $current_style;

		return $style;

	}
	add_filter('cortex_mikado_add_page_custom_style', 'cortex_mikado_page_boxed_style');
}

if(!function_exists('cortex_mikado_print_custom_css')) {
    /**
     * Prints out custom css from theme options
     */
    function cortex_mikado_print_custom_css() {
        $custom_css = cortex_mikado_options()->getOptionValue('custom_css');

        if($custom_css !== '') {
            wp_add_inline_style( 'cortex_mikado_modules', $custom_css);
        }
    }

    add_action('wp_enqueue_scripts', 'cortex_mikado_print_custom_css');
}

if(!function_exists('cortex_mikado_print_custom_js')) {
    /**
     * Prints out custom css from theme options
     */
    function cortex_mikado_print_custom_js() {
        $custom_js = cortex_mikado_options()->getOptionValue('custom_js');

        if($custom_js !== '') {
			wp_add_inline_script('cortex_mikado_modules', $custom_js);
        }

    }

	add_action('wp_enqueue_scripts', 'cortex_mikado_print_custom_js');
}


if(!function_exists('cortex_mikado_get_global_variables')) {
    /**
     * Function that generates global variables and put them in array so they could be used in the theme
     */
    function cortex_mikado_get_global_variables() {

        $global_variables = array();
        $element_appear_amount = -150;

        $global_variables['mkdfAddForAdminBar'] = is_admin_bar_showing() ? 32 : 0;
        $global_variables['mkdfElementAppearAmount'] = cortex_mikado_options()->getOptionValue('element_appear_amount') !== '' ? cortex_mikado_options()->getOptionValue('element_appear_amount') : $element_appear_amount;
        $global_variables['mkdfFinishedMessage'] = esc_html__('No more posts', 'cortex');
        $global_variables['mkdfLoadingMoreText'] = esc_html__('Loading...', 'cortex');
        $global_variables['mkdfMessage'] = esc_html__('Loading new posts...', 'cortex');
        $global_variables['mkdfLoadMoreText'] = esc_html__('Show More', 'cortex');
        $global_variables['mkdfAddingToCart'] = esc_html__('Adding to Cart...', 'cortex');
        $global_variables['mkdfFirstColor'] = cortex_mikado_options()->getOptionValue('first_color') !== '' ? cortex_mikado_options()->getOptionValue('first_color') : '#ff2c54';

        $global_variables = apply_filters('cortex_mikado_js_global_variables', $global_variables);

        wp_localize_script('cortex_mikado_modules', 'mkdfGlobalVars', array(
            'vars' => $global_variables
        ));

    }

    add_action('wp_enqueue_scripts', 'cortex_mikado_get_global_variables');
}

if(!function_exists('cortex_mikado_per_page_js_variables')) {
	/**
	 * Outputs global JS variable that holds page settings
	 */
	function cortex_mikado_per_page_js_variables() {
        $per_page_js_vars = apply_filters('cortex_mikado_per_page_js_vars', array());

        wp_localize_script('cortex_mikado_modules', 'mkdfPerPageVars', array(
            'vars' => $per_page_js_vars
        ));
    }

    add_action('wp_enqueue_scripts', 'cortex_mikado_per_page_js_variables');
}

if(!function_exists('cortex_mikado_content_elem_style_attr')) {
    /**
     * Defines filter for adding custom styles to content HTML element
     */
    function cortex_mikado_content_elem_style_attr() {
        $styles = apply_filters('cortex_mikado_content_elem_style_attr', array());

        cortex_mikado_inline_style($styles);
    }
}

if(!function_exists('cortex_mikado_is_woocommerce_installed')) {
    /**
     * Function that checks if woocommerce is installed
     * @return bool
     */
    function cortex_mikado_is_woocommerce_installed() {
        return function_exists('is_woocommerce');
    }
}

if(!function_exists('cortex_mikado_visual_composer_installed')) {
    /**
     * Function that checks if visual composer installed
     * @return bool
     */
    function cortex_mikado_visual_composer_installed() {
        //is Visual Composer installed?
        if(class_exists('WPBakeryVisualComposerAbstract')) {
            return true;
        }

        return false;
    }
}

if(!function_exists('cortex_mikado_contact_form_7_installed')) {
    /**
     * Function that checks if contact form 7 installed
     * @return bool
     */
    function cortex_mikado_contact_form_7_installed() {
        //is Contact Form 7 installed?
        if(defined('WPCF7_VERSION')) {
            return true;
        }

        return false;
    }
}

if(!function_exists('cortex_mikado_is_wpml_installed')) {
    /**
     * Function that checks if WPML plugin is installed
     * @return bool
     *
     * @version 0.1
     */
    function cortex_mikado_is_wpml_installed() {
        return defined('ICL_SITEPRESS_VERSION');
    }
}

if(!function_exists('cortex_mikado_max_image_width_srcset')) {
	/**
	 * Set max width for srcset to 1920
	 *
	 * @return int
	 */
	function cortex_mikado_max_image_width_srcset() {
        return 1920;
    }

	add_filter('max_srcset_image_width', 'cortex_mikado_max_image_width_srcset');
}


if(!function_exists('cortex_mikado_add_cc_mime_types')) {
    function cortex_mikado_add_cc_mime_types($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
    add_filter('upload_mimes', 'cortex_mikado_add_cc_mime_types');
}

if ( ! function_exists( 'cortex_mikado_is_gutenberg_installed' ) ) {
    /**
     * Function that checks if Gutenberg plugin installed
     * @return bool
     */
    function cortex_mikado_is_gutenberg_installed() {
        return function_exists( 'is_gutenberg_page' ) && is_gutenberg_page();
    }
}
//[dtdivider]
function dtdividertop_func( $atts ){
    $output = '<div class="uncode-divider-wrap uncode-divider-wrap-top z_index_0 uncode-divider-relative" style="height: 300px;" data-height="300" data-unit="px"><svg version="1.1" class="uncode-row-divider uncode-row-divider-swoosh-opacity" x="0px" y="0px" width="240px" height="24px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none"> <path fill="#ffffff" fill-opacity="0.33" d="M240,24V0c-51.797,0-69.883,13.18-94.707,15.59c-24.691,2.4-43.872-1.17-63.765-1.08
c-19.17,0.1-31.196,3.65-51.309,6.58C15.552,23.21,4.321,22.471,0,22.01V24H240z"></path> <path fill="#ffffff" fill-opacity="0.33" d="M240,24V2.21c-51.797,0-69.883,11.96-94.707,14.16
c-24.691,2.149-43.872-1.08-63.765-1.021c-19.17,0.069-31.196,3.311-51.309,5.971C15.552,23.23,4.321,22.58,0,22.189V24h239.766H240
z"></path> <path fill="#ffffff" d="M240,24V3.72c-51.797,0-69.883,11.64-94.707,14.021c-24.691,2.359-43.872-3.25-63.765-3.17
c-19.17,0.109-31.196,3.6-51.309,6.529C15.552,23.209,4.321,22.47,0,22.029V24H240z"></path> </svg></div>';
    return $output;
}
function dtdividerbot_func( $atts ){
    $output = '<div class="uncode-divider-wrap uncode-divider-wrap-bottom z_index_0 uncode-divider-relative uncode-divider-flip" style="height: 300px;" data-height="300" data-unit="px"><svg version="1.1" class="uncode-row-divider uncode-row-divider-swoosh-opacity" x="0px" y="0px" width="240px" height="24px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none"> <path fill="#ffffff" fill-opacity="0.33" d="M240,24V0c-51.797,0-69.883,13.18-94.707,15.59c-24.691,2.4-43.872-1.17-63.765-1.08
c-19.17,0.1-31.196,3.65-51.309,6.58C15.552,23.21,4.321,22.471,0,22.01V24H240z"></path> <path fill="#ffffff" fill-opacity="0.33" d="M240,24V2.21c-51.797,0-69.883,11.96-94.707,14.16
c-24.691,2.149-43.872-1.08-63.765-1.021c-19.17,0.069-31.196,3.311-51.309,5.971C15.552,23.23,4.321,22.58,0,22.189V24h239.766H240
z"></path> <path fill="#ffffff" d="M240,24V3.72c-51.797,0-69.883,11.64-94.707,14.021c-24.691,2.359-43.872-3.25-63.765-3.17
c-19.17,0.109-31.196,3.6-51.309,6.529C15.552,23.209,4.321,22.47,0,22.029V24H240z"></path> </svg></div>';
    return $output;
}
add_shortcode( 'dtdividertop', 'dtdividertop_func' );
add_shortcode( 'dtdividerbottom', 'dtdividerbot_func' );

if ( ! function_exists( 'cortex_mikado_is_wp_gutenberg_installed' ) ) {
    /**
     * Function that checks if WordPress 5.x with Gutenberg editor installed
     *
     * @return bool
     */
    function cortex_mikado_is_wp_gutenberg_installed() {
        return class_exists( 'WP_Block_Type' );
    }
}

add_filter( 'woocommerce_page_title', 'custom_woocommerce_page_title');
function custom_woocommerce_page_title( $page_title ) {
  //if( $page_title == 'Shop' ) {
    return "WooCommerce Demo Products";
  //}
}





// update 08-06-2020
add_action( 'manage_posts_extra_tablenav', 'admin_post_list_top_export_button', 20, 1 );
function admin_post_list_top_export_button( $which ) {
    global $typenow;

    if ( 'product' === $typenow && 'top' === $which ) {
        ?>
        <input type="submit" name="dorset_product_export" id="dorset_product_export" class="button button-primary" value="Export All Products" />
        <?php
    }
}



//export custom csv products
add_action( 'init', 'dorset_export_csv' );
function dorset_export_csv() {
    if(isset($_GET['dorset_product_export'])) {
        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        $query = new WP_Query( $args );

        if ( $query->have_posts() ) {

            header('Content-type: text/csv; charset=utf-8; encoding=utf-8');
            header('Content-Disposition: attachment; filename="'.time().'.csv"');
            header('Pragma: no-cache');
            header('Expires: 0');
            $file = fopen('php://output', 'w');

            fputs( $file, "\xEF\xBB\xBF" );

            fputcsv($file, array('Product Name', 'Product Description', 'Product SKU', 'Quantity', 'MSRP', 'Selling Price', 'Image URL 1', 'Image URL 2', 'Image URL 3', 'Image URL 4', 'Image URL 5', 'Image URL 6', 'Image URL 7'));
            $currency_sym = '£';

            while ( $query->have_posts() ) {
                $query->the_post();
                $product_id = get_the_ID();

                $product = wc_get_product($product_id);
                $image_id  = $product->get_image_id();
                $image_url = wp_get_attachment_image_url( $image_id, 'full' );

                if($product->get_regular_price() != '') {
                    $reg_price = $currency_sym.round(($product->get_regular_price()*1.1), 2);
                }

                if($product->get_sale_price() != '') {
                    $sale_price = $currency_sym.round($product->get_sale_price(),2);
                }

                $gallery_ids = $product->get_gallery_image_ids();

                for ($i=0; $i < 6; $i++) {
                    ${"img_count".$i} = "";
                }

                if(sizeof($gallery_ids) > 0) {
                    for ($i=0; $i < sizeof($gallery_ids); $i++) {
                        if($i<6) {
                            ${"img_count".$i} = wp_get_attachment_url($gallery_ids[$i]);
                        } else {
                            break;
                        }
                    }
                }
                if(($gallery_ids > 0 || $image_url != '') && $product->get_stock_quantity() > 0) {
                    fputcsv($file, array($product->get_name(), $product->get_description(), $product->get_sku(), $product->get_stock_quantity(), $reg_price, $sale_price, $image_url, $img_count0, $img_count1, $img_count2, $img_count3, $img_count4,$img_count5 ));
                }

            }

        }
        exit();

    }

}

// add_action('admin_init','test_mail_abc' );
function test_mail_abc(){
    $ret = array();
    // if ( ! $this->credentials_configured() ) {
    //     return false;
    // }

    global $wp_version;

    if ( version_compare( $wp_version, '5.4.99' ) > 0 ) {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        require_once ABSPATH . WPINC . '/PHPMailer/SMTP.php';
        require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';
        $mail = new PHPMailer( );
    } else {
        require_once ABSPATH . WPINC . '/class-phpmailer.php';
        $mail = new \PHPMailer(  );
    }
    $smtpopts        = get_option( 'swpsmtp_options' );
    print "<pre>";
    print_r($smtpopts);
    print "</pre>";

    // try {

    //     $charset       = get_bloginfo( 'charset' );
    //     $mail->CharSet = $charset;

    //     $from_name  = $this->opts['from_name_field'];
    //     $from_email = $this->opts['from_email_field'];

    //     $mail->IsSMTP();

    //     // send plain text test email
    //     $mail->ContentType = 'text/plain';
    //     $mail->IsHTML( false );

    //     /* If using smtp auth, set the username & password */
    //     if ( 'yes' === $this->opts['smtp_settings']['autentication'] ) {
    //         $mail->SMTPAuth = true;
    //         $mail->Username = $this->opts['smtp_settings']['username'];
    //         $mail->Password = $this->get_password();
    //     }

    //     /* Set the SMTPSecure value, if set to none, leave this blank */
    //     if ( 'none' !== $this->opts['smtp_settings']['type_encryption'] ) {
    //         $mail->SMTPSecure = $this->opts['smtp_settings']['type_encryption'];
    //     }

    //     /* PHPMailer 5.2.10 introduced this option. However, this might cause issues if the server is advertising TLS with an invalid certificate. */
    //     $mail->SMTPAutoTLS = false;

    //     if ( isset( $this->opts['smtp_settings']['insecure_ssl'] ) && false !== $this->opts['smtp_settings']['insecure_ssl'] ) {
    //         // Insecure SSL option enabled
    //         $mail->SMTPOptions = array(
    //             'ssl' => array(
    //                 'verify_peer'       => false,
    //                 'verify_peer_name'  => false,
    //                 'allow_self_signed' => true,
    //             ),
    //         );
    //     }

    //     /* Set the other options */
    //     $mail->Host = $this->opts['smtp_settings']['host'];
    //     $mail->Port = $this->opts['smtp_settings']['port'];

    //     //Add reply-to if set in settings.
    //     if ( ! empty( $this->opts['reply_to_email'] ) ) {
    //         $mail->AddReplyTo( $this->opts['reply_to_email'], $from_name );
    //     }

    //     //Add BCC if set in settings.
    //     if ( ! empty( $this->opts['bcc_email'] ) ) {
    //         $bcc_emails = explode( ',', $this->opts['bcc_email'] );
    //         foreach ( $bcc_emails as $bcc_email ) {
    //             $bcc_email = trim( $bcc_email );
    //             $mail->AddBcc( $bcc_email );
    //         }
    //     }

    //     $mail->SetFrom( $from_email, $from_name );
    //     //This should set Return-Path header for servers that are not properly handling it, but needs testing first
    //     //$mail->Sender		 = $mail->From;
    //     $mail->Subject = $subject;
    //     $mail->Body    = $message;
    //     $mail->AddAddress( $to_email );
    //     global $debug_msg;
    //     $debug_msg         = '';
    //     $mail->Debugoutput = function ( $str, $level ) {
    //         global $debug_msg;
    //         $debug_msg .= $str;
    //     };
    //     $mail->SMTPDebug   = 1;
    //     //set reasonable timeout
    //     $mail->Timeout = 10;

    //     /* Send mail and return result */
    //     $mail->Send();
    //     $mail->ClearAddresses();
    //     $mail->ClearAllRecipients();
    // } catch ( \Exception $e ) {
    //     $ret['error'] = $mail->ErrorInfo;
    // } catch ( \Throwable $e ) {
    //     $ret['error'] = $mail->ErrorInfo;
    // }

    // $ret['debug_log'] = $debug_msg;

    // return $ret;
}

add_action ( 'wp_head', 'custom_font_dorsettech', 99999 );
function custom_font_dorsettech() { ?>
    <link rel="stylesheet" href="https://use.typekit.net/ure1jwx.css">
<?php }