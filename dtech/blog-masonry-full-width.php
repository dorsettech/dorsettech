<?php
	/*
	Template Name: Blog: Masonry Full Width
	*/
?>
<?php get_header(); ?>

<?php cortex_mikado_get_title(); ?>
<?php get_template_part('slider'); ?>

	<div class="mkdf-full-width">
		<div class="mkdf-full-width-inner clearfix">
			<?php cortex_mikado_get_blog('masonry-full-width'); ?>
		</div>
	</div>
	<?php do_action('cortex_mikado_after_full_width_container_close') ?>
<?php get_footer(); ?>