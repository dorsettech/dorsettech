<?php $cortex_mikado_sidebar = cortex_mikado_sidebar_layout(); ?>
<?php get_header(); ?>
<?php 

$cortex_mikado_blog_page_range = cortex_mikado_get_blog_page_range();
$cortex_mikado_max_number_of_pages = cortex_mikado_get_max_number_of_pages();

if ( get_query_var('paged') ) { $cortex_mikado_paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $cortex_mikado_paged = get_query_var('page'); }
else { $cortex_mikado_paged = 1; }
?>
<?php cortex_mikado_get_title(); ?>
	<div class="mkdf-container">
		<?php do_action('cortex_mikado_after_container_open'); ?>
		<div class="mkdf-container-inner clearfix">
			<div class="mkdf-container">
				<?php do_action('cortex_mikado_after_container_open'); ?>
				<div class="mkdf-container-inner" >
					<div class="mkdf-blog-holder mkdf-blog-type-standard mkdf-search-page">
				<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="mkdf-post-content">
							<div class="mkdf-post-text">
								<div class="mkdf-post-text-inner">
									<h4>
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
									</h4>
									<?php
									if (shortcode_exists('mkdf_button')) {
										echo cortex_mikado_get_button_html(array(
											'size' => 'small',
											'link'         => get_the_permalink(),
											'text'         => esc_html__('Read More', 'cortex'),
										));
									} else { ?>
										<a href="<?php echo get_the_permalink();?>" target="_self" class="mkdf-btn mkdf-btn-small mkdf-btn-solid mkdf-btn-bckg-hover">
                                            <span class="mkdf-btn-top-shadow"></span>
											<span class="mkdf-btn-text"><?php esc_html_e('Read More', 'cortex');?></span>
                                            <span class="mkdf-btn-bottom-shadow"></span>
										</a>
									<?php } ?>
								</div>
							</div>
						</div>
					</article>
					<?php endwhile; ?>
					<?php
						if(cortex_mikado_options()->getOptionValue('pagination') == 'yes') {
							cortex_mikado_pagination($cortex_mikado_max_number_of_pages, $cortex_mikado_blog_page_range, $cortex_mikado_paged);
						}
					?>
					<?php else: ?>
					<div class="entry">
						<p><?php esc_html_e('No posts were found.', 'cortex'); ?></p>
					</div>
					<?php endif; ?>
				</div>
				<?php do_action('cortex_mikado_before_container_close'); ?>
			</div>
			</div>
		</div>
		<?php do_action('cortex_mikado_before_container_close'); ?>
	</div>
	<?php do_action('cortex_mikado_after_container_close'); ?>
<?php get_footer(); ?>