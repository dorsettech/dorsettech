<form method="get" id="searchform" action="<?php echo esc_url(home_url( '/' )); ?>">
	 <div class="mkdf-search-wrapper">
		<input type="text" value="" placeholder="<?php esc_html_e('Search', 'cortex'); ?>" name="s" id="s" />
		<input type="submit" id="searchsubmit" value="&#x55;" />
	</div>
</form>