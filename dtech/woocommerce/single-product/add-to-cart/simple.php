<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

$mikado_product_id = '';
$mikado_product_min_purchase = 1;
$mikado_product_max_purchase = '';

$mikado_product_id = $product->get_id();
$mikado_product_min_purchase = $product->get_min_purchase_quantity();
$mikado_product_max_purchase = $product->get_max_purchase_quantity();
?>

<?php

echo wc_get_stock_html( $product );
?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

			woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $mikado_product_min_purchase, $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $mikado_product_max_purchase, $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $mikado_product_min_purchase,
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
	 	?>

	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $mikado_product_id ); ?>" />

		<?php
			//Overriden add to cart button
			do_action('cortex_mikado_woocommerce_add_to_cart_button');
		?>

		<?php 
			/**
			 * @since 2.1.0.
			 */
			do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>