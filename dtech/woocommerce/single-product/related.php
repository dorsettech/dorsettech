<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	if ( $related_products ) : ?>

		<section class="related products">

			<div class="mkdf-related-products-title-holder">
				<h5><?php esc_html_e( 'Related Products', 'cortex' ); ?></h5>
				<div class="mkdf-related-nav-holder">
					<span class="mkdf-related-prev"><span class="lnr lnr-chevron-left"></span></span>
					<span class="mkdf-related-glob"><a href="<?php echo get_permalink( get_option( 'woocommerce_shop_page_id' ) ); ?>"><span class="icon_grid-3x3"></span></a></span>
					<span class="mkdf-related-next"><span class="lnr lnr-chevron-right"></span></span>
				</div>
			</div>

			<?php woocommerce_product_loop_start(); ?>

				<?php foreach ( $related_products as $related_product ) : ?>

					<?php
					 	$post_object = get_post( $related_product->get_id() );

						setup_postdata( $GLOBALS['post'] =& $post_object );

						wc_get_template_part( 'content', 'product' ); ?>

				<?php endforeach; ?>

			<?php woocommerce_product_loop_end(); ?>

		</section>

	<?php endif;

	wp_reset_postdata();