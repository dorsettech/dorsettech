<?php get_header(); ?>
	<div class="mkdf-container">
	<?php do_action('cortex_mikado_after_container_open'); ?>
		<div class="mkdf-container-inner mkdf-404-page">
<div style="text-align:center;">
                <img src="https://dorset.tech/wp-content/uploads/shhh-clipart-quiet-man.gif"/>
                </div>
			<div class="mkdf-page-not-found-part">
				<div class="mkdf-404-text"><?php esc_html_e('404', 'cortex'); ?></div>
			</div>

			<div class="mkdf-page-not-found">
				<h1>
					<?php if(cortex_mikado_options()->getOptionValue('404_title')){
						echo esc_html(cortex_mikado_options()->getOptionValue('404_title'));
					}
					else{
						esc_html_e('Page cannot be found.', 'cortex');
					} ?>
				</h1>
				<p>
					<?php if(cortex_mikado_options()->getOptionValue('404_text')){
						echo esc_html(cortex_mikado_options()->getOptionValue('404_text'));
					}
					else{
						esc_html_e('The page you are looking for does not exist. It may have been moved, or removed altogether. Perhaps you can return back to the site\'s homepage and see if you can find what you are looking for.', 'cortex');
					} ?>
				</p>
				<?php
					$params = array();
					if (cortex_mikado_options()->getOptionValue('404_back_to_home')){
						$params['text'] = cortex_mikado_options()->getOptionValue('404_back_to_home');
					}
					else{
						$params['text'] = "Back to Home";
					}
					$params['link'] = esc_url(home_url('/'));
					$params['target'] = '_self';

				if (shortcode_exists('mkdf_button')) {
				echo cortex_mikado_execute_shortcode('mkdf_button',$params);
				} else { ?>
					<a href="<?php echo esc_url(home_url('/'))?>" target="_self" class="mkdf-btn mkdf-btn-large mkdf-btn-solid mkdf-btn-bckg-hover">
						<span class="mkdf-btn-top-shadow"></span>
						<span class="mkdf-btn-text"><?php esc_html_e("Back to Home",'cortex')?></span>
						<span class="mkdf-btn-bottom-shadow"></span>
					</a>
				<?php } ?>
			</div>
		</div>
		<?php do_action('cortex_mikado_before_container_close'); ?>
	</div>
<?php get_footer(); ?>