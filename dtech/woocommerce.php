<?php 
/*
Template Name: WooCommerce
*/ 
?>
<?php

$cortex_mikado_id = get_option('woocommerce_shop_page_id');
$cortex_mikado_shop = get_post($cortex_mikado_id);
$cortex_mikado_sidebar = cortex_mikado_sidebar_layout();

if(get_post_meta($cortex_mikado_id, 'mkd_page_background_color', true) != ''){
	$cortex_mikado_background_color = 'background-color: '.esc_attr(get_post_meta($cortex_mikado_id, 'mkd_page_background_color', true));
}else{
	$cortex_mikado_background_color = '';
}

$cortex_mikado_content_style = '';
if(get_post_meta($cortex_mikado_id, 'mkd_content-top-padding', true) != '') {
	if(get_post_meta($cortex_mikado_id, 'mkd_content-top-padding-mobile', true) == 'yes') {
		$cortex_mikado_content_style = 'padding-top:'.esc_attr(get_post_meta($cortex_mikado_id, 'mkd_content-top-padding', true)).'px !important';
	} else {
		$cortex_mikado_content_style = 'padding-top:'.esc_attr(get_post_meta($cortex_mikado_id, 'mkd_content-top-padding', true)).'px';
	}
}

if ( get_query_var('paged') ) {
	$cortex_mikado_paged = get_query_var('paged');
} elseif ( get_query_var('page') ) {
	$cortex_mikado_paged = get_query_var('page');
} else {
	$cortex_mikado_paged = 1;
}

get_header();

cortex_mikado_get_title();
get_template_part('slider');

$cortex_mikado_full_width = false;

if ( cortex_mikado_options()->getOptionValue('mkdf_woo_products_list_full_width') == 'yes' && !is_singular('product') ) {
	$cortex_mikado_full_width = true;
}

if ( $cortex_mikado_full_width ) { ?>
	<div class="mkdf-full-width" <?php cortex_mikado_inline_style($cortex_mikado_background_color); ?>>
		<div class="mkdf-full-width-inner" <?php cortex_mikado_inline_style($cortex_mikado_content_style); ?>>
<?php } else { ?>
	<div class="mkdf-container" <?php cortex_mikado_inline_style($cortex_mikado_background_color); ?>>
		<div class="mkdf-container-inner clearfix" <?php cortex_mikado_inline_style($cortex_mikado_content_style); ?>>
<?php }
			//Woocommerce content
			if ( ! is_singular('product') ) {

				switch( $cortex_mikado_sidebar ) {

					case 'sidebar-33-right': ?>
						<div class="mkdf-two-columns-66-33 grid2 mkdf-woocommerce-with-sidebar clearfix">
							<div class="mkdf-column1">
								<div class="mkdf-column-inner">
									<?php cortex_mikado_woocommerce_content(); ?>
								</div>
							</div>
							<div class="mkdf-column2">
								<?php get_sidebar();?>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-25-right': ?>
						<div class="mkdf-two-columns-75-25 grid2 mkdf-woocommerce-with-sidebar clearfix">
							<div class="mkdf-column2">
								<?php get_sidebar();?>
							</div>
							<div class="mkdf-column1 mkdf-content-left-from-sidebar">
								<div class="mkdf-column-inner">
									<?php cortex_mikado_woocommerce_content(); ?>
								</div>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-33-left': ?>
						<div class="mkdf-two-columns-33-66 grid2 mkdf-woocommerce-with-sidebar clearfix">
							<div class="mkdf-column1">
								<?php get_sidebar();?>
							</div>
							<div class="mkdf-column2">
								<div class="mkdf-column-inner">
									<?php cortex_mikado_woocommerce_content(); ?>
								</div>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-25-left': ?>
						<div class="mkdf-two-columns-25-75 grid2 mkdf-woocommerce-with-sidebar clearfix">
							<div class="mkdf-column1">
								<?php get_sidebar();?>
							</div>
							<div class="mkdf-column2 mkdf-content-right-from-sidebar">
								<div class="mkdf-column-inner">
									<?php cortex_mikado_woocommerce_content(); ?>
								</div>
							</div>
						</div>
					<?php
						break;
					default:
						cortex_mikado_woocommerce_content();
				}

			} else {
				cortex_mikado_woocommerce_content();
			} ?>

			</div>
	</div>
	<?php do_action('cortex_mikado_after_container_close'); ?>
<?php get_footer(); ?>