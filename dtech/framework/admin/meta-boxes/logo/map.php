<?php

if(!function_exists('cortex_mikado_map_logo_meta_fields')) {

	function cortex_mikado_map_logo_meta_fields() {
		$general_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('page', 'portfolio-item', 'post'),
		        'title' => esc_html__('Logo','cortex'),
		        'name' => 'logo_meta'
		    )
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_logo_image_meta',
				'type' => 'image',
				'label' => esc_html__('Logo Image - Default','cortex'),
				'description' => esc_html__('Choose a default logo image to display','cortex'),
				'parent' => $general_meta_box
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_logo_image_dark_meta',
				'type' => 'image',
				'label' => esc_html__('Logo Image - Dark','cortex'),
				'description' => esc_html__('Choose a dark logo image to display','cortex'),
				'parent' => $general_meta_box
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_logo_image_light_meta',
				'type' => 'image',
				'label' => esc_html__('Logo Image - Light','cortex'),
				'description' => esc_html__('Choose a light logo image to display','cortex'),
				'parent' => $general_meta_box
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_logo_image_sticky_meta',
				'type' => 'image',
				'label' => esc_html__('Logo Image - Sticky','cortex'),
				'description' => esc_html__('Choose a sticky logo image to display','cortex'),
				'parent' => $general_meta_box
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_logo_image_mobile_meta',
				'type' => 'image',
				'label' => esc_html__('Logo Image - Mobile','cortex'),
				'description' => esc_html__('Choose a mobile logo image to display','cortex'),
				'parent' => $general_meta_box
			)
		);
	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_logo_meta_fields');
}
