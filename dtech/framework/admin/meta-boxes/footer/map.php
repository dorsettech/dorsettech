<?php

if(!function_exists('cortex_mikado_map_footer_meta_fields')) {

	function cortex_mikado_map_footer_meta_fields() {

		$footer_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('page', 'portfolio-item', 'post'),
		        'title' => esc_html__('Footer', 'cortex'),
		        'name' => 'footer_meta'
		    )
		);

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name' => 'mkdf_disable_footer_meta',
	            'type' => 'yesno',
	            'default_value' => 'no',
	            'label' => esc_html__('Disable Footer for this Page', 'cortex'),
	            'description' => esc_html__('Enabling this option will hide footer on this page', 'cortex'),
	            'parent' => $footer_meta_box,
	        )
	    );
	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_footer_meta_fields');
}