<?php

if(!function_exists('cortex_mikado_map_portfolio_settings')) {
    function cortex_mikado_map_portfolio_settings() {
        $meta_box = cortex_mikado_add_meta_box(array(
            'scope' => 'portfolio-item',
            'title' => esc_html__('Portfolio Settings', 'cortex'),
            'name'  => 'portfolio_settings_meta_box',
        ));

        cortex_mikado_add_meta_box_field(array(
            'name'        => 'mkdf_portfolio_single_template_meta',
            'type'        => 'select',
            'label'       =>esc_html__('Portfolio Type', 'cortex'),
            'description' => esc_html__('Choose a default type for Single Project pages', 'cortex'),
            'parent'      => $meta_box,
            'options'     => array(
                ''                   => esc_html__('Default','cortex'),
                'small-images'       => esc_html__('Portfolio small images left','cortex'),
                'small-images-right' => esc_html__('Portfolio small images right','cortex'),
                'small-slider'       => esc_html__('Portfolio small slider left','cortex'),
                'small-slider-right' => esc_html__('Portfolio small slider right','cortex'),
                'big-images'         => esc_html__('Portfolio big images','cortex'),
                'wide-images'        => esc_html__('Portfolio wide images left','cortex'),
                'wide-images-right'  => esc_html__('Portfolio wide images right','cortex'),
                'big-slider'         => esc_html__('Portfolio big slider','cortex'),
                'wide-slider'        => esc_html__('Portfolio wide slider','cortex'),
                'gallery'            => esc_html__('Portfolio gallery','cortex'),
                'small-masonry'      => esc_html__('Portfolio small masonry','cortex'),
                'big-masonry'        => esc_html__('Portfolio big masonry','cortex'),
                'split-screen'       => esc_html__('Portfolio split screen','cortex'),
                'full-screen-slider' => esc_html__('Portfolio full screen slider','cortex'),
                'custom'             => esc_html__('Portfolio custom','cortex'),
                'full-width-custom'  => esc_html__('Portfolio full width custom','cortex')
            )
        ));

        $all_pages = array();
        $pages     = get_pages();
        foreach($pages as $page) {
            $all_pages[$page->ID] = $page->post_title;
        }

        cortex_mikado_add_meta_box_field(array(
            'name'        => 'portfolio_single_back_to_link',
            'type'        => 'select',
            'label'       => esc_html__('"Back To" Link', 'cortex'),
            'description' => esc_html__('Choose "Back To" page to link from portfolio Single Project page', 'cortex'),
            'parent'      => $meta_box,
            'options'     => $all_pages
        ));

        cortex_mikado_add_meta_box_field(array(
            'name'        => 'portfolio_external_link',
            'type'        => 'text',
            'label'       => esc_html__('Portfolio External Link', 'cortex'),
            'description' => esc_html__('Enter URL to link from Portfolio List page', 'cortex'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        cortex_mikado_add_meta_box_field(array(
            'name'        => 'portfolio_masonry_dimenisions',
            'type'        => 'select',
            'label'       => esc_html__('Dimensions for Masonry', 'cortex'),
            'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists', 'cortex'),
            'parent'      => $meta_box,
            'options'     => array(
                'default'            => esc_html__('Default','cortex'),
                'large_width'        => esc_html__('Large width','cortex'),
                'large_height'       => esc_html__('Large height','cortex'),
                'large_width_height' => esc_html__('Large width/height','cortex'),
            )
        ));
    }

    add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_portfolio_settings');
}