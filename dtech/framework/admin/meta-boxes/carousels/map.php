<?php

//Carousels

if(!function_exists('cortex_mikado_map_carousel_meta_fields')) {

	function cortex_mikado_map_carousel_meta_fields() {

		$carousel_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('carousels'),
		        'title' => esc_html__('Carousel', 'cortex'),
		        'name' => 'carousel_meta',
		    )
		);

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        => 'mkdf_carousel_image',
	            'type'        => 'image',
	            'label'       => esc_html__('Carousel Image', 'cortex'),
	            'description' => esc_html__('Choose carousel image (min width needs to be 215px)', 'cortex'),
	            'parent'      => $carousel_meta_box
	        )
	    );

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        => 'mkdf_carousel_hover_image',
	            'type'        => 'image',
	            'label'       => esc_html__('Carousel Hover Image', 'cortex'),
	            'description' => esc_html__('Choose carousel hover image (min width needs to be 215px)', 'cortex'),
	            'parent'      => $carousel_meta_box
	        )
	    );

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        => 'mkdf_carousel_item_link',
	            'type'        => 'text',
	            'label'       => esc_html__('Link', 'cortex'),
	            'description' => esc_html__('Enter the URL to which you want the image to link to (e.g. http://www.example.com)', 'cortex'),
	            'parent'      => $carousel_meta_box
	        )
	    );

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        => 'mkdf_carousel_item_target',
	            'type'        => 'selectblank',
	            'label'       => esc_html__('Target', 'cortex'),
	            'description' => esc_html__('Specify where to open the linked document', 'cortex'),
	            'parent'      => $carousel_meta_box,
	            'options' => array(
	            	'_self' => esc_html__('Self','cortex'),
	            	'_blank' => esc_html__('Blank','cortex')
	        	)
	        )
	    );
	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_carousel_meta_fields');
}