<?php

//Testimonials

if(!function_exists('cortex_mikado_map_testimonials_meta_fields')) {

	function cortex_mikado_map_testimonials_meta_fields() {
		$testimonial_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('testimonials'),
		        'title' => esc_html__('Testimonial', 'cortex'),
		        'name' => 'testimonial_meta',
		    )
		);

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        	=> 'mkdf_testimonial_title',
	            'type'        	=> 'text',
	            'label'       	=> esc_html__('Title', 'cortex'),
	            'description' 	=> esc_html__('Enter testimonial title', 'cortex'),
	            'parent'      	=> $testimonial_meta_box,
	        )
	    );


	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        	=> 'mkdf_testimonial_author',
	            'type'        	=> 'text',
	            'label'       	=> esc_html__('Author', 'cortex'),
	            'description' 	=> esc_html__('Enter author name', 'cortex'),
	            'parent'      	=> $testimonial_meta_box,
	        )
	    );

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        	=> 'mkdf_testimonial_author_position',
	            'type'        	=> 'text',
	            'label'       	=> esc_html__('Job Position', 'cortex'),
	            'description' 	=> esc_html__('Enter job position', 'cortex'),
	            'parent'      	=> $testimonial_meta_box,
	        )
	    );

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        	=> 'mkdf_testimonial_text',
	            'type'        	=> 'text',
	            'label'       	=> esc_html__('Text', 'cortex'),
	            'description' 	=> esc_html__('Enter testimonial text', 'cortex'),
	            'parent'      	=> $testimonial_meta_box,
	        )
	    );
	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_testimonials_meta_fields');
}