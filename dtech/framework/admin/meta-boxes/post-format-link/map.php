<?php

/*** Link Post Format ***/

if(!function_exists('cortex_mikado_map_post_link_meta_fields')) {

	function cortex_mikado_map_post_link_meta_fields() {
		$link_post_format_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' => array('post'),
				'title' => esc_html__('Link Post Format', 'cortex'),
				'name' => 'post_format_link_meta',
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_link_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Link', 'cortex'),
				'description' => esc_html__('Enter link', 'cortex'),
				'parent'      => $link_post_format_meta_box,

			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_link_link_text_meta',
				'type'        => 'text',
				'label'       => esc_html__('Link Text', 'cortex'),
				'description' => esc_html__('Enter link text', 'cortex'),
				'parent'      => $link_post_format_meta_box,

			)
		);

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_post_link_meta_fields');
}