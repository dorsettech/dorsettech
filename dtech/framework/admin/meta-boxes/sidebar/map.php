<?php

if(!function_exists('cortex_mikado_map_sidebar_meta_fields')) {

	function cortex_mikado_map_sidebar_meta_fields() {
		$cortex_mikado_custom_sidebars = cortex_mikado_get_custom_sidebars();

		$cortex_mikado_sidebar_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('page', 'portfolio-item', 'post'),
		        'title' => esc_html__('Sidebar', 'cortex'),
		        'name' => 'sidebar_meta',
		    )
		);

		    cortex_mikado_add_meta_box_field(
		        array(
		            'name'        => 'mkdf_sidebar_meta',
		            'type'        => 'select',
		            'label'       => esc_html__('Layout', 'cortex'),
		            'description' => esc_html__('Choose the sidebar layout', 'cortex'),
		            'parent'      => $cortex_mikado_sidebar_meta_box,
		            'options'     => array(
								''			=> 'Default',
								'no-sidebar'		=> esc_html__('No Sidebar', 'cortex'),
								'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'cortex'),
								'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'cortex'),
								'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'cortex'),
								'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'cortex'),
							)
		        )
		    );

		if(count($cortex_mikado_custom_sidebars) > 0) {
		    cortex_mikado_add_meta_box_field(array(
		        'name' => 'mkdf_custom_sidebar_meta',
		        'type' => 'selectblank',
		        'label' => esc_html__('Choose Widget Area in Sidebar','cortex'),
		        'description' => esc_html__('Choose Custom Widget area to display in Sidebar"', 'cortex'),
		        'parent' => $cortex_mikado_sidebar_meta_box,
		        'options' => $cortex_mikado_custom_sidebars
		    ));
		}

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_sidebar_meta_fields');
}