<?php

/*** Audio Post Format ***/

if(!function_exists('cortex_mikado_map_post_audio_meta_fields')) {

	function cortex_mikado_map_post_audio_meta_fields() {
		$audio_post_format_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Audio Post Format', 'cortex'),
				'name' 	=> 'post_format_audio_meta',
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_audio_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Link', 'cortex'),
				'description' => esc_html__('Enter audion link', 'cortex'),
				'parent'      => $audio_post_format_meta_box,

			)
		);

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_post_audio_meta_fields');
}