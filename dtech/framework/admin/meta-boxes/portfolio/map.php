<?php

if(!function_exists('cortex_mikado_map_portfolio_meta_fields')) {

	function cortex_mikado_map_portfolio_meta_fields() {
		global $cortex_mikado_Framework;

		$mkd_pages = array();
		$pages = get_pages(); 
		foreach($pages as $page) {
			$mkd_pages[$page->ID] = $page->post_title;
		}

		//Portfolio Images

		$mkdPortfolioImages = new CortexMikadoMetaBox("portfolio-item", esc_html__("Portfolio Images (multiple upload)",'cortex'), '', '', 'portfolio_images');
		$cortex_mikado_Framework->mkdMetaBoxes->addMetaBox("portfolio_images",$mkdPortfolioImages);

			$mkd_portfolio_image_gallery = new CortexMikadoMultipleImages("mkd_portfolio-image-gallery", esc_html__("Portfolio Images",'cortex'), esc_html__("Choose your portfolio images",'cortex'));
			$mkdPortfolioImages->addChild("mkd_portfolio-image-gallery",$mkd_portfolio_image_gallery);

		//Portfolio Images/Videos 2

		$mkdPortfolioImagesVideos2 = new CortexMikadoMetaBox("portfolio-item", esc_html__("Portfolio Images/Videos (single upload)",'cortex'));
		$cortex_mikado_Framework->mkdMetaBoxes->addMetaBox("portfolio_images_videos2",$mkdPortfolioImagesVideos2);

			$mkd_portfolio_images_videos2 = new CortexMikadoImagesVideosFramework(esc_html__("Portfolio Images/Videos 2",'cortex'),esc_html__("ThisIsDescription",'cortex'));
			$mkdPortfolioImagesVideos2->addChild("mkd_portfolio_images_videos2",$mkd_portfolio_images_videos2);

		//Portfolio Additional Sidebar Items

		$mkdAdditionalSidebarItems = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('portfolio-item'),
		        'title' => esc_html__('Additional Portfolio Sidebar Items', 'cortex'),
		        'name' => 'portfolio_properties'
		    )
		);

		$mkd_portfolio_properties = cortex_mikado_add_options_framework(
		    array(
		        'label' => esc_html__('Portfolio Properties', 'cortex'),
		        'name' => 'mkd_portfolio_properties',
		        'parent' => $mkdAdditionalSidebarItems
		    )
		);

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_portfolio_meta_fields');
}