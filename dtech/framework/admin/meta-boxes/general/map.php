<?php

if(!function_exists('cortex_mikado_map_general_meta_fields')) {

	function cortex_mikado_map_general_meta_fields() {

		$general_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('page', 'portfolio-item', 'post'),
		        'title' => esc_html__('General', 'cortex'),
		        'name' => 'general_meta'
		    )
		);


	    cortex_mikado_add_meta_box_field(
	        array(
	            'name' => 'mkdf_page_background_color_meta',
	            'type' => 'color',
	            'default_value' => '',
	            'label' => esc_html__('Page Background Color', 'cortex'),
	            'description' => esc_html__('Choose background color for page content', 'cortex'),
	            'parent' => $general_meta_box
	        )
	    );
		
		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_page_padding_meta',
				'type' => 'text',
				'default_value' => '',
				'label' => esc_html__('Page Padding', 'cortex'),
				'description' => esc_html__('Insert padding in format 10px 10px 10px 10px', 'cortex'),
				'parent' => $general_meta_box
			)
		);

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name' => 'mkdf_page_slider_meta',
	            'type' => 'text',
	            'default_value' => '',
	            'label' => esc_html__('Slider Shortcode', 'cortex'),
	            'description' => esc_html__('Paste your slider shortcode here', 'cortex'),
	            'parent' => $general_meta_box
	        )
	    );

	    cortex_mikado_add_meta_box_field(
	    	array(
	    		'name' => 'mkdf_page_scroll_to_content_meta',
	    		'type' => 'select',
	    		'default_value' => '',
	    		'label' => esc_html__('One-Scroll To Page Content', 'cortex'),
	    		'description' => esc_html__('Enable this option will allow for direct scroll to content on first scroll.', 'cortex'),
	    		'options' => array(
	    			'no' => esc_html__('No','cortex'),
	    			'yes' => esc_html__('Yes','cortex')
	    		),
	    		'parent' => $general_meta_box
	    	)
	    );

		$boxed_option      = cortex_mikado_options()->getOptionValue('boxed');
		$boxed_default_dependency = array(
			'' => '#mkdf_boxed_container'
		);

		$boxed_show_array = array(
			'yes' => '#mkdf_boxed_container'
		);

		$boxed_hide_array = array(
			'no' => '#mkdf_boxed_container'
		);

		if($boxed_option === 'yes') {
			$boxed_show_array = array_merge($boxed_show_array, $boxed_default_dependency);
			$temp_boxed_no = array(
				'hidden_value' => 'no'
			);
		} else {
			$boxed_hide_array = array_merge($boxed_hide_array, $boxed_default_dependency);
			$temp_boxed_no = array(
				'hidden_values'   => array('','no')
			);
		}

		cortex_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_boxed_meta',
				'type'          => 'select',
				'label'         => esc_html__('Boxed Layout', 'cortex'),
				'parent'        => $general_meta_box,
				'default_value' => '',
				'options'     => array(
					''		=> esc_html__('Default', 'cortex'),
					'yes'	=> esc_html__('Yes', 'cortex'),
					'no'	=> esc_html__('No', 'cortex')
				),
				'args' => array(
					"dependence" => true,
					'show'       => $boxed_show_array,
					'hide'       => $boxed_hide_array
				)
			)
		);

		$boxed_container = cortex_mikado_add_admin_container_no_style(
			array_merge(
				array(
					'parent'            => $general_meta_box,
					'name'              => 'boxed_container',
					'hidden_property'   => 'mkdf_boxed_meta'
				),
				$temp_boxed_no
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_page_background_color_in_box_meta',
				'type'          => 'color',
				'label'         => esc_html__('Page Background Color', 'cortex'),
				'description'   => esc_html__('Choose the page background color outside box.', 'cortex'),
				'parent'        => $boxed_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_boxed_background_image_meta',
				'type'          => 'image',
				'label'         => esc_html__('Background Image', 'cortex'),
				'description'   => esc_html__('Choose an image to be displayed in background', 'cortex'),
				'parent'        => $boxed_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_boxed_background_image_repeating_meta',
				'type'          => 'select',
				'default_value' => 'no',
				'label'         => esc_html__('Use Background Image as Pattern', 'cortex'),
				'description'   => esc_html__('Set this option to "yes" to use the background image as repeating pattern', 'cortex'),
				'parent'        => $boxed_container,
				'options'       => array(
					'no'	=>	esc_html__('No', 'cortex'),
					'yes'	=>	esc_html__('Yes', 'cortex')

				)
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_boxed_background_image_attachment_meta',
				'type'          => 'select',
				'default_value' => 'fixed',
				'label'         => esc_html__('Background Image Behaviour', 'cortex'),
				'description'   => esc_html__('Choose background image behaviour', 'cortex'),
				'parent'        => $boxed_container,
				'options'       => array(
					'fixed'     => esc_html__('Fixed', 'cortex'),
					'scroll'    => esc_html__('Scroll', 'cortex')
				)
			)
		);
	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_general_meta_fields');
}