<?php

if(!function_exists('cortex_mikado_map_header_meta_fields')) {

	function cortex_mikado_map_header_meta_fields() {
		$cortex_mikado_custom_sidebars = cortex_mikado_get_custom_sidebars();

		$header_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('page', 'portfolio-item', 'post'),
		        'title' => esc_html__('Header', 'cortex'),
		        'name' => 'header_meta'
		    )
		);

		$temp_holder_show		= '';
		$temp_holder_hide		= '';
		$temp_array_standard	= array();
		$temp_array_vertical	= array();
		$temp_array_full_screen	= array();
		$temp_array_behaviour	= array();
		switch (cortex_mikado_options()->getOptionValue('header_type')) {

			case 'header-standard':
				$temp_holder_show = '#mkdf_mkdf_header_standard_type_meta_container,#mkdf_mkdf_header_behaviour_meta_container';
				$temp_holder_hide = '#mkdf_mkdf_header_vertical_type_meta_container,#mkdf_mkdf_header_full_screen_type_meta_container,#mkdf_mkdf_header_centered_type_meta_container';

				$temp_array_standard = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-vertical','header-full-screen','header-centered')
				);

				$temp_array_centered = array(
					'hidden_values' => array('','header-standard','header-vertical','header-full-screen')
				);

				$temp_array_vertical = array(
					'hidden_values' => array('','header-standard','header-full-screen','header-centered')
				);
				$temp_array_full_screen = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'header-vertical'
				);
				break;

			case 'header-centered':
				$temp_holder_show = '#mkdf_mkdf_header_centered_type_meta_container,#mkdf_mkdf_header_behaviour_meta_container';
				$temp_holder_hide = '#mkdf_mkdf_header_vertical_type_meta_container,#mkdf_mkdf_header_full_screen_type_meta_container,#mkdf_mkdf_header_standard_type_meta_container';

				$temp_array_standard = array(
					'hidden_values' => array('','header-vertical','header-full-screen','header-centered')
				);
				$temp_array_centered = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-standard','header-vertical','header-full-screen')
				);
				$temp_array_vertical = array(
					'hidden_values' => array('','header-standard','header-full-screen','header-centered')
				);
				$temp_array_full_screen = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'header-vertical'
				);
				break;

			case 'header-vertical':
				$temp_holder_show = '#mkdf_mkdf_header_vertical_type_meta_container';
				$temp_holder_hide = '#mkdf_mkdf_header_standard_type_meta_container,#mkdf_mkdf_header_full_screen_type_meta_container,#mkdf_mkdf_header_behaviour_meta_container,#mkdf_mkdf_header_centered_type_meta_container';

				$temp_array_standard = array(
					'hidden_values' => array('', 'header-vertical', 'header-full-screen','header-centered')
				);
				$temp_array_centered = array(
					'hidden_values' => array('','header-standard','header-vertical','header-full-screen')
				);
				$temp_array_vertical = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-standard','header-full-screen','header-centered')
				);
				$temp_array_full_screen = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'default',
					'hidden_values' => array('','header-vertical')
				);
				break;
			case 'header-full-screen':
				$temp_holder_show = '#mkdf_mkdf_header_full_screen_type_meta_container,#mkdf_mkdf_header_behaviour_meta_container';
				$temp_holder_hide = '#mkdf_mkdf_header_standard_type_meta_container,#mkdf_mkdf_header_vertical_type_meta_container,#mkdf_mkdf_header_centered_type_meta_container';
				$temp_array_standard = array(
					'hidden_values' => array('', 'header-vertical', 'header-standard','header-centered')
				);

				$temp_array_centered = array(
					'hidden_values' => array('','header-standard','header-vertical','header-full-screen')
				);

				$temp_array_vertical = array(
					'hidden_values' => array('', 'header-standard','header-full-screen','header-centered')
				);

				$temp_array_full_screen = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-vertical','header-full-screen','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'header-vertical'
				);
				break;
		}



		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_header_type_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Choose Header Type', 'cortex'),
				'description' => esc_html__('Select header type layout', 'cortex'),
				'parent' => $header_meta_box,
				'options' => array(
					'' => esc_html__('Default','cortex'),
					'header-standard' => esc_html__('Standard Header Layout','cortex'),
					'header-centered' => esc_html__('Centered Header Layout','cortex'),
					'header-vertical' => esc_html__('Vertical Header Layout','cortex'),
					'header-full-screen' => esc_html__('Full Screen Header Layout','cortex')
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"" => $temp_holder_hide,
						'header-standard' 		=> '#mkdf_mkdf_header_vertical_type_meta_container,#mkdf_mkdf_header_full_screen_type_meta_container,#mkdf_mkdf_header_centered_type_meta_container',
						'header-centered' 		=> '#mkdf_mkdf_header_vertical_type_meta_container,#mkdf_mkdf_header_full_screen_type_meta_container,#mkdf_mkdf_header_standard_type_meta_container',
						'header-vertical' 		=> '#mkdf_mkdf_header_standard_type_meta_container,#mkdf_mkdf_header_full_screen_type_meta_container,#mkdf_mkdf_header_centered_type_meta_container',
						'header-full-screen'	=> '#mkdf_mkdf_header_standard_type_meta_container,#mkdf_mkdf_header_vertical_type_meta_container,#mkdf_mkdf_header_centered_type_meta_container'
					),
					"show" => array(
						"" => $temp_holder_show,
						"header-standard" 		=> '#mkdf_mkdf_header_standard_type_meta_container',
						"header-centered" 		=> '#mkdf_mkdf_header_centered_type_meta_container',
						"header-vertical" 		=> '#mkdf_mkdf_header_vertical_type_meta_container',
						"header-full-screen" 	=> '#mkdf_mkdf_header_full_screen_type_meta_container'
					)
				)
			)
		);
		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_header_style_meta',
		        'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Header Skin', 'cortex'),
				'description' => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'cortex'),
				'parent' => $header_meta_box,
				'options' => array(
					'' => '',
					'light-header' => esc_html__('Light','cortex'),
					'dark-header' => esc_html__('Dark','cortex')
				)
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'parent' => $header_meta_box,
				'type' => 'select',
				'name' => 'mkdf_enable_header_style_on_scroll_meta',
				'default_value' => '',
				'label' => esc_html__('Enable Header Style on Scroll', 'cortex'),
				'description' => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style', 'cortex'),
				'options' => array(
					'' => '',
					'no' => esc_html__('No','cortex'),
					'yes' => esc_html__('Yes','cortex')
				)
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_disable_header_widget_area_meta',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Disable Header Widget Area', 'cortex'),
				'description' => esc_html__('Enabling this option will hide widget area from the right hand side of main menu', 'cortex'),
				'parent' => $header_meta_box
			)
		);

		if(count($cortex_mikado_custom_sidebars) > 0) {
			cortex_mikado_add_meta_box_field(array(
				'name' => 'mkdf_custom_header_widget_meta',
				'type' => 'selectblank',
				'label' => esc_html__('Choose Custom Widget Area in Header', 'cortex'),
				'description' => esc_html__('Choose custom widget area to display in header area from the right hand side of main menu"', 'cortex'),
				'parent' => $header_meta_box,
				'options' => $cortex_mikado_custom_sidebars
			));
		}
		if(in_array(cortex_mikado_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
			cortex_mikado_add_meta_box_field(
				array(
					'name' => 'mkdf_disable_sticky_header_widget_area_meta',
					'type' => 'yesno',
					'default_value' => 'no',
					'label' => esc_html__('Disable Sticky Header Widget Area', 'cortex'),
					'description' => esc_html__('Enabling this option will hide widget area from the right hand side of main menu', 'cortex'),
					'parent' => $header_meta_box
				)
			);

			if(count($cortex_mikado_custom_sidebars) > 0) {
				cortex_mikado_add_meta_box_field(array(
					'name' => 'mkdf_custom_sticky_header_widget_meta',
					'type' => 'selectblank',
					'label' => esc_html__('Choose Custom Widget Area in Sticky Header', 'cortex'),
					'description' => esc_html__('Choose custom widget area to display in header area from the right hand side of main menu"', 'cortex'),
					'parent' => $header_meta_box,
					'options' => $cortex_mikado_custom_sidebars
				));
			}
		}

		$header_standard_type_meta_container = cortex_mikado_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'mkdf_header_standard_type_meta_container',
					'hidden_property' => 'mkdf_header_type_meta',

				),
				$temp_array_standard
			)
		);

        cortex_mikado_add_meta_box_field(
            array(
                'name' => 'mkdf_set_menu_area_position_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Choose Menu Area Position', 'cortex'),
                'description' => esc_html__('Select menu area position in your header', 'cortex'),
                'parent' => $header_standard_type_meta_container,
                'options' => array(
                    '' => esc_html__('Default', 'cortex'),
                    'right' => esc_html__('Right', 'cortex'),
                    'left' => esc_html__('Left', 'cortex'),
                )
            )
        );

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_background_color_header_standard_meta',
				'type' => 'color',
				'label' => esc_html__('Background Color', 'cortex'),
				'description' => esc_html__('Choose a background color for header area', 'cortex'),
				'parent' => $header_standard_type_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_background_transparency_header_standard_meta',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'cortex'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'cortex'),
				'parent' => $header_standard_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_border_bottom_color_header_standard_meta',
				'type' => 'color',
				'label' => esc_html__('Border Bottom Color', 'cortex'),
				'description' => esc_html__('Choose a border bottom color for header area', 'cortex'),
				'parent' => $header_standard_type_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_border_bottom_transparency_header_standard_meta',
				'type' => 'text',
				'label' => esc_html__('Border Bottom Transparency', 'cortex'),
				'description' => esc_html__('Choose a transparency for the header border bottom color (0 = fully transparent, 1 = opaque)', 'cortex'),
				'parent' => $header_standard_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		$header_centered_type_meta_container = cortex_mikado_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'mkdf_header_centered_type_meta_container',
					'hidden_property' => 'mkdf_header_type_meta',

				),
				$temp_array_centered
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_background_color_header_centered_meta',
				'type' => 'color',
				'label' => esc_html__('Background Color', 'cortex'),
				'description' => esc_html__('Choose a background color for header area', 'cortex'),
				'parent' => $header_centered_type_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_background_transparency_header_centered_meta',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'cortex'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'cortex'),
				'parent' => $header_centered_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_border_bottom_color_header_centered_meta',
				'type' => 'color',
				'label' => esc_html__('Border Bottom Color', 'cortex'),
				'description' => esc_html__('Choose a border bottom color for header area', 'cortex'),
				'parent' => $header_centered_type_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_border_bottom_transparency_header_centered_meta',
				'type' => 'text',
				'label' => esc_html__('Border Bottom Transparency', 'cortex'),
				'description' => esc_html__('Choose a transparency for the header border bottom color (0 = fully transparent, 1 = opaque)', 'cortex'),
				'parent' => $header_centered_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		$header_vertical_type_meta_container = cortex_mikado_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' =>'mkdf_header_vertical_type_meta_container',
					'hidden_property' => 'mkdf_header_type_meta',
					'hidden_values' => array('header-standard')
				),
				$temp_array_vertical
			)
		);

		cortex_mikado_add_meta_box_field(array(
			'name'        => 'mkdf_vertical_header_background_color_meta',
			'type'        => 'color',
			'label'       => esc_html__('Background Color',  'cortex'),
			'parent'      => $header_vertical_type_meta_container
		));

		cortex_mikado_add_meta_box_field(array(
			'name'        => 'mkdf_vertical_header_transparency_meta',
			'type'        => 'text',
			'label'       => esc_html__('Background Transparency', 'cortex'),
			'description' => esc_html__('Enter transparency for vertical menu (value from 0 to 1)', 'cortex'),
			'parent'      => $header_vertical_type_meta_container,
			'args'        => array(
				'col_width' => 1
			)
		));

		cortex_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_vertical_header_background_image_meta',
				'type'          => 'image',
				'default_value' => '',
				'label'         => esc_html__('Background Image', 'cortex'),
				'description'   => esc_html__('Set background image for vertical menu', 'cortex'),
				'parent'        => $header_vertical_type_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_disable_vertical_header_background_image_meta',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Disable Background Image', 'cortex'),
				'description' => esc_html__('Enabling this option will hide background image in Vertical Menu', 'cortex'),
				'parent' => $header_vertical_type_meta_container
			)
		);

		$header_full_screen_type_meta_container = cortex_mikado_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'mkdf_header_full_screen_type_meta_container',
					'hidden_property' => 'mkdf_header_type_meta',

				),
				$temp_array_full_screen
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_background_color_header_full_screen_meta',
				'type' => 'color',
				'label' => esc_html__('Background Color', 'cortex'),
				'description' => esc_html__('Choose a background color for Full Screen header area', 'cortex'),
				'parent' => $header_full_screen_type_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_background_transparency_header_full_screen_meta',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'cortex'),
				'description' => esc_html__('Choose a transparency for the Full Screen header background color (0 = fully transparent, 1 = opaque)', 'cortex'),
				'parent' => $header_full_screen_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_border_bottom_color_header_full_screen_meta',
				'type' => 'color',
				'label' => esc_html__('Border Bottom Color', 'cortex'),
				'description' => esc_html__('Choose a border bottom color for Full Screen header area', 'cortex'),
				'parent' => $header_full_screen_type_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_menu_area_border_bottom_transparency_header_full_screen_meta',
				'type' => 'text',
				'label' => esc_html__('Border Bottom Transparency', 'cortex'),
				'description' => esc_html__('Choose a transparency for the Full Screen header border bottom color (0 = fully transparent, 1 = opaque)', 'cortex'),
				'parent' => $header_full_screen_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		$header_behaviour_meta_container = cortex_mikado_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'mkdf_header_behaviour_meta_container',
					'hidden_property' => 'mkdf_header_type_meta',

				),
				$temp_array_behaviour
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'            => 'mkdf_scroll_amount_for_sticky_meta',
				'type'            => 'text',
				'label'           => esc_html__('Scroll amount for sticky header appearance', 'cortex'),
				'description'     => esc_html__('Define scroll amount for sticky header appearance', 'cortex'),
				'parent'          => $header_behaviour_meta_container,
				'args'            => array(
					'col_width' => 2,
					'suffix'    => 'px'
				),
				'hidden_property' => 'mkdf_header_behaviour',
				'hidden_values'   => array("sticky-header-on-scroll-up", "fixed-on-scroll")
			)
		);


		cortex_mikado_add_admin_section_title(array(
			'name'   => 'top_bar_section_title',
			'parent' => $header_meta_box,
			'title'  => esc_html__('Top Bar','cortex')
		));

		$top_bar_global_option      = cortex_mikado_options()->getOptionValue('top_bar');
		$top_bar_default_dependency = array(
			'' => '#mkdf_top_bar_container_no_style'
		);

		$top_bar_show_array = array(
			'yes' => '#mkdf_top_bar_container_no_style'
		);

		$top_bar_hide_array = array(
			'no' => '#mkdf_top_bar_container_no_style'
		);

		if($top_bar_global_option === 'yes') {
			$top_bar_show_array = array_merge($top_bar_show_array, $top_bar_default_dependency);
			$temp_top_no = array(
				'hidden_value' => 'no'
			);
		} else {
			$top_bar_hide_array = array_merge($top_bar_hide_array, $top_bar_default_dependency);
			$temp_top_no = array(
				'hidden_values'   => array('','no')
			);
		}


		cortex_mikado_add_meta_box_field(array(
			'name'          => 'mkdf_top_bar_meta',
			'type'          => 'select',
			'label'         => esc_html__('Enable Top Bar on This Page', 'cortex'),
			'description'   => esc_html__('Enabling this option will enable top bar on this page', 'cortex'),
			'parent'        => $header_meta_box,
			'default_value' => '',
			'options'       => array(
				''    => esc_html__('Default','cortex'),
				'yes' => esc_html__('Yes','cortex'),
				'no'  => esc_html__('No','cortex')
			),
			'args' => array(
				"dependence" => true,
				'show'       => $top_bar_show_array,
				'hide'       => $top_bar_hide_array
			)
		));

		$top_bar_container = cortex_mikado_add_admin_container_no_style(array_merge(array(
			'name'            => 'top_bar_container_no_style',
			'parent'          => $header_meta_box,
			'hidden_property' => 'mkdf_top_bar_meta'
		),
			$temp_top_no));

		cortex_mikado_add_meta_box_field(array(
			'name'    => 'mkdf_top_bar_skin_meta',
			'type'    => 'select',
			'label'   => esc_html__('Top Bar Skin', 'cortex'),
			'options' => array(
				''      => esc_html__('Default','cortex'),
				'light' => esc_html__('Light','cortex'),
				'dark'  => esc_html__('Dark','cortex')
			),
			'parent'  => $top_bar_container
		));

		cortex_mikado_add_meta_box_field(array(
			'name'   => 'mkdf_top_bar_background_color_meta',
			'type'   => 'color',
			'label'  => esc_html__('Top Bar Background Color', 'cortex'),
			'parent' => $top_bar_container
		));

		cortex_mikado_add_meta_box_field(array(
			'name'        => 'mkdf_top_bar_background_transparency_meta',
			'label'       => esc_html__('Top Bar Background Color Transparency', 'cortex'),
			'description' => esc_html__('Set top bar background color transparenct. Value should be between 0 and 1', 'cortex'),
			'parent'      => $top_bar_container,
			'args'        => array(
				'col_width' => 3
			)
		));

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_header_meta_fields');
}