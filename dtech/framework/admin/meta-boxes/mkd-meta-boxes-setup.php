<?php

add_action('after_setup_theme', 'cortex_mikado_meta_boxes_map_init', 1);
function cortex_mikado_meta_boxes_map_init() {
    /**
    * Loades all meta-boxes by going through all folders that are placed directly in meta-boxes folder
    * and loads map.php file in each.
    *
    * @see http://php.net/manual/en/function.glob.php
    */

    do_action('cortex_mikado_before_meta_boxes_map');

	global $cortex_mikado_options;
	global $cortex_mikado_Framework;
	global $cortex_mikado_options_fontstyle;
	global $cortex_mikado_options_fontweight;
	global $cortex_mikado_options_texttransform;
	global $cortex_mikado_options_fontdecoration;
	global $cortex_mikado_options_arrows_type;

    foreach(glob(MIKADO_FRAMEWORK_ROOT_DIR.'/admin/meta-boxes/*/map.php') as $meta_box_load) {
        include_once $meta_box_load;
    }

	do_action('cortex_mikado_meta_boxes_map');

	do_action('cortex_mikado_after_meta_boxes_map');
}