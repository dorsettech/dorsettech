<?php

/*** Quote Post Format ***/

if(!function_exists('cortex_mikado_map_post_quote_meta_fields')) {

	function cortex_mikado_map_post_quote_meta_fields() {
		$quote_post_format_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Quote Post Format', 'cortex'),
				'name' 	=> 'post_format_quote_meta',
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__('Quote Text', 'cortex'),
				'description' => esc_html__('Enter Quote text', 'cortex'),
				'parent'      => $quote_post_format_meta_box,

			)
		);

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_post_quote_meta_fields');
}