<?php

if(!function_exists('cortex_mikado_map_blog_meta_fields')) {

	function cortex_mikado_map_blog_meta_fields() {

		$mkd_blog_categories = array();
		$categories = get_categories();
		foreach($categories as $category) {
		    $mkd_blog_categories[$category->term_id] = $category->name;
		}

		$blog_meta_box = cortex_mikado_add_meta_box(
		    array(
		        'scope' => array('page'),
		        'title' => esc_html__('Blog', 'cortex'),
		        'name' => 'blog_meta'
		    )
		);

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        =>'mkdf_blog_category_meta',
	            'type'        => 'selectblank',
	            'label'       => esc_html__('Blog Category', 'cortex'),
	            'description' => esc_html__('Choose category of posts to display (leave empty to display all categories)', 'cortex'),
	            'parent'      => $blog_meta_box,
	            'options'     => $mkd_blog_categories
	        )
	    );

	    cortex_mikado_add_meta_box_field(
	        array(
	            'name'        => 'mkdf_show_posts_per_page_meta',
	            'type'        => 'text',
	            'label'       => esc_html__('Number of Posts', 'cortex'),
	            'description' => esc_html__('Enter the number of posts to display', 'cortex'),
	            'parent'      => $blog_meta_box,
	            'options'     => $mkd_blog_categories,
	            'args'        => array("col_width" => 3)
	        )
	    );
	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_blog_meta_fields');
}

