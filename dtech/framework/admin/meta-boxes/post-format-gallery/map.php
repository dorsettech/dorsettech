<?php

/*** Gallery Post Format ***/

if(!function_exists('cortex_mikado_map_post_gallery_meta_fields')) {

	function cortex_mikado_map_post_gallery_meta_fields() {
		$gallery_post_format_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Gallery Post Format', 'cortex'),
				'name' 	=> 'post_format_gallery_meta',
			)
		);

		cortex_mikado_add_multiple_images_field(
			array(
				'name'        => 'mkdf_post_gallery_images_meta',
				'label'       => esc_html__('Gallery Images', 'cortex'),
				'description' => esc_html__('Choose your gallery images', 'cortex'),
				'parent'      => $gallery_post_format_meta_box,
			)
		);

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_post_gallery_meta_fields');
}