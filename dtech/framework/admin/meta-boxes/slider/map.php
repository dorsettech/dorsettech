<?php

//Slider

if(!function_exists('cortex_mikado_map_slider_meta_fields')) {

	function cortex_mikado_map_slider_meta_fields() {
		$slider_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' => array('slides'),
				'title' => esc_html__('Slide Background', 'cortex'),
				'name' => 'slides_type',
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_slide_background_type',
				'type'          => 'select',
				'default_value' => 'image',
				'label'         => esc_html__('Slide Background Type', 'cortex'),
				'description'   => esc_html__('Do you want to upload an image or video?', 'cortex'),
				'parent'        => $slider_meta_box,
				'options'       => array(
					"image" => esc_html__("Image", 'cortex'),
					"video" => esc_html__("Video", 'cortex'),
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"image" => "#mkdf_mkdf_slides_video_settings",
						"video" => "#mkdf_mkdf_slides_image_settings"
					),
					"show" => array(
						"image" => "#mkdf_mkdf_slides_image_settings",
						"video" => "#mkdf_mkdf_slides_video_settings"
					)
				)
			)
		);


		//Slide Image

		$image_meta_container = cortex_mikado_add_admin_container(
			array(
				'name' => 'mkdf_slides_image_settings',
				'parent' => $slider_meta_box,
				'hidden_property' => 'mkdf_slide_background_type',
				'hidden_values' => array('video')
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_image',
				'type'        => 'image',
				'label'       => esc_html__('Slide Image', 'cortex'),
				'description' => esc_html__('Choose background image', 'cortex'),
				'parent'      => $image_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_overlay_image',
				'type'        => 'image',
				'label'       => esc_html__('Overlay Image', 'cortex'),
				'description' => esc_html__('Choose overlay image (pattern) for background image', 'cortex'),
				'parent'      => $image_meta_container
			)
		);


		//Slide Video

		$video_meta_container = cortex_mikado_add_admin_container(
			array(
				'name' => 'mkdf_slides_video_settings',
				'parent' => $slider_meta_box,
				'hidden_property' => 'mkdf_slide_background_type',
				'hidden_values' => array('image')
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_video_webm',
				'type'        => 'text',
				'label'       => esc_html__('Video - webm', 'cortex'),
				'description' => esc_html__('Path to the webm file that you have previously uploaded in Media Section', 'cortex'),
				'parent'      => $video_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_video_mp4',
				'type'        => 'text',
				'label'       => esc_html__('Video - mp4', 'cortex'),
				'description' => esc_html__('Path to the mp4 file that you have previously uploaded in Media Section', 'cortex'),
				'parent'      => $video_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_video_ogv',
				'type'        => 'text',
				'label'       => esc_html__('Video - ogv', 'cortex'),
				'description' => esc_html__('Path to the ogv file that you have previously uploaded in Media Section', 'cortex'),
				'parent'      => $video_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_video_image',
				'type'        => 'image',
				'label'       => esc_html__('Video Preview Image', 'cortex'),
				'description' => esc_html__('Choose background image that will be visible until video is loaded. This image will be shown on touch devices too.', 'cortex'),
				'parent'      => $video_meta_container
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_slide_video_overlay',
				'type' => 'yesempty',
				'default_value' => '',
				'label' => esc_html__('Video Overlay Image', 'cortex'),
				'description' => esc_html__('Do you want to have a overlay image on video?', 'cortex'),
				'parent' => $video_meta_container,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_mkdf_slide_video_overlay_container"
				)
			)
		);

		$slide_video_overlay_container = cortex_mikado_add_admin_container(array(
			'name' => 'mkdf_slide_video_overlay_container',
			'parent' => $video_meta_container,
			'hidden_property' => 'mkdf_slide_video_overlay',
			'hidden_values' => array('','no')
		));

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_video_overlay_image',
				'type'        => 'image',
				'label'       => esc_html__('Overlay Image', 'cortex'),
				'description' => esc_html__('Choose overlay image (pattern) for background video.', 'cortex'),
				'parent'      => $slide_video_overlay_container
			)
		);


		//Slide Elements

		$elements_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' => array('slides'),
				'title' => esc_html__('Slide Elements', 'cortex'),
				'name' => 'mkdf_slides_elements'
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' => $elements_meta_box,
				'name' => 'mkdf_slides_elements_frame',
				'title' => esc_html__('Elements Holder Frame', 'cortex'),
			)
		);

		cortex_mikado_add_slide_holder_frame_scheme(
			array(
				'parent' => $elements_meta_box
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_elements_alignment',
				'type'        => 'select',
				'label'       => esc_html__('Elements Alignment', 'cortex'),
				'description' => esc_html__('How elements are aligned with respect to the Holder Frame', 'cortex'),
				'parent'      => $elements_meta_box,
				'default_value' => 'center',
				'options' => array(
					"center" => esc_html__("Center", 'cortex'),
					"left" => esc_html__("Left", 'cortex'),
					"right" => esc_html__("Right", 'cortex'),
					"custom" => esc_html__("Custom", 'cortex'),
				),
				'args'        => array(
					"dependence" => true,
					"hide" => array(
						"center" => "#mkdf_mkdf_slide_holder_frame_height",
						"left" => "#mkdf_mkdf_slide_holder_frame_height",
						"right" => "#mkdf_mkdf_slide_holder_frame_height",
						"custom" => ""
					),
					"show" => array(
						"center" => "",
						"left" => "",
						"right" => "",
						"custom" => "#mkdf_mkdf_slide_holder_frame_height"
					)
				)
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_in_grid',
				'type'        => 'select',
				'label'       => esc_html__('Holder Frame in Grid?', 'cortex'),
				'description' => esc_html__('Whether to keep the holder frame width the same as that of the grid.', 'cortex'),
				'parent'      => $elements_meta_box,
				'default_value' => 'no',
				'options' => array(
					"yes" => esc_html__("Yes", 'cortex'),
					"no" => esc_html__("No", 'cortex'),
				),
				'args'        => array(
					"dependence" => true,
					"hide" => array(
						"yes" => "#mkdf_mkdf_slide_holder_frame_width, #mkdf_mkdf_holder_frame_responsive_container",
						"no" => ""
					),
					"show" => array(
						"yes" => "",
						"no" => "#mkdf_mkdf_slide_holder_frame_width, #mkdf_mkdf_holder_frame_responsive_container"
					)
				)
			)
		);

		$holder_frame = cortex_mikado_add_admin_group(array(
			'title' => esc_html__('Holder Frame Properties', 'cortex'),
			'description' => esc_html__('The frame is always positioned centrally on the slide. All elements are positioned and sized relatively to the holder frame. Refer to the scheme above.', 'cortex'),
			'name' => 'mkdf_holder_frame',
			'parent' => $elements_meta_box
		));

		$row1 = cortex_mikado_add_admin_row(array(
			'name' => 'row1',
			'parent' => $holder_frame
		));

		$holder_frame_width = cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_width',
				'type'        => 'textsimple',
				'label'       => esc_html__('Relative width (C/A*100)', 'cortex'),
				'parent'      => $row1,
				'hidden_property' => 'mkdf_slide_holder_frame_in_grid',
				'hidden_values' => array('yes')
			)
		);

		$holder_frame_height = cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_height',
				'type'        => 'textsimple',
				'label'       => esc_html__('Height to width ratio (D/C*100)', 'cortex'),
				'parent'      => $row1,
				'hidden_property' => 'mkdf_slide_holder_elements_alignment',
				'hidden_values' => array('center', 'left', 'right')
			)
		);

		$holder_frame_responsive_container = cortex_mikado_add_admin_container(array(
			'name' => 'mkdf_holder_frame_responsive_container',
			'parent' => $elements_meta_box,
			'hidden_property' => 'mkdf_slide_holder_frame_in_grid',
			'hidden_values' => array('yes')
		));

		$holder_frame_responsive = cortex_mikado_add_admin_group(array(
			'title' => esc_html__('Responsive Relative Width', 'cortex'),
			'description' => esc_html__('Enter different relative widths of the holder frame for each responsive stage. Leave blank to have the frame width scale proportionally to the screen size.', 'cortex'),
			'name' => 'mkdf_holder_frame_responsive',
			'parent' => $holder_frame_responsive_container
		));

		$screen_widths_holder_frame = array(
			// These values must match those in mkd.layout.php, slider.php and shortcodes.js
			"mobile" => 600,
			"tabletp" => 800,
			"tabletl" => 1024,
			"laptop" => 1440
		);

		$row2 = cortex_mikado_add_admin_row(array(
			'name' => 'row2',
			'parent' => $holder_frame_responsive
		));

		$holder_frame_width = cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_width_mobile',
				'type'        => 'textsimple',
				'label'       => esc_html__('Mobile', 'cortex') . '(' . esc_html__('up to', 'cortex') . $screen_widths_holder_frame["mobile"].'px)',
				'parent'      => $row2
			)
		);

		$holder_frame_height = cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_width_tablet_p',
				'type'        => 'textsimple',
				'label'       => esc_html__('Tablet - Portrait', 'cortex') . ' ('.($screen_widths_holder_frame["mobile"]+1).'px - '.$screen_widths_holder_frame["tabletp"].'px)',
				'parent'      => $row2
			)
		);

		$holder_frame_height = cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_width_tablet_l',
				'type'        => 'textsimple',
				'label'       => esc_html__('Tablet - Landscape', 'cortex') . ' ('.($screen_widths_holder_frame["tabletp"]+1).'px - '.$screen_widths_holder_frame["tabletl"].'px)',
				'parent'      => $row2
			)
		);

		$row3 = cortex_mikado_add_admin_row(array(
			'name' => 'row3',
			'parent' => $holder_frame_responsive
		));

		$holder_frame_width = cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_width_laptop',
				'type'        => 'textsimple',
				'label'       => esc_html__('Laptop', 'cortex') . '('.($screen_widths_holder_frame["tabletl"]+1).'px - '.$screen_widths_holder_frame["laptop"].'px)',
				'parent'      => $row3
			)
		);

		$holder_frame_height = cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_slide_holder_frame_width_desktop',
				'type'        => 'textsimple',
				'label'       => esc_html__('Desktop', 'cortex') . '(' . esc_html__('above', 'cortex') . $screen_widths_holder_frame["laptop"].'px)',
				'parent'      => $row3
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'parent' => $elements_meta_box,
				'type' => 'text',
				'name' => 'mkdf_slide_elements_default_width',
				'label' => esc_html__('Default Screen Width in px (A)', 'cortex'),
				'description' => esc_html__('All elements marked as responsive scale at the ratio of the actual screen width to this screen width. Default is 1920px.', 'cortex'),
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'parent' => $elements_meta_box,
				'type' => 'select',
				'name' => 'mkdf_slide_elements_default_animation',
				'default_value' => 'none',
				'label' => esc_html__('Default Elements Animation', 'cortex'),
				'description' => esc_html__('This animation will be applied to all elements except those with their own animation settings.', 'cortex'),
				'options' => array(
					"none" => esc_html__("No Animation", 'cortex'),
					"flip" => esc_html__("Flip", 'cortex'),
					"spin" => esc_html__("Spin", 'cortex'),
					"fade" => esc_html__("Fade In", 'cortex'),
					"from_bottom" => esc_html__("Fly In From Bottom", 'cortex'),
					"from_top" => esc_html__("Fly In From Top", 'cortex'),
					"from_left" => esc_html__("Fly In From Left", 'cortex'),
					"from_right" => esc_html__("Fly In From Right", 'cortex'),
				)
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' => $elements_meta_box,
				'name' => 'mkdf_slides_elements_list',
				'title' => esc_html__('Elements', 'cortex'),
			)
		);

		$slide_elements = cortex_mikado_add_slide_elements_framework(
			array(
				'parent' => $elements_meta_box,
				'name' => 'mkdf_slides_elements_holder',
			)
		);

		//Slide Behaviour

		$behaviours_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' => array('slides'),
				'title' => esc_html__('Slide Behaviours', 'cortex'),
				'name' => 'mkdf_slides_behaviour_settings',
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' => $behaviours_meta_box,
				'name' => 'mkdf_header_styling_title',
				'title' => esc_html__('Header', 'cortex'),
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'parent' => $behaviours_meta_box,
				'type' => 'selectblank',
				'name' => 'mkdf_slide_header_style',
				'default_value' => '',
				'label' => esc_html__('Header Style', 'cortex'),
				'description' => esc_html__('Header style will be applied when this slide is in focus', 'cortex'),
				'options' => array(
					"light" => esc_html__("Light", 'cortex'),
					"dark" => esc_html__("Dark", 'cortex'),
				)
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' => $behaviours_meta_box,
				'name' => 'mkdf_image_animation_title',
				'title' => esc_html__('Slide Image Animation', 'cortex'),
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name' => 'mkdf_enable_image_animation',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Enable Image Animation', 'cortex'),
				'description' => esc_html__('Enabling this option will turn on a motion animation on the slide image', 'cortex'),
				'parent' => $behaviours_meta_box,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_mkdf_enable_image_animation_container"
				)
			)
		);

		$enable_image_animation_container = cortex_mikado_add_admin_container(array(
			'name' => 'mkdf_enable_image_animation_container',
			'parent' => $behaviours_meta_box,
			'hidden_property' => 'mkdf_enable_image_animation',
			'hidden_value' => 'no'
		));

		cortex_mikado_add_meta_box_field(
			array(
				'parent' => $enable_image_animation_container,
				'type' => 'select',
				'name' => 'mkdf_enable_image_animation_type',
				'default_value' => 'zoom_center',
				'label' => esc_html__('Animation Type', 'cortex'),
				'options' => array(
					"zoom_center" => esc_html__("Zoom In Center", 'cortex'),
					"zoom_top_left" => esc_html__("Zoom In to Top Left", 'cortex'),
					"zoom_top_right" => esc_html__("Zoom In to Top Right", 'cortex'),
					"zoom_bottom_left" => esc_html__("Zoom In to Bottom Left", 'cortex'),
					"zoom_bottom_right" => esc_html__("Zoom In to Bottom Right", 'cortex'),
				)
			)
		);
	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_slider_meta_fields');
}