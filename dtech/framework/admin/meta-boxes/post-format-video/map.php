<?php

/*** Video Post Format ***/

if(!function_exists('cortex_mikado_map_post_video_meta_fields')) {

	function cortex_mikado_map_post_video_meta_fields() {
		$video_post_format_meta_box = cortex_mikado_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Video Post Format', 'cortex'),
				'name' 	=> 'post_format_video_meta',
			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_video_type_meta',
				'type'        => 'select',
				'label'       => esc_html__('Video Type', 'cortex'),
				'description' => esc_html__('Choose video type', 'cortex'),
				'parent'      => $video_post_format_meta_box,
				'default_value' => 'youtube',
				'options'     => array(
					'youtube' => esc_html__('Youtube','cortex'),
					'vimeo' => esc_html__('Vimeo','cortex'),
					'self' => esc_html__('Self Hosted','cortex')
				),
				'args' => array(
				'dependence' => true,
				'hide' => array(
					'youtube' => '#mkdf_mkdf_video_self_hosted_container',
					'vimeo' => '#mkdf_mkdf_video_self_hosted_container',
					'self' => '#mkdf_mkdf_video_embedded_container'
				),
				'show' => array(
					'youtube' => '#mkdf_mkdf_video_embedded_container',
					'vimeo' => '#mkdf_mkdf_video_embedded_container',
					'self' => '#mkdf_mkdf_video_self_hosted_container')
			)
			)
		);

		$mkdf_video_embedded_container = cortex_mikado_add_admin_container(
			array(
				'parent' => $video_post_format_meta_box,
				'name' => 'mkdf_video_embedded_container',
				'hidden_property' => 'mkdf_video_type_meta',
				'hidden_value' => 'self'
			)
		);

		$mkdf_video_self_hosted_container = cortex_mikado_add_admin_container(
			array(
				'parent' => $video_post_format_meta_box,
				'name' => 'mkdf_video_self_hosted_container',
				'hidden_property' => 'mkdf_video_type_meta',
				'hidden_values' => array('youtube', 'vimeo')
			)
		);



		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_video_id_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video ID', 'cortex'),
				'description' => esc_html__('Enter Video ID', 'cortex'),
				'parent'      => $mkdf_video_embedded_container,

			)
		);


		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_video_image_meta',
				'type'        => 'image',
				'label'       => esc_html__('Video Image', 'cortex'),
				'description' => esc_html__('Upload video image', 'cortex'),
				'parent'      => $mkdf_video_self_hosted_container,

			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_video_webm_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video WEBM', 'cortex'),
				'description' => esc_html__('Enter video URL for WEBM format', 'cortex'),
				'parent'      => $mkdf_video_self_hosted_container,

			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_video_mp4_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video MP4', 'cortex'),
				'description' => esc_html__('Enter video URL for MP4 format', 'cortex'),
				'parent'      => $mkdf_video_self_hosted_container,

			)
		);

		cortex_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_video_ogv_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video OGV', 'cortex'),
				'description' => esc_html__('Enter video URL for OGV format', 'cortex'),
				'parent'      => $mkdf_video_self_hosted_container,

			)
		);

	}
	
	add_action('cortex_mikado_meta_boxes_map', 'cortex_mikado_map_post_video_meta_fields');
}