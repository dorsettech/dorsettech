<?php

if ( ! function_exists('cortex_mikado_load_elements_map') ) {
	/**
	 * Add Elements option page for shortcodes
	 */
	function cortex_mikado_load_elements_map() {

		cortex_mikado_add_admin_page(
			array(
				'slug' => '_elements_page',
				'title' => esc_html__('Elements', 'cortex'),
				'icon' => 'fa fa-flag-o'
			)
		);

		do_action( 'cortex_mikado_options_elements_map' );

	}

	add_action('cortex_mikado_options_map', 'cortex_mikado_load_elements_map', 10);

}