<?php

if ( ! function_exists('cortex_mikado_reset_options_map') ) {
	/**
	 * Reset options panel
	 */
	function cortex_mikado_reset_options_map() {

		cortex_mikado_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__('Reset', 'cortex'),
				'icon'  => 'fa fa-retweet'
			)
		);

		$panel_reset = cortex_mikado_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__('Reset', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(array(
			'type'	=> 'yesno',
			'name'	=> 'reset_to_defaults',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Reset to Defaults', 'cortex'),
			'description'	=> esc_html__('This option will reset all Mikado Options values to defaults', 'cortex'),
			'parent'		=> $panel_reset
		));

	}

	add_action( 'cortex_mikado_options_map', 'cortex_mikado_reset_options_map', 18);

}