<?php

if ( ! function_exists('cortex_mikado_parallax_options_map') ) {
	/**
	 * Parallax options page
	 */
	function cortex_mikado_parallax_options_map()
	{

		$panel_parallax = cortex_mikado_add_admin_panel(
			array(
				'page'  => '_elements_page',
				'name'  => 'panel_parallax',
				'title' => esc_html__('Parallax', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(array(
			'type'			=> 'onoff',
			'name'			=> 'parallax_on_off',
			'default_value'	=> 'off',
			'label'			=> esc_html__('Parallax on touch devices', 'cortex'),
			'description'	=> esc_html__('Enabling this option will allow parallax on touch devices', 'cortex'),
			'parent'		=> $panel_parallax
		));

		cortex_mikado_add_admin_field(array(
			'type'			=> 'text',
			'name'			=> 'parallax_min_height',
			'default_value'	=> '400',
			'label'			=> esc_html__('Parallax Min Height', 'cortex'),
			'description'	=> esc_html__('Set a minimum height for parallax images on small displays (phones, tablets, etc.)', 'cortex'),
			'args'			=> array(
				'col_width'	=> 3,
				'suffix'	=> 'px'
			),
			'parent'		=> $panel_parallax
		));

	}

	add_action( 'cortex_mikado_options_map', 'cortex_mikado_parallax_options_map',20);

}