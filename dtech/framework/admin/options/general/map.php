<?php

if ( ! function_exists('cortex_mikado_general_options_map') ) {
	/**
	 * General options page
	 */
	function cortex_mikado_general_options_map() {

		cortex_mikado_add_admin_page(
			array(
				'slug'  => '',
				'title' => esc_html__('General', 'cortex'),
				'icon'  => 'fa fa-institution'
			)
		);

        $panel_logo = cortex_mikado_add_admin_panel(
            array(
                'page' => '',
                'name' => 'panel_logo',
                'title' => esc_html__('Logo', 'cortex'),
            )
        );

        cortex_mikado_add_admin_field(
            array(
                'parent' => $panel_logo,
                'type' => 'yesno',
                'name' => 'hide_logo',
                'default_value' => 'no',
                'label' => esc_html__('Hide Logo', 'cortex'),
                'description' => esc_html__('Enabling this option will hide logo image', 'cortex'),
                'args' => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "#mkdf_hide_logo_container",
                    "dependence_show_on_yes" => ""
                )
            )
        );

        $hide_logo_container = cortex_mikado_add_admin_container(
            array(
                'parent' => $panel_logo,
                'name' => 'hide_logo_container',
                'hidden_property' => 'hide_logo',
                'hidden_value' => 'yes'
            )
        );

        cortex_mikado_add_admin_field(
            array(
                'name' => 'logo_image',
                'type' => 'image',
                'default_value' => MIKADO_ASSETS_ROOT."/img/logo.png",
                'label' => esc_html__('Logo Image - Default', 'cortex'),
                'description' => esc_html__('Choose a default logo image to display ', 'cortex'),
                'parent' => $hide_logo_container
            )
        );

        cortex_mikado_add_admin_field(
            array(
                'name' => 'logo_image_dark',
                'type' => 'image',
                'default_value' => MIKADO_ASSETS_ROOT."/img/logo_dark.png",
                'label' => esc_html__('Logo Image - Dark', 'cortex'),
                'description' => esc_html__('Choose a default logo image to display ', 'cortex'),
                'parent' => $hide_logo_container
            )
        );

        cortex_mikado_add_admin_field(
            array(
                'name' => 'logo_image_light',
                'type' => 'image',
                'default_value' => MIKADO_ASSETS_ROOT."/img/logo_light.png",
                'label' => esc_html__('Logo Image - Light', 'cortex'),
                'description' => esc_html__('Choose a default logo image to display ', 'cortex'),
                'parent' => $hide_logo_container
            )
        );

        cortex_mikado_add_admin_field(
            array(
                'name' => 'logo_image_sticky',
                'type' => 'image',
                'default_value' => MIKADO_ASSETS_ROOT."/img/logo.png",
                'label' => esc_html__('Logo Image - Sticky', 'cortex'),
                'description' => esc_html__('Choose a default logo image to display ', 'cortex'),
                'parent' => $hide_logo_container
            )
        );

        cortex_mikado_add_admin_field(
            array(
                'name' => 'logo_image_mobile',
                'type' => 'image',
                'default_value' => MIKADO_ASSETS_ROOT."/img/logo.png",
                'label' => esc_html__('Logo Image - Mobile', 'cortex'),
                'description' => esc_html__('Choose a default logo image to display ', 'cortex'),
                'parent' => $hide_logo_container
            )
        );

		$panel_design_style = cortex_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_design_style',
				'title' => esc_html__('Appearance', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'google_fonts',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__('Font Family', 'cortex'),
				'description'   => esc_html__('Choose a default Google font for your site', 'cortex'),
				'parent'		=> $panel_design_style
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'google_fonts_second',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__('Second Font Family','cortex'),
				'description'   => esc_html__('Choose a second default Google font for your site(default is Montserrat)','cortex'),
				'parent' 		=> $panel_design_style
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_fonts',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__('Additional Google Fonts', 'cortex'),
				'parent'        => $panel_design_style,
				'args'          => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_additional_google_fonts_container"
				)
			)
		);

		$additional_google_fonts_container = cortex_mikado_add_admin_container(
			array(
				'parent'            => $panel_design_style,
				'name'              => 'additional_google_fonts_container',
				'hidden_property'   => 'additional_google_fonts',
				'hidden_value'      => 'no'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font1',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__('Font Family', 'cortex'),
				'description'   => esc_html__('Choose additional Google font for your site', 'cortex'),
				'parent'        => $additional_google_fonts_container
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font2',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__('Font Family', 'cortex'),
				'description'   => esc_html__('Choose additional Google font for your site', 'cortex'),
				'parent'        => $additional_google_fonts_container
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font3',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__('Font Family', 'cortex'),
				'description'   => esc_html__('Choose additional Google font for your site', 'cortex'),
				'parent'        => $additional_google_fonts_container
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font4',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__('Font Family', 'cortex'),
				'description'   => esc_html__('Choose additional Google font for your site', 'cortex'),
				'parent'        => $additional_google_fonts_container
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font5',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__('Font Family', 'cortex'),
				'description'   => esc_html__('Choose additional Google font for your site', 'cortex'),
				'parent'        => $additional_google_fonts_container
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'first_color',
				'type'          => 'color',
				'label'         => esc_html__('First Main Color', 'cortex'),
				'description'   => esc_html__('Choose the most dominant theme color. Default color is #ff2c54', 'cortex'),
				'parent'        => $panel_design_style
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'second_color',
				'type'          => 'color',
				'label'         => esc_html__('Second Main Color', 'cortex'),
				'description'   => esc_html__('Choose the second dominant theme color. Default color is #41ebdf', 'cortex'),
				'parent'        => $panel_design_style
			)
		);

        cortex_mikado_add_admin_field(
            array(
                'name'          => 'third_color',
                'type'          => 'color',
                'label'         => esc_html__('Third Main Color', 'cortex'),
                'description'   => esc_html__('Choose the third dominant theme color. Default color is #41294a', 'cortex'),
                'parent'        => $panel_design_style
            )
        );

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'page_background_color',
				'type'          => 'color',
				'label'         => esc_html__('Page Background Color', 'cortex'),
				'description'   => esc_html__('Choose the background color for page content. Default color is #ffffff', 'cortex'),
				'parent'        => $panel_design_style
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'selection_color',
				'type'          => 'color',
				'label'         => esc_html__('Text Selection Color', 'cortex'),
				'description'   => esc_html__('Choose the color users see when selecting text', 'cortex'),
				'parent'        => $panel_design_style
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'boxed',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__('Boxed Layout', 'cortex'),
				'parent'        => $panel_design_style,
				'args'          => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_boxed_container"
				)
			)
		);

		$boxed_container = cortex_mikado_add_admin_container(
			array(
				'parent'            => $panel_design_style,
				'name'              => 'boxed_container',
				'hidden_property'   => 'boxed',
				'hidden_value'      => 'no'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'page_background_color_in_box',
				'type'          => 'color',
				'label'         => esc_html__('Page Background Color', 'cortex'),
				'description'   => esc_html__('Choose the page background color outside box.', 'cortex'),
				'parent'        => $boxed_container
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'boxed_background_image',
				'type'          => 'image',
				'label'         => esc_html__('Background Image', 'cortex'),
				'description'   => esc_html__('Choose an image to be displayed in background', 'cortex'),
				'parent'        => $boxed_container
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'boxed_background_image_repeating',
				'type'          => 'select',
				'default_value' => 'no',
				'label'         => esc_html__('Use Background Image as Pattern', 'cortex'),
				'description'   => esc_html__('Set this option to "yes" to use the background image as repeating pattern', 'cortex'),
				'parent'        => $boxed_container,
				'options'       => array(
					'no'	=>	esc_html__('No', 'cortex'),
					'yes'	=>	esc_html__('Yes', 'cortex'),
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'boxed_background_image_attachment',
				'type'          => 'select',
				'default_value' => 'fixed',
				'label'         => esc_html__('Background Image Behaviour', 'cortex'),
				'description'   => esc_html__('Choose background image behaviour', 'cortex'),
				'parent'        => $boxed_container,
				'options'       => array(
					'fixed'     => esc_html__('Fixed', 'cortex'),
					'scroll'    => esc_html__('Scroll', 'cortex'),
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'initial_content_width',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__('Initial Width of Content', 'cortex'),
				'description'   => esc_html__('Choose the initial width of content which is in grid. Applies to pages set to "Default Template" and rows set to "In Grid"', 'cortex'),
				'parent'        => $panel_design_style,
				'options'       => array(
					""          => esc_html__("1100px - default",'cortex'),
					"grid-1300" => esc_html__("1300px",'cortex'),
					"grid-1200" => esc_html__("1200px",'cortex'),
					"grid-1000" => esc_html__("1000px",'cortex'),
					"grid-800"  => esc_html__("800px",'cortex')
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'preload_pattern_image',
				'type'          => 'image',
				'label'         => esc_html__('Preload Pattern Image', 'cortex'),
				'description'   => esc_html__('Choose preload pattern image to be displayed until images are loaded ', 'cortex'),
				'parent'        => $panel_design_style
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name' => 'element_appear_amount',
				'type' => 'text',
				'label' => esc_html__('Element Appearance', 'cortex'),
				'description' => esc_html__('For animated elements, set distance (related to browser bottom) to start the animation', 'cortex'),
				'parent' => $panel_design_style,
				'args' => array(
					'col_width' => 2,
					'suffix' => 'px'
				)
			)
		);

		$panel_settings = cortex_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_settings',
				'title' => esc_html__('Behavior', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'smooth_scroll',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__('Smooth Scroll', 'cortex'),
				'description'   => esc_html__('Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices)', 'cortex'),
				'parent'        => $panel_settings
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'smooth_page_transitions',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__('Smooth Page Transitions', 'cortex'),
				'description'   => esc_html__('Enabling this option will perform a smooth transition between pages when clicking on links.', 'cortex'),
				'parent'        => $panel_settings,
				'args'          => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_page_transitions_container"
				)
			)
		);

		$page_transitions_container = cortex_mikado_add_admin_container(
			array(
				'parent'            => $panel_settings,
				'name'              => 'page_transitions_container',
				'hidden_property'   => 'smooth_page_transitions',
				'hidden_value'      => 'no'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'smooth_pt_bgnd_color',
				'type'          => 'color',
				'label'         => esc_html__('Page Loader Background Color', 'cortex'),
				'parent'        => $page_transitions_container
			)
		);

		$group_pt_spinner_animation = cortex_mikado_add_admin_group(array(
			'name'          => 'group_pt_spinner_animation',
			'title'         => esc_html__('Loader Style', 'cortex'),
			'description'   => esc_html__('Define styles for loader spinner animation', 'cortex'),
			'parent'        => $page_transitions_container
		));

		$row_pt_spinner_animation = cortex_mikado_add_admin_row(array(
			'name'      => 'row_pt_spinner_animation',
			'parent'    => $group_pt_spinner_animation
		));

		cortex_mikado_add_admin_field(array(
			'type'          => 'selectsimple',
			'name'          => 'smooth_pt_spinner_type',
			'default_value' => '',
			'label'         => esc_html__('Spinner Type', 'cortex'),
			'parent'        => $row_pt_spinner_animation,
			'options'       => array(
				"pulse" => esc_html__("Pulse", 'cortex'),
				"triple_bounce" => esc_html__("Triple Bounce", 'cortex'),
				"cube" => esc_html__("Cube", 'cortex'),
				"rotating_cubes" => esc_html__("Rotating Cubes", 'cortex'),
				"stripes" => esc_html__("Stripes", 'cortex'),
				"wave" => esc_html__("Wave", 'cortex'),
				"two_rotating_circles" => esc_html__("2 Rotating Circles", 'cortex'),
				"five_rotating_circles" => esc_html__("5 Rotating Circles", 'cortex'),
				"atom" => esc_html__("Atom", 'cortex'),
				"clock" => esc_html__("Clock", 'cortex'),
				"mitosis" => esc_html__("Mitosis", 'cortex'),
				"lines" => esc_html__("Lines", 'cortex'),
				"fussion" => esc_html__("Fussion", 'cortex'),
				"wave_circles" => esc_html__("Wave Circles", 'cortex'),
				"pulse_circles" => esc_html__("Pulse Circles", 'cortex'),
			),
			'args'          => array(
			    "dependence"             => true,
			    'show'        => array(
			        "pulse"                 => "",
			        "triple_bounce"         => "#mkdf_color_spinner_container",
			        "cube"                  => "",
			        "rotating_cubes"        => "",
			        "stripes"               => "",
			        "wave"                  => "",
			        "two_rotating_circles"  => "",
			        "five_rotating_circles" => "",
			        "atom"                  => "",
			        "clock"                 => "",
			        "mitosis"               => "",
			        "lines"                 => "",
			        "fussion"               => "",
			        "wave_circles"          => "",
			        "pulse_circles"         => ""
			    ),
			    'hide'        => array(
			        ""          			=> "#mkdf_color_spinner_container",
			        "pulse"                 => "#mkdf_color_spinner_container",
			        "triple_bounce"         => "",
			        "cube"                  => "#mkdf_color_spinner_container",
			        "rotating_cubes"        => "#mkdf_color_spinner_container",
			        "stripes"               => "#mkdf_color_spinner_container",
			        "wave"                  => "#mkdf_color_spinner_container",
			        "two_rotating_circles"  => "#mkdf_color_spinner_container",
			        "five_rotating_circles" => "#mkdf_color_spinner_container",
			        "atom"                  => "#mkdf_color_spinner_container",
			        "clock"                 => "#mkdf_color_spinner_container",
			        "mitosis"               => "#mkdf_color_spinner_container",
			        "lines"                 => "#mkdf_color_spinner_container",
			        "fussion"               => "#mkdf_color_spinner_container",
			        "wave_circles"          => "#mkdf_color_spinner_container",
			        "pulse_circles"         => "#mkdf_color_spinner_container"
			    )
			)
		));

		cortex_mikado_add_admin_field(array(
			'type'          => 'colorsimple',
			'name'          => 'smooth_pt_spinner_color',
			'default_value' => '',
			'label'         => esc_html__('Spinner Color', 'cortex'),
			'parent'        => $row_pt_spinner_animation
		));

		$color_spinner_container = cortex_mikado_add_admin_container(
		    array(
		        'parent'          => $panel_settings,
		        'name'            => 'color_spinner_container',
		        'hidden_property' => 'smooth_pt_spinner_type',
		        'hidden_value'    => '',
		        'hidden_values'   =>array(
		            "",
		            "pulse",
		            "cube",
		            "rotating_cubes",
		            "stripes",
		            "wave",
		            "two_rotating_circles",
		            "five_rotating_circles",
		            "atom",
		            "clock",
		            "mitosis",
		            "lines",
		            "fussion",
		            "wave_circles",
		            "pulse_circles"
		        )
		    )
		);

		$group_pt_spinner_additional_colors = cortex_mikado_add_admin_group(array(
		    'name'          => 'group_pt_spinner_additional_colors',
		    'title'         => 'Additional Colors',
		    'description'   => 'Define additional colors for Triple Bounce Spinner',
		    'parent'        => $color_spinner_container
		));

		$row_pt_spinner_additional_colors = cortex_mikado_add_admin_row(array(
		    'name'      => 'row_pt_spinner_additional_colors',
		    'parent'    => $group_pt_spinner_additional_colors
		));

		cortex_mikado_add_admin_field(
		    array(
		        'type'          => 'colorsimple',
		        'name'          => 'additional_color_1',
		        'label'         => 'First Additional Color',
		        'parent'        => $row_pt_spinner_additional_colors
		    )
		);

		cortex_mikado_add_admin_field(
		    array(
		        'type'          => 'colorsimple',
		        'name'          => 'additional_color_2',
		        'label'         => 'Second Additional Color',
		        'parent'        => $row_pt_spinner_additional_colors
		    )
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'show_back_button',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__('Show "Back To Top Button"', 'cortex'),
				'description'   => esc_html__('Enabling this option will display a Back to Top button on every page', 'cortex'),
				'parent'        => $panel_settings
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'responsiveness',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__('Responsiveness', 'cortex'),
				'description'   => esc_html__('Enabling this option will make all pages responsive', 'cortex'),
				'parent'        => $panel_settings
			)
		);

		$panel_custom_code = cortex_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_custom_code',
				'title' => esc_html__('Custom Code', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'custom_css',
				'type'          => 'textarea',
				'label'         => esc_html__('Custom CSS', 'cortex'),
				'description'   => esc_html__('Enter your custom CSS here', 'cortex'),
				'parent'        => $panel_custom_code
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'          => 'custom_js',
				'type'          => 'textarea',
				'label'         => esc_html__('Custom JS', 'cortex'),
				'description'   => esc_html__('Enter your custom Javascript here', 'cortex'),
				'parent'        => $panel_custom_code
			)
		);

		$panel_google_api = cortex_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_google_api',
				'title' => esc_html__('Google API', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name'        => 'google_maps_api_key',
				'type'        => 'text',
				'label'       => esc_html__('Google Maps Api Key', 'cortex'),
				'description' => esc_html__('Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our documentation.', 'cortex'),
				'parent'      => $panel_google_api
			)
		);
	}

	add_action( 'cortex_mikado_options_map', 'cortex_mikado_general_options_map', 1);

}