<?php

if ( ! function_exists('cortex_mikado_page_options_map') ) {

    function cortex_mikado_page_options_map() {

        cortex_mikado_add_admin_page(
            array(
                'slug'  => '_page_page',
                'title' => esc_html__('Page', 'cortex'),
                'icon'  => 'fa fa-file-text-o'
            )
        );

        $custom_sidebars = cortex_mikado_get_custom_sidebars();

        $panel_sidebar = cortex_mikado_add_admin_panel(
            array(
                'page'  => '_page_page',
                'name'  => 'panel_sidebar',
                'title' => esc_html__('Design Style', 'cortex'),
            )
        );

        cortex_mikado_add_admin_field(array(
            'name'        => 'page_sidebar_layout',
            'type'        => 'select',
            'label'       => esc_html__('Sidebar Layout', 'cortex'),
            'description' => esc_html__('Choose a sidebar layout for pages', 'cortex'),
            'default_value' => 'default',
            'parent'      => $panel_sidebar,
            'options'     => array(
                'default'			=> esc_html__('No Sidebar', 'cortex'),
                'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'cortex'),
                'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'cortex'),
                'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'cortex'),
                'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'cortex'),
            )
        ));


        if(count($custom_sidebars) > 0) {
            cortex_mikado_add_admin_field(array(
                'name' => 'page_custom_sidebar',
                'type' => 'selectblank',
                'label' => esc_html__('Sidebar to Display', 'cortex'),
                'description' => esc_html__('Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'cortex'),
                'parent' => $panel_sidebar,
                'options' => $custom_sidebars
            ));
        }

        $panel_widgets = cortex_mikado_add_admin_panel(
            array(
                'page'  => '_page_page',
                'name'  => 'panel_widgets',
                'title' => esc_html__('Widgets', 'cortex'),
            )
        );

        /**
         * Navigation style
         */
        cortex_mikado_add_admin_field(array(
            'type'			=> 'color',
            'name'			=> 'sidebar_background_color',
            'default_value'	=> '',
            'label'			=> esc_html__('Sidebar Background Color', 'cortex'),
            'description'	=> esc_html__('Choose background color for sidebar', 'cortex'),
            'parent'		=> $panel_widgets
        ));

        $group_sidebar_padding = cortex_mikado_add_admin_group(array(
            'name'		=> 'group_sidebar_padding',
            'title'		=> esc_html__('Padding', 'cortex'),
            'parent'	=> $panel_widgets
        ));

        $row_sidebar_padding = cortex_mikado_add_admin_row(array(
            'name'		=> 'row_sidebar_padding',
            'parent'	=> $group_sidebar_padding
        ));

        cortex_mikado_add_admin_field(array(
            'type'			=> 'textsimple',
            'name'			=> 'sidebar_padding_top',
            'default_value'	=> '',
            'label'			=> esc_html__('Top Padding', 'cortex'),
            'args'			=> array(
                'suffix'	=> 'px'
            ),
            'parent'		=> $row_sidebar_padding
        ));

        cortex_mikado_add_admin_field(array(
            'type'			=> 'textsimple',
            'name'			=> 'sidebar_padding_right',
            'default_value'	=> '',
            'label'			=> esc_html__('Right Padding', 'cortex'),
            'args'			=> array(
                'suffix'	=> 'px'
            ),
            'parent'		=> $row_sidebar_padding
        ));

        cortex_mikado_add_admin_field(array(
            'type'			=> 'textsimple',
            'name'			=> 'sidebar_padding_bottom',
            'default_value'	=> '',
            'label'			=> esc_html__('Bottom Padding', 'cortex'),
            'args'			=> array(
                'suffix'	=> 'px'
            ),
            'parent'		=> $row_sidebar_padding
        ));

        cortex_mikado_add_admin_field(array(
            'type'			=> 'textsimple',
            'name'			=> 'sidebar_padding_left',
            'default_value'	=> '',
            'label'			=> esc_html__('Left Padding', 'cortex'),
            'args'			=> array(
                'suffix'	=> 'px'
            ),
            'parent'		=> $row_sidebar_padding
        ));

        cortex_mikado_add_admin_field(array(
            'type'			=> 'select',
            'name'			=> 'sidebar_alignment',
            'default_value'	=> '',
            'label'			=> esc_html__('Text Alignment', 'cortex'),
            'description'	=> esc_html__('Choose text aligment', 'cortex'),
            'options'		=> array(
                'left' => esc_html__('Left', 'cortex'),
                'center' => esc_html__('Center', 'cortex'),
                'right' => esc_html__('Right', 'cortex'),
            ),
            'parent'		=> $panel_widgets
        ));

    }

    add_action( 'cortex_mikado_options_map', 'cortex_mikado_page_options_map', 8);

}