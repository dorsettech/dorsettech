<div class="mkdf-tabs-content">
    <div class="tab-content">
        <div class="tab-pane fade in active" id="import">
            <div class="mkdf-tab-content">
                <div class="mkdf-page-title-holder clearfix">
                    <h2 class="mkdf-page-title"><?php esc_html_e('Backup Options', 'cortex'); ?></h2>
                </div>
                <form method="post" class="mkd_ajax_form mkdf-backup-options-page-holder">
                    <div class="mkdf-page-form">
                        <div class="mkdf-page-form-section-holder">
                            <h3 class="mkdf-page-section-title"><?php esc_html_e('Export/Import Options', 'cortex'); ?></h3>
                            <div class="mkdf-page-form-section">
                                <div class="mkdf-field-desc">
                                    <h4><?php esc_html_e('Export', 'cortex'); ?></h4>
                                    <p><?php esc_html_e('Copy the code from this field and save it to a textual file to export your options. Save that textual file somewhere so you can later use it to import options if necessary.', 'cortex'); ?></p>
                                </div>
                                <!-- close div.mkdf-field-desc -->

                                <div class="mkdf-section-content">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <textarea name="export_options" id="export_options" class="form-control mkdf-form-element" rows="10" readonly><?php echo cortex_mikado_export_options(); ?></textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- close div.mkdf-section-content -->

                            </div>

                            <div class="mkdf-page-form-section">


                                <div class="mkdf-field-desc">
                                    <h4><?php esc_html_e('Import', 'cortex'); ?></h4>
									<p><?php esc_html_e('To import options, just paste the code you previously saved from the "Export" field into this field, and then click the "Import" button.', 'cortex'); ?></p>
                                </div>
                                <!-- close div.mkdf-field-desc -->



                                <div class="mkdf-section-content">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-lg-12">
												<textarea name="import_theme_options" id="import_theme_options" class="form-control mkdf-form-element" rows="10"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- close div.mkdf-section-content -->

                            </div>
							<div class="mkdf-page-form-section">


								<div class="mkdf-field-desc">
									<button type="button" class="btn btn-primary btn-sm " name="import" id="mkdf-import-theme-options-btn">Import</button>
									<?php wp_nonce_field('mkdf_import_theme_options_secret_value', 'mkdf_import_theme_options_secret', false); ?>
									<span class="mkdf-bckp-message"></span>
								</div>
							</div>
                            <div class="mkdf-page-form-section mkdf-import-button-wrapper">

                                <div class="alert alert-warning">
                                    <strong><?php esc_html_e('Important notes:', 'cortex') ?></strong>
                                    <ul>
                                        <li><?php esc_html_e('Please note that import process will overide all your existing options.', 'cortex'); ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div><!-- close mkdf-tab-content -->
        </div>
    </div>
</div> <!-- close div.mkdf-tabs-content -->