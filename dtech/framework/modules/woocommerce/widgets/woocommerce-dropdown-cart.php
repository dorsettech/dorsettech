<?php

class CortexMikadoWoocommerceDropdownCart extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'mkdf_woocommerce_dropdown_cart', // Base ID
			'Mikado Woocommerce Dropdown Cart', // Name
			array( 'description' => esc_html__( 'Mikado Woocommerce Dropdown Cart', 'cortex' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {
		global $post;
		extract( $args );

		global $woocommerce;
		global $cortex_mikado_options;

		$cart_style = 'mkdf-with-icon';

		?>
		<div class="mkdf-shopping-cart-outer">
			<div class="mkdf-shopping-cart-inner">
				<div class="mkdf-shopping-cart-header">
					<a class="mkdf-header-cart" href="<?php echo wc_get_cart_url(); ?>">
						<span class="mkdf-cart-icon ion-bag"></span>
						<span class="mkdf-cart-amount"><?php echo esc_html($woocommerce->cart->get_cart_contents_count()); ?></span>
					</a>

					<div class="mkdf-shopping-cart-dropdown">
						<?php
						$cart_is_empty = sizeof( $woocommerce->cart->get_cart() ) <= 0;
						$list_class    = array( 'mkdf-cart-list', 'product_list_widget' );
						?>
						<ul>

							<?php if ( ! $cart_is_empty ) : ?>

								<?php foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) :

									$_product      = $cart_item['data'];

									// Only display if allowed
									if ( ! $_product->exists() || $cart_item['quantity'] == 0 ) {
										continue;
									}

									// Get price
									if ( version_compare( WOOCOMMERCE_VERSION, '3.0' ) >= 0 ) {
										$product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? wc_get_price_excluding_tax($_product) : wc_get_price_including_tax($_product);
									} else {
										$product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();
									}
									?>


									<li>
										<div class="mkdf-item-image-holder">
											<a href="<?php echo esc_url( get_permalink( $cart_item['product_id'] ) ); ?>">
												<?php echo wp_kses( $_product->get_image(), array(
													'img' => array(
														'src'    => true,
														'width'  => true,
														'height' => true,
														'class'  => true,
														'alt'    => true,
														'title'  => true,
														'id'     => true
													)
												) ); ?>
											</a>
										</div>
										<div class="mkdf-item-info-holder">
											<div class="mkdf-item-left">
												<a href="<?php echo esc_url( get_permalink( $cart_item['product_id'] ) ); ?>">
													<?php echo apply_filters( 'woocommerce_widget_cart_product_title', $_product->get_title(), $_product ); ?>
												</a>
												<div class="mkdf-quantity-holder">
													<span class="mkdf-quantity"><?php echo esc_html( $cart_item['quantity'] ); ?></span>
													<span>x</span>
													<?php echo apply_filters( 'woocommerce_cart_item_price_html', wc_price( $product_price ), $cart_item, $cart_item_key ); ?>
												</div>
											</div>
											<div class="mkdf-cart-remove">
												<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="ion-close"></span></a>', esc_url( wc_get_cart_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'cortex') ), $cart_item_key ); ?>
											</div>
										</div>
									</li>

								<?php endforeach; ?>
							</ul>
								<div class="mkdf-cart-bottom">
									<div class="mkdf-subtotal-holder clearfix">
										<span class="mkdf-total"><?php esc_html_e( 'Total', 'cortex' ); ?>:</span>
										<span class="mkdf-total-amount">
											<?php echo wp_kses( $woocommerce->cart->get_cart_subtotal(), array(
												'span' => array(
													'class' => true,
													'id'    => true
												)
											) ); ?>
										</span>
									</div>
									<div class="mkdf-btns-holder clearfix">
										<a href="<?php echo wc_get_cart_url(); ?>"
										   class="mkdf-btn-small view-cart">
										   <span class="ion-bag"></span>
											<?php esc_html_e( 'Shopping Bag', 'cortex' ); ?>
										</a>
										<a href="<?php echo wc_get_checkout_url(); ?>"
										   class="mkdf-btn-small checkout">
										   <span class="ion-ios-checkmark-outline"></span>
											<?php esc_html_e( 'Checkout', 'cortex' ); ?>
										</a>
									</div>
								</div>
							<?php else : ?>

								<li class="mkdf-empty-cart"><?php esc_html_e( 'No products in the cart.', 'cortex' ); ?></li>
							</ul>

							<?php endif; ?>

						<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

						<?php endif; ?>


						<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

}

add_action('widgets_init', function () {
    register_widget( "CortexMikadoWoocommerceDropdownCart" );
});

?>
<?php

if ( version_compare( WOOCOMMERCE_VERSION, '3.0' ) >= 0 ) {
	add_filter( 'woocommerce_add_to_cart_fragments', 'cortex_mikado_woocommerce_header_add_to_cart_fragment' );
} else {
	add_filter( 'add_to_cart_fragments', 'cortex_mikado_woocommerce_header_add_to_cart_fragment' );
}
function cortex_mikado_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();
	?>
	<div class="mkdf-shopping-cart-header">
		<a class="mkdf-header-cart" href="<?php echo wc_get_cart_url(); ?>">
			<span class="mkdf-cart-icon ion-bag"></span>
			<span class="mkdf-cart-amount"><?php echo esc_html($woocommerce->cart->get_cart_contents_count()); ?></span>
		</a>

		<div class="mkdf-shopping-cart-dropdown">
			<?php
			$cart_is_empty = sizeof( $woocommerce->cart->get_cart() ) <= 0;
			//$list_class = array( 'mkdf-cart-list', 'product_list_widget' );
			?>
			<ul>

				<?php if ( ! $cart_is_empty ) : ?>

					<?php foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) :

						$_product      = $cart_item['data'];

						// Only display if allowed
						if ( ! $_product->exists() || $cart_item['quantity'] == 0 ) {
							continue;
						}

						// Get price
						if ( version_compare( WOOCOMMERCE_VERSION, '3.0' ) >= 0 ) {
							$product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? wc_get_price_excluding_tax($_product) : wc_get_price_including_tax($_product);
						} else {
							$product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();
						}
						?>

						<li>
							<div class="mkdf-item-image-holder">
								<?php echo wp_kses( $_product->get_image(), array(
									'img' => array(
										'src'    => true,
										'width'  => true,
										'height' => true,
										'class'  => true,
										'alt'    => true,
										'title'  => true,
										'id'     => true
									)
								) ); ?>
							</div>
							<div class="mkdf-item-info-holder">
								<div class="mkdf-item-left">
									<a href="<?php echo esc_url( get_permalink( $cart_item['product_id'] ) ); ?>">
										<?php echo apply_filters( 'woocommerce_widget_cart_product_title', $_product->get_title(), $_product ); ?>
									</a>
									<div class="mkdf-quantity-holder">
										<span class="mkdf-quantity"><?php echo esc_html( $cart_item['quantity'] ); ?></span>
										<span>x</span>
										<?php echo apply_filters( 'woocommerce_cart_item_price_html', wc_price( $product_price ), $cart_item, $cart_item_key ); ?>
									</div>
								</div>
								<div class="mkdf-cart-remove">
									<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="ion-close"></span></a>', esc_url( wc_get_cart_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'cortex') ), $cart_item_key ); ?>
								</div>
							</div>
						</li>

					<?php endforeach; ?>
				</ul>
					<div class="mkdf-cart-bottom">
						<div class="mkdf-subtotal-holder clearfix">
							<span class="mkdf-total"><?php esc_html_e( 'Total', 'cortex' ); ?>:</span>
								<span class="mkdf-total-amount">
									<?php echo wp_kses( $woocommerce->cart->get_cart_subtotal(), array(
										'span' => array(
											'class' => true,
											'id'    => true
										)
									) ); ?>
								</span>
						</div>
						<div class="mkdf-btns-holder clearfix">
							<a href="<?php echo wc_get_cart_url(); ?>"
							   class="mkdf-btn-small view-cart">
							   <span class="ion-bag"></span>
								<?php esc_html_e( 'Shopping Bag', 'cortex' ); ?>
							</a>
							<a href="<?php echo wc_get_checkout_url(); ?>"
							   class="mkdf-btn-small checkout">
							   <span class="ion-ios-checkmark-outline"></span>
								<?php esc_html_e( 'Checkout', 'cortex' ); ?>
							</a>
						</div>
					</div>
				<?php else : ?>

					<li class="mkdf-empty-cart"><?php esc_html_e( 'No products in the cart.', 'cortex' ); ?></li>
				</ul>

				<?php endif; ?>
			<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

			<?php endif; ?>


			<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

			<?php endif; ?>
		</div>
	</div>

	<?php
	$fragments['div.mkdf-shopping-cart-header'] = ob_get_clean();

	return $fragments;
}

?>