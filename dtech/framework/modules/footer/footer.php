<?php
/**
 * Footer template part
 */

cortex_mikado_get_content_bottom_area(); ?>
</div> <!-- close div.content_inner -->
</div>  <!-- close div.content -->

<div class="vc_row wpb_row vc_row-fluid mkdf-section mkdf-content-aligment-center mkdf-grid-section ftrTrust" style=""><div class="clearfix mkdf-section-inner"><div class="mkdf-section-inner-margin clearfix"><div class="wpb_column vc_column_container vc_col-sm-1/5"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="320" height="200" src="https://dorset.tech/wp-content/uploads/trust-gd.png" class="vc_single_image-img attachment-full" alt=""></div>
		</figure>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="320" height="200" src="https://dorset.tech/wp-content/uploads/trust-apd.png" class="vc_single_image-img attachment-full" alt=""></div>
		</figure>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="320" height="200" src="https://dorset.tech/wp-content/uploads/sem-gd.png" class="vc_single_image-img attachment-full" alt=""></div>
		</figure>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="320" height="200" src="https://dorset.tech/wp-content/uploads/sem-gpd.png" class="vc_single_image-img attachment-full" alt=""></div>
		</figure>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="320" height="200" src="https://dorset.tech/wp-content/uploads/ion-gd.png" class="vc_single_image-img attachment-full" alt=""></div>
		</figure>
	</div>
</div></div></div></div></div></div>

<?php if (!isset($_REQUEST["ajax_req"]) || $_REQUEST["ajax_req"] != 'yes') { ?>
<footer <?php cortex_mikado_class_attribute($footer_classes); ?>>
	<div class="mkdf-footer-inner clearfix">

		<?php

		if($display_footer_top) {
			cortex_mikado_get_footer_top();
		}
		if($display_footer_bottom) {
			cortex_mikado_get_footer_bottom();
		}
		?>

	</div>
</footer>
<?php } ?>

</div> <!-- close div.mkdf-wrapper-inner  -->
</div> <!-- close div.mkdf-wrapper -->
<?php wp_footer(); ?>
<script type="text/javascript" src="https://crm.zoho.eu/crm/javascript/zcga.js"> </script>
</body>
</html>
