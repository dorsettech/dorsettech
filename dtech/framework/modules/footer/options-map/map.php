<?php

if ( ! function_exists('cortex_mikado_footer_options_map') ) {
	/**
	 * Add footer options
	 */
	function cortex_mikado_footer_options_map() {

		cortex_mikado_add_admin_page(
			array(
				'slug' => '_footer_page',
				'title' => esc_html__('Footer', 'cortex'),
				'icon' => 'fa fa-sort-amount-asc'
			)
		);

		$footer_panel = cortex_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Footer', 'cortex'),
				'name' => 'footer',
				'page' => '_footer_page'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'uncovering_footer',
				'default_value' => 'no',
				'label' => esc_html__('Uncovering Footer', 'cortex'),
				'description' => esc_html__('Enabling this option will make Footer gradually appear on scroll', 'cortex'),
				'parent' => $footer_panel,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'footer_in_grid',
				'default_value' => 'yes',
				'label' => esc_html__('Footer in Grid', 'cortex'),
				'description' => esc_html__('Enabling this option will place Footer content in grid', 'cortex'),
				'parent' => $footer_panel,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_top',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Top', 'cortex'),
				'description' => esc_html__('Enabling this option will show Footer Top area', 'cortex'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_show_footer_top_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_top_container = cortex_mikado_add_admin_container(
			array(
				'name' => 'show_footer_top_container',
				'hidden_property' => 'show_footer_top',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns',
				'default_value' => '4',
				'label' => esc_html__('Footer Top Columns', 'cortex'),
				'description' => esc_html__('Choose number of columns for Footer Top area', 'cortex'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'5' => '3(25%+25%+50%)',
					'6' => '3(50%+25%+25%)',
					'4' => '4'
				),
				'parent' => $show_footer_top_container,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns_alignment',
				'default_value' => '',
				'label' => esc_html__('Footer Top Columns Alignment', 'cortex'),
				'description' => esc_html__('Text Alignment in Footer Columns', 'cortex'),
				'options' => array(
					'left' => esc_html__('Left', 'cortex'),
					'center' => esc_html__('Center', 'cortex'),
					'right' => esc_html__('Right', 'cortex'),
				),
				'parent' => $show_footer_top_container,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_bottom',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Bottom', 'cortex'),
				'description' => esc_html__('Enabling this option will show Footer Bottom area', 'cortex'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_show_footer_bottom_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_bottom_container = cortex_mikado_add_admin_container(
			array(
				'name' => 'show_footer_bottom_container',
				'hidden_property' => 'show_footer_bottom',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);


		cortex_mikado_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_bottom_columns',
				'default_value' => '2',
				'label' => esc_html__('Footer Bottom Columns', 'cortex'),
				'description' => esc_html__('Choose number of columns for Footer Bottom area', 'cortex'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3'
				),
				'parent' => $show_footer_bottom_container,
			)
		);

	}

	add_action( 'cortex_mikado_options_map', 'cortex_mikado_footer_options_map', 9);

}