<?php do_action('cortex_mikado_before_page_title'); ?>
<?php if(is_category()){ ?>

    <div class="mkdf-title <?php echo cortex_mikado_title_classes(); ?>" style="<?php echo esc_attr($title_height); echo esc_attr($title_background_color); echo esc_attr($title_background_image); ?>" data-height="<?php echo esc_attr(intval(preg_replace('/[^0-9]+/', '', $title_height), 10));?>" <?php echo esc_attr($title_background_image_width); ?>>
        <div class="mkdf-title-holder" <?php cortex_mikado_inline_style($title_holder_height); ?>>
            <div class="mkdf-container clearfix">
                <div class="mkdf-container-inner">
                    <div class="mkdf-title-subtitle-holder" style="<?php echo esc_attr($title_subtitle_holder_padding); ?>">
                        <div class="mkdf-title-subtitle-holder-inner">
                        <?php /*switch ($type){
                            case 'standard': ?>
                                <h1 <?php cortex_mikado_inline_style($title_color); ?>><span><?php cortex_mikado_title_text(); ?></span></h1>
                                <?php if($has_subtitle){ ?>
                                    <span class="mkdf-subtitle" <?php cortex_mikado_inline_style($subtitle_color); ?>><span><?php cortex_mikado_subtitle_text(); ?></span></span>
                                <?php } ?>
                                <?php if($enable_breadcrumbs){ ?>
                                    <div class="mkdf-breadcrumbs-holder"> <?php cortex_mikado_custom_breadcrumbs(); ?></div>
                                <?php } ?>
                            <?php break;
                            case 'breadcrumb': ?>
                                <div class="mkdf-breadcrumbs-holder"> <?php cortex_mikado_custom_breadcrumbs(); ?></div>
                            <?php break;
                            }*/
                            
                                echo '<h1>'; single_cat_title(); echo '</h1>';
                                if(category_description()){
                                    echo category_description();
                                }

                            
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php  }
if(is_singular('portfolio-item')){ ?>

    <div class="mkdf-title <?php echo cortex_mikado_title_classes(); ?>" style="<?php echo esc_attr($title_height); echo esc_attr($title_background_color); echo esc_attr($title_background_image); ?>" data-height="<?php echo esc_attr(intval(preg_replace('/[^0-9]+/', '', $title_height), 10));?>" <?php echo esc_attr($title_background_image_width); ?>>
        <div class="mkdf-title-holder" <?php cortex_mikado_inline_style($title_holder_height); ?>>
            <div class="mkdf-container clearfix">
                <div class="mkdf-container-inner">
                    <div class="mkdf-title-subtitle-holder" style="<?php echo esc_attr($title_subtitle_holder_padding); ?>">
                        <div class="mkdf-title-subtitle-holder-inner">
                        <?php switch ($type){
                            case 'standard': ?>
                                <h1 <?php cortex_mikado_inline_style($title_color); ?>><span><?php cortex_mikado_title_text(); ?></span></h1>
                                <?php if($has_subtitle){ ?>
                                    <span class="mkdf-subtitle" <?php cortex_mikado_inline_style($subtitle_color); ?>><span><?php cortex_mikado_subtitle_text(); ?></span></span>
                                <?php } ?>
                                <?php if($enable_breadcrumbs){ ?>
                                    <div class="mkdf-breadcrumbs-holder"> <?php cortex_mikado_custom_breadcrumbs(); ?></div>
                                <?php } ?>
                            <?php break;
                            case 'breadcrumb': ?>
                                <div class="mkdf-breadcrumbs-holder"> <?php cortex_mikado_custom_breadcrumbs(); ?></div>
                            <?php break;
                            }
                            
                               

                            
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php  } ?>
<?php do_action('cortex_mikado_after_page_title'); ?>