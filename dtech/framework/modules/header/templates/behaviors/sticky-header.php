<?php do_action('cortex_mikado_before_sticky_header'); ?>

<div class="mkdf-sticky-header">
    <?php do_action( 'cortex_mikado_after_sticky_menu_html_open' ); ?>
    <div class="mkdf-sticky-holder <?php echo esc_attr($sticky_standard_menu_area_class); ?>">
    <?php if($sticky_header_in_grid) : ?>
        <div class="mkdf-grid">
            <?php endif; ?>
            <div class=" mkdf-vertical-align-containers">
                <div class="mkdf-position-left">
                    <div class="mkdf-position-left-inner">
                        <?php if(!$hide_logo) {
                            cortex_mikado_get_logo('sticky');
                        } ?>
                        <?php if($set_menu_area_position === 'left') : ?>
                            <?php cortex_mikado_get_sticky_menu('mkdf-sticky-nav'); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="mkdf-position-right">
                    <div class="mkdf-position-right-inner">
                        <?php if($set_menu_area_position === 'right') : ?>
                            <?php cortex_mikado_get_sticky_menu('mkdf-sticky-nav'); ?>
                        <?php endif; ?>
                        <?php
                        	cortex_mikado_get_sticky_header_widget();
						?>
                    </div>
                </div>
            </div>
            <?php if($sticky_header_in_grid) : ?>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php do_action('cortex_mikado_after_sticky_header'); ?>