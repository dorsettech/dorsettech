<?php do_action('cortex_mikado_before_top_navigation'); ?>
<a href="javascript:void(0)" class="mkdf-fullscreen-menu-opener <?php echo esc_attr($icon_size)?>">
	<span class="mkdf-fullscreen-menu-opener-inner">
		<span class="mkdf-fullscreen-icon icon_menu"></span>
		<span class="mkdf-fullscreen-icon-close ion-close"></span>
	</span>
</a>
<?php do_action('cortex_mikado_after_top_navigation'); ?>