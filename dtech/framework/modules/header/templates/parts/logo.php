<?php do_action('cortex_mikado_before_site_logo'); ?>

<div class="mkdf-logo-wrapper">
    <a href="<?php echo esc_url(home_url('/')); ?>" <?php cortex_mikado_inline_style($logo_styles); ?>>
        <img class="mkdf-normal-logo" src="<?php echo esc_url($logo_image); ?>" alt="<?php esc_html_e('Web Design Dorset','cortex'); ?>"/>
        <?php if(!empty($logo_image_dark)){ ?><img class="mkdf-dark-logo" src="<?php echo esc_url($logo_image_dark); ?>" alt="<?php esc_html_e('Web Design Dorset','cortex'); ?>"/><?php } ?>
        <?php if(!empty($logo_image_light)){ ?><img class="mkdf-light-logo" src="<?php echo esc_url($logo_image_light); ?>" alt="<?php esc_html_e('Web Design Dorset','cortex'); ?>"/><?php } ?>
    </a>
</div>

<?php do_action('cortex_mikado_after_site_logo'); ?>