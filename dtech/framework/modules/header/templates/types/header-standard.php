<?php do_action('cortex_mikado_before_page_header'); ?>

<header class="mkdf-page-header">
    <?php if($show_fixed_wrapper) : ?>
        <div class="mkdf-fixed-wrapper">
    <?php endif; ?>
    <div class="mkdf-menu-area <?php echo esc_attr($standard_menu_area_class); ?>" <?php cortex_mikado_inline_style(array($menu_area_background_color, $menu_area_border_bottom_color)); ?>>
        <?php if($menu_area_in_grid) : ?>
            <div class="mkdf-grid">
        <?php endif; ?>
			<?php do_action( 'cortex_mikado_after_header_menu_area_html_open' )?>
            <div class="mkdf-vertical-align-containers">
                <div class="mkdf-position-left">
                    <div class="mkdf-position-left-inner">
                        <?php if(!$hide_logo) {
                            cortex_mikado_get_logo();
                        } ?>
                        <?php if($set_menu_area_position === 'left') : ?>
                            <?php cortex_mikado_get_main_menu(); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="mkdf-position-right">
                    <div class="mkdf-position-right-inner">
                        <?php if($set_menu_area_position === 'right') : ?>
                            <?php cortex_mikado_get_main_menu(); ?>
                        <?php endif; ?>
                        <?php cortex_mikado_get_header_widget(); ?>
                    </div>
                </div>
            </div>
        <?php if($menu_area_in_grid) : ?>
        </div>
        <?php endif; ?>
    </div>
    <?php if($show_fixed_wrapper) : ?>
        </div>
    <?php endif; ?>
    <?php if($show_sticky) {
        cortex_mikado_get_sticky_header();
    } ?>
</header>

<?php do_action('cortex_mikado_after_page_header'); ?>

