<?php

if(!function_exists('cortex_mikado_register_top_header_areas')) {
    /**
     * Registers widget areas for top header bar when it is enabled
     */
    function cortex_mikado_register_top_header_areas() {
        $top_bar_layout  = cortex_mikado_options()->getOptionValue('top_bar_layout');
            register_sidebar(array(
                'name'          => esc_html__('Top Bar Left', 'cortex'),
                'id'            => 'mkdf-top-bar-left',
                'before_widget' => '<div id="%1$s" class="widget %2$s mkdf-top-bar-widget">',
                'after_widget'  => '</div>'
            ));

            //register this widget area only if top bar layout is three columns
            if($top_bar_layout === 'three-columns') {
                register_sidebar(array(
                    'name'          => esc_html__('Top Bar Center', 'cortex'),
                    'id'            => 'mkdf-top-bar-center',
                    'before_widget' => '<div id="%1$s" class="widget %2$s mkdf-top-bar-widget">',
                    'after_widget'  => '</div>'
                ));
            }

            register_sidebar(array(
                'name'          => esc_html__('Top Bar Right', 'cortex'),
                'id'            => 'mkdf-top-bar-right',
                'before_widget' => '<div id="%1$s" class="widget %2$s mkdf-top-bar-widget">',
                'after_widget'  => '</div>'
            ));
    }

    add_action('widgets_init', 'cortex_mikado_register_top_header_areas');
}

if(!function_exists('cortex_mikado_header_widget_areas')) {
    /**
     * Registers widget areas for header
     */
    function cortex_mikado_header_widget_areas() {
            register_sidebar(array(
                'name'          => esc_html__('Header Widget Area', 'cortex'),
                'id'            => 'mkdf-header-widget-area',
                'before_widget' => '<div id="%1$s" class="widget %2$s mkdf-header-widget">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from the main menu, or in bottom of vertical menu', 'cortex')
            ));
    }

    add_action('widgets_init', 'cortex_mikado_header_widget_areas');
}

if(!function_exists('cortex_mikado_register_mobile_header_areas')) {
    /**
     * Registers widget areas for mobile header
     */
    function cortex_mikado_register_mobile_header_areas() {
        if(cortex_mikado_is_responsive_on()) {
            register_sidebar(array(
                'name'          => esc_html__('Mobile Header Widget Area', 'cortex'),
                'id'            => 'mkdf-mobile-widget-area',
                'before_widget' => '<div id="%1$s" class="widget %2$s mkdf-mobile-header-widget">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from the mobile logo', 'cortex')
            ));
        }
    }

    add_action('widgets_init', 'cortex_mikado_register_mobile_header_areas');
}

if(!function_exists('cortex_mikado_register_sticky_header_areas')) {
    /**
     * Registers widget area for sticky header
     */
    function cortex_mikado_register_sticky_header_areas() {
        if(in_array(cortex_mikado_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
            register_sidebar(array(
                'name'          => esc_html__('Sticky Header Widget Area', 'cortex'),
                'id'            => 'mkdf-sticky-widget-area',
                'before_widget' => '<div id="%1$s" class="widget %2$s mkdf-sticky-widget">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side in sticky menu', 'cortex')
            ));
        }
    }

    add_action('widgets_init', 'cortex_mikado_register_sticky_header_areas');
}