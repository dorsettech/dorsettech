<?php

if(!function_exists('cortex_mikado_header_top_bar_styles')) {
    /**
     * Generates styles for header top bar
     */
    function cortex_mikado_header_top_bar_styles() {
        global $cortex_mikado_options;

        if($cortex_mikado_options['top_bar_height'] !== '') {
            echo cortex_mikado_dynamic_css('.mkdf-top-bar', array('height' => $cortex_mikado_options['top_bar_height'].'px'));
            echo cortex_mikado_dynamic_css('.mkdf-top-bar .mkdf-logo-wrapper a', array('max-height' => $cortex_mikado_options['top_bar_height'].'px'));
        }

        if($cortex_mikado_options['top_bar_in_grid'] == 'yes') {
            $top_bar_grid_selector = '.mkdf-top-bar .mkdf-grid .mkdf-vertical-align-containers';
            $top_bar_grid_styles = array();
            if($cortex_mikado_options['top_bar_grid_background_color'] !== '') {
                $grid_background_color    = $cortex_mikado_options['top_bar_grid_background_color'];
                $grid_background_transparency = 1;

                if(cortex_mikado_options()->getOptionValue('top_bar_grid_background_transparency')) {
                    $grid_background_transparency = cortex_mikado_options()->getOptionValue('top_bar_grid_background_transparency');
                }

                $grid_background_color = cortex_mikado_rgba_color($grid_background_color, $grid_background_transparency);
                $top_bar_grid_styles['background-color'] = $grid_background_color;
            }

            echo cortex_mikado_dynamic_css($top_bar_grid_selector, $top_bar_grid_styles);
        }

        $background_color = cortex_mikado_options()->getOptionValue('top_bar_background_color');
        $top_bar_styles = array();
        if($background_color !== '') {
            $background_transparency = 1;
            if(cortex_mikado_options()->getOptionValue('top_bar_background_transparency') !== '') {
               $background_transparency = cortex_mikado_options()->getOptionValue('top_bar_background_transparency');
            }

            $background_color = cortex_mikado_rgba_color($background_color, $background_transparency);
            $top_bar_styles['background-color'] = $background_color;
        }

        echo cortex_mikado_dynamic_css('.mkdf-top-bar', $top_bar_styles);

    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_header_top_bar_styles');
}


if (!function_exists('cortex_mikado_header_top_bar_responsive_styles')){
	/**
     * Generates styles for header top bar on responsive
     */
	function cortex_mikado_header_top_bar_responsive_styles(){
		$top_bar_responsive_styles = array();

        $hide_top_bar_on_responsive = cortex_mikado_options()->getOptionValue('hide_top_bar_on_responsive');

        if($hide_top_bar_on_responsive === 'yes') { 
        	$top_bar_responsive_styles['height'] = '0';
        	$top_bar_responsive_styles['display'] = 'none';
        }

        echo cortex_mikado_dynamic_css('.mkdf-top-bar', $top_bar_responsive_styles);

	}

	add_action('cortex_mikado_style_dynamic_responsive_1024', 'cortex_mikado_header_top_bar_responsive_styles');
}

if(!function_exists('cortex_mikado_header_standard_menu_area_styles')) {
    /**
     * Generates styles for header standard menu
     */
    function cortex_mikado_header_standard_menu_area_styles() {
        global $cortex_mikado_options;

        $menu_area_header_standard_styles = array();

        if($cortex_mikado_options['menu_area_background_color_header_standard'] !== '') {
            $menu_area_background_color        = $cortex_mikado_options['menu_area_background_color_header_standard'];
            $menu_area_background_transparency = 1;

            if($cortex_mikado_options['menu_area_background_transparency_header_standard'] !== '') {
                $menu_area_background_transparency = $cortex_mikado_options['menu_area_background_transparency_header_standard'];
            }

            $menu_area_header_standard_styles['background-color'] = cortex_mikado_rgba_color($menu_area_background_color, $menu_area_background_transparency);
        }

        if($cortex_mikado_options['menu_area_height_header_standard'] !== '') {
            $max_height = intval(cortex_mikado_filter_px($cortex_mikado_options['menu_area_height_header_standard']) * 0.9).'px';
            echo cortex_mikado_dynamic_css('.mkdf-header-standard .mkdf-page-header .mkdf-logo-wrapper a', array('max-height' => $max_height));

            $menu_area_header_standard_styles['height'] = cortex_mikado_filter_px($cortex_mikado_options['menu_area_height_header_standard']).'px';

        }

        echo cortex_mikado_dynamic_css('.mkdf-header-standard .mkdf-page-header .mkdf-menu-area', $menu_area_header_standard_styles);

    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_header_standard_menu_area_styles');
}

if(!function_exists('cortex_mikado_header_centered_menu_area_styles')) {
    /**
     * Generates styles for header centered menu
     */
    function cortex_mikado_header_centered_menu_area_styles() {
        global $cortex_mikado_options;

        $menu_area_header_centered_styles = array();

        if($cortex_mikado_options['menu_area_background_color_header_centered'] !== '') {
            $menu_area_background_color        = $cortex_mikado_options['menu_area_background_color_header_centered'];
            $menu_area_background_transparency = 1;

            if($cortex_mikado_options['menu_area_background_transparency_header_centered'] !== '') {
                $menu_area_background_transparency = $cortex_mikado_options['menu_area_background_transparency_header_centered'];
            }

            $menu_area_header_centered_styles['background-color'] = cortex_mikado_rgba_color($menu_area_background_color, $menu_area_background_transparency);
        }

        if($cortex_mikado_options['menu_area_height_header_centered'] !== '') {
            $max_height = intval(cortex_mikado_filter_px($cortex_mikado_options['menu_area_height_header_centered']) * 0.9).'px';
            echo cortex_mikado_dynamic_css('.mkdf-header-centered .mkdf-page-header .mkdf-logo-wrapper a', array('max-height' => $max_height));

            $menu_area_header_centered_styles['height'] = cortex_mikado_filter_px($cortex_mikado_options['menu_area_height_header_centered']).'px';

        }

        echo cortex_mikado_dynamic_css('.mkdf-header-centered .mkdf-page-header .mkdf-menu-area', $menu_area_header_centered_styles);

    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_header_centered_menu_area_styles');
}

if(!function_exists('cortex_mikado_vertical_menu_styles')) {
    /**
     * Generates styles for sticky haeder
     */
    function cortex_mikado_vertical_menu_styles() {

        $vertical_header_styles = array();

        $vertical_header_selectors = array(
            '.mkdf-header-vertical .mkdf-vertical-area-background'
        );

        if(cortex_mikado_options()->getOptionValue('vertical_header_background_color') !== '') {
            $vertical_header_styles['background-color'] = cortex_mikado_options()->getOptionValue('vertical_header_background_color');
        }

        if(cortex_mikado_options()->getOptionValue('vertical_header_transparency') !== '') {
            $vertical_header_styles['opacity'] = cortex_mikado_options()->getOptionValue('vertical_header_transparency');
        }

        if(cortex_mikado_options()->getOptionValue('vertical_header_background_image') !== '') {
            $vertical_header_styles['background-image'] = 'url('.cortex_mikado_options()->getOptionValue('vertical_header_background_image').')';
        }


        echo cortex_mikado_dynamic_css($vertical_header_selectors, $vertical_header_styles);
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_vertical_menu_styles');
}

if(!function_exists('cortex_mikado_header_full_screen_menu_area_styles')) {
	/**
	 * Generates styles for header standard menu
	 */
	function cortex_mikado_header_full_screen_menu_area_styles() {
		global $cortex_mikado_options;

		$menu_area_header_full_screen_styles = array();

		if($cortex_mikado_options['menu_area_background_color_header_full_screen'] !== '') {
			$menu_area_background_color        = $cortex_mikado_options['menu_area_background_color_header_full_screen'];
			$menu_area_background_transparency = 1;

			if($cortex_mikado_options['menu_area_background_transparency_header_full_screen'] !== '') {
				$menu_area_background_transparency = $cortex_mikado_options['menu_area_background_transparency_header_full_screen'];
			}

			$menu_area_header_full_screen_styles['background-color'] = cortex_mikado_rgba_color($menu_area_background_color, $menu_area_background_transparency);
		}

		if($cortex_mikado_options['menu_area_height_header_full_screen'] !== '') {
			$max_height = intval(cortex_mikado_filter_px($cortex_mikado_options['menu_area_height_header_full_screen']) * 0.9).'px';
			echo cortex_mikado_dynamic_css('.mkdf-header-full-screen .mkdf-page-header .mkdf-logo-wrapper a', array('max-height' => $max_height));

			$menu_area_header_full_screen_styles['height'] = cortex_mikado_filter_px($cortex_mikado_options['menu_area_height_header_full_screen']).'px';

		}

		echo cortex_mikado_dynamic_css('.mkdf-header-full-screen .mkdf-page-header .mkdf-menu-area', $menu_area_header_full_screen_styles);

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_header_full_screen_menu_area_styles');
}

if(!function_exists('cortex_mikado_sticky_header_styles')) {
    /**
     * Generates styles for sticky haeder
     */
    function cortex_mikado_sticky_header_styles() {
        global $cortex_mikado_options;

        if($cortex_mikado_options['sticky_header_in_grid'] == 'yes' && $cortex_mikado_options['sticky_header_grid_background_color'] !== '') {
            $sticky_header_grid_background_color        = $cortex_mikado_options['sticky_header_grid_background_color'];
            $sticky_header_grid_background_transparency = 1;

            if($cortex_mikado_options['sticky_header_grid_transparency'] !== '') {
                $sticky_header_grid_background_transparency = $cortex_mikado_options['sticky_header_grid_transparency'];
            }

            echo cortex_mikado_dynamic_css('.mkdf-page-header .mkdf-sticky-header .mkdf-grid .mkdf-vertical-align-containers', array('background-color' => cortex_mikado_rgba_color($sticky_header_grid_background_color, $sticky_header_grid_background_transparency)));
        }

        if($cortex_mikado_options['sticky_header_background_color'] !== '') {

            $sticky_header_background_color              = $cortex_mikado_options['sticky_header_background_color'];
            $sticky_header_background_color_transparency = 1;

            if($cortex_mikado_options['sticky_header_transparency'] !== '') {
                $sticky_header_background_color_transparency = $cortex_mikado_options['sticky_header_transparency'];
            }

            echo cortex_mikado_dynamic_css('.mkdf-page-header .mkdf-sticky-header .mkdf-sticky-holder', array('background-color' => cortex_mikado_rgba_color($sticky_header_background_color, $sticky_header_background_color_transparency)));
        }

        if($cortex_mikado_options['sticky_header_height'] !== '') {
            $max_height = intval(cortex_mikado_filter_px($cortex_mikado_options['sticky_header_height']) * 0.9).'px';

            echo cortex_mikado_dynamic_css('.mkdf-page-header .mkdf-sticky-header', array('height' => $cortex_mikado_options['sticky_header_height'].'px'));
            echo cortex_mikado_dynamic_css('.mkdf-page-header .mkdf-sticky-header .mkdf-logo-wrapper a', array('max-height' => $max_height));
        }

        $sticky_menu_item_styles = array();
        if($cortex_mikado_options['sticky_color'] !== '') {
            $sticky_menu_item_styles['color'] = $cortex_mikado_options['sticky_color'];
        }
        if($cortex_mikado_options['sticky_google_fonts'] !== '-1') {
            $sticky_menu_item_styles['font-family'] = cortex_mikado_get_formatted_font_family($cortex_mikado_options['sticky_google_fonts']);
        }
        if($cortex_mikado_options['sticky_fontsize'] !== '') {
            $sticky_menu_item_styles['font-size'] = $cortex_mikado_options['sticky_fontsize'].'px';
        }
        if($cortex_mikado_options['sticky_lineheight'] !== '') {
            $sticky_menu_item_styles['line-height'] = $cortex_mikado_options['sticky_lineheight'].'px';
        }
        if($cortex_mikado_options['sticky_texttransform'] !== '') {
            $sticky_menu_item_styles['text-transform'] = $cortex_mikado_options['sticky_texttransform'];
        }
        if($cortex_mikado_options['sticky_fontstyle'] !== '') {
            $sticky_menu_item_styles['font-style'] = $cortex_mikado_options['sticky_fontstyle'];
        }
        if($cortex_mikado_options['sticky_fontweight'] !== '') {
            $sticky_menu_item_styles['font-weight'] = $cortex_mikado_options['sticky_fontweight'];
        }
        if($cortex_mikado_options['sticky_letterspacing'] !== '') {
            $sticky_menu_item_styles['letter-spacing'] = $cortex_mikado_options['sticky_letterspacing'].'px';
        }

        $sticky_menu_item_selector = array(
            '.mkdf-main-menu.mkdf-sticky-nav > ul > li > a'
        );

        echo cortex_mikado_dynamic_css($sticky_menu_item_selector, $sticky_menu_item_styles);

        $sticky_menu_item_hover_styles = array();
        if($cortex_mikado_options['sticky_hovercolor'] !== '') {
            $sticky_menu_item_hover_styles['color'] = $cortex_mikado_options['sticky_hovercolor'];
        }

        $sticky_menu_item_hover_selector = array(
            '.mkdf-main-menu.mkdf-sticky-nav > ul > li:hover > a',
            '.mkdf-main-menu.mkdf-sticky-nav > ul > li.mkdf-active-item > a'
        );

        echo cortex_mikado_dynamic_css($sticky_menu_item_hover_selector, $sticky_menu_item_hover_styles);
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_sticky_header_styles');
}

if(!function_exists('cortex_mikado_fixed_header_styles')) {
    /**
     * Generates styles for fixed header
     */
    function cortex_mikado_fixed_header_styles() {
        global $cortex_mikado_options;

        if($cortex_mikado_options['fixed_header_grid_background_color'] !== '') {

            $fixed_header_grid_background_color              = $cortex_mikado_options['fixed_header_grid_background_color'];
            $fixed_header_grid_background_color_transparency = 1;

            if($cortex_mikado_options['fixed_header_grid_transparency'] !== '') {
                $fixed_header_grid_background_color_transparency = $cortex_mikado_options['fixed_header_grid_transparency'];
            }

            echo cortex_mikado_dynamic_css('.mkdf-fixed-on-scroll .mkdf-fixed-wrapper.fixed .mkdf-grid .mkdf-vertical-align-containers',
                array('background-color' => cortex_mikado_rgba_color($fixed_header_grid_background_color, $fixed_header_grid_background_color_transparency)));
        }

        if($cortex_mikado_options['fixed_header_background_color'] !== '') {

            $fixed_header_background_color              = $cortex_mikado_options['fixed_header_background_color'];
            $fixed_header_background_color_transparency = 1;

            if($cortex_mikado_options['fixed_header_transparency'] !== '') {
                $fixed_header_background_color_transparency = $cortex_mikado_options['fixed_header_transparency'];
            }

            echo cortex_mikado_dynamic_css('.mkdf-fixed-on-scroll .mkdf-fixed-wrapper.fixed .mkdf-menu-area',
                array('background-color' => cortex_mikado_rgba_color($fixed_header_background_color, $fixed_header_background_color_transparency)));
        }

    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_fixed_header_styles');
}

if(!function_exists('cortex_mikado_main_menu_styles')) {
    /**
     * Generates styles for main menu
     */
    function cortex_mikado_main_menu_styles() {
        global $cortex_mikado_options;

        if($cortex_mikado_options['menu_color'] !== '' || $cortex_mikado_options['menu_fontsize'] != '' || $cortex_mikado_options['menu_fontstyle'] !== '' || $cortex_mikado_options['menu_fontweight'] !== '' || $cortex_mikado_options['menu_texttransform'] !== '' || $cortex_mikado_options['menu_letterspacing'] !== '' || $cortex_mikado_options['menu_google_fonts'] != "-1") { ?>
            .mkdf-main-menu.mkdf-default-nav > ul > li > a,
            .mkdf-page-header #lang_sel > ul > li > a,
            .mkdf-page-header #lang_sel_click > ul > li > a,
            .mkdf-page-header #lang_sel ul > li:hover > a{
            <?php if($cortex_mikado_options['menu_color']) { ?> color: <?php echo esc_attr($cortex_mikado_options['menu_color']); ?>; <?php } ?>
            <?php if($cortex_mikado_options['menu_google_fonts'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $cortex_mikado_options['menu_google_fonts'])); ?>', sans-serif;
            <?php } ?>
            <?php if($cortex_mikado_options['menu_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($cortex_mikado_options['menu_fontsize']); ?>px; <?php } ?>
            <?php if($cortex_mikado_options['menu_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($cortex_mikado_options['menu_fontstyle']); ?>; <?php } ?>
            <?php if($cortex_mikado_options['menu_fontweight'] !== '') { ?> font-weight: <?php echo esc_attr($cortex_mikado_options['menu_fontweight']); ?>; <?php } ?>
            <?php if($cortex_mikado_options['menu_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($cortex_mikado_options['menu_texttransform']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['menu_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($cortex_mikado_options['menu_letterspacing']); ?>px; <?php } ?>
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_google_fonts'] != "-1") { ?>
            .mkdf-page-header #lang_sel_list{
            font-family: '<?php echo esc_attr(str_replace('+', ' ', $cortex_mikado_options['menu_google_fonts'])); ?>', sans-serif !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_hovercolor'] !== '') { ?>
            .mkdf-main-menu.mkdf-default-nav > ul > li:hover > a,
            .mkdf-main-menu.mkdf-default-nav > ul > li.mkdf-active-item:hover > a,
            .mkdf-page-header #lang_sel ul li a:hover,
            .mkdf-page-header #lang_sel_click > ul > li a:hover{
            color: <?php echo esc_attr($cortex_mikado_options['menu_hovercolor']); ?> !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_activecolor'] !== '') { ?>
            .mkdf-main-menu.mkdf-default-nav > ul > li.mkdf-active-item > a{
            color: <?php echo esc_attr($cortex_mikado_options['menu_activecolor']); ?>;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_item_icon_position'] == "top" && $cortex_mikado_options['menu_item_icon_size'] !== "") { ?>
            body.mkdf-menu-with-large-icons .mkdf-main-menu.mkdf-default-nav > ul > li > a span.mkdf-item-inner i{
            font-size: <?php echo esc_attr($cortex_mikado_options['menu_item_icon_size']); ?>px !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_light_hovercolor'] !== '') { ?>
			.mkdf-light-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-main-menu > ul > li > a:hover,
            .mkdf-light-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-main-menu > ul > li.mkdf-active-item > a:hover,
            .mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu > ul > li > a:hover,
            .mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu > ul > li.mkdf-active-item > a:hover{
            color: <?php echo esc_attr($cortex_mikado_options['menu_light_hovercolor']); ?> !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_light_activecolor'] !== '') { ?>
            .mkdf-light-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-main-menu > ul > li.mkdf-active-item > a,
            .mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu > ul > li.mkdf-active-item > a{
            color: <?php echo esc_attr($cortex_mikado_options['menu_light_activecolor']); ?> !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_dark_hovercolor'] !== '') { ?>
            .mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-main-menu > ul > li > a:hover,
            .mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-main-menu > ul > li.mkdf-active-item > a:hover,
            .mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu > ul > li > a:hover,
            .mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu > ul > li.mkdf-active-item > a:hover{
            color: <?php echo esc_attr($cortex_mikado_options['menu_dark_hovercolor']); ?> !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_dark_activecolor'] !== '') { ?>
            .mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-main-menu > ul > li.mkdf-active-item > a,
            .mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-main-menu > ul > li.mkdf-active-item > a{
            color: <?php echo esc_attr($cortex_mikado_options['menu_dark_activecolor']); ?>;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_lineheight'] != "" || $cortex_mikado_options['menu_padding_left_right'] !== '') { ?>
            .mkdf-main-menu.mkdf-default-nav > ul > li > a span.mkdf-item-inner{
            <?php if($cortex_mikado_options['menu_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($cortex_mikado_options['menu_lineheight']); ?>px; <?php } ?>
            <?php if($cortex_mikado_options['menu_padding_left_right']) { ?> padding: 0  <?php echo esc_attr($cortex_mikado_options['menu_padding_left_right']); ?>px; <?php } ?>
            }
        <?php } ?>

        <?php if($cortex_mikado_options['menu_margin_left_right'] !== '') { ?>
            .mkdf-main-menu.mkdf-default-nav > ul > li{
            margin: 0  <?php echo esc_attr($cortex_mikado_options['menu_margin_left_right']); ?>px;
            }
        <?php } ?>

        <?php
        if($cortex_mikado_options['dropdown_background_color'] != "" || $cortex_mikado_options['dropdown_background_transparency'] != "") {

            //dropdown background and transparency styles
            $dropdown_bg_color_initial        = '#ffffff';
            $dropdown_bg_transparency_initial = 1;

            $dropdown_bg_color        = $cortex_mikado_options['dropdown_background_color'] !== "" ? $cortex_mikado_options['dropdown_background_color'] : $dropdown_bg_color_initial;
            $dropdown_bg_transparency = $cortex_mikado_options['dropdown_background_transparency'] !== "" ? $cortex_mikado_options['dropdown_background_transparency'] : $dropdown_bg_transparency_initial;

            $dropdown_bg_color_rgb = cortex_mikado_hex2rgb($dropdown_bg_color);

            ?>

            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul,
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li ul,
            .shopping_cart_dropdown,
            .mkdf-drop-down .mkdf-menu-narrow .mkdf-menu-second .mkdf-menu-inner ul,
            .mkdf-main-menu.mkdf-default-nav #lang_sel ul ul,
            .mkdf-main-menu.mkdf-default-nav #lang_sel_click  ul ul,
            .header-widget.widget_nav_menu ul ul,
            .mkdf-drop-down .mkdf-menu-wide.mkdf-wide-background .mkdf-menu-second{
            background-color: <?php echo esc_attr($dropdown_bg_color); ?>;
            background-color: rgba(<?php echo esc_attr($dropdown_bg_color_rgb[0]); ?>,<?php echo esc_attr($dropdown_bg_color_rgb[1]); ?>,<?php echo esc_attr($dropdown_bg_color_rgb[2]); ?>,<?php echo esc_attr($dropdown_bg_transparency); ?>);
            }

        <?php } //end dropdown background and transparency styles ?>

        <?php
        if($cortex_mikado_options['dropdown_top_padding'] !== '') {

            $menu_inner_ul_top = 15; //default value without border
            if($cortex_mikado_options['dropdown_top_padding'] !== '') {
                ?>
                .mkdf-drop-down .mkdf-menu-narrow .mkdf-menu-second .mkdf-menu-inner ul,
                .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner > ul{
                padding-top: <?php echo esc_attr($cortex_mikado_options['dropdown_top_padding']); ?>px;
                }
                <?php
                $menu_inner_ul_top = $cortex_mikado_options['dropdown_top_padding']; //overwrite default value
            } ?>
            .mkdf-drop-down .mkdf-menu-narrow .mkdf-menu-second .mkdf-menu-inner ul li ul,
            body.mkdf-slide-from-bottom .mkdf-drop-down .mkdf-menu-narrow .mkdf-menu-second .mkdf-menu-inner ul li:hover ul,
            body.mkdf-slide-from-top .mkdf-menu-narrow .mkdf-menu-second .mkdf-menu-inner ul li:hover ul{
            top:-<?php echo esc_attr($menu_inner_ul_top); ?>px;
            }

        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_bottom_padding'] !== '') { ?>
		    .mkdf-drop-down .mkdf-menu-narrow .mkdf-menu-second .mkdf-menu-inner ul,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner > ul{
            padding-bottom: <?php echo esc_attr($cortex_mikado_options['dropdown_bottom_padding']); ?>px;
            }
        <?php } ?>

        <?php
        $dropdown_separator_full_width = 'no';
        if($cortex_mikado_options['enable_dropdown_separator_full_width'] == "yes") {
            $dropdown_separator_full_width = $cortex_mikado_options['enable_dropdown_separator_full_width']; ?>
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner > ul > li:last-child > a,
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner > ul > li > ul > li:last-child > a,
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner > ul > li > ul > li > ul > li:last-child > a{
            border-bottom:1px solid transparent;
            }

        <?php }
        if($dropdown_separator_full_width !== 'yes' && $cortex_mikado_options['dropdown_separator_color'] !== "") {
            ?>
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li a,
            .header-widget.widget_nav_menu ul.menu li ul li a {
            border-color: <?php echo esc_attr($cortex_mikado_options['dropdown_separator_color']); ?>;
            }
        <?php } ?>
        <?php
        if($dropdown_separator_full_width == 'yes') {
            ?>
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li,
            .header-widget.widget_nav_menu ul.menu li ul li{
	        border-bottom-width:1px;
	        border-bottom-style:solid;
            <?php if($cortex_mikado_options['dropdown_separator_color'] !== "") {?> border-bottom-color: <?php echo esc_attr($cortex_mikado_options['dropdown_separator_color']); ?>; <?php } ?>
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_top_position'] !== '') { ?>
            header .mkdf-drop-down .mkdf-menu-second {
            top: <?php echo esc_attr($cortex_mikado_options['dropdown_top_position']).'%;'; ?>
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_color'] !== '' || $cortex_mikado_options['dropdown_fontsize'] !== '' || $cortex_mikado_options['dropdown_lineheight'] !== '' || $cortex_mikado_options['dropdown_fontstyle'] !== '' || $cortex_mikado_options['dropdown_fontweight'] !== '' || $cortex_mikado_options['dropdown_google_fonts'] != "-1" || $cortex_mikado_options['dropdown_texttransform'] !== '' || $cortex_mikado_options['dropdown_letterspacing'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner > ul > li > a,
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner > ul > li > h4,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner > ul > li > h4,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner > ul > li > a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second ul li ul li.menu-item-has-children > a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner ul li.mkdf-sub ul li.menu-item-has-children > a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel ul li li a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel_click ul li ul li a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel ul ul a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel_click ul ul a{
            <?php if(!empty($cortex_mikado_options['dropdown_color'])) { ?> color: <?php echo esc_attr($cortex_mikado_options['dropdown_color']); ?>; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_google_fonts'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $cortex_mikado_options['dropdown_google_fonts'])); ?>', sans-serif !important;
            <?php } ?>
            <?php if($cortex_mikado_options['dropdown_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($cortex_mikado_options['dropdown_fontsize']); ?>px; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($cortex_mikado_options['dropdown_lineheight']); ?>px; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($cortex_mikado_options['dropdown_fontstyle']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_fontweight'] !== '') { ?>font-weight: <?php echo esc_attr($cortex_mikado_options['dropdown_fontweight']); ?>; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($cortex_mikado_options['dropdown_texttransform']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($cortex_mikado_options['dropdown_letterspacing']); ?>px;  <?php } ?>
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_color'] !== '') { ?>
            .shopping_cart_dropdown ul li
            .item_info_holder .item_left a,
            .shopping_cart_dropdown ul li .item_info_holder .item_right .amount,
            .shopping_cart_dropdown .cart_bottom .subtotal_holder .total,
            .shopping_cart_dropdown .cart_bottom .subtotal_holder .total_amount{
            color: <?php echo esc_attr($cortex_mikado_options['dropdown_color']); ?>;
            }
        <?php } ?>

        <?php if(!empty($cortex_mikado_options['dropdown_hovercolor'])) { ?>
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner > ul > li:hover > a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second ul li ul li.menu-item-has-children:hover > a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner ul li.mkdf-sub ul li.menu-item-has-children:hover > a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel ul li li:hover a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel_click ul li ul li:hover a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel ul li:hover > a,
            .mkdf-main-menu.mkdf-default-nav #lang_sel_click ul li:hover > a{
            color: <?php echo esc_attr($cortex_mikado_options['dropdown_hovercolor']); ?> !important;
            }
        <?php } ?>

        <?php if(!empty($cortex_mikado_options['dropdown_padding_top_bottom'])) { ?>
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second>.mkdf-menu-inner>ul>li.mkdf-sub>ul>li>a,
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second ul li a,
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul.right li a{
            padding-top: <?php echo esc_attr($cortex_mikado_options['dropdown_padding_top_bottom']); ?>px;
            padding-bottom: <?php echo esc_attr($cortex_mikado_options['dropdown_padding_top_bottom']); ?>px;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_wide_color'] !== '' || $cortex_mikado_options['dropdown_wide_fontsize'] !== '' || $cortex_mikado_options['dropdown_wide_lineheight'] !== '' || $cortex_mikado_options['dropdown_wide_fontstyle'] !== '' || $cortex_mikado_options['dropdown_wide_fontweight'] !== '' || $cortex_mikado_options['dropdown_wide_google_fonts'] !== "-1" || $cortex_mikado_options['dropdown_wide_texttransform'] !== '' || $cortex_mikado_options['dropdown_wide_letterspacing'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner > ul > li > a{
            <?php if($cortex_mikado_options['dropdown_wide_color'] !== '') { ?> color: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_color']); ?>; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_google_fonts'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $cortex_mikado_options['dropdown_wide_google_fonts'])); ?>', sans-serif !important;
            <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_fontsize']); ?>px; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_lineheight']); ?>px; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_fontstyle']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_fontweight'] !== '') { ?>font-weight: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_fontweight']); ?>; <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_texttransform']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_letterspacing']); ?>px;  <?php } ?>
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_wide_hovercolor'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner > ul > li:hover > a {
            color: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_hovercolor']); ?> !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_wide_padding_top_bottom'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second>.mkdf-menu-inner > ul > li.mkdf-sub > ul > li > a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner ul li a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second ul li a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner ul.right li a{
            padding-top: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_padding_top_bottom']); ?>px;
            padding-bottom: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_padding_top_bottom']); ?>px;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_color_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_fontsize_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_lineheight_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_fontstyle_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_fontweight_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_google_fonts_thirdlvl'] != "-1" || $cortex_mikado_options['dropdown_texttransform_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_letterspacing_thirdlvl'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li.mkdf-sub ul li a{
            <?php if($cortex_mikado_options['dropdown_color_thirdlvl'] !== '') { ?> color: <?php echo esc_attr($cortex_mikado_options['dropdown_color_thirdlvl']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_google_fonts_thirdlvl'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $cortex_mikado_options['dropdown_google_fonts_thirdlvl'])); ?>', sans-serif;
            <?php } ?>
            <?php if($cortex_mikado_options['dropdown_fontsize_thirdlvl'] !== '') { ?> font-size: <?php echo esc_attr($cortex_mikado_options['dropdown_fontsize_thirdlvl']); ?>px;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_lineheight_thirdlvl'] !== '') { ?> line-height: <?php echo esc_attr($cortex_mikado_options['dropdown_lineheight_thirdlvl']); ?>px;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_fontstyle_thirdlvl'] !== '') { ?> font-style: <?php echo esc_attr($cortex_mikado_options['dropdown_fontstyle_thirdlvl']); ?>;   <?php } ?>
            <?php if($cortex_mikado_options['dropdown_fontweight_thirdlvl'] !== '') { ?> font-weight: <?php echo esc_attr($cortex_mikado_options['dropdown_fontweight_thirdlvl']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_texttransform_thirdlvl'] !== '') { ?> text-transform: <?php echo esc_attr($cortex_mikado_options['dropdown_texttransform_thirdlvl']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_letterspacing_thirdlvl'] !== '') { ?> letter-spacing: <?php echo esc_attr($cortex_mikado_options['dropdown_letterspacing_thirdlvl']); ?>px;  <?php } ?>
            }
        <?php } ?>
        <?php if($cortex_mikado_options['dropdown_hovercolor_thirdlvl'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li.mkdf-sub ul li:hover > a,
            .mkdf-drop-down .mkdf-menu-second .mkdf-menu-inner ul li ul li:hover > a{
            color: <?php echo esc_attr($cortex_mikado_options['dropdown_hovercolor_thirdlvl']); ?> !important;
            }
        <?php } ?>

        <?php if($cortex_mikado_options['dropdown_wide_color_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_wide_fontsize_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_wide_lineheight_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_wide_fontstyle_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_wide_fontweight_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_wide_google_fonts_thirdlvl'] != "-1" || $cortex_mikado_options['dropdown_wide_texttransform_thirdlvl'] !== '' || $cortex_mikado_options['dropdown_wide_letterspacing_thirdlvl'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner ul li.mkdf-sub ul li a,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second ul li ul li a{
            <?php if($cortex_mikado_options['dropdown_wide_color_thirdlvl'] !== '') { ?> color: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_color_thirdlvl']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_google_fonts_thirdlvl'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $cortex_mikado_options['dropdown_wide_google_fonts_thirdlvl'])); ?>', sans-serif;
            <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_fontsize_thirdlvl'] !== '') { ?> font-size: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_fontsize_thirdlvl']); ?>px;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_lineheight_thirdlvl'] !== '') { ?> line-height: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_lineheight_thirdlvl']); ?>px;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_fontstyle_thirdlvl'] !== '') { ?> font-style: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_fontstyle_thirdlvl']); ?>;   <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_fontweight_thirdlvl'] !== '') { ?> font-weight: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_fontweight_thirdlvl']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_texttransform_thirdlvl'] !== '') { ?> text-transform: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_texttransform_thirdlvl']); ?>;  <?php } ?>
            <?php if($cortex_mikado_options['dropdown_wide_letterspacing_thirdlvl'] !== '') { ?> letter-spacing: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_letterspacing_thirdlvl']); ?>px;  <?php } ?>
            }
        <?php } ?>
        <?php if($cortex_mikado_options['dropdown_wide_hovercolor_thirdlvl'] !== '') { ?>
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner ul li.mkdf-sub ul li) > a:hover,
            .mkdf-drop-down .mkdf-menu-wide .mkdf-menu-second .mkdf-menu-inner ul li ul li > a:hover{
            color: <?php echo esc_attr($cortex_mikado_options['dropdown_wide_hovercolor_thirdlvl']); ?> !important;
            }
        <?php }

    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_main_menu_styles');
}

if(!function_exists('cortex_mikado_vertical_main_menu_styles')) {
    /**
     * Generates styles for vertical main main menu
     */
    function cortex_mikado_vertical_main_menu_styles() {
        $dropdown_styles = array();

        if(cortex_mikado_options()->getOptionValue('vertical_dropdown_background_color') !== '') {
            $dropdown_styles['background-color'] = cortex_mikado_options()->getOptionValue('vertical_dropdown_background_color');
        }

        $dropdown_selector = array(
            '.mkdf-header-vertical .mkdf-vertical-dropdown-float .menu-item .mkdf-menu-second',
            '.mkdf-header-vertical .mkdf-vertical-dropdown-float .mkdf-menu-second .mkdf-menu-inner ul ul'
        );

        echo cortex_mikado_dynamic_css($dropdown_selector, $dropdown_styles);

        $fist_level_styles = array();
        $fist_level_hover_styles = array();

        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_color') !== '') {
            $fist_level_styles['color'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_color');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_google_fonts') !== '-1') {
            $fist_level_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('vertical_menu_1st_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_fontsize') !== '') {
            $fist_level_styles['font-size'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_fontsize').'px';
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_lineheight') !== '') {
            $fist_level_styles['line-height'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_lineheight').'px';
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_texttransform') !== '') {
            $fist_level_styles['text-transform'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_fontstyle') !== '') {
            $fist_level_styles['font-style'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_fontweight') !== '') {
            $fist_level_styles['font-weight'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_letter_spacing') !== '') {
            $fist_level_styles['letter-spacing'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_letter_spacing').'px';
        }

        if(cortex_mikado_options()->getOptionValue('vertical_menu_1st_hover_color') !== '') {
            $fist_level_hover_styles['color'] = cortex_mikado_options()->getOptionValue('vertical_menu_1st_hover_color');
        }

        $first_level_selector = array(
            '.mkdf-header-vertical .mkdf-vertical-menu > ul > li > a'
        );
        $first_level_hover_selector = array(
            '.mkdf-header-vertical .mkdf-vertical-menu > ul > li > a:hover',
            '.mkdf-header-vertical .mkdf-vertical-menu > ul > li.mkdf-active-item > a'
        );

        echo cortex_mikado_dynamic_css($first_level_selector, $fist_level_styles);
        echo cortex_mikado_dynamic_css($first_level_hover_selector, $fist_level_hover_styles);

        $second_level_styles = array();
        $second_level_hover_styles = array();

        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_color') !== '') {
            $second_level_styles['color'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_color');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_google_fonts') !== '-1') {
            $second_level_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_fontsize') !== '') {
            $second_level_styles['font-size'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_fontsize').'px';
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_lineheight') !== '') {
            $second_level_styles['line-height'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_lineheight').'px';
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_texttransform') !== '') {
            $second_level_styles['text-transform'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_fontstyle') !== '') {
            $second_level_styles['font-style'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_fontweight') !== '') {
            $second_level_styles['font-weight'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_letter_spacing') !== '') {
            $second_level_styles['letter-spacing'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_letter_spacing').'px';
        }

        if(cortex_mikado_options()->getOptionValue('vertical_menu_2nd_hover_color') !== '') {
            $second_level_hover_styles['color'] = cortex_mikado_options()->getOptionValue('vertical_menu_2nd_hover_color');
        }

        $second_level_selector = array(
            '.mkdf-header-vertical .mkdf-vertical-menu .mkdf-menu-second .mkdf-menu-inner > ul > li > a'
        );

        $second_level_hover_selector = array(
            '.mkdf-header-vertical .mkdf-vertical-menu .mkdf-menu-second .mkdf-menu-inner > ul > li > a:hover',
            '.mkdf-header-vertical .mkdf-vertical-menu .mkdf-menu-second .mkdf-menu-inner > ul > li > a.mkdf-active-item'
        );

        echo cortex_mikado_dynamic_css($second_level_selector, $second_level_styles);
        echo cortex_mikado_dynamic_css($second_level_hover_selector, $second_level_hover_styles);

        $third_level_styles = array();
        $third_level_hover_styles = array();

        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_color') !== '') {
            $third_level_styles['color'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_color');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_google_fonts') !== '-1') {
            $third_level_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_google_fonts'));
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_fontsize') !== '') {
            $third_level_styles['font-size'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_fontsize').'px';
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_lineheight') !== '') {
            $third_level_styles['line-height'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_lineheight').'px';
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_texttransform') !== '') {
            $third_level_styles['text-transform'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_texttransform');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_fontstyle') !== '') {
            $third_level_styles['font-style'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_fontstyle');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_fontweight') !== '') {
            $third_level_styles['font-weight'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_fontweight');
        }
        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_letter_spacing') !== '') {
            $third_level_styles['letter-spacing'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_letter_spacing').'px';
        }

        if(cortex_mikado_options()->getOptionValue('vertical_menu_3rd_hover_color') !== '') {
            $third_level_hover_styles['color'] = cortex_mikado_options()->getOptionValue('vertical_menu_3rd_hover_color');
        }

        $third_level_selector = array(
            '.mkdf-header-vertical .mkdf-vertical-menu .mkdf-menu-second .mkdf-menu-inner ul li ul li a'
        );

        $third_level_hover_selector = array(
            '.mkdf-header-vertical .mkdf-vertical-menu .mkdf-menu-second .mkdf-menu-inner ul li ul li a:hover',
            '.mkdf-header-vertical .mkdf-vertical-menu .mkdf-menu-second .mkdf-menu-inner ul li ul li a.mkdf-active-item'
        );

        echo cortex_mikado_dynamic_css($third_level_selector, $third_level_styles);
        echo cortex_mikado_dynamic_css($third_level_hover_selector, $third_level_hover_styles);
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_vertical_main_menu_styles');
}