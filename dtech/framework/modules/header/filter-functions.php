<?php

if(!function_exists('cortex_mikado_header_class')) {
    /**
     * Function that adds class to header based on theme options
     * @param array array of classes from main filter
     * @return array array of classes with added header class
     */
    function cortex_mikado_header_class($classes) {
        $header_type = cortex_mikado_get_meta_field_intersect('header_type', cortex_mikado_get_page_id());

        $classes[] = 'mkdf-'.$header_type;

        return $classes;
    }

    add_filter('body_class', 'cortex_mikado_header_class');
}

if(!function_exists('cortex_mikado_header_behaviour_class')) {
    /**
     * Function that adds behaviour class to header based on theme options
     * @param array array of classes from main filter
     * @return array array of classes with added behaviour class
     */
    function cortex_mikado_header_behaviour_class($classes) {

        $classes[] = 'mkdf-'.cortex_mikado_options()->getOptionValue('header_behaviour');

        return $classes;
    }

    add_filter('body_class', 'cortex_mikado_header_behaviour_class');
}

if(!function_exists('cortex_mikado_menu_item_icon_position_class')) {
    /**
     * Function that adds menu item icon position class to header based on theme options
     * @param array array of classes from main filter
     * @return array array of classes with added menu item icon position class
     */
    function cortex_mikado_menu_item_icon_position_class($classes) {

        if(cortex_mikado_options()->getOptionValue('menu_item_icon_position') == 'top'){
            $classes[] = 'mkdf-menu-with-large-icons';
        }

        return $classes;
    }

    add_filter('body_class', 'cortex_mikado_menu_item_icon_position_class');
}

if(!function_exists('cortex_mikado_mobile_header_class')) {
    function cortex_mikado_mobile_header_class($classes) {
        $classes[] = 'mkdf-default-mobile-header';

        $classes[] = 'mkdf-sticky-up-mobile-header';

        return $classes;
    }

    add_filter('body_class', 'cortex_mikado_mobile_header_class');
}

if(!function_exists('cortex_mikado_menu_dropdown_appearance')) {
    /**
     * Function that adds menu dropdown appearance class to body tag
     * @param array array of classes from main filter
     * @return array array of classes with added menu dropdown appearance class
     */
    function cortex_mikado_menu_dropdown_appearance($classes) {

        if(cortex_mikado_options()->getOptionValue('menu_dropdown_appearance') !== 'default'){
            $classes[] = 'mkdf-'.cortex_mikado_options()->getOptionValue('menu_dropdown_appearance');
        }

        return $classes;
    }

    add_filter('body_class', 'cortex_mikado_menu_dropdown_appearance');
}

if (!function_exists('cortex_mikado_header_skin_class')) {

    function cortex_mikado_header_skin_class( $classes ) {

        $id = cortex_mikado_get_page_id();

		if(($meta_temp = get_post_meta($id, 'mkdf_header_style_meta', true)) !== ''){
			$classes[] = 'mkdf-' . $meta_temp;
		} else if ( cortex_mikado_options()->getOptionValue('header_style') !== '' ) {
            $classes[] = 'mkdf-' . cortex_mikado_options()->getOptionValue('header_style');
        }

        return $classes;

    }

    add_filter('body_class', 'cortex_mikado_header_skin_class');

}

if (!function_exists('cortex_mikado_header_scroll_style_class')) {

	function cortex_mikado_header_scroll_style_class( $classes ) {

		if (cortex_mikado_get_meta_field_intersect('enable_header_style_on_scroll') == 'yes' ) {
			$classes[] = 'mkdf-header-style-on-scroll';
		}

		return $classes;

	}

	add_filter('body_class', 'cortex_mikado_header_scroll_style_class');

}

if(!function_exists('cortex_mikado_header_global_js_var')) {
    function cortex_mikado_header_global_js_var($global_variables) {

        $global_variables['mkdfTopBarHeight'] = cortex_mikado_get_top_bar_height();
        $global_variables['mkdfStickyHeaderHeight'] = cortex_mikado_get_sticky_header_height();
        $global_variables['mkdfStickyHeaderTransparencyHeight'] = cortex_mikado_get_sticky_header_height_of_complete_transparency();
        $global_variables['mkdfStickyScrollAmount'] = cortex_mikado_get_sticky_scroll_amount();

        return $global_variables;
    }

    add_filter('cortex_mikado_js_global_variables', 'cortex_mikado_header_global_js_var');
}

if(!function_exists('cortex_mikado_header_per_page_js_var')) {
    function cortex_mikado_header_per_page_js_var($perPageVars) {

        $perPageVars['mkdfStickyScrollAmount'] = cortex_mikado_get_sticky_scroll_amount_per_page();

        return $perPageVars;
    }

    add_filter('cortex_mikado_per_page_js_vars', 'cortex_mikado_header_per_page_js_var');
}

if(!function_exists('cortex_mikado_get_top_bar_styles')) {
	/**
	 * Sets per page styles for header top bar
	 *
	 * @param $styles
	 *
	 * @return array
	 */
	function cortex_mikado_get_top_bar_styles($styles) {
		$id            = cortex_mikado_get_page_id();
		$class_prefix  = cortex_mikado_get_unique_page_class();
		$top_bar_style = array();

		$top_bar_bg_color     = get_post_meta($id, 'mkdf_top_bar_background_color_meta', true);

		$top_bar_selector = array(
			$class_prefix.' .mkdf-top-bar'
		);

		if($top_bar_bg_color !== '') {
			$top_bar_transparency = get_post_meta($id, 'mkdf_top_bar_background_transparency_meta', true);
			if($top_bar_transparency === '') {
				$top_bar_transparency = 1;
			}

			$top_bar_style['background-color'] = cortex_mikado_rgba_color($top_bar_bg_color, $top_bar_transparency);
		}

		$styles[] = cortex_mikado_dynamic_css($top_bar_selector, $top_bar_style);

		return $styles;
	}

	add_filter('cortex_mikado_add_page_custom_style', 'cortex_mikado_get_top_bar_styles');
}
if(!function_exists('cortex_mikado_top_bar_skin_class')) {
	/**
	 * @param $classes
	 *
	 * @return array
	 */
	function cortex_mikado_top_bar_skin_class($classes) {
		$id = cortex_mikado_get_page_id();
		$top_bar_skin = get_post_meta($id, 'mkdf_top_bar_skin_meta', true);

		if($top_bar_skin !== '') {
			$classes[] = 'mkdf-top-bar-'.$top_bar_skin;
		}

		return $classes;
	}

	add_filter('body_class', 'cortex_mikado_top_bar_skin_class');
}