<?php

if ( ! function_exists('cortex_mikado_header_options_map') ) {

	function cortex_mikado_header_options_map() {

		cortex_mikado_add_admin_page(
			array(
				'slug' => '_header_page',
				'title' => esc_html__('Header', 'cortex'),
				'icon' => 'fa fa-header'
			)
		);

		$panel_header = cortex_mikado_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header',
				'title' => esc_html__('Header', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'radiogroup',
				'name' => 'header_type',
				'default_value' => 'header-standard',
				'label' => esc_html__('Choose Header Type', 'cortex'),
				'description' => esc_html__('Select the type of header you would like to use', 'cortex'),
				'options' => array(
					'header-standard' => array(
						'image' => MIKADO_ROOT . '/framework/admin/assets/img/header-standard.png',
						'label' => esc_html__('Header Standard', 'cortex')
					),
					'header-centered' => array(
						'image' => MIKADO_ROOT . '/framework/admin/assets/img/header-centered.png',
						'label' => esc_html__('Header Centered', 'cortex')
					),
					'header-vertical' => array(
						'image' => MIKADO_ROOT . '/framework/admin/assets/img/header-vertical.png',
						'label' => esc_html__('Header Vertical', 'cortex')
					),
					'header-full-screen' => array(
						'image' => MIKADO_ROOT . '/framework/admin/assets/img/header-full-screen.png',
						'label' => esc_html__('Header Full Screen', 'cortex')
					)
				),
				'args' => array(
					'use_images' => true,
					'hide_labels' => true,
					'dependence' => true,
					'show' => array(
						'header-standard' => '#mkdf_panel_header_standard,#mkdf_header_behaviour,#mkdf_panel_fixed_header,#mkdf_panel_sticky_header,#mkdf_panel_main_menu',
						'header-centered' => '#mkdf_panel_header_centered,#mkdf_header_behaviour,#mkdf_panel_fixed_header,#mkdf_panel_sticky_header,#mkdf_panel_main_menu',
						'header-vertical' => '#mkdf_panel_header_vertical,#mkdf_panel_vertical_main_menu',
						'header-full-screen' => '#mkdf_panel_header_full_screen,#mkdf_fullscreen_menu,#mkdf_header_behaviour',
					),
					'hide' => array(
						'header-standard' => '#mkdf_panel_header_vertical,#mkdf_panel_vertical_main_menu,#mkdf_panel_header_full_screen,#mkdf_fullscreen_menu,#mkdf_panel_header_centered',
						'header-centered' => '#mkdf_panel_header_vertical,#mkdf_panel_vertical_main_menu,#mkdf_panel_header_full_screen,#mkdf_fullscreen_menu,#mkdf_panel_header_standard',
						'header-vertical' => '#mkdf_panel_header_standard,#mkdf_header_behaviour,#mkdf_panel_fixed_header,#mkdf_panel_sticky_header,#mkdf_panel_main_menu,#mkdf_panel_header_full_screen,#mkdf_fullscreen_menu,#mkdf_panel_header_centered',
						'header-full-screen' => '#mkdf_panel_header_standard,#mkdf_panel_fixed_header,#mkdf_panel_sticky_header,#mkdf_panel_main_menu,#mkdf_panel_header_vertical,#mkdf_panel_vertical_main_menu,#mkdf_panel_header_centered',
					)
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'select',
				'name' => 'header_behaviour',
				'default_value' => 'sticky-header-on-scroll-up',
				'label' => esc_html__('Choose Header behaviour', 'cortex'),
				'description' => esc_html__('Select the behaviour of header when you scroll down to page', 'cortex'),
				'options' => array(
					'sticky-header-on-scroll-up' => esc_html__('Sticky on scrol up', 'cortex'),
					'sticky-header-on-scroll-down-up' => esc_html__('Sticky on scrol up/down', 'cortex'),
					'fixed-on-scroll' => esc_html__('Fixed on scroll', 'cortex'),
				),
                'hidden_property' => 'header_type',
                'hidden_value' => '',
                'hidden_values' => array('header-vertical'),
				'args' => array(
					'dependence' => true,
					'show' => array(
						'sticky-header-on-scroll-up' => '#mkdf_panel_sticky_header',
						'sticky-header-on-scroll-down-up' => '#mkdf_panel_sticky_header',
						'fixed-on-scroll' => '#mkdf_panel_fixed_header'
					),
					'hide' => array(
						'sticky-header-on-scroll-up' => '#mkdf_panel_fixed_header',
						'sticky-header-on-scroll-down-up' => '#mkdf_panel_fixed_header',
						'fixed-on-scroll' => '#mkdf_panel_sticky_header',
					)
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name' => 'top_bar',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Top Bar', 'cortex'),
				'description' => esc_html__('Enabling this option will show top bar area', 'cortex'),
				'parent' => $panel_header,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_top_bar_container"
				)
			)
		);

		$top_bar_container = cortex_mikado_add_admin_container(array(
			'name' => 'top_bar_container',
			'parent' => $panel_header,
			'hidden_property' => 'top_bar',
			'hidden_value' => 'no'
		));

		cortex_mikado_add_admin_field(
			array(
				'parent' => $top_bar_container,
				'type' => 'select',
				'name' => 'top_bar_layout',
				'default_value' => 'two-columns',
				'label' => esc_html__('Choose top bar layout', 'cortex'),
				'description' => esc_html__('Select the layout for top bar', 'cortex'),
				'options' => array(
					'two-columns' => esc_html__('Two columns', 'cortex'),
					'three-columns' => esc_html__('Three columns', 'cortex'),
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"two-columns" => "#mkdf_top_bar_layout_container",
						"three-columns" => ""
					),
					"show" => array(
						"two-columns" => "",
						"three-columns" => "#mkdf_top_bar_layout_container"
					)
				)
			)
		);

		$top_bar_layout_container = cortex_mikado_add_admin_container(array(
			'name' => 'top_bar_layout_container',
			'parent' => $top_bar_container,
			'hidden_property' => 'top_bar_layout',
			'hidden_value' => '',
			'hidden_values' => array("two-columns"),
		));

		cortex_mikado_add_admin_field(
			array(
				'parent' => $top_bar_layout_container,
				'type' => 'select',
				'name' => 'top_bar_column_widths',
				'default_value' => '30-30-30',
				'label' => esc_html__('Choose column widths', 'cortex'),
				'options' => array(
					'30-30-30' => '33% - 33% - 33%',
					'25-50-25' => '25% - 50% - 25%'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name' => 'top_bar_in_grid',
				'type' => 'yesno',
				'default_value' => 'yes',
				'label' => esc_html__('Top Bar in grid', 'cortex'),
				'description' => esc_html__('Set top bar content to be in grid', 'cortex'),
				'parent' => $top_bar_container,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_top_bar_in_grid_container"
				)
			)
		);

		$top_bar_in_grid_container = cortex_mikado_add_admin_container(array(
			'name' => 'top_bar_in_grid_container',
			'parent' => $top_bar_container,
			'hidden_property' => 'top_bar_in_grid',
			'hidden_value' => 'no'
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'top_bar_grid_background_color',
			'type' => 'color',
			'label' => esc_html__('Grid Background Color', 'cortex'),
			'description' => esc_html__('Set grid background color for top bar', 'cortex'),
			'parent' => $top_bar_in_grid_container
		));


		cortex_mikado_add_admin_field(array(
			'name' => 'top_bar_grid_background_transparency',
			'type' => 'text',
			'label' => esc_html__('Grid Background Transparency', 'cortex'),
			'description' => esc_html__('Set grid background transparency for top bar', 'cortex'),
			'parent' => $top_bar_in_grid_container,
			'args' => array('col_width' => 3)
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'top_bar_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color','cortex'),
			'description' => esc_html__('Set background color for top bar', 'cortex'),
			'parent' => $top_bar_container
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'top_bar_background_transparency',
			'type' => 'text',
			'label' => esc_html__('Background Transparency', 'cortex'),
			'description' => esc_html__('Set background transparency for top bar', 'cortex'),
			'parent' => $top_bar_container,
			'args' => array('col_width' => 3)
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'top_bar_height',
			'type' => 'text',
			'label' => esc_html__('Top bar height', 'cortex'),
			'description' => esc_html__('Enter top bar height (Default is 40px)', 'cortex'),
			'parent' => $top_bar_container,
			'args' => array(
				'col_width' => 2,
				'suffix' => 'px'
			)
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'hide_top_bar_on_responsive',
			'type' => 'yesno',
			'default_value' => 'yes',
			'label' => esc_html__('Hide Top Bar on Responsive', 'cortex'),
			'description' => esc_html__('Enabling this option you will hide top header area on responsive', 'cortex'),
			'parent' => $top_bar_container,
		));
		
		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'select',
				'name' => 'header_style',
				'default_value' => '',
				'label' => esc_html__('Header Skin', 'cortex'),
				'description' => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'cortex'),
				'options' => array(
					'' => '',
					'light-header' => esc_html__('Light', 'cortex'),
					'dark-header' => esc_html__('Dark', 'cortex'),
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'yesno',
				'name' => 'enable_header_style_on_scroll',
				'default_value' => 'no',
				'label' => esc_html__('Enable Header Style on Scroll', 'cortex'),
				'description' => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style', 'cortex'),
			)
		);


		$panel_header_standard = cortex_mikado_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_standard',
				'title' => esc_html__('Header Standard', 'cortex'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-centered',
                    'header-vertical',
					'header-full-screen'
				)
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' => $panel_header_standard,
				'name' => 'menu_area_title',
				'title' => esc_html__('Menu Area', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'yesno',
				'name' => 'menu_area_in_grid_header_standard',
				'default_value' => 'yes',
				'label' => esc_html__('Header in grid', 'cortex'),
				'description' => esc_html__('Set header content to be in grid', 'cortex'),
			)
		);

        cortex_mikado_add_admin_field(
            array(
                'parent' => $panel_header_standard,
                'type' => 'select',
                'name' => 'set_menu_area_position',
                'default_value' => 'right',
                'label' => esc_html__('Choose Menu Area Position', 'cortex'),
                'description' => esc_html__('Select menu area position in your header', 'cortex'),
                'options' => array(
                    'right' => esc_html__('Right', 'cortex'),
                    'left' => esc_html__('Left', 'cortex')
                )
            )
        );

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'color',
				'name' => 'menu_area_background_color_header_standard',
				'default_value' => '',
				'label' => esc_html__('Background color', 'cortex'),
				'description' => esc_html__('Set background color for header', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'text',
				'name' => 'menu_area_background_transparency_header_standard',
				'default_value' => '',
				'label' => esc_html__('Background transparency', 'cortex'),
				'description' => esc_html__('Set background transparency for header', 'cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'text',
				'name' => 'menu_area_height_header_standard',
				'default_value' => '',
				'label' => esc_html__('Height', 'cortex'),
				'description' => esc_html__('Enter header height (default is 65px)', 'cortex'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		$panel_header_centered = cortex_mikado_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_centered',
				'title' => esc_html__('Header Centered', 'cortex'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
                    'header-standard',
                    'header-vertical',
					'header-full-screen'
				)
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' => $panel_header_centered,
				'name' => 'menu_area_title',
				'title' => esc_html__('Menu Area', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_centered,
				'type' => 'yesno',
				'name' => 'menu_area_in_grid_header_centered',
				'default_value' => 'yes',
				'label' => esc_html__('Header in grid', 'cortex'),
				'description' => esc_html__('Set header content to be in grid', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_centered,
				'type' => 'color',
				'name' => 'menu_area_background_color_header_centered',
				'default_value' => '',
				'label' => esc_html__('Background color', 'cortex'),
				'description' => esc_html__('Set background color for header', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_centered,
				'type' => 'text',
				'name' => 'menu_area_background_transparency_header_centered',
				'default_value' => '',
				'label' => esc_html__('Background transparency', 'cortex'),
				'description' => esc_html__('Set background transparency for header', 'cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_centered,
				'type' => 'text',
				'name' => 'menu_area_height_header_centered',
				'default_value' => '',
				'label' => esc_html__('Height', 'cortex'),
				'description' => esc_html__('Enter header height (default is 65px)', 'cortex'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

        $panel_header_vertical = cortex_mikado_add_admin_panel(
            array(
                'page' => '_header_page',
                'name' => 'panel_header_vertical',
                'title' => esc_html__('Header Vertical', 'cortex'),
                'hidden_property' => 'header_type',
                'hidden_value' => '',
                'hidden_values' => array(
                    'header-standard',
                    'header-centered',
					'header-full-screen'
                )
            )
        );

            cortex_mikado_add_admin_field(array(
                'name' => 'vertical_header_background_color',
                'type' => 'color',
                'label' => esc_html__('Background Color', 'cortex'),
                'description' => esc_html__('Set background color for vertical menu', 'cortex'),
                'parent' => $panel_header_vertical
            ));

            cortex_mikado_add_admin_field(array(
                'name' => 'vertical_header_transparency',
                'type' => 'text',
                'label' => esc_html__('Transparency', 'cortex'),
                'description' => esc_html__('Enter transparency for vertical menu (value from 0 to 1)', 'cortex'),
                'parent' => $panel_header_vertical,
                'args' => array(
                    'col_width' => 1
                )
            ));

            cortex_mikado_add_admin_field(
                array(
                    'name' => 'vertical_header_background_image',
                    'type' => 'image',
                    'default_value' => '',
                    'label' => esc_html__('Background Image', 'cortex'),
                    'description' => esc_html__('Set background image for vertical menu', 'cortex'),
                    'parent' => $panel_header_vertical
                )
            );


		$panel_header_full_screen = cortex_mikado_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_full_screen',
				'title' => esc_html__('Header Full Screen', 'cortex'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-standard',
					'header-centered',
					'header-vertical'
				)
			)
		);


		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'yesno',
				'name' => 'menu_area_in_grid_header_full_screen',
				'default_value' => 'yes',
				'label' => esc_html__('Header in grid', 'cortex'),
				'description' => esc_html__('Set header content to be in grid', 'cortex'),
			)
		);



		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'color',
				'name' => 'menu_area_background_color_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Background color', 'cortex'),
				'description' => esc_html__('Set background color for header', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'text',
				'name' => 'menu_area_background_transparency_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Background transparency', 'cortex'),
				'description' => esc_html__('Set background transparency for header', 'cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'text',
				'name' => 'menu_area_height_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Height', 'cortex'),
				'description' => esc_html__('Enter header height (default is 120px)', 'cortex'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);


		$panel_sticky_header = cortex_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Sticky Header', 'cortex'),
				'name' => 'panel_sticky_header',
				'page' => '_header_page',
				'hidden_property' => 'header_behaviour',
				'hidden_values' => array(
					'fixed-on-scroll'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name' => 'scroll_amount_for_sticky',
				'type' => 'text',
				'label' => esc_html__('Scroll Amount for Sticky', 'cortex'),
				'description' => esc_html__('Enter scroll amount for Sticky Menu to appear (deafult is header height)', 'cortex'),
				'parent' => $panel_sticky_header,
				'args' => array(
					'col_width' => 2,
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'name' => 'sticky_header_in_grid',
				'type' => 'yesno',
				'default_value' => 'yes',
				'label' => esc_html__('Sticky Header in grid', 'cortex'),
				'description' => esc_html__('Set sticky header content to be in grid', 'cortex'),
				'parent' => $panel_sticky_header,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_sticky_header_in_grid_container"
				)
			)
		);

		$sticky_header_in_grid_container = cortex_mikado_add_admin_container(array(
			'name' => 'sticky_header_in_grid_container',
			'parent' => $panel_sticky_header,
			'hidden_property' => 'sticky_header_in_grid',
			'hidden_value' => 'no'
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'sticky_header_grid_background_color',
			'type' => 'color',
			'label' => esc_html__('Grid Background Color', 'cortex'),
			'description' => esc_html__('Set grid background color for sticky header', 'cortex'),
			'parent' => $sticky_header_in_grid_container
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'sticky_header_grid_transparency',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Grid Transparency', 'cortex'),
			'description' => esc_html__('Enter transparency for sticky header grid (value from 0 to 1)', 'cortex'),
			'parent' => $sticky_header_in_grid_container,
			'args' => array(
				'col_width' => 1
			)
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'sticky_header_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color', 'cortex'),
			'description' => esc_html__('Set background color for sticky header', 'cortex'),
			'parent' => $panel_sticky_header
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'sticky_header_transparency',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Transparency', 'cortex'),
			'description' => esc_html__('Enter transparency for sticky header (value from 0 to 1)', 'cortex'),
			'parent' => $panel_sticky_header,
			'args' => array(
				'col_width' => 1
			)
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'sticky_header_height',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Height', 'cortex'),
			'description' => esc_html__('Enter height for sticky header (default is 65px)', 'cortex'),
			'parent' => $panel_sticky_header,
			'args' => array(
				'col_width' => 2,
				'suffix' => 'px'
			)
		));

		$group_sticky_header_menu = cortex_mikado_add_admin_group(array(
			'title' => esc_html__('Sticky Header Menu', 'cortex'),
			'name' => 'group_sticky_header_menu',
			'parent' => $panel_sticky_header,
			'description' => esc_html__('Define styles for sticky menu items', 'cortex'),
		));

		$row1_sticky_header_menu = cortex_mikado_add_admin_row(array(
			'name' => 'row1',
			'parent' => $group_sticky_header_menu
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'sticky_color',
			'type' => 'colorsimple',
			'label' => esc_html__('Text Color', 'cortex'),
			'parent' => $row1_sticky_header_menu
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'sticky_hovercolor',
			'type' => 'colorsimple',
			'label' => esc_html__('Hover/Active color', 'cortex'),
			'parent' => $row1_sticky_header_menu
		));

		$row2_sticky_header_menu = cortex_mikado_add_admin_row(array(
			'name' => 'row2',
			'parent' => $group_sticky_header_menu
		));

		cortex_mikado_add_admin_field(
			array(
				'name' => 'sticky_google_fonts',
				'type' => 'fontsimple',
				'label' => esc_html__('Font Family', 'cortex'),
				'default_value' => '-1',
				'parent' => $row2_sticky_header_menu,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_fontsize',
				'label' => esc_html__('Font Size', 'cortex'),
				'default_value' => '',
				'parent' => $row2_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_lineheight',
				'label' => esc_html__('Line height', 'cortex'),
				'default_value' => '',
				'parent' => $row2_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_texttransform',
				'label' => esc_html__('Text transform', 'cortex'),
				'default_value' => '',
				'options' => cortex_mikado_get_text_transform_array(),
				'parent' => $row2_sticky_header_menu
			)
		);

		$row3_sticky_header_menu = cortex_mikado_add_admin_row(array(
			'name' => 'row3',
			'parent' => $group_sticky_header_menu
		));

		cortex_mikado_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font Style', 'cortex'),
				'options' => cortex_mikado_get_font_style_array(),
				'parent' => $row3_sticky_header_menu
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font Weight', 'cortex'),
				'options' => cortex_mikado_get_font_weight_array(),
				'parent' => $row3_sticky_header_menu
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_letterspacing',
				'label' => esc_html__('Letter Spacing', 'cortex'),
				'default_value' => '',
				'parent' => $row3_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$panel_fixed_header = cortex_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Fixed Header', 'cortex'),
				'name' => 'panel_fixed_header',
				'page' => '_header_page',
				'hidden_property' => 'header_behaviour',
				'hidden_values' => array('sticky-header-on-scroll-up', 'sticky-header-on-scroll-down-up')
			)
		);

		cortex_mikado_add_admin_field(array(
			'name' => 'fixed_header_grid_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' => esc_html__('Grid Background Color', 'cortex'),
			'description' => esc_html__('Set grid background color for fixed header', 'cortex'),
			'parent' => $panel_fixed_header
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'fixed_header_grid_transparency',
			'type' => 'text',
			'default_value' => '',
			'label' => esc_html__('Header Transparency Grid', 'cortex'),
			'description' => esc_html__('Enter transparency for fixed header grid (value from 0 to 1)', 'cortex'),
			'parent' => $panel_fixed_header,
			'args' => array(
				'col_width' => 1
			)
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'fixed_header_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' => esc_html__('Background Color', 'cortex'),
			'description' => esc_html__('Set background color for fixed header', 'cortex'),
			'parent' => $panel_fixed_header
		));

		cortex_mikado_add_admin_field(array(
			'name' => 'fixed_header_transparency',
			'type' => 'text',
			'label' => esc_html__('Header Transparency', 'cortex'),
			'description' => esc_html__('Enter transparency for fixed header (value from 0 to 1)', 'cortex'),
			'parent' => $panel_fixed_header,
			'args' => array(
				'col_width' => 1
			)
		));


		$panel_main_menu = cortex_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Main Menu', 'cortex'),
				'name' => 'panel_main_menu',
				'page' => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values' => array(
					'header-vertical',
					'header-full-screen'
				)
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' => $panel_main_menu,
				'name' => 'main_menu_area_title',
				'title' => esc_html__('Main Menu General Settings', 'cortex'),
			)
		);


		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'select',
				'name' => 'menu_item_icon_position',
				'default_value' => 'left',
				'label' => esc_html__('Icon Position in 1st Level Menu', 'cortex'),
				'description' => esc_html__('Choose position of icon selected in Appearance->Menu->Menu Structure', 'cortex'),
				'options' => array(
					'left' => esc_html__('Left', 'cortex'),
					'top' => esc_html__('Top', 'cortex'),
				),
				'args' => array(
					'dependence' => true,
					'hide' => array(
						'left' => '#mkdf_menu_item_icon_position_container'
					),
					'show' => array(
						'top' => '#mkdf_menu_item_icon_position_container'
					)
				)
			)
		);

		$menu_item_icon_position_container = cortex_mikado_add_admin_container(
			array(
				'parent' => $panel_main_menu,
				'name' => 'menu_item_icon_position_container',
				'hidden_property' => 'menu_item_icon_position',
				'hidden_value' => 'left'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $menu_item_icon_position_container,
				'type' => 'text',
				'name' => 'menu_item_icon_size',
				'default_value' => '',
				'label' => esc_html__('Icon Size', 'cortex'),
				'description' => esc_html__('Choose position of icon selected in Appearance->Menu->Menu Structure', 'cortex'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'select',
				'name' => 'menu_item_style',
				'default_value' => 'small_item',
				'label' => esc_html__('Item Height in 1st Level Menu', 'cortex'),
				'description' => esc_html__('Choose menu item height', 'cortex'),
				'options' => array(
					'small_item' => esc_html__('Small', 'cortex'),
					'large_item' => esc_html__('Big', 'cortex'),
				)
			)
		);

		$drop_down_group = cortex_mikado_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'drop_down_group',
				'title' => esc_html__('Main Dropdown Menu', 'cortex'),
				'description' => esc_html__('Choose a color and transparency for the main menu background (0 = fully transparent, 1 = opaque)', 'cortex'),
			)
		);

		$drop_down_row1 = cortex_mikado_add_admin_row(
			array(
				'parent' => $drop_down_group,
				'name' => 'drop_down_row1',
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_background_color',
				'default_value' => '',
				'label' => esc_html__('Background Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'textsimple',
				'name' => 'dropdown_background_transparency',
				'default_value' => '',
				'label' => esc_html__('Transparency', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_separator_color',
				'default_value' => '',
				'label' => esc_html__('Item Bottom Separator Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'yesnosimple',
				'name' => 'enable_dropdown_separator_full_width',
				'default_value' => 'no',
				'label' => esc_html__('Item Separator Full Width', 'cortex'),
			)
		);

		$drop_down_padding_group = cortex_mikado_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'drop_down_padding_group',
				'title' => esc_html__('Main Dropdown Menu Padding', 'cortex'),
				'description' => esc_html__('Choose a top/bottom padding for dropdown menu', 'cortex'),
			)
		);

		$drop_down_padding_row = cortex_mikado_add_admin_row(
			array(
				'parent' => $drop_down_padding_group,
				'name' => 'drop_down_padding_row',
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $drop_down_padding_row,
				'type' => 'textsimple',
				'name' => 'dropdown_top_padding',
				'default_value' => '',
				'label' => esc_html__('Top Padding', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $drop_down_padding_row,
				'type' => 'textsimple',
				'name' => 'dropdown_bottom_padding',
				'default_value' => '',
				'label' => esc_html__('Bottom Padding', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'select',
				'name' => 'menu_dropdown_appearance',
				'default_value' => 'default',
				'label' => esc_html__('Main Dropdown Menu Appearance', 'cortex'),
				'description' => esc_html__('Choose appearance for dropdown menu', 'cortex'),
				'options' => array(
					'dropdown-default' => esc_html__('Default', 'cortex'),
					'dropdown-slide-from-bottom' => esc_html__('Slide From Bottom', 'cortex'),
					'dropdown-slide-from-top' => esc_html__('Slide From Top', 'cortex'),
					'dropdown-animate-height' => esc_html__('Animate Height', 'cortex'),
					'dropdown-slide-from-left' => esc_html__('Slide From Left', 'cortex'),
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'text',
				'name' => 'dropdown_top_position',
				'default_value' => '',
				'label' => esc_html__('Dropdown position', 'cortex'),
				'description' => esc_html__('Enter value in percentage of entire header height', 'cortex'),
				'args' => array(
					'col_width' => 3,
					'suffix' => '%'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'yesno',
				'name' => 'enable_wide_menu_background',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Full Width Background for Wide Dropdown Type', 'cortex'),
				'description' => esc_html__('Enabling this option will show full width background  for wide dropdown type', 'cortex'),
			)
		);

		$first_level_group = cortex_mikado_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'first_level_group',
				'title' => esc_html__('1st Level Menu', 'cortex'),
				'description' => esc_html__('Define styles for 1st level in Top Navigation Menu', 'cortex'),
			)
		);

		$first_level_row1 = cortex_mikado_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row1'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'colorsimple',
				'name' => 'menu_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'colorsimple',
				'name' => 'menu_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Hover Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'colorsimple',
				'name' => 'menu_activecolor',
				'default_value' => '',
				'label' => esc_html__('Active Text Color', 'cortex'),
			)
		);

		$first_level_row2 = cortex_mikado_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row2',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'colorsimple',
				'name' => 'menu_light_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Light Menu Hover Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'colorsimple',
				'name' => 'menu_light_activecolor',
				'default_value' => '',
				'label' => esc_html__('Light Menu Active Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'colorsimple',
				'name' => 'menu_dark_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Dark Menu Hover Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'colorsimple',
				'name' => 'menu_dark_activecolor',
				'default_value' => '',
				'label' => esc_html__('Dark Menu Active Text Color', 'cortex'),
			)
		);

		$first_level_row3 = cortex_mikado_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row3',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row3,
				'type' => 'fontsimple',
				'name' => 'menu_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row3,
				'type' => 'textsimple',
				'name' => 'menu_fontsize',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'menu_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font Style', 'cortex'),
				'options' => cortex_mikado_get_font_style_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'menu_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font Weight', 'cortex'),
				'options' => cortex_mikado_get_font_weight_array()
			)
		);


		$first_level_row4 = cortex_mikado_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row4',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row4,
				'type' => 'textsimple',
				'name' => 'menu_letterspacing',
				'default_value' => '',
				'label' => esc_html__('Letter Spacing', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row4,
				'type' => 'selectblanksimple',
				'name' => 'menu_texttransform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'cortex'),
				'options' => cortex_mikado_get_text_transform_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row4,
				'type' => 'textsimple',
				'name' => 'menu_lineheight',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$first_level_row5 = cortex_mikado_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row5',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row5,
				'type' => 'textsimple',
				'name' => 'menu_padding_left_right',
				'default_value' => '',
				'label' => esc_html__('Padding Left/Right', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $first_level_row5,
				'type' => 'textsimple',
				'name' => 'menu_margin_left_right',
				'default_value' => '',
				'label' => esc_html__('Margin Left/Right', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_group = cortex_mikado_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'second_level_group',
				'title' => esc_html__('2nd Level Menu', 'cortex'),
				'description' => esc_html__('Define styles for 2nd level in Top Navigation Menu', 'cortex'),
			)
		);

		$second_level_row1 = cortex_mikado_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row1'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'cortex'),
			)
		);

		$second_level_row2 = cortex_mikado_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row2',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_fontsize',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_lineheight',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_padding_top_bottom',
				'default_value' => '',
				'label' => esc_html__('Padding Top/Bottom', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_row3 = cortex_mikado_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row3',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font style', 'cortex'),
				'options' => cortex_mikado_get_font_style_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'cortex'),
				'options' => cortex_mikado_get_font_weight_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_letterspacing',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_texttransform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'cortex'),
				'options' => cortex_mikado_get_text_transform_array()
			)
		);

		$second_level_wide_group = cortex_mikado_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'second_level_wide_group',
				'title' => esc_html__('2nd Level Wide Menu', 'cortex'),
				'description' => esc_html__('Define styles for 2nd level in Wide Menu', 'cortex'),
			)
		);

		$second_level_wide_row1 = cortex_mikado_add_admin_row(
			array(
				'parent' => $second_level_wide_group,
				'name' => 'second_level_wide_row1'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'cortex'),
			)
		);

		$second_level_wide_row2 = cortex_mikado_add_admin_row(
			array(
				'parent' => $second_level_wide_group,
				'name' => 'second_level_wide_row2',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_wide_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_fontsize',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_lineheight',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_padding_top_bottom',
				'default_value' => '',
				'label' => esc_html__('Padding Top/Bottom', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_wide_row3 = cortex_mikado_add_admin_row(
			array(
				'parent' => $second_level_wide_group,
				'name' => 'second_level_wide_row3',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font style', 'cortex'),
				'options' => cortex_mikado_get_font_style_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'cortex'),
				'options' => cortex_mikado_get_font_weight_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_letterspacing',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_texttransform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'cortex'),
				'options' => cortex_mikado_get_text_transform_array()
			)
		);

		$third_level_group = cortex_mikado_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'third_level_group',
				'title' => esc_html__('3nd Level Menu', 'cortex'),
				'description' => esc_html__('Define styles for 3nd level in Top Navigation Menu', 'cortex'),
			)
		);

		$third_level_row1 = cortex_mikado_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => 'third_level_row1'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_color_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_hovercolor_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'cortex'),
			)
		);

		$third_level_row2 = cortex_mikado_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => 'third_level_row2',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_google_fonts_thirdlvl',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_fontsize_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_lineheight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$third_level_row3 = cortex_mikado_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => 'third_level_row3',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontstyle_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font style', 'cortex'),
				'options' => cortex_mikado_get_font_style_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontweight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'cortex'),
				'options' => cortex_mikado_get_font_weight_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_letterspacing_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_texttransform_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'cortex'),
				'options' => cortex_mikado_get_text_transform_array()
			)
		);


		/***********************************************************/
		$third_level_wide_group = cortex_mikado_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'third_level_wide_group',
				'title' => esc_html__('3rd Level Wide Menu', 'cortex'),
				'description' => esc_html__('Define styles for 3rd level in Wide Menu', 'cortex'),
			)
		);

		$third_level_wide_row1 = cortex_mikado_add_admin_row(
			array(
				'parent' => $third_level_wide_group,
				'name' => 'third_level_wide_row1'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_color_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_hovercolor_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'cortex'),
			)
		);

		$third_level_wide_row2 = cortex_mikado_add_admin_row(
			array(
				'parent' => $third_level_wide_group,
				'name' => 'third_level_wide_row2',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_wide_google_fonts_thirdlvl',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_fontsize_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_lineheight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$third_level_wide_row3 = cortex_mikado_add_admin_row(
			array(
				'parent' => $third_level_wide_group,
				'name' => 'third_level_wide_row3',
				'next' => true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontstyle_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font style', 'cortex'),
				'options' => cortex_mikado_get_font_style_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontweight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'cortex'),
				'options' => cortex_mikado_get_font_weight_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_letterspacing_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'cortex'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_texttransform_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'cortex'),
				'options' => cortex_mikado_get_text_transform_array()
			)
		);

        $panel_vertical_main_menu = cortex_mikado_add_admin_panel(
            array(
                'title' => esc_html__('Vertical Main Menu', 'cortex'),
                'name' => 'panel_vertical_main_menu',
                'page' => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values' => array(
					'header-standard',
					'header-centered',
					'header-full-screen'
				)
            )
        );

        $drop_down_group = cortex_mikado_add_admin_group(
            array(
                'parent' => $panel_vertical_main_menu,
                'name' => 'vertical_drop_down_group',
                'title' => esc_html__('Main Dropdown Menu', 'cortex'),
                'description' => esc_html__('Set a style for dropdown menu', 'cortex'),
            )
        );

        $vertical_drop_down_row1 = cortex_mikado_add_admin_row(
            array(
                'parent' => $drop_down_group,
                'name' => 'mkdf_drop_down_row1',
            )
        );

        cortex_mikado_add_admin_field(
            array(
                'parent' => $vertical_drop_down_row1,
                'type' => 'colorsimple',
                'name' => 'vertical_dropdown_background_color',
                'default_value' => '',
                'label' => esc_html__('Background Color', 'cortex'),
            )
        );

        $group_vertical_first_level = cortex_mikado_add_admin_group(array(
            'name'			=> 'group_vertical_first_level',
            'title'			=> esc_html__('1st level', 'cortex'),
            'description'	=> esc_html__('Define styles for 1st level menu', 'cortex'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_first_level_1 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_1',
                'parent'	=> $group_vertical_first_level
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_1st_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color', 'cortex'),
                'parent'		=> $row_vertical_first_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_1st_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color', 'cortex'),
                'parent'		=> $row_vertical_first_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_fontsize',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Size', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_lineheight',
                'default_value'	=> '',
                'label'			=> esc_html__('Line Height', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_1
            ));

            $row_vertical_first_level_2 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_2',
                'parent'	=> $group_vertical_first_level,
                'next'		=> true
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_texttransform',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Transform', 'cortex'),
                'options'		=> cortex_mikado_get_text_transform_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_1st_google_fonts',
                'default_value'	=> '-1',
                'label'			=> esc_html__('Font Family', 'cortex'),
                'parent'		=> $row_vertical_first_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_fontstyle',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Style', 'cortex'),
                'options'		=> cortex_mikado_get_font_style_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_fontweight',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Weight', 'cortex'),
                'options'		=> cortex_mikado_get_font_weight_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            $row_vertical_first_level_3 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_3',
                'parent'	=> $group_vertical_first_level,
                'next'		=> true
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_letter_spacing',
                'default_value'	=> '',
                'label'			=> esc_html__('Letter Spacing', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_3
            ));

        $group_vertical_second_level = cortex_mikado_add_admin_group(array(
            'name'			=> 'group_vertical_second_level',
            'title'			=> esc_html__('2nd level', 'cortex'),
            'description'	=> esc_html__('Define styles for 2nd level menu', 'cortex'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_second_level_1 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_1',
                'parent'	=> $group_vertical_second_level
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_2nd_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color', 'cortex'),
                'parent'		=> $row_vertical_second_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_2nd_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color', 'cortex'),
                'parent'		=> $row_vertical_second_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_fontsize',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Size', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_lineheight',
                'default_value'	=> '',
                'label'			=> esc_html__('Line Height', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_1
            ));

            $row_vertical_second_level_2 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_2',
                'parent'	=> $group_vertical_second_level,
                'next'		=> true
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_texttransform',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Transform', 'cortex'),
                'options'		=> cortex_mikado_get_text_transform_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_2nd_google_fonts',
                'default_value'	=> '-1',
                'label'			=> esc_html__('Font Family', 'cortex'),
                'parent'		=> $row_vertical_second_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_fontstyle',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Style', 'cortex'),
                'options'		=> cortex_mikado_get_font_style_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_fontweight',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Weight', 'cortex'),
                'options'		=> cortex_mikado_get_font_weight_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            $row_vertical_second_level_3 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_3',
                'parent'	=> $group_vertical_second_level,
                'next'		=> true
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_letter_spacing',
                'default_value'	=> '',
                'label'			=> esc_html__('Letter Spacing', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_3
            ));

        $group_vertical_third_level = cortex_mikado_add_admin_group(array(
            'name'			=> 'group_vertical_third_level',
            'title'			=> esc_html__('3rd level', 'cortex'),
            'description'	=> esc_html__('Define styles for 3rd level menu', 'cortex'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_third_level_1 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_1',
                'parent'	=> $group_vertical_third_level
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_3rd_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color', 'cortex'),
                'parent'		=> $row_vertical_third_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_3rd_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color', 'cortex'),
                'parent'		=> $row_vertical_third_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_fontsize',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Size', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_1
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_lineheight',
                'default_value'	=> '',
                'label'			=> esc_html__('Line Height', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_1
            ));

            $row_vertical_third_level_2 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_2',
                'parent'	=> $group_vertical_third_level,
                'next'		=> true
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_texttransform',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Transform', 'cortex'),
                'options'		=> cortex_mikado_get_text_transform_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_3rd_google_fonts',
                'default_value'	=> '-1',
                'label'			=> esc_html__('Font Family', 'cortex'),
                'parent'		=> $row_vertical_third_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_fontstyle',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Style', 'cortex'),
                'options'		=> cortex_mikado_get_font_style_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_fontweight',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Weight', 'cortex'),
                'options'		=> cortex_mikado_get_font_weight_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            $row_vertical_third_level_3 = cortex_mikado_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_3',
                'parent'	=> $group_vertical_third_level,
                'next'		=> true
            ));

            cortex_mikado_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_letter_spacing',
                'default_value'	=> '',
                'label'			=> esc_html__('Letter Spacing', 'cortex'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_3
            ));

        $panel_mobile_header = cortex_mikado_add_admin_panel(array(
            'title' => esc_html__('Mobile header', 'cortex'),
            'name'  => 'panel_mobile_header',
            'page'  => '_header_page'
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_header_height',
            'type'        => 'text',
            'label'       => esc_html__('Mobile Header Height', 'cortex'),
            'description' => esc_html__('Enter height for mobile header in pixels', 'cortex'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_header_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Header Background Color', 'cortex'),
            'description' => esc_html__('Choose color for mobile header', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_menu_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Menu Background Color', 'cortex'),
            'description' => esc_html__('Choose color for mobile menu', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_menu_separator_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Menu Item Separator Color', 'cortex'),
            'description' => esc_html__('Choose color for mobile menu horizontal separators', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_logo_height',
            'type'        => 'text',
            'label'       => esc_html__('Logo Height For Mobile Header', 'cortex'),
            'description' => esc_html__('Define logo height for screen size smaller than 1000px', 'cortex'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_logo_height_phones',
            'type'        => 'text',
            'label'       => esc_html__('Logo Height For Mobile Devices', 'cortex'),
            'description' => esc_html__('Define logo height for screen size smaller than 480px', 'cortex'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        cortex_mikado_add_admin_section_title(array(
            'parent' => $panel_mobile_header,
            'name'   => 'mobile_header_fonts_title',
            'title'  => esc_html__('Typography', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_text_color',
            'type'        => 'color',
            'label'       => esc_html__('Navigation Text Color', 'cortex'),
            'description' => esc_html__('Define color for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_text_hover_color',
            'type'        => 'color',
            'label'       => esc_html__('Navigation Hover/Active Color', 'cortex'),
            'description' => esc_html__('Define hover/active color for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_font_family',
            'type'        => 'font',
            'label'       => esc_html__('Navigation Font Family', 'cortex'),
            'description' => esc_html__('Define font family for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_font_size',
            'type'        => 'text',
            'label'       => esc_html__('Navigation Font Size', 'cortex'),
            'description' => esc_html__('Define font size for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_line_height',
            'type'        => 'text',
            'label'       => esc_html__('Navigation Line Height', 'cortex'),
            'description' => esc_html__('Define line height for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_text_transform',
            'type'        => 'select',
            'label'       => esc_html__('Navigation Text Transform', 'cortex'),
            'description' => esc_html__('Define text transform for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header,
            'options'     => cortex_mikado_get_text_transform_array(true)
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_font_style',
            'type'        => 'select',
            'label'       => esc_html__('Navigation Font Style', 'cortex'),
            'description' => esc_html__('Define font style for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header,
            'options'     => cortex_mikado_get_font_style_array(true)
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_font_weight',
            'type'        => 'select',
            'label'       => esc_html__('Navigation Font Weight', 'cortex'),
            'description' => esc_html__('Define font weight for mobile navigation text', 'cortex'),
            'parent'      => $panel_mobile_header,
            'options'     => cortex_mikado_get_font_weight_array(true)
        ));

        cortex_mikado_add_admin_section_title(array(
            'name' => 'mobile_opener_panel',
            'parent' => $panel_mobile_header,
            'title' => esc_html__('Mobile Menu Opener','cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_icon_pack',
            'type'        => 'select',
            'label'       => esc_html__('Mobile Navigation Icon Pack', 'cortex'),
            'default_value' => 'font_awesome',
            'description' => esc_html__('Choose icon pack for mobile navigation icon', 'cortex'),
            'parent'      => $panel_mobile_header,
            'options'     => cortex_mikado_icon_collections()->getIconCollectionsExclude(array('linea_icons'))
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_icon_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Navigation Icon Color', 'cortex'),
            'description' => esc_html__('Choose color for icon header', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_icon_hover_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Navigation Icon Hover Color', 'cortex'),
            'description' => esc_html__('Choose hover color for mobile navigation icon ', 'cortex'),
            'parent'      => $panel_mobile_header
        ));

        cortex_mikado_add_admin_field(array(
            'name'        => 'mobile_icon_size',
            'type'        => 'text',
            'label'       => esc_html__('Mobile Navigation Icon size', 'cortex'),
            'description' => esc_html__('Choose size for mobile navigation icon ', 'cortex'),
            'parent'      => $panel_mobile_header,
            'args' => array(
                'col_width' => 3,
                'suffix' => 'px'
            )
        ));
	}

	add_action( 'cortex_mikado_options_map', 'cortex_mikado_header_options_map', 3);

}