<?php

//top header bar
add_action('cortex_mikado_before_page_header', 'cortex_mikado_get_header_top');

//mobile header
add_action('cortex_mikado_after_page_header', 'cortex_mikado_get_mobile_header');