<?php

if (!function_exists('cortex_mikado_fullscreen_menu_general_styles')) {

	function cortex_mikado_fullscreen_menu_general_styles()
	{
		$fullscreen_menu_background_color = '';
		if (cortex_mikado_options()->getOptionValue('fullscreen_alignment') !== '') {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu ul li, .mkdf-fullscreen-above-menu-widget-holder, .mkdf-fullscreen-below-menu-widget-holder', array(
				'text-align' => cortex_mikado_options()->getOptionValue('fullscreen_alignment')
			));
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_background_color') !== '') {
			$fullscreen_menu_background_color = cortex_mikado_hex2rgb(cortex_mikado_options()->getOptionValue('fullscreen_menu_background_color'));
			if (cortex_mikado_options()->getOptionValue('fullscreen_menu_background_transparency') !== '') {
				$fullscreen_menu_background_transparency = cortex_mikado_options()->getOptionValue('fullscreen_menu_background_transparency');
			} else {
				$fullscreen_menu_background_transparency = 0.9;
			}
		}

		if ($fullscreen_menu_background_color !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-holder', array(
				'background-color' => 'rgba(' . $fullscreen_menu_background_color[0] . ',' . $fullscreen_menu_background_color[1] . ',' . $fullscreen_menu_background_color[2] . ',' . $fullscreen_menu_background_transparency . ')'
			));
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_background_image') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-holder', array(
				'background-image' => 'url(' . cortex_mikado_options()->getOptionValue('fullscreen_menu_background_image') . ')',
				'background-position' => 'center 0',
				'background-repeat' => 'no-repeat'
			));
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_pattern_image') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-holder', array(
				'background-image' => 'url(' . cortex_mikado_options()->getOptionValue('fullscreen_menu_pattern_image') . ')',
				'background-repeat' => 'repeat',
				'background-position' => '0 0'
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_fullscreen_menu_general_styles');

}

if (!function_exists('cortex_mikado_fullscreen_menu_first_level_style')) {

	function cortex_mikado_fullscreen_menu_first_level_style()
	{

		$first_menu_style = array();

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_color') !== '') {
			$first_menu_style['color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_color');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_google_fonts') !== '-1') {
			$first_menu_style['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('fullscreen_menu_google_fonts')) . ',sans-serif';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontsize') !== '') {
			$first_menu_style['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_fontsize')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_lineheight') !== '') {
			$first_menu_style['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_lineheight')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontstyle') !== '') {
			$first_menu_style['font-style'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_fontstyle');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontweight') !== '') {
			$first_menu_style['font-weight'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_fontweight');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_letterspacing') !== '') {
			$first_menu_style['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_letterspacing')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_texttransform') !== '') {
			$first_menu_style['text-transform'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_texttransform');
		}

		if (!empty($first_menu_style)) {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu > ul > li > a, nav.mkdf-fullscreen-menu > ul > li > h6', $first_menu_style);
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener.opened .mkdf-fullscreen-icon', array(
				'color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_color')
			));
		}

		$first_menu_hover_style = array();

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_color') !== '') {
			$first_menu_hover_style['color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_color');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_background_color') !== '') {
			$first_menu_hover_style['background-color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_background_color');
		}

		if (!empty($first_menu_hover_style)) {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu > ul > li > a:hover, nav.mkdf-fullscreen-menu > ul > li > h6:hover', $first_menu_hover_style);
		}

		$first_menu_active_style = array();

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_active_color') !== '') {
			$first_menu_active_style['color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_active_color');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_active_background_color') !== '') {
			$first_menu_active_style['background-color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_active_background_color');
		}

		if (!empty($first_menu_active_style)) {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu > ul > li > a.current', $first_menu_active_style);
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_fullscreen_menu_first_level_style');

}

if (!function_exists('cortex_mikado_fullscreen_menu_second_level_style')) {

	function cortex_mikado_fullscreen_menu_second_level_style()
	{
		$second_menu_style = array();
		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_color_2nd') !== '') {
			$second_menu_style['color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_color_2nd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_google_fonts_2nd') !== '-1') {
			$second_menu_style['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('fullscreen_menu_google_fonts_2nd')) . ',sans-serif';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontsize_2nd') !== '') {
			$second_menu_style['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_fontsize_2nd')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_lineheight_2nd') !== '') {
			$second_menu_style['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_lineheight_2nd')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontstyle_2nd') !== '') {
			$second_menu_style['font-style'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_fontstyle_2nd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontweight_2nd') !== '') {
			$second_menu_style['font-weight'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_fontweight_2nd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_letterspacing_2nd') !== '') {
			$second_menu_style['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_letterspacing_2nd')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_texttransform_2nd') !== '') {
			$second_menu_style['text-transform'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_texttransform_2nd');
		}

		if (!empty($second_menu_style)) {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu ul li ul li a, nav.mkdf-fullscreen-menu ul li ul li h6', $second_menu_style);
		}

		$second_menu_hover_style = array();

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_color_2nd') !== '') {
			$second_menu_hover_style['color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_color_2nd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_background_color_2nd') !== '') {
			$second_menu_hover_style['background-color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_background_color_2nd');
		}

		if (!empty($second_menu_hover_style)) {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu ul li ul li a:hover, nav.mkdf-fullscreen-menu ul li ul li h6:hover', $second_menu_hover_style);
		}
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_fullscreen_menu_second_level_style');

}

if (!function_exists('cortex_mikado_fullscreen_menu_third_level_style')) {

	function cortex_mikado_fullscreen_menu_third_level_style()
	{
		$third_menu_style = array();
		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_color_3rd') !== '') {
			$third_menu_style['color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_color_3rd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_google_fonts_3rd') !== '-1') {
			$third_menu_style['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('fullscreen_menu_google_fonts_3rd')) . ',sans-serif';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontsize_3rd') !== '') {
			$third_menu_style['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_fontsize_3rd')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_lineheight_3rd') !== '') {
			$third_menu_style['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_lineheight_3rd')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontstyle_3rd') !== '') {
			$third_menu_style['font-style'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_fontstyle_3rd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_fontweight_3rd') !== '') {
			$third_menu_style['font-weight'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_fontweight_3rd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_letterspacing_3rd') !== '') {
			$third_menu_style['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_letterspacing_3rd')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_texttransform_3rd') !== '') {
			$third_menu_style['text-transform'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_texttransform_3rd');
		}

		if (!empty($third_menu_style)) {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu ul li ul li ul li a', $third_menu_style);
		}

		$third_menu_hover_style = array();

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_color_3rd') !== '') {
			$third_menu_hover_style['color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_color_3rd');
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_background_color_3rd') !== '') {
			$third_menu_hover_style['background-color'] = cortex_mikado_options()->getOptionValue('fullscreen_menu_hover_background_color_3rd');
		}

		if (!empty($third_menu_hover_style)) {
			echo cortex_mikado_dynamic_css('nav.mkdf-fullscreen-menu ul li ul li ul li a:hover', $third_menu_hover_style);
		}
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_fullscreen_menu_third_level_style');

}

if (!function_exists('cortex_mikado_fullscreen_menu_icon_styles')) {

	function cortex_mikado_fullscreen_menu_icon_styles()
	{

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_color') !== '') {

			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener .mkdf-fullscreen-icon', array(
				'color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_color')
			));

		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_hover_color') !== '') {

			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener:hover .mkdf-fullscreen-icon', array(
				'color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_hover_color')
			));

		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_light_icon_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-light-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon,
			.mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon,
			.mkdf-light-header .mkdf-top-bar .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon', array(
				'color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_light_icon_color') . ' !important'
			));

		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_light_icon_hover_color') !== '') {

			echo cortex_mikado_dynamic_css('.mkdf-light-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-fullscreen-icon,
			.mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-fullscreen-icon,
			.mkdf-light-header .mkdf-top-bar .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-fullscreen-icon', array(
				'color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_light_icon_hover_color') . ' !important'
			));

		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_dark_icon_color') !== '') {

			echo cortex_mikado_dynamic_css('.mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon,
			.mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon,
			.mkdf-dark-header .mkdf-top-bar .mkdf-fullscreen-menu-opener:not(.opened) .mkdf-fullscreen-icon', array(
				'color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_dark_icon_color') . ' !important'
			));

		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_dark_icon_hover_color') !== '') {

			echo cortex_mikado_dynamic_css('.mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-fullscreen-icon,
			.mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-fullscreen-icon,
			.mkdf-dark-header .mkdf-top-bar .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-fullscreen-icon', array(
				'color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_dark_icon_hover_color') . ' !important'
			));

		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_background_color') !== '') {

			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener', array(
				'-webkit-backface-visibility' => 'hidden',
				'display' => 'inline-block'
			));
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener.normal', array(
				'padding' => '10px 15px',
			));
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener.medium', array(
				'padding' => '10px 13px',
			));
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener.large', array(
				'padding' => '15px',
			));
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener:not(.opened)', array(
				'background-color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_background_color')
			));

		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_background_hover_color') !== '') {

			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-menu-opener.normal:not(.opened):hover, .mkdf-fullscreen-menu-opener.medium:not(.opened):hover, .mkdf-fullscreen-menu-opener.large:not(.opened):hover', array(
				'background-color' => cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_background_hover_color')
			));

		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_fullscreen_menu_icon_styles');

}

if (!function_exists('cortex_mikado_fullscreen_menu_icon_spacing')) {

	function cortex_mikado_fullscreen_menu_icon_spacing()
	{

		$fullscreen_menu_icon_spacing = array();

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_padding_left') !== '') {
			$fullscreen_menu_icon_spacing['padding-left'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_padding_left')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_padding_right') !== '') {
			$fullscreen_menu_icon_spacing['padding-right'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_padding_right')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_margin_left') !== '') {
			$fullscreen_menu_icon_spacing['margin-left'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_margin_left')) . 'px';
		}

		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_margin_right') !== '') {
			$fullscreen_menu_icon_spacing['margin-right'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_margin_right')) . 'px';
		}

		if (!empty($fullscreen_menu_icon_spacing)) {
			echo cortex_mikado_dynamic_css('a.mkdf-fullscreen-menu-opener', $fullscreen_menu_icon_spacing);
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_fullscreen_menu_icon_spacing');

}