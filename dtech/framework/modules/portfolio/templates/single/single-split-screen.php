<div class="mkdf-two-columns-50-50 clearfix">
	<div class="mkdf-two-columns-50-50-inner">
		<div class="mkdf-column">
			<div class="mkdf-column-inner">
				<?php
				$media = cortex_mikado_get_portfolio_single_media();

				if(is_array($media) && count($media)) : ?>
					<div class="mkdf-portfolio-media">
						<?php foreach($media as $single_media) : ?>
							<div class="mkdf-portfolio-single-media">
								<?php cortex_mikado_portfolio_get_media_html($single_media); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="mkdf-column">
			<div class="mkdf-column-inner">
				<div class="mkdf-portfolio-info-holder">
					<?php
					
					//get portfolio content section
					cortex_mikado_portfolio_get_info_part('content');

	                //get portfolio info title
	                cortex_mikado_portfolio_get_info_part('info-title');

					//get portfolio custom fields section
					cortex_mikado_portfolio_get_info_part('custom-fields');

	                //get portfolio categories section
	                cortex_mikado_portfolio_get_info_part('categories');

					//get portfolio tags section
					cortex_mikado_portfolio_get_info_part('tags');

					//get portfolio date section
					cortex_mikado_portfolio_get_info_part('date');

					//get portfolio share section
					cortex_mikado_portfolio_get_info_part('social');
					?>
				</div>
			</div>
		</div>
	</div>
</div>