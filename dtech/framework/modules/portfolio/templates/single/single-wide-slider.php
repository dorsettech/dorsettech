<div class="mkdf-grid">
	<div class="mkdf-two-columns-75-25 clearfix">
		<div class="mkdf-column1">
			<div class="mkdf-column-inner">
				<?php cortex_mikado_portfolio_get_info_part('content'); ?>
			</div>
		</div>
		<div class="mkdf-column2">
			<div class="mkdf-column-inner">
				<div class="mkdf-portfolio-info-holder">
					<?php

	                //get portfolio info title
	                cortex_mikado_portfolio_get_info_part('info-title');

					//get portfolio custom fields section
					cortex_mikado_portfolio_get_info_part('custom-fields');

					//get portfolio categories section
					cortex_mikado_portfolio_get_info_part('categories');

					//get portfolio tags section
					cortex_mikado_portfolio_get_info_part('tags');

					//get portfolio date section
					cortex_mikado_portfolio_get_info_part('date');

					//get portfolio share section
					cortex_mikado_portfolio_get_info_part('social');
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mkdf-big-image-holder">
	<?php
	$media = cortex_mikado_get_portfolio_single_media();

	if(is_array($media) && count($media)) : ?>
		<div class="mkdf-portfolio-media mkdf-ptf-wide-slider mkdf-slick-slider-navigation-style">
			<?php foreach($media as $single_media) : ?>
				<div class="mkdf-portfolio-single-media">
					<?php cortex_mikado_portfolio_get_media_html($single_media); ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>