<?php $date_format = 'F d, Y';?>
<div class="mkdf-portfolio-slider-content">
	<span class="mkdf-control mkdf-open arrow_up"></span>

	<div class="mkdf-description">
		<div class="mkdf-ptf-table">
			<div class="mkdf-ptf-table-cell">
				<h3><?php the_title(); ?></h3>
				<span class="mkdf-ptf-date"><?php the_time($date_format); ?></span>
			</div>
		</div>
	</div>

	<div class="mkdf-portfolio-slider-content-info">
		<div class="mkdf-ptf-table">
			<div class="mkdf-ptf-table-cell">
				<div class="egtf-ptf-content-holder">
					<?php cortex_mikado_portfolio_get_info_part('content'); ?>
					<span class="mkdf-control mkdf-close arrow_down"></span>
				</div>
				<div class="mkdf-portfolio-info-holder">
					<?php
						//get portfolio info title
						cortex_mikado_portfolio_get_info_part('info-title');

						//get portfolio custom fields section
						cortex_mikado_portfolio_get_info_part('custom-fields');

						//get portfolio categories section
						cortex_mikado_portfolio_get_info_part('categories');

						//get portfolio tags section
						cortex_mikado_portfolio_get_info_part('tags');

						//get portfolio date section
						cortex_mikado_portfolio_get_info_part('date');

						//get portfolio share section
						cortex_mikado_portfolio_get_info_part('social');
					?>
				</div>
			</div>
		</div>
	</div>

</div>
<div class="mkdf-full-screen-slider-holder">
	<?php
	$media = cortex_mikado_get_portfolio_single_media();

	if(is_array($media) && count($media)) : ?>
		<div class="mkdf-portfolio-full-screen-slider">
			<?php foreach($media as $single_media) : ?>
				<div class="mkdf-portfolio-single-media">
					<?php cortex_mikado_portfolio_get_media_html($single_media); ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>