<?php if ( cortex_mikado_options()->getOptionValue( 'portfolio_single_hide_pagination' ) !== 'yes' ) : ?>

	<?php
	$nav_same_category = cortex_mikado_options()->getOptionValue( 'portfolio_single_nav_same_category' ) == 'yes';
	?>

	<div class="mkdf-portfolio-single-nav">
		<div class="mkdf-portfolio-single-nav-inner">
			<?php if ( get_previous_post() !== '' ) : ?>
				<div class="mkdf-portfolio-prev">
					<?php if ( $nav_same_category ) {
						previous_post_link( '%link', '<span class="arrow_left"></span>'.esc_html__( 'Previous Project', 'cortex' ), true, '', 'portfolio_category' );
					} else {
						previous_post_link( '%link', '<span class="arrow_left"></span>'.esc_html__( 'Previous Project', 'cortex' ) );
					} ?>
				</div>
			<?php endif; ?>
			<?php if ( get_next_post() !== '' ) : ?>
				<div class="mkdf-portfolio-next">
					<?php if ( $nav_same_category ) {
						next_post_link( '%link', esc_html__( 'Next Project', 'cortex' ).'<span class="arrow_right"></span>', true, '', 'portfolio_category' );
					} else {
						next_post_link( '%link', esc_html__( 'Next Project', 'cortex' ).'<span class="arrow_right"></span>');
					} ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

<?php endif; ?>