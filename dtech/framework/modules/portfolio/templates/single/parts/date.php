<?php if(cortex_mikado_options()->getOptionValue('portfolio_single_hide_date') !== 'yes') : ?>

    <div class="mkdf-portfolio-info-item mkdf-portfolio-date">
        <span class="mkdf-portfolio-info-item-title"><?php esc_html_e('Date:', 'cortex'); ?></span>

        <p><?php the_time(get_option('date_format')); ?></p>
    </div>

<?php endif; ?>