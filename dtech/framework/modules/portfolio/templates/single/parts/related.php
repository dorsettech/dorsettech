<?php 
$back_to_link = get_post_meta( get_the_ID(), 'portfolio_single_back_to_link', true );
?>
<div class="mkdf-portfolio-list-holder-outer mkdf-ptf-gallery mkdf-portfolio-slider-holder mkdf-portfolio-related-holder mkdf-ptf-hover-zoom-out-simple mkdf-ptf-nav-hidden" data-items='3'>
    <h5><?php esc_html_e('Related Projects', 'cortex'); ?></h5>
	<div class="mkdf-related-nav-holder">
		<span class="mkdf-related-prev"><span class="lnr lnr-chevron-left"></span></span>
		<?php if ( $back_to_link !== '' ) : ?>
			<div class="mkdf-portfolio-back-btn">
				<a href="<?php echo esc_url( get_permalink( $back_to_link ) ); ?>">
					<span class="icon_grid-3x3"></span>
				</a>
			</div>
		<?php endif; ?>
		<span class="mkdf-related-next"><span class="lnr lnr-chevron-right"></span></span>
	</div>
    <div class="mkdf-portfolio-list-holder clearfix">
        <?php
        $query = cortex_mikado_get_related_post_type(get_the_ID(), array('posts_per_page' => '6'));
        if (is_object($query)) {
            if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                <?php if (has_post_thumbnail()) {
                    $id = get_the_ID();
                    $item_link = get_permalink($id);
                    if (get_post_meta($id, 'portfolio_external_link', true) !== '') {
                        $item_link = get_post_meta($id, 'portfolio_external_link', true);
                    }

                    $categories = wp_get_post_terms($id, 'portfolio-category');
                    $category_html = '';
                    $k = 1;
                    foreach ($categories as $cat) {
                        $category_html .= '<span>' . $cat->name . '</span>';
                        if (count($categories) != $k) {
                            $category_html .= ' / ';
                        }
                        $k++;
                    }
                    ?>
                    <article class="mkdf-portfolio-item mix">
	                    <div class="mkdf-portfolio-item-inner">
	                    	<div class = "mkdf-item-image-holder">
								<a class="mkdf-portfolio-link" href="<?php echo esc_url($item_link); ?>"></a>
									<?php
										echo get_the_post_thumbnail(get_the_ID(),'cortex_mikado_square');
									?>
								<div class="mkdf-item-text-overlay">
									<div class="mkdf-item-text-overlay-inner">
										<div class="mkdf-item-text-holder">
											<h4 class="mkdf-item-title">
												<a class="mkdf-portfolio-title-link" href="<?php echo esc_url($item_link); ?>">
													<?php echo esc_attr(get_the_title()); ?>
												</a>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </article>
                <?php } ?>
                <?php
            endwhile;
            endif;
            wp_reset_postdata();
        } else { ?>
            <p><?php esc_html_e('No related portfolios were found.', 'cortex'); ?></p>
        <?php }
        ?>
    </div>
</div>

