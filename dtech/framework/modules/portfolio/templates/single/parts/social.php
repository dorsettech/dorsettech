<?php if(cortex_mikado_options()->getOptionValue('enable_social_share') == 'yes'
    && cortex_mikado_options()->getOptionValue('enable_social_share_on_portfolio-item') == 'yes') : ?>
    <div class="mkdf-portfolio-social">
        <?php echo cortex_mikado_get_social_share_html() ?>
    </div>
<?php endif; ?>