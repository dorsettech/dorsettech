<?php $title_tag = 'h1';

?>
<div class="mkdf-portfolio-info-item mkdf-content-item">
    <<?php echo esc_attr($title_tag); ?> class="mkdf-portfolio-title"><?php the_title(); ?></<?php echo esc_attr($title_tag); ?>>
    <div class="mkdf-portfolio-content">
        <?php the_content(); ?>
    </div>
</div>