<?php

if ( ! function_exists('cortex_mikado_portfolio_options_map') ) {

	function cortex_mikado_portfolio_options_map() {

		cortex_mikado_add_admin_page(array(
			'slug'  => '_portfolio',
			'title' => esc_html__('Portfolio','cortex'),
			'icon'  => 'fa fa-camera-retro'
		));

		$panel = cortex_mikado_add_admin_panel(array(
			'title' => esc_html__('Portfolio Single','cortex'),
			'name'  => 'panel_portfolio_single',
			'page'  => '_portfolio'
		));

		cortex_mikado_add_admin_field(array(
			'name'        => 'portfolio_single_template',
			'type'        => 'select',
			'label'       => esc_html__('Portfolio Type','cortex'),
			'default_value'	=> 'small-images',
			'description' => esc_html__('Choose a default type for Single Project pages','cortex'),
			'parent'      => $panel,
			'options'     => array(
				'small-images' => esc_html__('Portfolio small images left','cortex'),
				'small-images-right' => esc_html__('Portfolio small images right','cortex'),
				'small-slider' => esc_html__('Portfolio small slider left','cortex'),
				'small-slider-right' => esc_html__('Portfolio small slider right','cortex'),
				'big-images' => esc_html__('Portfolio big images','cortex'),
				'wide-images' => esc_html__('Portfolio wide images left','cortex'),
                'wide-images-right' => esc_html__('Portfolio wide images right','cortex'),
				'big-slider' => esc_html__('Portfolio big slider','cortex'),
                'wide-slider' => esc_html__('Portfolio wide slider','cortex'),
				'small-masonry' => esc_html__('Portfolio small masonry','cortex'),
				'big-masonry' => esc_html__('Portfolio big masonry','cortex'),
				'gallery' => esc_html__('Portfolio gallery','cortex'),
                'split-screen' => esc_html__('Portfolio split screen','cortex'),
                'full-screen-slider' => esc_html__('Portfolio full screen slider','cortex'),
				'custom' => esc_html__('Portfolio custom','cortex'),
				'full-width-custom' => esc_html__('Portfolio full width custom','cortex')
			)
		));

		cortex_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_images',
			'type'          => 'yesno',
			'label'         => esc_html__('Lightbox for Images','cortex'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for projects with images.','cortex'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		cortex_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_videos',
			'type'          => 'yesno',
			'label'         => esc_html__('Lightbox for Videos','cortex'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for YouTube/Vimeo projects.','cortex'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		cortex_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_hide_categories',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Categories','cortex'),
			'description'   => esc_html__('Enabling this option will disable category meta description on Single Projects.','cortex'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		cortex_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_hide_date',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Date','cortex'),
			'description'   => esc_html__('Enabling this option will disable date meta on Single Projects.','cortex'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		cortex_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_show_related',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Related Projects','cortex'),
			'description'   => esc_html__('Enabling this option will show related projects on your page.','cortex'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		cortex_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_sticky_sidebar',
			'type'          => 'yesno',
			'label'         => esc_html__('Sticky Side Text','cortex'),
			'description'   => esc_html__('Enabling this option will make side text sticky on Single Project pages','cortex'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

        cortex_mikado_add_admin_field(array(
            'name'          => 'portfolio_single_comments',
            'type'          => 'yesno',
            'label'         => esc_html__('Show Comments','cortex'),
            'description'   => esc_html__('Enabling this option will show comments on your portfolio.','cortex'),
            'parent'        => $panel,
            'default_value' => 'no'
        ));

		cortex_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_hide_pagination',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Pagination','cortex'),
			'description'   => esc_html__('Enabling this option will turn off portfolio pagination functionality.','cortex'),
			'parent'        => $panel,
			'default_value' => 'yes',
			'args' => array(
				'dependence' => true,
				'dependence_hide_on_yes' => '#mkdf_navigate_same_category_container'
			)
		));

		$container_navigate_category = cortex_mikado_add_admin_container(array(
			'name'            => 'navigate_same_category_container',
			'parent'          => $panel,
			'hidden_property' => 'portfolio_single_hide_pagination',
			'hidden_value'    => 'yes'
		));

		cortex_mikado_add_admin_field(array(
			'name'            => 'portfolio_single_nav_same_category',
			'type'            => 'yesno',
			'label'           => esc_html__('Enable Pagination Through Same Category','cortex'),
			'description'     => esc_html__('Enabling this option will make portfolio pagination sort through current category.','cortex'),
			'parent'          => $container_navigate_category,
			'default_value'   => 'no'
		));

		cortex_mikado_add_admin_field(array(
			'name'        => 'portfolio_single_numb_columns',
			'type'        => 'select',
			'label'       => esc_html__('Number of Columns','cortex'),
			'default_value' => 'three-columns',
			'description' => esc_html__('Enter the number of columns for Portfolio Gallery type','cortex'),
			'parent'      => $panel,
			'options'     => array(
				'two-columns' => esc_html__('2 columns','cortex'),
				'three-columns' => esc_html__('3 columns','cortex'),
				'four-columns' => esc_html__('4 columns','cortex')
			)
		));

		cortex_mikado_add_admin_field(array(
			'name'        => 'portfolio_single_slug',
			'type'        => 'text',
			'label'       => esc_html__('Portfolio Single Slug','cortex'),
			'description' => esc_html__('Enter if you wish to use a different Single Project slug (Note: After entering slug, navigate to Settings -> Permalinks and click "Save" in order for changes to take effect)','cortex'),
			'parent'      => $panel,
			'args'        => array(
				'col_width' => 3
			)
		));

	}

	add_action( 'cortex_mikado_options_map', 'cortex_mikado_portfolio_options_map', 12);

}