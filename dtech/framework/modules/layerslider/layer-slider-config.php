<?php
	if(!function_exists('cortex_mikado_layerslider_overrides')) {
		/**
		 * Disables Layer Slider auto update box
		 */
		function cortex_mikado_layerslider_overrides() {
			$GLOBALS['lsAutoUpdateBox'] = false;
		}

		add_action('layerslider_ready', 'cortex_mikado_layerslider_overrides');
	}
?>