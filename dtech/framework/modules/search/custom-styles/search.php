<?php

if (!function_exists('cortex_mikado_search_opener_icon_size')) {

	function cortex_mikado_search_opener_icon_size()
	{

		if (cortex_mikado_options()->getOptionValue('header_search_icon_size')) {
			echo cortex_mikado_dynamic_css('.mkdf-search-opener, .mkdf-header-standard .mkdf-search-opener', array(
				'font-size' => cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('header_search_icon_size')) . 'px'
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_opener_icon_size');

}

if (!function_exists('cortex_mikado_search_opener_icon_colors')) {

	function cortex_mikado_search_opener_icon_colors()
	{

		if (cortex_mikado_options()->getOptionValue('header_search_icon_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-opener', array(
				'color' => cortex_mikado_options()->getOptionValue('header_search_icon_color')
			));
		}

		if (cortex_mikado_options()->getOptionValue('header_search_icon_hover_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-opener:hover', array(
				'color' => cortex_mikado_options()->getOptionValue('header_search_icon_hover_color')
			));
		}

		if (cortex_mikado_options()->getOptionValue('header_light_search_icon_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-light-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-search-opener,
			.mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-search-opener,
			.mkdf-light-header .mkdf-top-bar .mkdf-search-opener', array(
				'color' => cortex_mikado_options()->getOptionValue('header_light_search_icon_color') . ' !important'
			));
		}

		if (cortex_mikado_options()->getOptionValue('header_light_search_icon_hover_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-light-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-search-opener:hover,
			.mkdf-light-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-search-opener:hover,
			.mkdf-light-header .mkdf-top-bar .mkdf-search-opener:hover', array(
				'color' => cortex_mikado_options()->getOptionValue('header_light_search_icon_hover_color') . ' !important'
			));
		}

		if (cortex_mikado_options()->getOptionValue('header_dark_search_icon_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-search-opener,
			.mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-search-opener,
			.mkdf-dark-header .mkdf-top-bar .mkdf-search-opener', array(
				'color' => cortex_mikado_options()->getOptionValue('header_dark_search_icon_color') . ' !important'
			));
		}
		if (cortex_mikado_options()->getOptionValue('header_dark_search_icon_hover_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-search-opener:hover,
			.mkdf-dark-header.mkdf-header-style-on-scroll .mkdf-page-header .mkdf-search-opener:hover,
			.mkdf-dark-header .mkdf-top-bar .mkdf-search-opener:hover', array(
				'color' => cortex_mikado_options()->getOptionValue('header_dark_search_icon_hover_color') . ' !important'
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_opener_icon_colors');

}

if (!function_exists('cortex_mikado_search_opener_icon_background_colors')) {

	function cortex_mikado_search_opener_icon_background_colors()
	{

		if (cortex_mikado_options()->getOptionValue('search_icon_background_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-opener', array(
				'background-color' => cortex_mikado_options()->getOptionValue('search_icon_background_color')
			));
		}

		if (cortex_mikado_options()->getOptionValue('search_icon_background_hover_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-opener:hover', array(
				'background-color' => cortex_mikado_options()->getOptionValue('search_icon_background_hover_color')
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_opener_icon_background_colors');
}

if (!function_exists('cortex_mikado_search_opener_text_styles')) {

	function cortex_mikado_search_opener_text_styles()
	{
		$text_styles = array();

		if (cortex_mikado_options()->getOptionValue('search_icon_text_color') !== '') {
			$text_styles['color'] = cortex_mikado_options()->getOptionValue('search_icon_text_color');
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_text_fontsize') !== '') {
			$text_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_icon_text_fontsize')) . 'px';
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_text_lineheight') !== '') {
			$text_styles['line-height'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_icon_text_lineheight')) . 'px';
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_text_texttransform') !== '') {
			$text_styles['text-transform'] = cortex_mikado_options()->getOptionValue('search_icon_text_texttransform');
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('search_icon_text_google_fonts')) . ', sans-serif';
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_text_fontstyle') !== '') {
			$text_styles['font-style'] = cortex_mikado_options()->getOptionValue('search_icon_text_fontstyle');
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_text_fontweight') !== '') {
			$text_styles['font-weight'] = cortex_mikado_options()->getOptionValue('search_icon_text_fontweight');
		}		
		if (cortex_mikado_options()->getOptionValue('search_icon_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_icon_text_letterspacing')).'px';
		}

		if (!empty($text_styles)) {
			echo cortex_mikado_dynamic_css('.mkdf-search-icon-text', $text_styles);
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_text_color_hover') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-opener:hover .mkdf-search-icon-text', array(
				'color' => cortex_mikado_options()->getOptionValue('search_icon_text_color_hover')
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_opener_text_styles');
}

if (!function_exists('cortex_mikado_search_opener_spacing')) {

	function cortex_mikado_search_opener_spacing()
	{
		$spacing_styles = array();

		if (cortex_mikado_options()->getOptionValue('search_padding_left') !== '') {
			$spacing_styles['padding-left'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_padding_left')) . 'px';
		}
		if (cortex_mikado_options()->getOptionValue('search_padding_right') !== '') {
			$spacing_styles['padding-right'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_padding_right')) . 'px';
		}
		if (cortex_mikado_options()->getOptionValue('search_margin_left') !== '') {
			$spacing_styles['margin-left'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_margin_left')) . 'px';
		}
		if (cortex_mikado_options()->getOptionValue('search_margin_right') !== '') {
			$spacing_styles['margin-right'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_margin_right')) . 'px';
		}

		if (!empty($spacing_styles)) {
			echo cortex_mikado_dynamic_css('.mkdf-search-opener', $spacing_styles);
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_opener_spacing');
}

if (!function_exists('cortex_mikado_search_bar_background')) {

	function cortex_mikado_search_bar_background()
	{

		if (cortex_mikado_options()->getOptionValue('search_background_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover, .mkdf-search-fade .mkdf-fullscreen-search-holder .mkdf-fullscreen-search-table, .mkdf-fullscreen-search-overlay', array(
				'background-color' => cortex_mikado_options()->getOptionValue('search_background_color')
			));
		}
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_bar_background');
}

if (!function_exists('cortex_mikado_search_text_styles')) {

	function cortex_mikado_search_text_styles()
	{
		$text_styles = array();

		if (cortex_mikado_options()->getOptionValue('search_text_color') !== '') {
			$text_styles['color'] = cortex_mikado_options()->getOptionValue('search_text_color');
			$placeholder_webkit = array(
				'.mkdf-search-cover input[type="text"]::-webkit-input-placeholder',
				'.mkdf-form-holder .mkdf-search-field::-webkit-input-placeholder',
				'.mkdf-fullscreen-search-opened .mkdf-form-holder .mkdf-search-field::-webkit-input-placeholder'
			);
			$placeholder_moz1 = array(
				'.mkdf-search-cover input[type="text"]:-moz-input-placeholder',
				'.mkdf-form-holder .mkdf-search-field:-moz-input-placeholder',
				'mkdf-fullscreen-search-opened .mkdf-form-holder .mkdf-search-field:-moz-input-placeholder'
			);
			$placeholder_moz2 = array(
				'.mkdf-search-cover input[type="text"]::-moz-input-placeholder',
				'.mkdf-form-holder .mkdf-search-field::-moz-input-placeholder',
				'mkdf-fullscreen-search-opened .mkdf-form-holder .mkdf-search-field::-moz-input-placeholder'
			);
			$placeholder_ms = array(
				'.mkdf-search-cover input[type="text"]:-ms-input-placeholder',
				'.mkdf-form-holder .mkdf-search-field:-ms-input-placeholder',
				'mkdf-fullscreen-search-opened .mkdf-form-holder .mkdf-search-field:-ms-input-placeholder'
			);
			echo cortex_mikado_dynamic_css($placeholder_webkit,array('color' => $text_styles['color']));
			echo cortex_mikado_dynamic_css($placeholder_moz1,array('color' => $text_styles['color']));
			echo cortex_mikado_dynamic_css($placeholder_moz2,array('color' => $text_styles['color']));
			echo cortex_mikado_dynamic_css($placeholder_ms,array('color' => $text_styles['color']));
		}
		if (cortex_mikado_options()->getOptionValue('search_text_fontsize') !== '') {
			$text_styles['font-size'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_text_fontsize')) . 'px';
		}
		if (cortex_mikado_options()->getOptionValue('search_text_texttransform') !== '') {
			$text_styles['text-transform'] = cortex_mikado_options()->getOptionValue('search_text_texttransform');
		}
		if (cortex_mikado_options()->getOptionValue('search_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = cortex_mikado_get_formatted_font_family(cortex_mikado_options()->getOptionValue('search_text_google_fonts')) . ', sans-serif';
		}
		if (cortex_mikado_options()->getOptionValue('search_text_fontstyle') !== '') {
			$text_styles['font-style'] = cortex_mikado_options()->getOptionValue('search_text_fontstyle');
		}
		if (cortex_mikado_options()->getOptionValue('search_text_fontweight') !== '') {
			$text_styles['font-weight'] = cortex_mikado_options()->getOptionValue('search_text_fontweight');
		}
		if (cortex_mikado_options()->getOptionValue('search_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_text_letterspacing')) . 'px';
		}

		if (!empty($text_styles)) {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover input[type="text"], .mkdf-fullscreen-search-opened .mkdf-form-holder .mkdf-search-field', $text_styles);
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_text_styles');
}

if (!function_exists('cortex_mikado_search_icon_styles')) {

	function cortex_mikado_search_icon_styles()
	{

		if (cortex_mikado_options()->getOptionValue('search_icon_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover input[type="submit"], .mkdf-fullscreen-search-holder .mkdf-search-submit', array(
				'color' => cortex_mikado_options()->getOptionValue('search_icon_color')
			));
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_hover_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover input[type="submit"]:hover, .mkdf-fullscreen-search-holder .mkdf-search-submit:hover', array(
				'color' => cortex_mikado_options()->getOptionValue('search_icon_hover_color')
			));
		}
		if (cortex_mikado_options()->getOptionValue('search_icon_size') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover input[type="submit"], .mkdf-fullscreen-search-holder .mkdf-search-submit', array(
				'font-size' => cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_icon_size')) . 'px'
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_icon_styles');
}

if (!function_exists('cortex_mikado_search_close_icon_styles')) {

	function cortex_mikado_search_close_icon_styles()
	{

		if (cortex_mikado_options()->getOptionValue('search_close_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover .mkdf-search-close a, .mkdf-fullscreen-search-holder .mkdf-fullscreen-search-close-container a', array(
				'color' => cortex_mikado_options()->getOptionValue('search_close_color')
			));
		}
		if (cortex_mikado_options()->getOptionValue('search_close_hover_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover .mkdf-search-close a:hover, .mkdf-fullscreen-search-holder .mkdf-fullscreen-search-close-container a:hover', array(
				'color' => cortex_mikado_options()->getOptionValue('search_close_hover_color')
			));
		}
		if (cortex_mikado_options()->getOptionValue('search_close_size') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-search-cover .mkdf-search-close a, .mkdf-fullscreen-search-holder .mkdf-fullscreen-search-close-container a', array(
				'font-size' => cortex_mikado_filter_px(cortex_mikado_options()->getOptionValue('search_close_size')) . 'px'
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_close_icon_styles');
}

if (!function_exists('cortex_mikado_search_border_styles')) {

	function cortex_mikado_search_border_styles()
	{

		if (cortex_mikado_options()->getOptionValue('search_border_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-search-holder .mkdf-field-holder', array(
				'border-color' => cortex_mikado_options()->getOptionValue('search_border_color')
			));
		}
		if (cortex_mikado_options()->getOptionValue('search_border_focus_color') !== '') {
			echo cortex_mikado_dynamic_css('.mkdf-fullscreen-search-holder .mkdf-field-holder .mkdf-line', array(
				'background-color' => cortex_mikado_options()->getOptionValue('search_border_focus_color')
			));
		}

	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_search_border_styles');
}

?>
