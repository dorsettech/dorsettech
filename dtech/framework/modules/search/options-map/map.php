<?php

if ( ! function_exists('cortex_mikado_search_options_map') ) {

	function cortex_mikado_search_options_map() {

		cortex_mikado_add_admin_page(
			array(
				'slug' => '_search_page',
				'title' => esc_html__('Search', 'cortex'),
				'icon' => 'fa fa-search'
			)
		);

		$search_panel = cortex_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Search', 'cortex'),
				'name' => 'search',
				'page' => '_search_page'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'select',
				'name'			=> 'search_type',
				'default_value'	=> 'search-covers-header',
				'label' 		=> esc_html__('Select Search Type', 'cortex'),
				'description' 	=> esc_html__("Choose a type of Select search bar (Note: Slide From Header Bottom search type doesn't work with transparent header)", 'cortex'),
				'options' 		=> array(
					'search-covers-header' => esc_html__('Search Covers Header', 'cortex'),
					'fullscreen-search' => esc_html__('Fullscreen Search', 'cortex'),
				),
				'args'			=> array(
					'dependence'=> true,
					'hide'		=> array(
						'search-covers-header' => '#mkdf_search_animation_container',
						'fullscreen-search' => '',
					),
					'show'		=> array(
						'search-covers-header' => '',
						'fullscreen-search' => '#mkdf_search_animation_container',
					)
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'select',
				'name'			=> 'search_icon_pack',
				'default_value'	=> 'font_elegant',
				'label'			=> esc_html__('Search Icon Pack', 'cortex'),
				'description'	=> esc_html__('Choose icon pack for search icon', 'cortex'),
				'options'		=> cortex_mikado_icon_collections()->getIconCollectionsExclude(array('linea_icons'))
			)
		);

		$search_animation_container = cortex_mikado_add_admin_container(
			array(
				'parent'			=> $search_panel,
				'name'				=> 'search_animation_container',
				'hidden_property'	=> 'search_type',
				'hidden_value'		=> '',
				'hidden_values'		=> array(
					'search-covers-header',
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_animation_container,
				'type'			=> 'select',
				'name'			=> 'search_animation',
				'default_value'	=> 'search-fade',
				'label'			=> esc_html__('Fullscreen Search Overlay Animation', 'cortex'),
				'description'	=> esc_html__('Choose animation for fullscreen search overlay', 'cortex'),
				'options'		=> array(
					'search-fade'			=> esc_html__('Fade', 'cortex'),
					'search-from-circle'	=> esc_html__('Circle appear', 'cortex'),
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'yesno',
				'name'			=> 'search_in_grid',
				'default_value'	=> 'no',
				'label'			=> esc_html__('Search area in grid', 'cortex'),
				'description'	=> esc_html__('Set search area to be in grid', 'cortex'),
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' 	=> $search_panel,
				'name'		=> 'initial_header_icon_title',
				'title'		=> esc_html__('Initial Search Icon in Header','cortex')
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'text',
				'name'			=> 'header_search_icon_size',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Size', 'cortex'),
				'description'	=> esc_html__('Set size for icon', 'cortex'),
				'args'			=> array(
					'col_width' => 3,
					'suffix'	=> 'px'
				)
			)
		);

		$search_icon_color_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Icon Colors', 'cortex'),
				'description'	=> esc_html__('Define color style for icon', 'cortex'),
				'name'		=> 'search_icon_color_group'
			)
		);

		$search_icon_color_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_icon_color_group,
				'name'		=> 'search_icon_color_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'	=> $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_color',
				'label'		=> esc_html__('Color', 'cortex'),
			)
		);
		cortex_mikado_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_hover_color',
				'label'		=> esc_html__('Hover Color', 'cortex'),
			)
		);
		cortex_mikado_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_light_search_icon_color',
				'label'		=> esc_html__('Light Header Icon Color', 'cortex'),
			)
		);
		cortex_mikado_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_light_search_icon_hover_color',
				'label'		=> esc_html__('Light Header Icon Hover Color', 'cortex'),
			)
		);

		$search_icon_color_row2 = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_icon_color_group,
				'name'		=> 'search_icon_color_row2',
				'next'		=> true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $search_icon_color_row2,
				'type'		=> 'colorsimple',
				'name'		=> 'header_dark_search_icon_color',
				'label'		=> esc_html__('Dark Header Icon Color', 'cortex'),
			)
		);
		cortex_mikado_add_admin_field(
			array(
				'parent' => $search_icon_color_row2,
				'type'		=> 'colorsimple',
				'name'		=> 'header_dark_search_icon_hover_color',
				'label'		=> esc_html__('Dark Header Icon Hover Color', 'cortex'),
			)
		);


		$search_icon_background_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Icon Background Style', 'cortex'),
				'description'	=> esc_html__('Define background style for icon', 'cortex'),
				'name'		=> 'search_icon_background_group'
			)
		);

		$search_icon_background_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_icon_background_group,
				'name'		=> 'search_icon_background_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_background_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_background_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Background Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_background_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_background_hover_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Background Hover Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'yesno',
				'name'			=> 'enable_search_icon_text',
				'default_value'	=> 'no',
				'label'			=> esc_html__('Enable Search Icon Text', 'cortex'),
				'description'	=> esc_html__("Enable this option to show 'Search' text next to search icon in header", 'cortex'),
				'args'			=> array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_enable_search_icon_text_container'
				)
			)
		);

		$enable_search_icon_text_container = cortex_mikado_add_admin_container(
			array(
				'parent'			=> $search_panel,
				'name'				=> 'enable_search_icon_text_container',
				'hidden_property'	=> 'enable_search_icon_text',
				'hidden_value'		=> 'no'
			)
		);

		$enable_search_icon_text_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $enable_search_icon_text_container,
				'title'		=> esc_html__('Search Icon Text', 'cortex'),
				'name'		=> 'enable_search_icon_text_group',
				'description'	=> esc_html__('Define Style for Search Icon Text', 'cortex'),
			)
		);

		$enable_search_icon_text_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color',
				'label'			=> esc_html__('Text Color', 'cortex'),
				'default_value'	=> ''
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color_hover',
				'label'			=> esc_html__('Text Hover Color', 'cortex'),
				'default_value'	=> ''
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_fontsize',
				'label'			=> esc_html__('Font Size', 'cortex'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_lineheight',
				'label'			=> esc_html__('Line Height', 'cortex'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$enable_search_icon_text_row2 = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row2',
				'next'		=> true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_texttransform',
				'label'			=> esc_html__('Text Transform', 'cortex'),
				'default_value'	=> '',
				'options'		=> cortex_mikado_get_text_transform_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'fontsimple',
				'name'			=> 'search_icon_text_google_fonts',
				'label'			=> esc_html__('Font Family', 'cortex'),
				'default_value'	=> '-1',
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_fontstyle',
				'label'			=> esc_html__('Font Style', 'cortex'),
				'default_value'	=> '',
				'options'		=> cortex_mikado_get_font_style_array(),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_fontweight',
				'label'			=> esc_html__('Font Weight', 'cortex'),
				'default_value'	=> '',
				'options'		=> cortex_mikado_get_font_weight_array(),
			)
		);

		$enable_search_icon_text_row3 = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row3',
				'next'		=> true
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row3,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_letterspacing',
				'label'			=> esc_html__('Letter Spacing', 'cortex'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_icon_spacing_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Icon Spacing', 'cortex'),
				'description'	=> esc_html__('Define padding and margins for Search icon', 'cortex'),
				'name'		=> 'search_icon_spacing_group'
			)
		);

		$search_icon_spacing_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_icon_spacing_group,
				'name'		=> 'search_icon_spacing_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_padding_left',
				'default_value'	=> '',
				'label'			=> esc_html__('Padding Left', 'cortex'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_padding_right',
				'default_value'	=> '',
				'label'			=> esc_html__('Padding Right', 'cortex'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_margin_left',
				'default_value'	=> '',
				'label'			=> esc_html__('Margin Left', 'cortex'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_margin_right',
				'default_value'	=> '',
				'label'			=> esc_html__('Margin Right', 'cortex'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		cortex_mikado_add_admin_section_title(
			array(
				'parent' 	=> $search_panel,
				'name'		=> 'search_form_title',
				'title'		=> esc_html__('Search Bar', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'color',
				'name'			=> 'search_background_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Background Color', 'cortex'),
				'description'	=> esc_html__('Choose a background color for Select search bar', 'cortex'),
			)
		);

		$search_input_text_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Input Text', 'cortex'),
				'description'	=> esc_html__('Define style for search text', 'cortex'),
				'name'		=> 'search_input_text_group'
			)
		);

		$search_input_text_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_input_text_group,
				'name'		=> 'search_input_text_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_input_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_text_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Text Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_input_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_text_fontsize',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Size', 'cortex'),
				'args'			=> array(
					'suffix' => 'px'
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_input_text_row,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_text_texttransform',
				'default_value'	=> '',
				'label'			=> esc_html__('Text Transform', 'cortex'),
				'options'		=> cortex_mikado_get_text_transform_array()
			)
		);

		$search_input_text_row2 = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_input_text_group,
				'name'		=> 'search_input_text_row2'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'fontsimple',
				'name'			=> 'search_text_google_fonts',
				'default_value'	=> '-1',
				'label'			=> esc_html__('Font Family', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_text_fontstyle',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Style', 'cortex'),
				'options'		=> cortex_mikado_get_font_style_array(),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_text_fontweight',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Weight', 'cortex'),
				'options'		=> cortex_mikado_get_font_weight_array()
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'textsimple',
				'name'			=> 'search_text_letterspacing',
				'default_value'	=> '',
				'label'			=> esc_html__('Letter Spacing', 'cortex'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_icon_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Icon', 'cortex'),
				'description'	=> esc_html__('Define style for search icon', 'cortex'),
				'name'		=> 'search_icon_group'
			)
		);

		$search_icon_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_icon_group,
				'name'		=> 'search_icon_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_hover_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Hover Color', 'cortex'),
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_icon_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_size',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Size', 'cortex'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_close_icon_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Close', 'cortex'),
				'description'	=> esc_html__('Define style for search close icon', 'cortex'),
				'name'		=> 'search_close_icon_group'
			)
		);

		$search_close_icon_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_close_icon_group,
				'name'		=> 'search_icon_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_close_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_close_color',
				'label'			=> esc_html__('Icon Color', 'cortex'),
				'default_value'	=> ''
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_close_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_close_hover_color',
				'label'			=> esc_html__('Icon Hover Color', 'cortex'),
				'default_value'	=> ''
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_close_icon_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_close_size',
				'label'			=> esc_html__('Icon Size','cortex'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_bottom_border_group = cortex_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Bottom Border', 'cortex'),
				'description'	=> esc_html__('Define style for Search text input bottom border (for Fullscreen search type)', 'cortex'),
				'name'		=> 'search_bottom_border_group'
			)
		);

		$search_bottom_border_row = cortex_mikado_add_admin_row(
			array(
				'parent'	=> $search_bottom_border_group,
				'name'		=> 'search_icon_row'
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_bottom_border_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_border_color',
				'label'			=> esc_html__('Border Color', 'cortex'),
				'default_value'	=> ''
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent'		=> $search_bottom_border_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_border_focus_color',
				'label'			=> esc_html__('Border Focus Color', 'cortex'),
				'default_value'	=> ''
			)
		);

	}

	add_action('cortex_mikado_options_map', 'cortex_mikado_search_options_map', 6);

}