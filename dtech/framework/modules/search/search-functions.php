<?php

if( !function_exists('cortex_mikado_search_body_class') ) {
	/**
	 * Function that adds body classes for different search types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function cortex_mikado_search_body_class($classes) {

		if ( is_active_widget( false, false, 'mkd_search_opener' ) ) {

			$classes[] = 'mkdf-' . cortex_mikado_options()->getOptionValue('search_type');

			if ( cortex_mikado_options()->getOptionValue('search_type') == 'fullscreen-search' ) {

				$classes[] = 'mkdf-' . cortex_mikado_options()->getOptionValue('search_animation');

			}

		}
		return $classes;

	}

	add_filter('body_class', 'cortex_mikado_search_body_class');
}

if ( ! function_exists('cortex_mikado_get_search') ) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function cortex_mikado_get_search() {

		if ( cortex_mikado_active_widget( false, false, 'mkd_search_opener' ) ) {

			$search_type = cortex_mikado_options()->getOptionValue('search_type');

			if ($search_type == 'search-covers-header') {
				cortex_mikado_set_position_for_covering_search();
				return;
			}

			cortex_mikado_load_search_template();

		}
	}

}

if ( ! function_exists('cortex_mikado_set_position_for_covering_search') ) {
	/**
	 * Finds part of header where search template will be loaded
	 */
	function cortex_mikado_set_position_for_covering_search() {

		$containing_sidebar = cortex_mikado_active_widget( false, false, 'mkd_search_opener' );

		foreach ($containing_sidebar as $sidebar) {

			if ( strpos( $sidebar, 'top-bar' ) !== false ) {
				add_action( 'cortex_mikado_after_header_top_html_open', 'cortex_mikado_load_search_template');
			} else if ( strpos( $sidebar, 'header-widget-area' ) !== false ) {
				add_action( 'cortex_mikado_after_header_menu_area_html_open', 'cortex_mikado_load_search_template');
			} else if ( strpos( $sidebar, 'mobile' ) !== false ) {
				add_action( 'cortex_mikado_after_mobile_header_html_open', 'cortex_mikado_load_search_template');
			} else if ( strpos( $sidebar, 'logo' ) !== false ) {
				add_action( 'cortex_mikado_after_header_logo_area_html_open', 'cortex_mikado_load_search_template');
			} else if ( strpos( $sidebar, 'sticky' ) !== false ) {
				add_action( 'cortex_mikado_after_sticky_menu_html_open', 'cortex_mikado_load_search_template');
			}

		}

	}

}

if ( ! function_exists('cortex_mikado_load_search_template') ) {
	/**
	 * Loads HTML template with parameters
	 */
	function cortex_mikado_load_search_template() {
		global $cortex_mikado_IconCollections;

		$search_type = cortex_mikado_options()->getOptionValue('search_type');

		$search_icon = '';
		$search_icon_close = '';
		if ( cortex_mikado_options()->getOptionValue('search_icon_pack') !== '' ) {
			$search_icon = $cortex_mikado_IconCollections->getSearchIcon( cortex_mikado_options()->getOptionValue('search_icon_pack'), true );
			$search_icon_close = $cortex_mikado_IconCollections->getSearchClose( cortex_mikado_options()->getOptionValue('search_icon_pack'), true );
		}

		$parameters = array(
			'search_in_grid'		=> cortex_mikado_options()->getOptionValue('search_in_grid') == 'yes' ? true : false,
			'search_icon'			=> $search_icon,
			'search_icon_close'		=> $search_icon_close
		);

		cortex_mikado_get_module_template_part( 'templates/types/'.$search_type, 'search', '', $parameters );

	}

}