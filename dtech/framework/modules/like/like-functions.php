<?php

if ( ! function_exists('cortex_mikado_like') ) {
	/**
	 * Returns CortexMikadoLike instance
	 *
	 * @return CortexMikadoLike
	 */
	function cortex_mikado_like() {
		return CortexMikadoLike::get_instance();
	}

}

function cortex_mikado_get_like() {

	echo wp_kses(cortex_mikado_like()->add_like(), array(
		'span' => array(
			'class' => true,
			'aria-hidden' => true,
			'style' => true,
			'id' => true
		),
		'i' => array(
			'class' => true,
			'style' => true,
			'id' => true
		),
		'a' => array(
			'href' => true,
			'class' => true,
			'id' => true,
			'title' => true,
			'style' => true
		)
	));
}

if ( ! function_exists('cortex_mikado_like_blog_single') ) {
	/**
	 * Add like to blog single
	 *
	 * @return string
	 */
	function cortex_mikado_like_blog_single() {
		return cortex_mikado_like()->add_blog_single_like();
	}

}

if ( ! function_exists('cortex_mikado_like_latest_posts') ) {
	/**
	 * Add like to latest post
	 *
	 * @return string
	 */
	function cortex_mikado_like_latest_posts() {
		return cortex_mikado_like()->add_like();
	}

}

if ( ! function_exists('cortex_mikado_like_portfolio_list') ) {
	/**
	 * Add like to portfolio project
	 *
	 * @param $portfolio_project_id
	 * @return string
	 */
	function cortex_mikado_like_portfolio_list($portfolio_project_id) {
		return cortex_mikado_like()->add_like_portfolio_list($portfolio_project_id);
	}

}