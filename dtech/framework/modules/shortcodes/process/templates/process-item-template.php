<div <?php cortex_mikado_class_attribute($item_classes); ?>>
	<div class="mkdf-pi-holder-inner">
		<?php if(!empty($number)) : ?>
			<div class="mkdf-pi-number-holder">
				<span class="mkdf-pi-arrow"><span class="arrow_right"></span></span>
				<span class="mkdf-pi-number"><?php echo esc_html($number); ?></span>
			</div>
		<?php endif; ?>
		<?php if(!empty($title) || !empty($text)) : ?>
			<div class="mkdf-pi-content-holder">
				<?php if(!empty($title)) : ?>
					<div class="mkdf-pi-title-holder">
						<h5 class="mkdf-pi-title"><?php echo esc_html($title); ?></h5>
					</div>
				<?php endif; ?>

				<?php if(!empty($text)) : ?>
					<div class="mkdf-pi-text-holder">
						<p><?php echo esc_html($text); ?></p>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>