<div <?php cortex_mikado_class_attribute($holder_classes); ?>>
	<div class="mkdf-process-inner clearfix">
		<?php echo do_shortcode($content); ?>
	</div>
</div>