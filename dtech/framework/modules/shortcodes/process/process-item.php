<?php
namespace CortexMikado\Modules\Shortcodes\Process;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProcessItem implements ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'mkdf_process_item';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'                    => esc_html__('Process Item','cortex'),
			'base'                    => $this->getBase(),
			'as_child'                => array('only' => 'mkdf_process_holder'),
			'category'                => esc_html__('by MIKADO', 'cortex'),
			'icon'                    => 'icon-wpb-process-item extended-custom-icon',
			'show_settings_on_create' => true,
			'params'                  => array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__('Number', 'cortex'),
					'param_name'  => 'number',
					'admin_label' => true
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__('Title', 'cortex'),
					'param_name'  => 'title',
					'admin_label' => true
				),
				array(
					'type'        => 'textarea',
					'heading'     => esc_html__('Text', 'cortex'),
					'param_name'  => 'text',
					'admin_label' => true
				),
			)
		));
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'number'     => '',
			'title'     => '',
			'text'      => '',
		);

		$params = shortcode_atts($default_atts, $atts);

		$params['item_classes'] = array(
			'mkdf-process-item-holder'
		);

		return cortex_mikado_get_shortcode_module_template_part('templates/process-item-template', 'process', '', $params);
	}

}