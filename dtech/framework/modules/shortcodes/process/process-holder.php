<?php
namespace CortexMikado\Modules\Shortcodes\Process;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProcessHolder implements ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'mkdf_process_holder';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'                    => esc_html__('Process','cortex'),
			'base'                    => $this->getBase(),
			'as_parent'               => array('only' => 'mkdf_process_item'),
			'content_element'         => true,
			'category'                => esc_html__('by MIKADO', 'cortex'),
			'icon'                    => 'icon-wpb-process-holder extended-custom-icon',
			'js_view'                 => 'VcColumnView',
			'params'                  => array(
				array(
					'type'        => 'dropdown',
					'param_name'  => 'number_of_items',
					'heading'     => esc_html__('Number of Process Items', 'cortex'),
					'value'       => array(
						esc_html__('Three', 'cortex') => 'three',
						esc_html__('Four', 'cortex')  => 'four',
						esc_html__('Five', 'cortex')  => 'five'
					),
					'admin_label' => true,
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'number_of_items' => 'three'
		);

		$params            = shortcode_atts($default_atts, $atts);
		$params['content'] = $content;

		$params['holder_classes'] = array(
			'mkdf-process-holder',
			'mkdf-process-holder-items-'.$params['number_of_items']
		);

		return cortex_mikado_get_shortcode_module_template_part('templates/process-holder-template', 'process', '', $params);
	}
}