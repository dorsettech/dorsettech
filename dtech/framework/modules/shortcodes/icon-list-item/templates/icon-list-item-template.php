<?php
$icon_html = cortex_mikado_icon_collections()->renderIcon($icon, $icon_pack, $params);
?>
<div class="mkdf-icon-list-item">
	<div class="mkdf-icon-list-icon-holder">
        <div class="mkdf-icon-list-icon-holder-inner clearfix">
			<?php 
			print $icon_html;
			?>
		</div>
	</div>
	<p class="mkdf-icon-list-text" <?php echo cortex_mikado_get_inline_style($title_style)?> > <?php echo esc_attr($title)?></p>
</div>