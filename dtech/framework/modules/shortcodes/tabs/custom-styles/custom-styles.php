<?php
if(!function_exists('cortex_mikado_tabs_typography_styles')){
	function cortex_mikado_tabs_typography_styles(){
		$selector = '.mkdf-tabs .mkdf-tabs-nav li a';
		$tabs_tipography_array = array();
		$font_family = cortex_mikado_options()->getOptionValue('tabs_font_family');
		
		if(cortex_mikado_is_font_option_valid($font_family)){
			$tabs_tipography_array['font-family'] = cortex_mikado_get_font_option_val($font_family);
		}
		
		$text_transform = cortex_mikado_options()->getOptionValue('tabs_text_transform');
        if(!empty($text_transform)) {
            $tabs_tipography_array['text-transform'] = $text_transform;
        }

        $font_style = cortex_mikado_options()->getOptionValue('tabs_font_style');
        if(!empty($font_style)) {
            $tabs_tipography_array['font-style'] = $font_style;
        }

        $letter_spacing = cortex_mikado_options()->getOptionValue('tabs_letter_spacing');
        if($letter_spacing !== '') {
            $tabs_tipography_array['letter-spacing'] = cortex_mikado_filter_px($letter_spacing).'px';
        }

        $font_weight = cortex_mikado_options()->getOptionValue('tabs_font_weight');
        if(!empty($font_weight)) {
            $tabs_tipography_array['font-weight'] = $font_weight;
        }

        echo cortex_mikado_dynamic_css($selector, $tabs_tipography_array);
	}
	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_tabs_typography_styles');
}

if(!function_exists('cortex_mikado_tabs_inital_color_styles')){

	function cortex_mikado_tabs_inital_color_styles(){
		$selector = '.mkdf-tabs:not(.mkdf-tab-boxed) .mkdf-tabs-nav li a';
		$border_selector = '.mkdf-tabs.mkdf-horizontal-tab .mkdf-tab-container,.mkdf-tabs.mkdf-vertical-tab .mkdf-tabs-nav li a,.mkdf-tabs.mkdf-horizontal-tab .mkdf-tabs-nav li a';

		$styles = array();
		
		if(cortex_mikado_options()->getOptionValue('tabs_color')) {
            $styles['color'] = cortex_mikado_options()->getOptionValue('tabs_color');
        }

		if(cortex_mikado_options()->getOptionValue('tabs_border_color')) {
            echo cortex_mikado_dynamic_css($border_selector,array(
            	'border-color' => cortex_mikado_options()->getOptionValue('tabs_border_color')
            	)
            );
        }
		
		echo cortex_mikado_dynamic_css($selector, $styles);
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_tabs_inital_color_styles');
}

if(!function_exists('cortex_mikado_tabs_active_color_styles')){

	function cortex_mikado_tabs_active_color_styles(){
		$selector = array(
			'.mkdf-tabs:not(.mkdf-tab-boxed) .mkdf-tabs-nav li.ui-state-active a',
			'.mkdf-tabs:not(.mkdf-tab-boxed) .mkdf-tabs-nav li.ui-state-hover a'
		);
		$border_selector = array(
			'.mkdf-tabs .mkdf-tabs-nav li.ui-state-active:after',
			'.mkdf-tabs .mkdf-tabs-nav li.ui-state-hover:after'
		);

		$styles = array();
		
		if(cortex_mikado_options()->getOptionValue('tabs_color_active')) {
            $styles['color'] = cortex_mikado_options()->getOptionValue('tabs_color_active');
        }

		if(cortex_mikado_options()->getOptionValue('tabs_border_color_active')) {
            echo cortex_mikado_dynamic_css($border_selector,array(
            	'background-color' => cortex_mikado_options()->getOptionValue('tabs_border_color_active')
            	)
            );
        }
		
		echo cortex_mikado_dynamic_css($selector, $styles);
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_tabs_active_color_styles');
}

if(!function_exists('cortex_mikado_boxed_tabs_inital_color_styles')){

	function cortex_mikado_boxed_tabs_inital_color_styles(){
		$selector = '.mkdf-tabs.mkdf-tab-boxed .mkdf-tabs-nav li a';
		$styles = array();
		
		if(cortex_mikado_options()->getOptionValue('boxed_tabs_color')) {
            $styles['color'] = cortex_mikado_options()->getOptionValue('boxed_tabs_color');
        }

		if(cortex_mikado_options()->getOptionValue('boxed_tabs_back_color')) {
            $styles['background-color'] = cortex_mikado_options()->getOptionValue('boxed_tabs_back_color');
        }
		
		echo cortex_mikado_dynamic_css($selector, $styles);
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_boxed_tabs_inital_color_styles');
}
if(!function_exists('cortex_mikado_tabs_boxed_active_color_styles')){

	function cortex_mikado_tabs_boxed_active_color_styles(){
		$selector = array(
			'.mkdf-tabs.mkdf-tab-boxed .mkdf-tabs-nav li.ui-state-active a',
			'.mkdf-tabs.mkdf-tab-boxed .mkdf-tabs-nav li.ui-state-hover a'
		);

		$styles = array();
		
		if(cortex_mikado_options()->getOptionValue('boxed_tabs_color_active')) {
            $styles['color'] = cortex_mikado_options()->getOptionValue('boxed_tabs_color_active');
        }

		if(cortex_mikado_options()->getOptionValue('boxed_tabs_back_color_active')) {
            $styles['background-color'] = cortex_mikado_options()->getOptionValue('boxed_tabs_back_color_active');
        }
		
		echo cortex_mikado_dynamic_css($selector, $styles);
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_tabs_boxed_active_color_styles');
}