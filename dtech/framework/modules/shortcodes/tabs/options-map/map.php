<?php

if(!function_exists('cortex_mikado_tabs_map')) {
    function cortex_mikado_tabs_map() {
		
        $panel = cortex_mikado_add_admin_panel(array(
            'title' => esc_html__('Tabs', 'cortex'),
            'name'  => 'panel_tabs',
            'page'  => '_elements_page'
        ));

        //Typography options
        cortex_mikado_add_admin_section_title(array(
            'name' => 'typography_section_title',
            'title' => esc_html__('Tabs Navigation Typography', 'cortex'),
            'parent' => $panel
        ));

        $typography_group = cortex_mikado_add_admin_group(array(
            'name' => 'typography_group',
            'title' => esc_html__('Tabs Navigation Typography', 'cortex'),
			'description' => esc_html__('Setup typography for tabs navigation', 'cortex'),
            'parent' => $panel
        ));

        $typography_row = cortex_mikado_add_admin_row(array(
            'name' => 'typography_row',
            'next' => true,
            'parent' => $typography_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'fontsimple',
            'name'          => 'tabs_font_family',
            'default_value' => '',
            'label'         => esc_html__('Font Family', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'tabs_text_transform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'cortex'),
            'options'       => cortex_mikado_get_text_transform_array()
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'tabs_font_style',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'cortex'),
            'options'       => cortex_mikado_get_font_style_array()
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'textsimple',
            'name'          => 'tabs_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'cortex'),
            'args'          => array(
                'suffix' => 'px'
            )
        ));

        $typography_row2 = cortex_mikado_add_admin_row(array(
            'name' => 'typography_row2',
            'next' => true,
            'parent' => $typography_group
        ));		
		
        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row2,
            'type'          => 'selectsimple',
            'name'          => 'tabs_font_weight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'cortex'),
            'options'       => cortex_mikado_get_font_weight_array()
        ));
		
		//Initial Tab Color Styles
		
		cortex_mikado_add_admin_section_title(array(
            'name' => 'tab_color_section_title',
            'title' => esc_html__('Tab Navigation Color Styles', 'cortex'),
            'parent' => $panel
        ));
		$tabs_color_group = cortex_mikado_add_admin_group(array(
            'name' => 'tabs_color_group',
            'title' => esc_html__('Navigation Color Styles', 'cortex'),
			'description' => esc_html__('Set color styles for tab navigation', 'cortex'),
            'parent' => $panel
        ));
		$tabs_color_row = cortex_mikado_add_admin_row(array(
            'name' => 'tabs_color_row',
            'next' => true,
            'parent' => $tabs_color_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'tabs_color',
            'default_value' => '',
            'label'         => esc_html__('Color', 'cortex'),
        ));

		cortex_mikado_add_admin_field(array(
            'parent'        => $tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'tabs_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color', 'cortex'),
        ));
		
		//Active Tab Color Styles
		
		$active_tabs_color_group = cortex_mikado_add_admin_group(array(
            'name' => 'active_tabs_color_group',
            'title' => esc_html__('Active and Hover Navigation Color Styles', 'cortex'),
			'description' => esc_html__('Set color styles for active and hover tabs', 'cortex'),
            'parent' => $panel
        ));
		$active_tabs_color_row = cortex_mikado_add_admin_row(array(
            'name' => 'active_tabs_color_row',
            'next' => true,
            'parent' => $active_tabs_color_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $active_tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'tabs_color_active',
            'default_value' => '',
            'label'         => esc_html__('Color', 'cortex'),
        ));

		cortex_mikado_add_admin_field(array(
            'parent'        => $active_tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'tabs_border_color_active',
            'default_value' => '',
            'label'         => esc_html__('Border Color', 'cortex'),
        ));


        //Initial Boxed Tab Color Styles
		
		cortex_mikado_add_admin_section_title(array(
            'name' => 'boxed_tab_color_section_title',
            'title' => esc_html__('Boxed Tab Navigation Color Styles', 'cortex'),
            'parent' => $panel
        ));
		$boxed_tabs_color_group = cortex_mikado_add_admin_group(array(
            'name' => 'boxed_tabs_color_group',
            'title' => esc_html__('Navigation Color Styles', 'cortex'),
			'description' => esc_html__('Set color styles for tab navigation', 'cortex'),
            'parent' => $panel
        ));
		$boxed_tabs_color_row = cortex_mikado_add_admin_row(array(
            'name' => 'boxed_tabs_color_row',
            'next' => true,
            'parent' => $boxed_tabs_color_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $boxed_tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'boxed_tabs_color',
            'default_value' => '',
            'label'         => esc_html__('Color', 'cortex'),
        ));
		cortex_mikado_add_admin_field(array(
            'parent'        => $boxed_tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'boxed_tabs_back_color',
            'default_value' => '',
            'label'         => esc_html__('Background Color', 'cortex'),
        ));
		
		//Active Boxed Tab Color Styles
		
		$active_boxed_tabs_color_group = cortex_mikado_add_admin_group(array(
            'name' => 'active_boxed_tabs_color_group',
            'title' => esc_html__('Active and Hover Navigation Color Styles', 'cortex'),
			'description' => esc_html__('Set color styles for active and hover tabs', 'cortex'),
            'parent' => $panel
        ));

		$active_boxed_tabs_color_row = cortex_mikado_add_admin_row(array(
            'name' => 'active_boxed_tabs_color_row',
            'next' => true,
            'parent' => $active_boxed_tabs_color_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $active_boxed_tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'boxed_tabs_color_active',
            'default_value' => '',
            'label'         => esc_html__('Color', 'cortex'),
        ));

		cortex_mikado_add_admin_field(array(
            'parent'        => $active_boxed_tabs_color_row,
            'type'          => 'colorsimple',
            'name'          => 'boxed_tabs_back_color_active',
            'default_value' => '',
            'label'         => esc_html__('Background Color', 'cortex'),
        ));
    }

    add_action('cortex_mikado_options_elements_map', 'cortex_mikado_tabs_map');
}