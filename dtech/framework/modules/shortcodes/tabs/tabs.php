<?php
namespace CortexMikado\Modules\Shortcodes\Tabs;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Tabs
 */

class Tabs implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;
	function __construct() {
		$this->base = 'mkdf_tabs';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Tabs', 'cortex'),
			'base' => $this->getBase(),
			'as_parent' => array('only' => 'mkdf_tab'),
			'content_element' => true,
			'show_settings_on_create' => true,
			'category' => esc_html__('by MIKADO', 'cortex'),
			'icon' => 'icon-wpb-tabs extended-custom-icon',
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'dropdown',
					'admin-label' => true,
					'heading' => esc_html__('Style','cortex'),
					'param_name' => 'style',
					'value' => array(
						esc_html__('Horizontal', 'cortex') => 'horizontal_tab',
						esc_html__('Horizontal Boxed', 'cortex') => 'horizontal_tab_boxed',
						esc_html__('Vertical', 'cortex') => 'vertical_tab',
						esc_html__('Vertical Boxed', 'cortex') => 'vertical_tab_boxed'
					),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'admin-label' => true,
					'heading' => esc_html__('Title Layout', 'cortex'),
					'param_name' => 'title_layout',
					'value' => array(
						esc_html__('Without Icon', 'cortex') => 'without_icon',
						esc_html__('With Icon', 'cortex') => 'with_icon',
						esc_html__('Only Icon', 'cortex') => 'only_icon'
					),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Color Style','cortex'),
					'param_name' => 'color_style',
					'value' => array(
						esc_html__('Default','cortex') => '',
						esc_html__('Grey','cortex') => 'grey',
						esc_html__('White','cortex') => 'white'
					),
					'description' => '',
					'dependency' => array('element' => 'style', 'value' => array('horizontal_tab_boxed','vertical_tab_boxed'))
				)
			)
		));

	}

	public function render($atts, $content = null) {
		$args = array(
			'style' => 'horizontal_tab',
			'title_layout' => 'without_icon',
			'color_style' => ''
		);
		
		$args = array_merge($args, cortex_mikado_icon_collections()->getShortcodeParams());
        $params  = shortcode_atts($args, $atts);
		
		extract($params);
		
		// Extract tab titles
		preg_match_all('/tab_title="([^\"]+)"/i', $content, $matches, PREG_OFFSET_CAPTURE);
		$tab_titles = array();

		/**
		 * get tab titles array
		 *
		 */
		if (isset($matches[0])) {
			$tab_titles = $matches[0];
		}
		
		$tab_title_array = array();
		
		foreach($tab_titles as $tab) {
			preg_match('/tab_title="([^\"]+)"/i', $tab[0], $tab_matches, PREG_OFFSET_CAPTURE);
			$tab_title_array[] = $tab_matches[1][0];
		}
		
		$params['tabs_titles'] = $tab_title_array;
		$params['tab_class'] = $this->getTabClass($params);
		$params['content'] = $content;

		
		$output = '';
		
		$output .= cortex_mikado_get_shortcode_module_template_part('templates/tab-template','tabs', '', $params);
		
		return $output;
		}
		
		/**
	   * Generates tabs class
	   *
	   * @param $params
	   *
	   * @return array
	   */
	private function getTabClass($params){
		$tab_style = $params['style'];
		$tab_title_layout = $params['title_layout'];
		$tab_classes = array();

		$tab_classes[] = 'mkdf-tabs';
		$tab_classes[] = 'clearfix';

		switch ($tab_style) {
			case 'vertical_tab':
				$tab_classes[] = 'mkdf-vertical-tab';
				break;
			case 'vertical_tab_boxed':
				$tab_classes[] = 'mkdf-vertical-tab';
				$tab_classes[] = 'mkdf-tab-boxed';
				break;
			case 'horizontal_tab_boxed':
				$tab_classes[] = 'mkdf-horizontal-tab';
				$tab_classes[] = 'mkdf-tab-boxed';
				break;
			default :
				$tab_classes[] = 'mkdf-horizontal-tab';
				break;
		}

		switch ($tab_title_layout) {
			case 'with_icon':
				$tab_classes[] = 'mkdf-tab-with-icon';
				break;
			case 'only_icon':
				$tab_classes[] = 'mkdf-tab-only-icon';
				break;
			default :
				$tab_classes[] = 'mkdf-tab-without-icon';
				break;
		}

		if ($params['color_style'] !== ''){
			$tab_classes[] = ' mkdf-style-'.$params['color_style'];
		}

		return $tab_classes;
	}

}