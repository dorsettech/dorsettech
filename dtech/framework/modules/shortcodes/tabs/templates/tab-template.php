<div <?php cortex_mikado_class_attribute($tab_class)?>>
	<ul class="mkdf-tabs-nav">
		<?php foreach ($tabs_titles as $tab_title) { ?>
			<li>
				<a href="#tab-<?php echo sanitize_title($tab_title)?>">
					<?php if(in_array('mkdf-vertical-tab', $tab_class) && (in_array('mkdf-tab-with-icon', $tab_class) || in_array('mkdf-tab-only-icon', $tab_class))) { ?>
						<span class="mkdf-icon-frame"></span>
					<?php } ?>

					<?php if($tab_title !== '' && !in_array('mkdf-tab-only-icon', $tab_class)) { ?>
						<span class="mkdf-tab-text-after-icon">
							<?php echo esc_attr($tab_title)?>
						</span>
					<?php } ?>
						
					<?php if(!in_array('mkdf-vertical-tab', $tab_class) && (in_array('mkdf-tab-with-icon', $tab_class) || in_array('mkdf-tab-only-icon', $tab_class))) { ?>
						<span class="mkdf-icon-frame"></span>
					<?php } ?>
				</a>
			 </li>
		<?php } ?>
	</ul> 
	<?php echo do_shortcode($content); ?>
</div>