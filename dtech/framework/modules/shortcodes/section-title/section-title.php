<?php
namespace CortexMikado\Modules\Shortcodes\SectionTitle;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class SectionTitle
 */
class SectionTitle implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'mkdf_section_title';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 */
	public function vcMap() {

		vc_map( array(
				'name' => esc_html__('Section Title', 'cortex'),
				'base' => $this->getBase(),
				'category' => esc_html__('by MIKADO','cortex'),
				'icon' => 'icon-wpb-section-title extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array(
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__('Title Text','cortex'),
						'param_name'	=> 'title_text',
						'value'			=> '',
						'admin_label'	=> true
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__('Highlighted Title Text','cortex'),
						'param_name'	=> 'highlighted_text',
						'value'			=> '',
						'admin_label'	=> true,
						'description'   =>esc_html__('Highlighted title text will be appended to title text','cortex')
					),
					array(
						'type'			=> 'dropdown',
						'heading'		=> esc_html__('Text Align','cortex'),
						'param_name'	=> 'text_align',
						'value'			=> array(
							''			=> '',
							esc_html__('Center','cortex')	=> 'center',
                            esc_html__('Left','cortex')	=> 'left',
							esc_html__('Right','cortex')	=> 'right',
							esc_html__('Justify','cortex')	=> 'justify'
						)
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Text Color','cortex'),
						'param_name'  => 'text_color',
						'dependency'  => array('element' => 'title_text', 'not_empty' => true)
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Text Size (px)', 'cortex'),
						'param_name' => 'text_size',
						'dependency' => array('element' => 'title_text', 'not_empty' => true),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Highlighted Color','cortex'),
						'param_name'  => 'highlighted_color',
						'dependency'  => array('element' => 'highlighted_text', 'not_empty' => true)
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Highlighted Effect','cortex'),
						'param_name'  => 'highlighted_effect',
						'value'			=> array(
							esc_html__('None','cortex')	 => '',
							esc_html__('Blink','cortex')	 => 'blink',
							esc_html__('Type Out','cortex') => 'type-out',
						),
						'dependency'  => array('element' => 'highlighted_text', 'not_empty' => true)
					)
				)
		) );
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'title_text' => '',
			'highlighted_text' => '',
			'text_align' => 'center',
			'text_color' => '',
			'text_size' => '',
			'highlighted_color'	=> '',
			'highlighted_effect' => ''
		);

		$params = shortcode_atts($args, $atts);

		$params['classes'] = $this->getTitleSectionClass($params);
		$params['title_style'] = $this->getTitleStyle($params);
		$params['highlighted_style'] = $this->getTitleHighlightedStyle($params);

		//Get HTML from template
		$html = cortex_mikado_get_shortcode_module_template_part('templates/section-title-template', 'section-title', '', $params);

		return $html;

	}

    /**
     * Generates classes for title section
     *
     * @param $params
     *
     * @return array
     */
	private function getTitleSectionClass($params){
		$classes = array();
		$classes[] = 'mkdf-section-title';

		if ($params['text_align'] != ''){
			$classes[] = 'mkdf-section-align-'.$params['text_align'];
		}

		if ($params['highlighted_effect'] != ''){
			$classes[] = 'mkdf-section-'.$params['highlighted_effect'];
		}

		return $classes;
	}

    /**
     * Generates style for title text
     *
     * @param $params
     *
     * @return string
     */
	private function getTitleStyle($params){
		$title_style = array();

		if ($params['text_color'] != ''){
			$title_style[] = 'color: '.$params['text_color'];
		}

		if ($params['text_size'] != ''){
			$title_style[] = 'font-size: '.cortex_mikado_filter_px($params['text_size']).'px';
		}

		return implode(';', $title_style);
	}

    /**
     * Generates style for title highlighted text
     *
     * @param $params
     *
     * @return string
     */
	private function getTitleHighlightedStyle($params){
		$highlighted_style = array();

		if ($params['highlighted_color'] != ''){
			$highlighted_style[] = 'color: '.$params['highlighted_color'];
		}

		return implode(';', $highlighted_style);
	}
}