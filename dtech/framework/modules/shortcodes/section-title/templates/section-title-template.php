<?php
/**
 * Section Title shortcode template
 */
?>

<div <?php cortex_mikado_class_attribute($classes); ?> <?php cortex_mikado_inline_style($title_style)?>>
	<?php echo esc_html($title_text);?><?php if ($highlighted_text !== ''){ ?><span class="mkdf-section-highlighted" <?php cortex_mikado_inline_style($highlighted_style)?>><?php echo esc_html($highlighted_text);?></span><?php } ?>
</div>