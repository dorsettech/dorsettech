<?php
/**
 * Blockquote shortcode template
 */
?>

<blockquote class="mkdf-blockquote-shortcode" <?php cortex_mikado_inline_style($blockquote_style); ?> >
	<span class="mkdf-icon-quotations-holder">
		<?php echo cortex_mikado_icon_collections()->getQuoteIcon("font_elegant", true); ?>
	</span>
	<p class="mkdf-blockquote-text">
		<?php echo esc_attr($text); ?>
	</p>
</blockquote>