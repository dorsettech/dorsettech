<?php
/**
 * Counter shortcode template
 */
?>
<div class="mkdf-counter-holder <?php echo esc_attr($position); ?>" <?php echo cortex_mikado_get_inline_style($counter_holder_styles); ?>>
	<?php
	if (is_array($icon_parameters) && count($icon_parameters)) { ?>
		<span class="mkdf-counter-icon">
			<?php echo cortex_mikado_execute_shortcode('mkdf_icon', $icon_parameters); ?>
		</span>
	<?php }	?>
	<span class="mkdf-counter <?php echo esc_attr($type) ?>" <?php echo cortex_mikado_get_inline_style($counter_styles); ?>>
		<?php echo esc_attr($digit); ?>
	</span>
	<?php if($symbol != '') { ?>
		<span class="mkdf-counter-symbol" <?php echo cortex_mikado_get_inline_style($counter_styles); ?>>
			<?php echo esc_attr($symbol); ?>
		</span>
	<?php } ?>
	<?php if($title != '') { ?>
		<<?php echo esc_html($title_tag); ?> class="mkdf-counter-title" <?php echo cortex_mikado_get_inline_style($title_styles); ?>>
			<?php echo esc_attr($title); ?>
		</<?php echo esc_html($title_tag);; ?>>
	<?php } ?>
	<?php if ($text != "") { ?>
		<p class="mkdf-counter-text" <?php echo cortex_mikado_get_inline_style($text_styles); ?>><?php echo esc_html($text); ?></p>
	<?php } ?>

</div>