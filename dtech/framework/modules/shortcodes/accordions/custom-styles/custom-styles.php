<?php 

if(!function_exists('cortex_mikado_accordions_typography_styles')){
	function cortex_mikado_accordions_typography_styles(){
		$selector = '.mkdf-accordion-holder .mkdf-title-holder';
		$styles = array();
		
		$font_family = cortex_mikado_options()->getOptionValue('accordions_font_family');
		if(cortex_mikado_is_font_option_valid($font_family)){
			$styles['font-family'] = cortex_mikado_get_font_option_val($font_family);
		}
		
		$text_transform = cortex_mikado_options()->getOptionValue('accordions_text_transform');
       if(!empty($text_transform)) {
           $styles['text-transform'] = $text_transform;
       }

       $font_style = cortex_mikado_options()->getOptionValue('accordions_font_style');
       if(!empty($font_style)) {
           $styles['font-style'] = $font_style;
       }

       $letter_spacing = cortex_mikado_options()->getOptionValue('accordions_letter_spacing');
       if($letter_spacing !== '') {
           $styles['letter-spacing'] = cortex_mikado_filter_px($letter_spacing).'px';
       }

       $font_weight = cortex_mikado_options()->getOptionValue('accordions_font_weight');
       if(!empty($font_weight)) {
           $styles['font-weight'] = $font_weight;
       }

       echo cortex_mikado_dynamic_css($selector, $styles);
	}
	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_accordions_typography_styles');
}

if(!function_exists('cortex_mikado_accordions_initial_color_styles')){

	function cortex_mikado_accordions_initial_color_styles(){

		$selector = '.mkdf-accordion-holder.mkdf-initial .mkdf-title-holder';
		$styles = array();
		
		if(cortex_mikado_options()->getOptionValue('accordions_title_color')) {
			$styles['color'] = cortex_mikado_options()->getOptionValue('accordions_title_color');
		}
		
		if(cortex_mikado_options()->getOptionValue('accordions_back_color')) {
			$styles['background-color'] = cortex_mikado_options()->getOptionValue('accordions_back_color');
		}

		echo cortex_mikado_dynamic_css($selector, $styles);
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_accordions_initial_color_styles');
}

if(!function_exists('cortex_mikado_accordions_active_color_styles')){
	
	function cortex_mikado_accordions_active_color_styles(){
		$selector = array(
			'.mkdf-accordion-holder.mkdf-initial .mkdf-title-holder.ui-state-active',
			'.mkdf-accordion-holder.mkdf-initial .mkdf-title-holder.ui-state-hover'
		);
		$styles = array();
		
		if(cortex_mikado_options()->getOptionValue('accordions_title_color_active')) {
			$styles['color'] = cortex_mikado_options()->getOptionValue('accordions_title_color_active');
		}
		
		if(cortex_mikado_options()->getOptionValue('accordions_back_color_active')) {
			$styles['background-color'] = cortex_mikado_options()->getOptionValue('accordions_back_color_active');
		}

		echo cortex_mikado_dynamic_css($selector, $styles);
	}

	add_action('cortex_mikado_style_dynamic', 'cortex_mikado_accordions_active_color_styles');
}