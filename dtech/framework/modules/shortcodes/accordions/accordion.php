<?php
namespace CortexMikado\Modules\Shortcodes\Accordion;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
	* class Accordions
*/
class Accordion implements ShortcodeInterface{
	/**
	 * @var string
	 */
	private $base;

	function __construct() {
		$this->base = 'mkdf_accordion';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return	$this->base;
	}

	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Accordion', 'cortex'),
			'base' => $this->base,
			'as_parent' => array('only' => 'mkdf_accordion_tab'),
			'content_element' => true,
			'category' => esc_html__('by MIKADO', 'cortex'),
			'icon' => 'icon-wpb-accordion extended-custom-icon',
			'show_settings_on_create' => true,
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'cortex' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'cortex' )
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Style', 'cortex'),
					'param_name' => 'style',
					'value' => array(
						esc_html__('Accordion', 'cortex') => 'accordion',
						esc_html__('Toggle', 'cortex') => 'toggle'
					),
					'save_always' => true
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Color Style','cortex'),
					'param_name' => 'color_style',
					'value' => array(
						esc_html__('Default','cortex') => '',
						esc_html__('Grey','cortex') => 'grey',
						esc_html__('White','cortex') => 'white'
					),
					'description' => ''
				)
			)
		) );
	}
	public function render($atts, $content = null) {
		$default_atts=(array(
			'title' => '',
			'style' => 'accordion',
			'color_style' => ''
		));
		$params = shortcode_atts($default_atts, $atts);
		extract($params);
		
 		$acc_class = $this->getAccordionClasses($params);
		$params['acc_class'] = $acc_class;
		$params['content'] = $content;
		
		$output = '';
		
		$output .= cortex_mikado_get_shortcode_module_template_part('templates/accordion-holder-template','accordions', '', $params);

		return $output;
	}

	/**
	   * Generates accordion classes
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getAccordionClasses($params){
		
		$acc_class = '';
		$style = $params['style'];
		switch($style) {
			case 'toggle':
				$acc_class .= 'mkdf-toggle mkdf-initial';
				break;
			default:
				$acc_class = 'mkdf-accordion mkdf-initial';
		}

		if ($params['color_style'] !== ''){
			$acc_class .= ' mkdf-style-'.$params['color_style'];
		}

		return $acc_class;
	}
}
