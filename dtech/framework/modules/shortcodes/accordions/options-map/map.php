<?php 
if(!function_exists('cortex_mikado_accordions_map')) {
    /**
     * Add Accordion options to elements panel
     */
   function cortex_mikado_accordions_map() {
		
       $panel = cortex_mikado_add_admin_panel(array(
           'title' => esc_html__('Accordions', 'cortex'),
           'name'  => 'panel_accordions',
           'page'  => '_elements_page'
       ));

       //Typography options
       cortex_mikado_add_admin_section_title(array(
           'name' => 'typography_section_title',
           'title' => esc_html__('Typography', 'cortex'),
           'parent' => $panel
       ));

       $typography_group = cortex_mikado_add_admin_group(array(
           'name' => 'typography_group',
           'title' => esc_html__('Typography', 'cortex'),
			'description' => esc_html__('Setup typography for accordions navigation', 'cortex'),
           'parent' => $panel
       ));

       $typography_row = cortex_mikado_add_admin_row(array(
           'name' => 'typography_row',
           'next' => true,
           'parent' => $typography_group
       ));

       cortex_mikado_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'fontsimple',
           'name'          => 'accordions_font_family',
           'default_value' => '',
           'label'         => esc_html__('Font Family', 'cortex'),
       ));

       cortex_mikado_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'selectsimple',
           'name'          => 'accordions_text_transform',
           'default_value' => '',
           'label'         => esc_html__('Text Transform', 'cortex'),
           'options'       => cortex_mikado_get_text_transform_array()
       ));

       cortex_mikado_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'selectsimple',
           'name'          => 'accordions_font_style',
           'default_value' => '',
           'label'         => esc_html__('Font Style', 'cortex'),
           'options'       => cortex_mikado_get_font_style_array()
       ));

       cortex_mikado_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'textsimple',
           'name'          => 'accordions_letter_spacing',
           'default_value' => '',
           'label'         => esc_html__('Letter Spacing', 'cortex'),
           'args'          => array(
               'suffix' => 'px'
           )
       ));

       $typography_row2 = cortex_mikado_add_admin_row(array(
           'name' => 'typography_row2',
           'next' => true,
           'parent' => $typography_group
       ));		
		
       cortex_mikado_add_admin_field(array(
           'parent'        => $typography_row2,
           'type'          => 'selectsimple',
           'name'          => 'accordions_font_weight',
           'default_value' => '',
           'label'         => esc_html__('Font Weight', 'cortex'),
           'options'       => cortex_mikado_get_font_weight_array()
       ));
		
		//Initial Accordion Color Styles
		
		cortex_mikado_add_admin_section_title(array(
           'name' => 'accordion_color_section_title',
           'title' => esc_html__('Basic Accordions Color Styles', 'cortex'),
           'parent' => $panel
       ));
		
		$accordions_color_group = cortex_mikado_add_admin_group(array(
           'name' => 'accordions_color_group',
           'title' => esc_html__('Accordion Color Styles', 'cortex'),
			'description' => esc_html__('Set color styles for accordion title', 'cortex'),
           'parent' => $panel
       ));
		$accordions_color_row = cortex_mikado_add_admin_row(array(
           'name' => 'accordions_color_row',
           'next' => true,
           'parent' => $accordions_color_group
       ));
		cortex_mikado_add_admin_field(array(
           'parent'        => $accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_title_color',
           'default_value' => '',
           'label'         => esc_html__('Title/Icon Color', 'cortex'),
       ));

		cortex_mikado_add_admin_field(array(
           'parent'        => $accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_back_color',
           'default_value' => '',
           'label'         => esc_html__('Background Color', 'cortex'),
       ));
		
		$active_accordions_color_group = cortex_mikado_add_admin_group(array(
           'name' => 'active_accordions_color_group',
           'title' => esc_html__('Active and Hover Accordion Color Styles', 'cortex'),
			'description' => esc_html__('Set color styles for active and hover accordions', 'cortex'),
           'parent' => $panel
       ));
		$active_accordions_color_row = cortex_mikado_add_admin_row(array(
           'name' => 'active_accordions_color_row',
           'next' => true,
           'parent' => $active_accordions_color_group
       ));
		cortex_mikado_add_admin_field(array(
           'parent'        => $active_accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_title_color_active',
           'default_value' => '',
           'label'         => esc_html__('Title/Icon Color', 'cortex'),
       ));

		cortex_mikado_add_admin_field(array(
           'parent'        => $active_accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_back_color_active',
           'default_value' => '',
           'label'         => esc_html__('Background Color', 'cortex'),
       ));
       
   }
   add_action('cortex_mikado_options_elements_map', 'cortex_mikado_accordions_map');
}