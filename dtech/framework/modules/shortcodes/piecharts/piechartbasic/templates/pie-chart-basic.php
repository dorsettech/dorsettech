<?php
/**
 * Pie Chart Basic Shortcode Template
 */
?>
<div class="mkdf-pie-chart-holder">
	<div class="mkdf-percentage" <?php echo cortex_mikado_get_inline_attrs($pie_chart_data); ?>>
		<?php if ($type_of_central_text == "title") { ?>
			<<?php echo esc_attr($title_tag); ?> class="mkdf-pie-title">
				<?php echo esc_html($title); ?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } else { ?>
			<span class="mkdf-to-counter">
				<?php echo esc_html($percent ); ?>
			</span>
			<span class="mkdf-percent-sign">%</span>
		<?php } ?>
	</div>
	<div class="mkdf-pie-chart-text" <?php cortex_mikado_inline_style($pie_chart_style); ?>>
		<?php if ($type_of_central_text == "title") { ?>
			<span class="mkdf-to-counter">
				<?php echo esc_html($percent ); ?>
			</span>
		<?php } else { ?>
			<<?php echo esc_attr($title_tag); ?> class="mkdf-pie-title">
				<?php echo esc_html($title); ?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } ?>
		<span><?php echo esc_html($text); ?></span>
	</div>
</div>