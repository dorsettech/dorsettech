<?php if($image != '') {  ?>
	<div class="mkdf-client-holder <?php echo esc_attr($class);?>">
		<div class="mkdf-client-holder-inner">
			<div class="mkdf-client-image-holder">
				<div class="mkdf-client-image-holder-inner">
				<?php if($link != '') { ?>
					<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($link_target); ?>">
				<?php } ?>
					<span class="mkdf-client-image">
						<img src="<?php echo esc_url($image); ?>" alt="<?php echo esc_url($image_alt); ?>" />
					</span>
					<?php if($hover_image != '') {  ?>
						<span class="mkdf-client-hover-image">
							<img src="<?php echo esc_url($hover_image); ?>" alt="<?php echo esc_url($hover_image_alt); ?>" />
						</span>
					<?php } ?>
				<?php if($link != '') { ?>
					</a>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>