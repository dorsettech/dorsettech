<?php
namespace CortexMikado\Modules\Shortcodes\Clients;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class Clients
 */

class Clients implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'mkdf_clients';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Clients', 'cortex'),
			'base' => $this->base,
			'as_parent' => array('only' => 'mkdf_client'),
			'content_element' => true,
			'category' => esc_html__('by MIKADO', 'cortex'),
			'icon' => 'icon-wpb-clients extended-custom-icon',
			'show_settings_on_create' => true,
			'params' => array(
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => esc_html__('Columns', 'cortex'),
					'param_name' => 'columns',
					'value' => array(
						esc_html__('Two', 'cortex') => 'two-columns',
						esc_html__('Three','cortex') => 'three-columns',
						esc_html__('Four', 'cortex') => 'four-columns',
						esc_html__('Five', 'cortex') => 'five-columns',
						esc_html__('Six', 'cortex') => 'six-columns'
					),
					'save_always' => true,
				)
			),
			'js_view' => 'VcColumnView'

		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'columns' 			=> ''
		);
		$params = shortcode_atts($args, $atts);
		extract($params);
		$params['content']= $content;
		$html						= '';

		$html = cortex_mikado_get_shortcode_module_template_part('templates/clients-template', 'clients', '', $params);

		return $html;

	}

}
