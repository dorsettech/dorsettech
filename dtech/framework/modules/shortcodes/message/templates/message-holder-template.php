<div class="mkdf-message  <?php echo esc_attr($message_classes)?>" <?php echo cortex_mikado_get_inline_style($message_styles); ?>>
	<div class="mkdf-message-inner">
		<?php		
		if($type == 'with_icon'){
			$icon_html = cortex_mikado_get_shortcode_module_template_part('templates/' . $type, 'message', '', $params);
			print $icon_html;
		}
		?>
		<a href="javascript:void(0)" class="mkdf-close" <?php cortex_mikado_inline_style($close_icon_holder_style); ?>><i class="mkdf-font-elegant-icon icon_close" <?php cortex_mikado_inline_style($close_icon_style); ?>></i></a>
		<div class="mkdf-message-text-holder">
			<div class="mkdf-message-text">
				<div class="mkdf-message-text-inner"><?php echo do_shortcode($content); ?></div>
			</div>
		</div>
	</div>
</div>