<?php
/**
 * Team info on hover shortcode template
 */
?>

<div class="mkdf-team <?php echo esc_attr( $team_type )?>">
	<div class="mkdf-team-inner">
		<?php if ( $team_image !== '' ) { ?>
		<div class="mkdf-team-image">
            <?php echo wp_get_attachment_image($team_image,'full');?>
			<div class="mkdf-team-info-holder">
				<div class="mkdf-team-info">
					<div class="mkdf-team-info-inner">
						<div class="mkdf-team-title-holder">
							<?php if ( $team_name !== '' ) { ?>
							<div class="mkdf-team-title-holder-inner">
								<<?php echo esc_attr($team_name_tag); ?> class="mkdf-team-name">
									<?php echo esc_attr( $team_name ); ?>
								</<?php echo esc_attr($team_name_tag); ?>>
							</div>
							<?php }
							if ( $team_position !== '' ) { ?>
							<div class="mkdf-team-position-holder">
								<p class="mkdf-team-position">
									<?php echo esc_attr( $team_position ); ?>
								</p>
							</div>
							<?php } ?>
						</div>
						<?php if(!empty($team_social_icons)) : ?>
							<div class="mkdf-team-social">
								<div class="mkdf-team-social-wrapp">
									<?php foreach( $team_social_icons as $team_social_icon ) {
										print $team_social_icon;
									} ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>