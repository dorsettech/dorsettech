<div class="mkdf-text-slider-item">
	<?php if ($item_title !== '') { ?>
		<<?php echo esc_attr($title_tag);?> class="mkdf-ts-item-title">
			<?php echo esc_html($item_title); ?>
		</<?php echo esc_attr($title_tag);?>>
	<?php } ?>
	<?php if ($show_separator == 'yes'){
		echo cortex_mikado_execute_shortcode('mkdf_separator', array('position' => 'left'));
	} ?>
	<?php if ($item_text !== '') { ?>
		<p class="mkdf-ts-item-text"> <?php echo esc_html($item_text); ?> </p>
	<?php } ?>
</div>