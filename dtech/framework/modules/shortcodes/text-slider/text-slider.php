<?php
namespace CortexMikado\Modules\Shortcodes\TextSlider;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class TextSlider
 */
class TextSlider implements ShortcodeInterface	{
	private $base; 
	
	function __construct() {
		$this->base = 'mkdf_text_slider';

		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	/**
		* Returns base for shortcode
		* @return string
	 */
	public function getBase() {
		return $this->base;
	}	
	public function vcMap() {
						
		vc_map( array(
			'name' => esc_html__('Text Slider', 'cortex'),
			'base' => $this->base,
			'category' => esc_html__('by MIKADO', 'cortex'),
			'icon' => 'icon-wpb-text-slider extended-custom-icon',
            'as_parent' => array('only' => 'mkdf_text_slider_item'),
            'js_view' => 'VcColumnView',
			'params' =>	array(
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Slider Alignment','cortex'),
					'param_name' => 'alignment',
					'value' => array(
						esc_html__('Left','cortex') => 'left',
						esc_html__('Center','cortex') => 'center',
						esc_html__('Right','cortex') => 'right',
					)
				),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__('Show Bullets', 'cortex'),
                    'param_name' => 'bullets',
                    'value' => array(
                    	esc_html__('Yes','cortex') => 'yes',
                    	esc_html__('No','cortex') => 'no',
                	)
                ),
            )
		) );

	}

	public function render($atts, $content = null) {
		
		$args = array(
			'alignment' => 'left',
            'bullets' => 'yes',
        );

		$params = shortcode_atts($args, $atts);

        extract($params);
        $data = $this->getDataParams($params);

        $html = '';

        $text_slider_class = $this->getClasses($params);

        $html .= '<div '. cortex_mikado_get_class_attribute($text_slider_class) .' '.esc_attr($data). '>';
            $html .= do_shortcode($content);
        $html .= '</div>';

        return $html;

	}


	private function getDataParams($params){
		$data_array = array();

		if ($params['bullets'] !== ''){
			$data_array[] = 'data-bullets='.$params['bullets'];
		}

		return implode(' ', $data_array);
	}

	private function getClasses($params){
		$classes = array();
		$classes[] = 'clearfix';
		$classes[] = 'mkdf-text-slider';

		if ($params['alignment'] !== ''){
			$classes[] = 'mkdf-text-slider-align-'.$params['alignment'];
		}

		return implode(' ', $classes);
	}
}