<?php
namespace CortexMikado\Modules\Shortcodes\OrderedList;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class OrderedList implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'mkdf_list_ordered';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('List - Ordered', 'cortex'),
			'base' => $this->base,
			'icon' => 'icon-wpb-ordered-list extended-custom-icon',
			'category' => esc_html__('by MIKADO', 'cortex'),
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type'			=> 'textarea_html',
					'admin_label'	=> true,
					'heading'		=> esc_html__('Content', 'cortex'),
					'param_name'	=> 'content',
					'value'			=> '<ol><li>Lorem Ipsum</li><li>Lorem Ipsum</li><li>Lorem Ipsum</li></ol>'
				)
			)
		) );
	}

	public function render($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
		$html = '<div class= "mkdf-ordered-list" >' . do_shortcode($content) . '</div>';
        return $html;
	}
}