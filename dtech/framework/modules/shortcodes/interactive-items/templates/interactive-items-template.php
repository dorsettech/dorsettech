<div <?php cortex_mikado_class_attribute($interactive_classes)?>>
	<div class="mkdf-int-item" <?php cortex_mikado_inline_style($title_area_style);?>>
		<div class="mkdf-int-item-table">
			<div class="mkdf-int-item-cell">
				<?php 
				if ($title_text !== '' || $highlighted_text !== '') {
					echo cortex_mikado_execute_shortcode('mkdf_section_title', $title_params);
				}
				?>
			</div>
		</div>
	</div>
	<?php echo do_shortcode($content);?>
</div>