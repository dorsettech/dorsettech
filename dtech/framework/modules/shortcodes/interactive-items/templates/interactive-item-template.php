<div class="mkdf-int-item">
	<?php if ($link != '') { ?>
		<a href="<?php echo esc_url($link) ?>" target="<?php echo esc_attr($target) ?>"></a>
	<?php } ?>
	<div class="mkdf-int-front-holder" <?php cortex_mikado_inline_style($interactive_item_holder_styles); ?>>
		<div class="mkdf-int-item-table">
			<div class="mkdf-int-item-cell">
				<?php if (is_array($icon_parameters) && count($icon_parameters)) {
					echo cortex_mikado_execute_shortcode('mkdf_icon', $icon_parameters);
				} ?>
				<?php if ($title !== '') { ?>
					<<?php echo esc_attr($title_tag);?> class="mkdf-int-title">
						<?php echo esc_attr($title); ?>
					</<?php echo esc_attr($title_tag);?>>
				<?php } ?>
				<?php if ($text !== '') { ?>
					<p class="mkdf-int-text"><?php echo esc_html($text); ?></p>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="mkdf-int-back-holder" <?php cortex_mikado_inline_style($interactive_back_styles);?>></div>
</div>