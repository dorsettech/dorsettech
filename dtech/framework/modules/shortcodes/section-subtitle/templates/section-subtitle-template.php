<?php
/**
 * Section Subtitle shortcode template
 */
?>

<span class="mkdf-section-subtitle" <?php cortex_mikado_inline_style($subtitle_style); ?>>
	<?php echo esc_html($subtitle_text);?>
</span>