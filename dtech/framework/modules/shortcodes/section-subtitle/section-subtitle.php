<?php
namespace CortexMikado\Modules\Shortcodes\SectionSubtitle;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class SectionSubtitle
 */
class SectionSubtitle implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'mkdf_section_subtitle';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 */
	public function vcMap() {

		vc_map( array(
				'name' => esc_html__('Section Subtitle', 'cortex'),
				'base' => $this->getBase(),
				'category' => esc_html__('by MIKADO','cortex'),
				'icon' => 'icon-wpb-section-subtitle extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array(
					array(
						'type'			=> 'textarea',
						'heading'		=> esc_html__('Subtitle Text','cortex'),
						'param_name'	=> 'subtitle_text',
						'value'			=> '',
						'admin_label'	=> true
					),
					array(
						'type'			=> 'dropdown',
						'heading'		=> esc_html__('Text Align','cortex'),
						'param_name'	=> 'text_align',
						'value'			=> array(
							''			=> '',
							esc_html__('Left','cortex')	=> 'left',
							esc_html__('Center','cortex')	=> 'center',
							esc_html__('Right','cortex')	=> 'right',
							esc_html__('Justify','cortex')	=> 'justify'
						)
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Text Color','cortex'),
						'param_name'  => 'text_color',
						'dependency'  => array('element' => 'subtitle_text', 'not_empty' => true)
					),
				)
		) );
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'subtitle_text' => '',
			'text_align'	=> '',
			'text_color'	=> ''
		);

		$params = shortcode_atts($args, $atts);

		$params['subtitle_style'] = array();
		if($params['text_align'] != '') {
			$params['subtitle_style'][] = 'text-align:' . $params['text_align'];
		}
		if($params['text_color'] != '') {
			$params['subtitle_style'][] = 'color:' . $params['text_color'];
		}
		//Get HTML from template
		$html = cortex_mikado_get_shortcode_module_template_part('templates/section-subtitle-template', 'section-subtitle', '', $params);

		return $html;

	}


}