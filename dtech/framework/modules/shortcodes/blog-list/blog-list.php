<?php

namespace CortexMikado\Modules\Shortcodes\BlogList;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class BlogList
 */
class BlogList implements ShortcodeInterface {
	/**
	* @var string
	*/
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_blog_list';
		
		add_action('vc_before_init', array($this,'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Blog List', 'cortex'),
			'base' => $this->base,
			'icon' => 'icon-wpb-blog-list extended-custom-icon',
			'category' => esc_html__('by MIKADO', 'cortex'),
			'allowed_container_element' => 'vc_row',
			'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Type', 'cortex'),
						'param_name' => 'type',
						'value' => array(
							esc_html__('Simple', 'cortex') => 'simple',
							esc_html__('Boxes', 'cortex') => 'boxes',
							esc_html__('Minimal', 'cortex') => 'minimal',
							esc_html__('Image in box', 'cortex') => 'image_in_box'
						),
						'admin_label' => true
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Number of Posts', 'cortex'),
						'param_name' => 'number_of_posts'
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Show Posts with Listed IDs','cortex'),
						'param_name' => 'selected_posts',
						'value' => '',
						'admin_label' => true,
						'description' => esc_html__('Delimit ID numbers by comma (leave empty for all)','cortex')
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Number of Columns', 'cortex'),
						'param_name' => 'number_of_columns',
						'value' => array(
							esc_html__('One', 'cortex')=> '1',
							esc_html__('Two', 'cortex') => '2',
							esc_html__('Three', 'cortex') => '3',
							esc_html__('Four', 'cortex') => '4'
						),
						'dependency' => Array('element' => 'type', 'value' => array('boxes','simple')),
                        'save_always' => true
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Order By', 'cortex'),
						'param_name' => 'order_by',
						'value' => array(
							esc_html__('Title', 'cortex') => 'title',
							esc_html__('Date', 'cortex') => 'date'
						),
						'save_always' => true,
						'description' => ''
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Order', 'cortex'),
						'param_name' => 'order',
						'value' => array(
							esc_html__('ASC', 'cortex') => 'ASC',
							esc_html__('DESC', 'cortex') => 'DESC'
						),
						'save_always' => true,
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Category Slug', 'cortex'),
						'param_name' => 'category',
						'admin_label' => true,
						'description' => esc_html__('Leave empty for all or use comma for list', 'cortex')
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('List Title', 'cortex'),
						'param_name' => 'list_title',
						'dependency' => Array('element' => 'type', 'value' => array('boxes')),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('List Highlighted Text', 'cortex'),
						'param_name' => 'list_highlighted',
						'description' => esc_html__('Highlighted text will be appended to title', 'cortex'),
						'dependency' => Array('element' => 'type', 'value' => array('boxes')),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Show Image', 'cortex'),
						'param_name' => 'show_image',
						'value' => array(
							esc_html__('No', 'cortex') => 'no',
							esc_html__('Yes', 'cortex') => 'yes'
						),
						'dependency' => Array('element' => 'type', 'value' => array('simple')),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Image Size', 'cortex'),
						'param_name' => 'image_size',
						'value' => array(
							esc_html__('Original', 'cortex') => 'original',
							esc_html__('Landscape', 'cortex') => 'landscape',
							esc_html__('Square', 'cortex') => 'square'
						),
						'dependency' => Array('element' => 'show_image', 'value' => array('yes')),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Text length', 'cortex'),
						'param_name' => 'text_length',
						'description' => esc_html__('Number of characters', 'cortex')
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Title Tag', 'cortex'),
						'param_name' => 'title_tag',
						'value' => array(
							''   => '',
							'h2' => 'h2',
							'h3' => 'h3',
							'h4' => 'h4',
							'h5' => 'h5',
							'h6' => 'h6',
						)
					)
				)
		) );

	}
	public function render($atts, $content = null) {
		
		$default_atts = array(
			'type' 					=> 'simple',
            'number_of_posts' 		=> '',
            'selected_posts'		=> '',
            'number_of_columns'		=> '',
            'show_image'			=> 'no',
            'image_size'			=> 'original',
            'order_by'				=> '',
            'order'					=> '',
            'category'				=> '',
            'title_tag'				=> 'h3',
			'text_length'			=> '90',
			'list_title'			=> '',
			'list_highlighted'		=> ''
        );
		
		$params = shortcode_atts($default_atts, $atts);
		extract($params);
		$params['holder_classes'] = $this->getBlogHolderClasses($params);
	
		$queryArray = $this->generateBlogQueryArray($params);
		$query_result = new \WP_Query($queryArray);
		$params['query_result'] = $query_result;	
     
		
        $thumbImageSize = $this->generateImageSize($params);
		$params['thumb_image_size'] = $thumbImageSize;

		$html ='';
        $html .= cortex_mikado_get_shortcode_module_template_part('templates/blog-list-holder', 'blog-list', '', $params);
		return $html;
		
	}

	/**
	   * Generates holder classes
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getBlogHolderClasses($params){
		$holderClasses = '';
		
		$columnNumber = $this->getColumnNumberClass($params);
		
		if(!empty($params['type'])){
			switch($params['type']){
				case 'image_in_box':
					$holderClasses = 'mkdf-image-in-box';
				break;
				case 'simple' : 
					$holderClasses = 'mkdf-simple';
				break;	
				case 'boxes' : 
					$holderClasses = 'mkdf-boxes';
				break;	
				case 'masonry' : 
					$holderClasses = 'mkdf-masonry';
				break;
				case 'minimal' :
					$holderClasses = 'mkdf-minimal';
				break;
				default: 
					$holderClasses = 'mkdf-simple';
			}
		}
		
		$holderClasses .= ' '.$columnNumber;
		
		return $holderClasses;
		
	}

	/** 
	   * Generates column classes for boxes type
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getColumnNumberClass($params){
		
		$columnsNumber = '';
		$type = $params['type'];
		$columns = $params['number_of_columns'];
		
        if ($type == 'boxes' || $type == 'simple') {
            switch ($columns) {
                case 1:
                    $columnsNumber = 'mkdf-one-column';
                    break;
                case 2:
                    $columnsNumber = 'mkdf-two-columns';
                    break;
                case 3:
                    $columnsNumber = 'mkdf-three-columns';
                    break;
                case 4:
                    $columnsNumber = 'mkdf-four-columns';
                    break;
                default:
					$columnsNumber = 'mkdf-one-column';
                    break;
            }
        }
		return $columnsNumber;
	}

	/**
	   * Generates query array
	   *
	   * @param $params
	   *
	   * @return array
	*/
	public function generateBlogQueryArray($params){
		
		$queryArray = array(
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['number_of_posts'],
			'category_name' => $params['category']
		);

		$post_ids = null;
		if (!empty($params['selected_posts'])) {
			$post_ids = explode(',', $params['selected_posts']);
			$queryArray['post__in'] = $post_ids;
		}

		return $queryArray;
	}

	/**
	   * Generates image size option
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function generateImageSize($params){
		$thumbImageSize = '';
		$imageSize = $params['image_size'];
		
		if ($imageSize !== '' && $imageSize == 'landscape') {
            $thumbImageSize .= 'cortex_mikado_landscape';
        } else if($imageSize === 'square'){
			$thumbImageSize .= 'cortex_mikado_square';
		} else if ($imageSize !== '' && $imageSize == 'original') {
            $thumbImageSize .= 'full';
        }
		return $thumbImageSize;
	}
	
}
