<li class="mkdf-blog-list-item clearfix mkdf-blog-list-title">
	<div class="mkdf-blog-list-item-table">
		<div class="mkdf-blog-list-item-table-cell">
			<?php if ($list_title !== '') {
				echo cortex_mikado_execute_shortcode('mkdf_section_title', array('title_text' => $list_title,'highlighted_text' => $list_highlighted));
			} ?>
		</div>
	</div>
</li>