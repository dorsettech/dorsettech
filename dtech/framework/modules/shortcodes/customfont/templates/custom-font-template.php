<?php
/**
 * Custom Font shortcode template
 */
?>

<<?php echo esc_attr($custom_font_tag);?> class="mkdf-custom-font-holder" <?php cortex_mikado_inline_style($custom_font_style); echo ' '.esc_attr($custom_font_data);?>>
	<?php if($link_content == 'yes' && $link != '') { ?>
		<a href="<?php echo esc_attr($link); ?>">
	<?php } ?>
	<?php echo wp_kses_post($content_custom_font);?><?php if ($highlighted_text !== ''){ ?><span class="mkdf-highlighted" <?php cortex_mikado_inline_style($highlighted_style)?>><?php echo esc_html($highlighted_text);?></span><?php } ?>
	<?php if($type_out_effect == 'yes') { ?>
		<span class="mkdf-typed-wrap" <?php cortex_mikado_inline_style($type_out_style);?>>
			<span class="mkdf-typed">
				<?php if($typed_ending_1 != '') { ?>
					<span class="mkdf-typed-1"><?php echo esc_html($typed_ending_1); ?></span>
				<?php } if($typed_ending_2 != '') { ?>
					<span class="mkdf-typed-2"><?php echo esc_html($typed_ending_2); ?></span>
				<?php } if($typed_ending_3 != '') { ?>
					<span class="mkdf-typed-3"><?php echo esc_html($typed_ending_3); ?></span>
				<?php } ?>
			</span>
		</span>
	<?php } ?>
    <?php if($link_content == 'yes' && $link != '') { ?>
        </a>
    <?php } ?>
</<?php echo esc_attr($custom_font_tag);?>>