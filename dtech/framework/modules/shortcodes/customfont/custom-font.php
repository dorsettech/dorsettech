<?php
namespace CortexMikado\Modules\Shortcodes\CustomFont;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class CustomFont
 */
class CustomFont implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'mkdf_custom_font';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see mkd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		vc_map( array(
				'name' => esc_html__('Custom Font', 'cortex'),
				'base' => $this->getBase(),
				'category' => esc_html__('by MIKADO','cortex'),
				'icon' => 'icon-wpb-custom-font extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array(
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Custom Font Tag", 'cortex'),
						"param_name" => "custom_font_tag",
						"value" => array(
							"" => "",
							"h2" => "h2",
							"h3" => "h3",
							"h4" => "h4",
							"h5" => "h5",
							"h6" => "h6",
							"p" => "p",
							"div" => "div",
						)
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Font family", 'cortex'),
						"param_name" => "font_family",
						"value" => ""
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Font size (px)", 'cortex'),
						"param_name" => "font_size",
						"value" => ""
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Line height (px)", 'cortex'),
						"param_name" => "line_height",
						"value" => ""
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Font Style", 'cortex'),
						"param_name" => "font_style",
						"value" => cortex_mikado_get_font_style_array(),
					),
					array(
						"type" => "dropdown",
						"heading" => "Font weight",
						"param_name" => esc_html__("font_weight", 'cortex'),
						"value" => cortex_mikado_get_font_weight_array(),
						"save_always" => true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Letter Spacing (px)", 'cortex'),
						"param_name" => "letter_spacing",
						"value" => ""
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Text transform", 'cortex'),
						"param_name" => "text_transform",
						"value" => array_flip(cortex_mikado_get_text_transform_array()),
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Text decoration", 'cortex'),
						"param_name" => "text_decoration",
						"value" => array(
							esc_html__("None", 'cortex') => "",
							esc_html__("Underline", 'cortex') => "underline",
							esc_html__("Overline", 'cortex') => "overline",
							esc_html__("Line Through",'cortex') => "line-through"
						),
					),
					array(
						"type" => "colorpicker",
						"heading" => esc_html__("Color", 'cortex'),
						"param_name" => "color",
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Text Align", 'cortex'),
						"param_name" => "text_align",
						"value" => array(
							"" => "",
							esc_html__("Left", 'cortex') => "left",
							esc_html__("Center",'cortex') => "center",
							esc_html__("Right", 'cortex') => "right",
							esc_html__("Justify", 'cortex') => "justify"
						),
					),
					array(
						"type" => "textarea",
						"heading" => esc_html__("Content", 'cortex'),
						"param_name" => "content_custom_font",
						"value" => "Custom Font Content",
						"save_always" => true
					),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Link content?", 'cortex'),
                        "param_name" => "link_content",
                        "value" => array(
                            esc_html__("No", 'cortex') => "no",
                            esc_html__("Yes", 'cortex') => "yes",
                        ),
                        "description" => esc_html__("Wraps entire content within a link", 'cortex'),
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__("Link", 'cortex'),
                        "param_name" => "link",
                        "value" => "",
                        'dependency' => Array('element' => 'link_content', 'value' => array('yes'))
                    ),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Enable Type Out Effect", 'cortex'),
						"param_name" => "type_out_effect",
						"value" => array(
							esc_html__("No", 'cortex') => "no",
							esc_html__("Yes", 'cortex') => "yes",
						),
						"description" => esc_html__("Adds a type out effect at the end of the custom font content.", 'cortex'),
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Typed Phrase 1", 'cortex'),
						"param_name" => "typed_ending_1",
						"value" => "",
						'dependency' => Array('element' => 'type_out_effect', 'value' => array('yes'))
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Typed Phrase 2", 'cortex'),
						"param_name" => "typed_ending_2",
						"value" => "",
						'dependency' => array('element' => 'typed_ending_1', 'not_empty' => true)
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Typed Phrase 3", 'cortex'),
						"param_name" => "typed_ending_3",
						"value" => "",
						'dependency' => array('element' => 'typed_ending_2', 'not_empty' => true)
					),
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__('Typed Out Color','cortex'),
						'param_name'	=> 'type_out_color',
						'dependency'	=> array('element' => 'type_out_effect', 'value' => array('yes'))
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__('Highlighted Text','cortex'),
						'param_name'	=> 'highlighted_text',
						'description'   =>esc_html__('Highlighted text will be appended to content','cortex'),
						'dependency'	=> array('element' => 'type_out_effect', 'value' => array('no'))
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Highlighted Color','cortex'),
						'param_name'  => 'highlighted_color',
						'dependency'  => array('element' => 'highlighted_text', 'not_empty' => true)
					),
				)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'custom_font_tag' => 'div',
			'font_family' => '',
			'font_size' => '',
			'line_height' => '',
			'font_style' => '',
			'font_weight' => '',
			'letter_spacing' => '',
			'text_transform' => '',
			'text_decoration' => '',
			'text_align' => '',
			'color' => '',
			'content_custom_font' => '',
            'link_content' => '',
            'link'        => '',
			'type_out_effect' => '',
			'typed_ending_1' => '',
			'typed_ending_2' => '',
			'typed_ending_3' => '',
			'type_out_color' => '',
			'highlighted_text' => '',
			'highlighted_color' => ''
		);

		$params = shortcode_atts($args, $atts);

		$params['custom_font_style'] = $this->getCustomFontStyle($params);
		$params['custom_font_tag'] = $this->getCustomFontTag($params,$args);
		$params['custom_font_data'] = $this->getCustomFontData($params);
		$params['type_out_style'] = $this->getTypedOutStyle($params);
		$params['highlighted_style'] = $this->getHighlightedStyle($params);

		//Get HTML from template
		$html = cortex_mikado_get_shortcode_module_template_part('templates/custom-font-template', 'customfont', '', $params);

		return $html;

	}

	/**
	 * Return Style for Custom Font
	 *
	 * @param $params
	 * @return string
	 */
	private function getCustomFontStyle($params) {
		$custom_font_style = array();

		if ($params['font_family'] !== '') {
			$custom_font_style[] = 'font-family: '.$params['font_family'];
		}

		if ($params['font_size'] !== '') {
			$font_size = strstr($params['font_size'], 'px') ? $params['font_size'] : $params['font_size'].'px';
			$custom_font_style[] = 'font-size: '.$font_size;
		}

		if ($params['line_height'] !== '') {
			$line_height = strstr($params['line_height'], 'px') ? $params['line_height'] : $params['line_height'].'px';
			$custom_font_style[] = 'line-height: '.$line_height;
		}

		if ($params['font_style'] !== '') {
			$custom_font_style[] = 'font-style: '.$params['font_style'];
		}

		if ($params['font_weight'] !== '') {
			$custom_font_style[] = 'font-weight: '.$params['font_weight'];
		}

		if ($params['letter_spacing'] !== '') {
			$letter_spacing = strstr($params['letter_spacing'], 'px') ? $params['letter_spacing'] : $params['letter_spacing'].'px';
			$custom_font_style[] = 'letter-spacing: '.$letter_spacing;
		}

		if ($params['text_transform'] !== '') {
			$custom_font_style[] = 'text-transform: '.$params['text_transform'];
		}

		if ($params['text_decoration'] !== '') {
			$custom_font_style[] = 'text-decoration: '.$params['text_decoration'];
		}

		if ($params['text_align'] !== '') {
			$custom_font_style[] = 'text-align: '.$params['text_align'];
		}

		if ($params['color'] !== '') {
			$custom_font_style[] = 'color: '.$params['color'];
		}

		return implode(';', $custom_font_style);
	}

	/**
	 * Return Custom Font Tag. If provided heading isn't valid get the default one
	 *
	 * @param $params
	 * @return string
	 */
	private function getCustomFontTag($params,$args) {
		$tag_array = array('h2', 'h3', 'h4', 'h5', 'h6','p','div');
		return (in_array($params['custom_font_tag'], $tag_array)) ? $params['custom_font_tag'] : $args['custom_font_tag'];
	}

	/**
	 * Return Custom Font Data Attr
	 *
	 * @param $params
	 * @return string
	 */
	private function getCustomFontData($params) {
		$data_array = array();

		if ($params['font_size'] !== '') {
			$data_array[] = 'data-font-size= '.$params['font_size'];
		}

		if ($params['line_height'] !== '') {
			$data_array[] = 'data-line-height= '.$params['line_height'];
		}
		return implode(' ', $data_array);
	}

	/**
	 * Return Style for Typed Out
	 *
	 * @param $params
	 * @return string
	 */
	private function getTypedOutStyle($params) {
		$typed_style = array();

		if ($params['type_out_color'] !== '') {
			$typed_style[] = 'color: '.$params['type_out_color'];
		}

		return implode(';', $typed_style);
	}


    /**
     * Generates style for highlighted text
     *
     * @param $params
     *
     * @return string
     */
	private function getHighlightedStyle($params){
		$highlighted_style = array();

		if ($params['highlighted_color'] != ''){
			$highlighted_style[] = 'color: '.$params['highlighted_color'];
		}

		return implode(';', $highlighted_style);
	}
}