<div class="mkdf-section-item <?php echo esc_attr($section_item_class); ?>" <?php echo cortex_mikado_get_inline_style($section_item_style); ?>>
	<div class="mkdf-section-item-inner">
		<div class="mkdf-section-item-content">
			<?php echo do_shortcode($content); ?>
		</div>
	</div>
</div>