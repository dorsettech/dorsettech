<div <?php cortex_mikado_class_attribute($section_classes)?>>
	<div class="mkdf-sh-title-area" <?php cortex_mikado_inline_style($title_area_style);?>>
		<div class="mkdf-sh-title-area-inner">
			<?php 
			if ($title_text !== '' || $highlighted_text !== '') {
				echo cortex_mikado_execute_shortcode('mkdf_section_title', $title_params);
			}
			if ($show_separator == 'yes'){
				echo cortex_mikado_execute_shortcode('mkdf_separator', array('position' => 'left'));
			}
			if ($subtitle_text !== ''){
				echo cortex_mikado_execute_shortcode('mkdf_section_subtitle', $subtitle_params);
			}
			?>
		</div>
	</div>
	<div class="mkdf-sh-content-area">
		<?php echo do_shortcode($content);?>
	</div>
</div>