<?php
namespace CortexMikado\Modules\Shortcodes\CallToAction;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class CallToAction
 */
class CallToAction implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'mkdf_call_to_action';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see mkd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		$call_to_action_button_icons_array = array();
		$call_to_action_button_IconCollections = cortex_mikado_icon_collections()->iconCollections;
		foreach($call_to_action_button_IconCollections as $collection_key => $collection) {

			$call_to_action_button_icons_array[] = array(
				'type' => 'dropdown',
				'heading' => esc_html__('Button Icon', 'cortex'),
				'param_name' => 'button_'.$collection->param,
				'value' => $collection->getIconsArray(),
				'save_always' => true,
				'dependency' => Array('element' => 'button_icon_pack', 'value' => array($collection_key))
			);

		}

		vc_map( array(
				'name' => esc_html__('Call To Action', 'cortex'),
				'base' => $this->getBase(),
				'category' => esc_html__('by MIKADO', 'cortex'),
				'icon' => 'icon-wpb-call-to-action extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array_merge(
					array(
						array(
							'type'          => 'dropdown',
							'heading'       => esc_html__('Full Width', 'cortex'),
							'param_name'    => 'full_width',
							'admin_label'	=> true,
							'value'         => array(
								esc_html__('Yes', 'cortex') => 'yes',
								esc_html__('No', 'cortex')   => 'no'
							),
							'save_always' 	=> true,
						),
						array(
							'type'          => 'dropdown',
							'heading'       => esc_html__('Content in grid', 'cortex'),
							'param_name'    => 'content_in_grid',
							'value'         => array(
								esc_html__('Yes', 'cortex') => 'yes',
								esc_html__('No', 'cortex')  => 'no'
							),
							'save_always'	=> true,
							'dependency'    => array('element' => 'full_width', 'value' => 'yes')
						),
						array(
							'type'          => 'dropdown',
							'heading'       => esc_html__('Content Layout', 'cortex'),
							'param_name'    => 'grid_size',
							'value'         => array(
								'75/25'     => '75',
								'50/50'     => '50',
								'66/33'     => '66'
							),
							'save_always' 	=> true
						),
						array(
							'type' 			=> 'dropdown',
							'heading'		=> esc_html__('Type', 'cortex'),
							'param_name' 	=> 'type',
							'admin_label' 	=> true,
							'value' 		=> array(
								esc_html__('Normal', 'cortex')	=> 'normal',
								esc_html__('With Icon', 'cortex') => 'with-icon',
							),
							'save_always' 	=> true,
						)
					),
					cortex_mikado_icon_collections()->getVCParamsArray(array('element' => 'type', 'value' => array('with-icon'))),
					array(
						array(
							'type' 			=> 'textfield',
							'heading' 		=> esc_html__('Icon Size (px)', 'cortex'),
							'param_name' 	=> 'icon_size',
							'dependency' 	=> Array('element' => 'type', 'value' => array('with-icon')),
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' 			=> 'textfield',
							'heading' 		=> esc_html__('Box Padding (top right bottom left) px', 'cortex'),
							'param_name' 	=> 'box_padding',
							'description' 	=> esc_html__('Default padding is 20px on all sides', 'cortex'),
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' 			=> 'colorpicker',
							'heading' 		=> esc_html__('Background Color','cortex'),
							'param_name' 	=> 'background_color',
							'description' 	=> '',
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__('Text Color','cortex'),
							'param_name'  => 'text_color',
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' 			=> 'textfield',
							'heading' 		=> esc_html__('Default Text Font Size (px)', 'cortex'),
							'param_name' 	=> 'text_size',
							'description' 	=> esc_html__('Font size for p tag', 'cortex'),
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' 			=> 'dropdown',
							'heading' 		=> esc_html__('Show Button', 'cortex'),
							'param_name' 	=> 'show_button',
							'value' 		=> array(
								esc_html__('Yes', 'cortex') => 'yes',
								esc_html__('No', 'cortex') => 'no'
							),
							'save_always' 	=> true,
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Position', 'cortex'),
							'param_name' => 'button_position',
							'value' => array(
								esc_html__('Default/right', 'cortex') => '',
								esc_html__('Center', 'cortex') => 'center',
								esc_html__('Left', 'cortex') => 'left'
							),
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__('Button Text', 'cortex'),
							'param_name' => 'button_text',
							'description' => esc_html__('Default text is "button"', 'cortex'),
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__('Button Link', 'cortex'),
							'param_name' => 'button_link',
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Target', 'cortex'),
							'param_name' => 'button_target',
							'value' => array(
								'' => '',
								esc_html__('Self', 'cortex') => '_self',
								esc_html__('Blank', 'cortex') => '_blank'
							),
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Icon Pack', 'cortex'),
							'param_name' => 'button_icon_pack',
							'value' => array_merge(array(esc_html__('No Icon','cortex') => ''),cortex_mikado_icon_collections()->getIconCollectionsVC()),
							'save_always' => true,
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Size', 'cortex'),
							'param_name' => 'button_size',
							'value' => array(
								esc_html__('Default', 'cortex') => '',
								esc_html__('Small', 'cortex') => 'small',
								esc_html__('Medium', 'cortex') => 'medium',
								esc_html__('Large', 'cortex') => 'large',
								esc_html__('Extra Large', 'cortex') => 'huge'
							),
							'dependency' => array('element' => 'show_button', 'value' => array('yes')),
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' => 'colorpicker',
							'heading' => esc_html__('Button Text Color', 'cortex'),
							'param_name' => 'button_text_color',
							'dependency' => array('element' => 'show_button', 'value' => array('yes')),
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' => 'colorpicker',
							'heading' => esc_html__('Button Background Color', 'cortex'),
							'param_name' => 'button_bckg_color',
							'dependency' => array('element' => 'show_button', 'value' => array('yes')),
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' => 'colorpicker',
							'heading' => esc_html__('Button Hover Text Color', 'cortex'),
							'param_name' => 'button_text_hover_color',
							'dependency' => array('element' => 'show_button', 'value' => array('yes')),
							'group'         => esc_html__('Design Options','cortex'),
						),
						array(
							'type' => 'colorpicker',
							'heading' => esc_html__('Button Hover Background Color', 'cortex'),
							'param_name' => 'button_hover_bckg_color',
							'dependency' => array('element' => 'show_button', 'value' => array('yes')),
							'group'         => esc_html__('Design Options','cortex'),
						),
					),
					$call_to_action_button_icons_array,
					array(
						array(
							'type' => 'textarea_html',
							'admin_label' => true,
							'heading' => esc_html__('Content', 'cortex'),
							'param_name' => 'content',
							'value' => '',
						)
					)
				)
		) );

	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'type' => 'normal',
			'full_width' => 'yes',
			'content_in_grid' => 'yes',
			'grid_size' => '75',
			'icon_size' => '',
			'box_padding' => '20px',
			'text_size' => '',
			'show_button' => 'yes',
			'button_position' => 'right',
			'button_size' => '',
			'button_text_color' => '',
			'button_bckg_color' => '',
			'button_text_hover_color' => '',
			'button_hover_bckg_color' => '',
			'button_link' => '',
			'button_text' => 'button',
			'button_target' => '',
			'button_icon_pack' => '',
			'text_color' => '',
			'background_color' => '',
		);

		$call_to_action_icons_form_fields = array();

		foreach (cortex_mikado_icon_collections()->iconCollections as $collection_key => $collection) {

			$call_to_action_icons_form_fields['button_' . $collection->param ] = '';

		}

		$args = array_merge($args, cortex_mikado_icon_collections()->getShortcodeParams(),$call_to_action_icons_form_fields);

		$params = shortcode_atts($args, $atts);

		$params['content']= preg_replace('#^<\/p>|<p>$#', '', $content);
		$params['text_wrapper_classes'] = $this->getTextWrapperClasses($params);
		$params['content_styles'] = $this->getContentStyles($params);
		$params['row_classes'] = $this->getCallToActionRowClasses($params);
		$params['call_to_action_styles'] = $this->getCallToActionStyles($params);
		$params['call_to_action_padding'] = $this->getCallToActionPadding($params);
		$params['icon'] = $this->getCallToActionIcon($params);
		$params['button_parameters'] = $this->getButtonParameters($params);

		//Get HTML from template
		$html = cortex_mikado_get_shortcode_module_template_part('templates/call-to-action-template', 'calltoaction', '', $params);

		return $html;

	}

	/**
	 * Return Classes for Call To Action text wrapper
	 *
	 * @param $params
	 * @return string
	 */
	private function getTextWrapperClasses($params) {
		return ( $params['show_button'] == 'yes') ? 'mkdf-call-to-action-column1 mkdf-call-to-action-cell' : '';
	}

	/**
	 * Return CSS styles for Call To Action Icon
	 *
	 * @param $params
	 * @return string
	 */
	private function getIconStyles($params) {
		$icon_style = array();

		if ($params['icon_size'] !== '') {
			$icon_style[] = 'font-size: ' . $params['icon_size'] . 'px';
		}

		return implode(';', $icon_style);
	}

	/**
	 * Return CSS styles for Call To Action Content
	 *
	 * @param $params
	 * @return string
	 */
	private function getContentStyles($params) {
		$content_styles = array();

		if ($params['text_size'] !== '') {
			$content_styles[] = 'font-size: ' . $params['text_size'] . 'px';
		}

		
		return implode(';', $content_styles);
	}
	/**
	 * Return classes for Call To Action row
	 *
	 * @param $params
	 * @return string
	 */
	private function getCallToActionRowClasses($params) {
		$classes = array();
		switch ($params['grid_size']) {
			case '75':
				$classes[] = 'mkdf-call-to-action-row-75-25';
				break;			
			case '66':
				$classes[] = 'mkdf-call-to-action-row-66-33';
				break;
			default:
				$classes[] = 'mkdf-call-to-action-row-50-50';
				break;
		}

		$classes[] = 'clearfix';

		return implode(' ', $classes);
	}

	/**
	 * Return CSS styles for Call To Action shortcode
	 *
	 * @param $params
	 * @return string
	 */
	private function getCallToActionStyles($params) {
		$call_to_action_styles = array();

		if ($params['text_color'] != '') {
			$call_to_action_styles[] = 'color: ' . $params['text_color'] . ';';
		}
		if ($params['background_color'] != '') {
			$call_to_action_styles[] = 'background-color: ' . $params['background_color'] . ';';
		}

		return implode(';', $call_to_action_styles);
	}

	/**
	 * Return Padding styles for Call To Action shortcode
	 *
	 * @param $params
	 * @return string
	 */
	private function getCallToActionPadding($params) {
		$call_to_action_padding_styles = array();

		if ($params['box_padding'] != '') {
			$call_to_action_padding_styles[] = 'padding: ' . $params['box_padding'] . ';';
		}

		return implode(';', $call_to_action_padding_styles);
	}

	/**
	 * Return Icon for Call To Action Shortcode
	 *
	 * @param $params
	 * @return mixed
	 */
	private function getCallToActionIcon($params) {

		$icon = cortex_mikado_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);
		$iconStyles = array();
		$iconStyles['icon_attributes']['style'] = $this->getIconStyles($params);
		$call_to_action_icon = '';
		if(!empty($params[$icon])){			
			$call_to_action_icon = cortex_mikado_icon_collections()->renderIcon( $params[$icon], $params['icon_pack'], $iconStyles );
		}
		return $call_to_action_icon;

	}
	
	private function getButtonParameters($params) {
		$button_params_array = array();
		
		if(!empty($params['button_link'])) {
			$button_params_array['link'] = $params['button_link'];
		}
		
		if(!empty($params['button_size'])) {
			$button_params_array['size'] = $params['button_size'];
		}
		else{
			$button_params_array['size'] = 'huge';
		}

		if(!empty($params['button_text_color'])) {
			$button_params_array['color'] = $params['button_text_color'];
		}

		if(!empty($params['button_bckg_color'])) {
			$button_params_array['background_color'] = $params['button_bckg_color'];
		}

		if(!empty($params['button_text_hover_color'])) {
			$button_params_array['hover_color'] = $params['button_text_hover_color'];
		}

		if(!empty($params['button_hover_bckg_color'])) {
			$button_params_array['hover_background_color'] = $params['button_hover_bckg_color'];
		}
		
		if(!empty($params['button_icon_pack'])) {
			$button_params_array['icon_pack'] = $params['button_icon_pack'];
			$iconPackName = cortex_mikado_icon_collections()->getIconCollectionParamNameByKey($params['button_icon_pack']);
			$button_params_array[$iconPackName] = $params['button_'.$iconPackName];		
		}
				
		if(!empty($params['button_target'])) {
			$button_params_array['target'] = $params['button_target'];
		}
		
		if(!empty($params['button_text'])) {
			$button_params_array['text'] = $params['button_text'];
		}
		
		return $button_params_array;
	}
}