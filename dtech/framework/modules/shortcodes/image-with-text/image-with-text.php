<?php
namespace CortexMikado\Modules\Shortcodes\ImageWithText;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class ImageWithText implements ShortcodeInterface{

	private $base;

	/**
	 * Image Gallery constructor.
	 */
	public function __construct() {
		$this->base = 'mkdf_image_with_text';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see mkd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		vc_map(array(
			'name'                      => esc_html__('Image With Text', 'cortex'),
			'base'                      => $this->getBase(),
			'category'                  => esc_html__('by MIKADO', 'cortex'),
			'icon'                      => 'icon-wpb-image-with-text extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Layout','cortex'),
					'param_name'	=> 'layout',
					'value'			=> array(
						esc_html__('Default','cortex') => 'default',
						esc_html__('Text on Hover','cortex') => 'text-on-hover',
					),
				),
				array(
					'type'			=> 'attach_image',
					'heading'		=> esc_html__('Image', 'cortex'),
					'param_name'	=> 'image',
					'description'	=> esc_html__('Select image from media library', 'cortex')
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> esc_html__('Image Size', 'cortex'),
					'param_name'	=> 'image_size',
					'description'	=> esc_html__('Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size', 'cortex'),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> esc_html__('Title','cortex'),
					'param_name'	=> 'title'
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Title Style','cortex'),
					'param_name'	=> 'title_tag',
					'value'			=> array(
						esc_html__('Section Title','cortex') => 'span',
						'h2' => 'h2',
						'h3' => 'h3',
						'h4' => 'h4',
						'h5' => 'h5',
						'h6' => 'h6',
					),
					'dependency'	=> array('element' => 'title', 'not_empty' => true)
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> esc_html__('Title Color','cortex'),
					'param_name'	=> 'title_color',
					'dependency'	=> array('element' => 'title', 'not_empty' => true)
				),
                array(
                    'type'			=> 'textfield',
                    'heading'		=> esc_html__('Text','cortex'),
                    'param_name'	=> 'text'
                ),
				array(
					'type'			=> 'textfield',
					'heading'		=> esc_html__('Link','cortex'),
					'param_name'	=> 'link'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> esc_html__('Link Text','cortex'),
					'param_name'	=> 'link_text',
                    'dependency' => array('element' => 'layout', 'value' => array('default')),
				),
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__('Target', 'cortex'),
                    'param_name' => 'target',
                    'value'      => array(
						esc_html__('Self', 'cortex')  => '_self',
						esc_html__('Blank','cortex') => '_blank'
                    ),
                    'dependency' => array('element' => 'link', 'not_empty' => true),
                ),
			)
		));

	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'layout'			=> 'default',
			'image'				=> '',
			'image_size'		=> 'thumbnail',
			'title'				=> '',
			'title_tag'			=> 'h3',
			'title_color'		=> '',
            'text'				=> '',
			'link'				=> '',
			'link_text'			=> '',
			'target'			=> '_self'
		);

		$params = shortcode_atts($args, $atts);
        $params['holder_classes']  = $this->getHolderClasses($params);
		$params['image_size'] = $this->getImageSize($params['image_size']);
		$params['title_style'] = $this->getTitleStyle($params);

		$html = cortex_mikado_get_shortcode_module_template_part('templates/image-with-text-template' , 'image-with-text', '', $params);

		return $html;

	}


	/**
	 * Return image size or custom image size array
	 *
	 * @param $image_size
	 * @return array
	 */
	private function getImageSize($image_size) {

		$image_size = trim($image_size);
		//Find digits
		preg_match_all( '/\d+/', $image_size, $matches );
		if(in_array( $image_size, array('thumbnail', 'thumb', 'medium', 'large', 'full'))) {
			return $image_size;
		} elseif(!empty($matches[0])) {
			return array(
					$matches[0][0],
					$matches[0][1]
			);
		} else {
			return 'thumbnail';
		}
	}

	private function getTitleStyle($params){
		$style = array();

		if ($params['title_color'] !== ''){
			$style[] = 'color: '.$params['title_color'];
		}

		return implode(';', $style);
	}

	/**
	 * Returns array of holder classes
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getHolderClasses($params) {
	    $classes = array('mkdf-image-with-text');

	    if(!empty($params['layout'])) {
	        $classes[] = 'mkdf-iwt-layout-'.$params['layout'];
	    }

	    return $classes;
	}
}