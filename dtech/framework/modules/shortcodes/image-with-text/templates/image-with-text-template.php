<div <?php cortex_mikado_class_attribute($holder_classes); ?>>
	<?php if ($link !== '' && $link_text == ''){ ?>	
		<a class="mkdf-iwt-link-abs" href="<?php echo esc_url($link)?>" target="<?php echo esc_attr($target);?>"></a>
	<?php } ?>
	<div class="mkdf-iwt-image">
		<?php if(is_array($image_size) && count($image_size)) : ?>
			<?php echo cortex_mikado_generate_thumbnail($image, null, $image_size[0], $image_size[1]); ?>
		<?php else: ?>
			<?php echo wp_get_attachment_image($image, $image_size); ?>
		<?php endif; ?>
	</div>
	<div class="mkdf-iwt-text">
		<?php if ($title !== '') { ?>
			<<?php echo esc_attr($title_tag) ?> class="mkdf-iwt-title" <?php cortex_mikado_inline_style($title_style);?>>
				<?php echo esc_html($title);?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } ?>
		<?php if ($text !== '') { ?>
			<p class="mkdf-iwt-paragraph">
			    <?php echo esc_html($text);?>
			</p>
		<?php } ?>
		<?php if ($link !== '' && $link_text !== ''){ ?>	
			<a class="mkdf-iwt-link" href="<?php echo esc_url($link)?>" target="<?php echo esc_attr($target);?>">
				<?php echo esc_html($link_text); ?>
				<span class="arrow_right"></span>
			</a>
		<?php } ?>
	</div>
</div>