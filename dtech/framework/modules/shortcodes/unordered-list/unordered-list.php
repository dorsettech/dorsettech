<?php
namespace CortexMikado\Modules\Shortcodes\UnorderedList;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class unordered List
 */
class UnorderedList implements ShortcodeInterface{

	private $base;

	function __construct() {
		$this->base='mkdf_unordered_list';
		
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**\
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('List - Unordered', 'cortex'),
			'base' => $this->base,
			'icon' => 'icon-wpb-unordered-list extended-custom-icon',
			'category' => esc_html__('by MIKADO', 'cortex'),
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type'			=> 'dropdown',
					'admin_label'	=> true,
					'heading'		=> esc_html__('Style', 'cortex'),
					'param_name'	=> 'style',
					'value'			=> array(
						esc_html__('Circle', 'cortex') => 'circle',
						esc_html__('Line', 'cortex')	 => 'line',
						esc_html__('Arrow', 'cortex')  => 'arrow'
					)
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Animate List', 'cortex'),
					'param_name'	=> 'animate',
					'value'			=> array(
						esc_html__('No', 'cortex') => 'no',
						esc_html__('Yes', 'cortex') => 'yes'
					)
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Hover Effect','cortex'),
					'param_name'	=> 'hover',
					'value'			=> array(
						esc_html__('No', 'cortex') => 'no',
						esc_html__('Yes', 'cortex') => 'yes'
					),
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Font Weight', 'cortex'),
					'param_name' => 'font_weight',
					'value' => array(
						esc_html__('Default', 'cortex') => '',
						esc_html__('Light', 'cortex') => 'light',
						esc_html__('Normal', 'cortex') => 'normal',
						esc_html__('Bold', 'cortex') => 'bold'
					),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Padding left (px)', 'cortex'),
					'param_name' => 'padding_left',
					'value' => ''
				),
				array(
					'type' => 'textarea_html',
					'heading' => esc_html__('Content', 'cortex'),
					'param_name' => 'content',
					'value' => '<ul><li>Lorem Ipsum</li><li>Lorem Ipsum</li><li>Lorem Ipsum</li></ul>',
				)
			)
		) );
	}

	public function render($atts, $content = null) {
		$args = array(
            'style' => '',
            'animate' => '',
            'hover' => '',
            'font_weight' => '',
            'padding_left' => ''
        );
		$params = shortcode_atts($args, $atts);
		
		//Extract params for use in method
		extract($params);
		
		$list_item_classes = "";

        if ($style != '') {
			$list_item_classes .= ' mkdf-'.$style;
        }

		if ($animate == 'yes') {
			$list_item_classes .= ' mkdf-animate-list';
		}

		if ($hover == 'yes') {
			$list_item_classes .= ' mkdf-hover-list';
		}
		
		$list_style = '';
		if($padding_left != '') {
			$list_style .= 'padding-left: ' . $padding_left .'px;';
		}
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
        $html = '<div class="mkdf-unordered-list '.$list_item_classes.'" '.  cortex_mikado_get_inline_style($list_style).'>'.do_shortcode($content).'</div>';
        return $html;
	}
}