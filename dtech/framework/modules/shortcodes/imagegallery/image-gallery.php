<?php
namespace CortexMikado\Modules\Shortcodes\ImageGallery;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class ImageGallery implements ShortcodeInterface{

	private $base;

	/**
	 * Image Gallery constructor.
	 */
	public function __construct() {
		$this->base = 'mkdf_image_gallery';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 */
	public function vcMap() {

		vc_map(array(
			'name'                      => esc_html__('Image Gallery', 'cortex'),
			'base'                      => $this->getBase(),
			'category'                  => esc_html__('by MIKADO', 'cortex'),
			'icon'                      => 'icon-wpb-image-gallery extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'			=> 'attach_images',
					'heading'		=> esc_html__('Images', 'cortex'),
					'param_name'	=> 'images',
					'description'	=> esc_html__('Select images from media library', 'cortex')
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__('Gallery Type', 'cortex'),
					'admin_label' => true,
					'param_name'  => 'type',
					'value'       => array(
						esc_html__('Slider', 'cortex')		=> 'slider',
						esc_html__('Carousel', 'cortex')	=> 'carousel',
						esc_html__('Image Grid', 'cortex')	=> 'image_grid',
						esc_html__('Masonry', 'cortex')	=> 'masonry',
					),
					'description' => esc_html__('Select gallery type', 'cortex'),
					'save_always' => true
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> esc_html__('Image Size', 'cortex'),
					'param_name'	=> 'image_size',
					'description'	=> esc_html__('Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size', 'cortex'),
					'dependency'	=> array('element' => 'type', 'value' => array('slider','image_grid','carousel'))
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Slide duration', 'cortex'),
					'admin_label'	=> true,
					'param_name'	=> 'autoplay',
					'value'			=> array(
						'3'			=> '3',
						'5'			=> '5',
						'10'		=> '10',
						esc_html__('Disable','cortex')	=> 'disable'
					),
					'save_always'	=> true,
					'dependency'	=> array('element'	=> 'type','value'=> array('slider','carousel')),
					'description' => esc_html__('Auto rotate slides each X seconds', 'cortex'),
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Slide Animation', 'cortex'),
					'admin_label'	=> true,
					'param_name'	=> 'slide_animation',
					'value'			=> array(
						esc_html__('Slide', 'cortex')	=> 'slide',
						esc_html__('Fade','cortex')	=> 'fade'
					),
					'save_always'	=> true,
					'dependency'	=> array(
						'element'	=> 'type',
						'value'		=> array(
							'slider'
						)
					)
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Column Number', 'cortex'),
					'param_name'	=> 'column_number',
					'value'			=> array(2, 3, 4, 5),
					'save_always'	=> true,
					'dependency'	=> array(
						'element' 	=> 'type',
						'value'		=> array(
							'image_grid'
						)
					),
				),
                array(
                    'type'           => 'dropdown',
                    'heading'        => esc_html__('Spaces between images','cortex'),
                    'param_name'     => 'images_space',
                    'value'          => array(
                        esc_html__('Yes','cortex') => 'yes',
                        esc_html__('No','cortex') => 'no',
                    ),
                    'dependency'     => array(
                        'element'    => 'type',
                        'value'      => array(
                            'image_grid',
                            'masonry'
                        )
                    )
                ),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Open PrettyPhoto on click', 'cortex'),
					'param_name'	=> 'pretty_photo',
					'value'			=> array(
						esc_html__('No', 'cortex')		=> 'no',
						esc_html__('Yes', 'cortex')	=> 'yes'
					),
					'save_always'	=> true,
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Grayscale Images', 'cortex'),
					'param_name' => 'grayscale',
					'value' => array(
						esc_html__('No', 'cortex') => 'no',
						esc_html__('Yes', 'cortex') => 'yes'
					),
					'save_always'	=> true,
					'dependency'	=> array(
						'element'	=> 'type',
						'value'		=> array(
							'image_grid'
						)
					)
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Show Navigation Arrows', 'cortex'),
					'param_name' 	=> 'navigation',
					'value' 		=> array(
						esc_html__('Yes', 'cortex') => 'yes',
						esc_html__('No', 'cortex')	=> 'no'
					),
					'dependency' 	=> array(
						'element' => 'type',
						'value' => array(
							'slider',
							'carousel'
						)
					),
					'save_always'	=> true
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__('Show Pagination', 'cortex'),
					'param_name' 	=> 'pagination',
					'value' 		=> array(
						esc_html__('Yes', 'cortex') => 'yes',
						esc_html__('No', 'cortex')	=> 'no'
					),
					'save_always'	=> true,
					'dependency'	=> array(
						'element' => 'type',
						'value' => array(
							'slider',
							'carousel'
						)
					)
				)
			)
		));

	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'images'			=> '',
			'image_size'		=> 'thumbnail',
			'type'				=> 'slider',
			'autoplay'			=> '5000',
			'slide_animation'	=> 'slide',
			'images_space'		=> 'yes',
			'pretty_photo'		=> '',
			'column_number'		=> '',
			'grayscale'			=> '',
			'navigation'		=> 'yes',
			'pagination'		=> 'yes'
		);

		$params = shortcode_atts($args, $atts);
		$params['slider_data'] = $this->getSliderData($params);
		$params['image_size'] = $this->getImageSize($params['image_size']);
		$params['images'] = $this->getGalleryImages($params);
		$params['pretty_photo'] = ($params['pretty_photo'] == 'yes') ? true : false;
		$params['columns'] = 'mkdf-gallery-columns-' . $params['column_number'];
		$params['gallery_classes'] = $this->getGalleryClasses($params);
		$params['slider_classes'] = $this->getSliderClasses($params);

		if ($params['type'] == 'image_grid') {
			$template = 'gallery-grid';
		} elseif ($params['type'] == 'slider' || $params['type'] == 'carousel') {
			$template = 'gallery-slider';
		} elseif ($params['type'] == 'masonry') {
			$template = 'gallery-masonry';
		}

		$html = cortex_mikado_get_shortcode_module_template_part('templates/' . $template, 'imagegallery', '', $params);

		return $html;

	}

	/**
	 * Return images for gallery
	 *
	 * @param $params
	 * @return array
	 */
	private function getGalleryImages($params) {
		$image_ids = array();
		$images = array();
		$i = 0;

		if ($params['images'] !== '') {
			$image_ids = explode(',', $params['images']);
		}

		foreach ($image_ids as $id) {

			$image['image_id'] = $id;
			$image['class'] = '';
			if ($params['type'] == 'masonry') {
		        $size = get_post_meta($id,'_gallery_masonry_image_size', true);
		        $size = ($size)?$size:'mkdf-default-masonry-item';
		        switch($size){
			        case 'mkdf-large-height-masonry-item' :
				        $img_size = 'cortex_mikado_large_height';
				        $image['class'] = 'mkdf-size-portrait';
				        break;
			        case 'mkdf-large-width-masonry-item' :
				        $img_size = 'cortex_mikado_large_width';
				        $image['class'] = 'mkdf-size-landscape';
				        break;
			        case 'mkdf-large-width-height-masonry-item' :
				        $img_size = 'cortex_mikado_large_width_height';
				        $image['class'] = 'mkdf-size-big-square';
				        break;
			        default:
				        $img_size = 'cortex_mikado_square';
				        $image['class'] = 'mkdf-size-square';
				        break;
		        }
			}
			else{
				$img_size = 'full';
			}
			$image_original = wp_get_attachment_image_src($id, $img_size);
			$image['masonry_size'] = $img_size;
			$image['url'] = $image_original[0];
			$image['title'] = get_the_title($id);
			$image['link'] = get_post_meta($id,'_attachment_image_custom_link', true);
			$image['link_target'] = get_post_meta($id,'_attachment_image_link_target', true);

			if ($image['link_target'] == ''){
				$image['link_target'] = '_self';
			}

			$images[$i] = $image;
			$i++;
		}

		return $images;

	}

	/**
	 * Return image size or custom image size array
	 *
	 * @param $image_size
	 * @return array
	 */
	private function getImageSize($image_size) {

		$image_size = trim($image_size);
		//Find digits
		preg_match_all( '/\d+/', $image_size, $matches );
		if(in_array( $image_size, array('thumbnail', 'thumb', 'medium', 'large', 'full'))) {
			return $image_size;
		} elseif(!empty($matches[0])) {
			return array(
					$matches[0][0],
					$matches[0][1]
			);
		} else {
			return 'thumbnail';
		}
	}

	/**
	 * Return all configuration data for slider
	 *
	 * @param $params
	 * @return array
	 */
	private function getSliderData($params) {

		$slider_data = array();

		$slider_data['data-autoplay'] = ($params['autoplay'] !== '') ? $params['autoplay'] : '';
		$slider_data['data-animation'] = ($params['slide_animation'] !== '') ? $params['slide_animation'] : '';
		$slider_data['data-navigation'] = ($params['navigation'] !== '') ? $params['navigation'] : '';
		$slider_data['data-pagination'] = ($params['pagination'] !== '') ? $params['pagination'] : '';

		return $slider_data;

	}

    /**
     * Generates css classes for Gallery
     *
     * @param $params
     *
     * @return array
     */
    private function getGalleryClasses($params){

        $gallery_classes = array();

        if ($params['images_space'] == 'no'){
            $gallery_classes[] = 'mkdf-gallery-no-space';
        }
        else{
            $gallery_classes[] = 'mkdf-gallery-with-space';
        }

        if ($params['grayscale'] == 'yes' && $params['type'] == 'image_grid'){
            $gallery_classes[] = 'mkdf-grayscale';
        }

        return implode(' ', $gallery_classes);
    }

    /**
     * Generates css classes for slider and carousel
     *
     * @param $params
     *
     * @return array
     */
    private function getSliderClasses($params){

        $classes = array();

        if ($params['type'] == 'carousel') {
        	$classes[] = 'mkdf-gallery-image-carousel';
        }
        elseif ($params['type'] == 'slider'){
        	$classes[] = 'mkdf-gallery-image-slider';
        }

        return implode(' ', $classes);
    }
}