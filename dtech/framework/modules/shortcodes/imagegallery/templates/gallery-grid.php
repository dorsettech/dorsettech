<div class="mkdf-image-gallery">
	<div class="mkdf-image-gallery-grid <?php echo esc_html($columns); ?> <?php echo esc_html($gallery_classes); ?> clearfix" >
		<?php foreach ($images as $image) { ?>
			<div class="mkdf-gallery-image">
				<?php if ($pretty_photo) { ?>
					<a href="<?php echo esc_url($image['url'])?>" data-rel="prettyPhoto[single_pretty_photo]" title="<?php echo esc_attr($image['title']); ?>">
				<?php } 
				elseif ($image['link'] !== '') { ?>
					<a class="mkdf-ig-link" href="<?php echo esc_url($image['link'])?>" target="<?php echo esc_attr($image['link_target']);?>">
						<div class="mkdf-ig-overlay">
							<?php if ($image['title'] !== ''){ ?>
								<div class="mkdf-ig-overlay-table">
									<div class="mkdf-ig-overlay-table-cell">
										<h3 class="mkdf-ig-overlay-title"><?php echo esc_html($image['title']); ?></h3>
									</div>
								</div>
							<?php } ?>
						</div>
				<?php } ?>
					<div class="mkdf-ig-image-holder">
						<?php if(is_array($image_size) && count($image_size)) : ?>
							<?php echo cortex_mikado_generate_thumbnail($image['image_id'], null, $image_size[0], $image_size[1]); ?>
						<?php else: ?>
							<?php echo wp_get_attachment_image($image['image_id'], $image_size); ?>
						<?php endif; ?>
					</div>
				<?php if ($pretty_photo || $image['link'] !== '') {?>
					</a>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
</div>