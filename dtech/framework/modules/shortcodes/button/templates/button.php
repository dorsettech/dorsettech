<button type="submit" <?php cortex_mikado_inline_style($button_styles); ?> <?php cortex_mikado_class_attribute($button_classes); ?> <?php echo cortex_mikado_get_inline_attrs($button_data); ?> <?php echo cortex_mikado_get_inline_attrs($button_custom_attrs); ?>>
    <?php if ($text !== '') { ?>
		<?php if($type == 'solid' && $icon == '' && $icon_pack == '') { ?>
			<span class="mkdf-btn-top-shadow"></span>
		<?php } ?>
		<span class="mkdf-btn-text"><?php echo esc_html($text); ?></span>
		<?php if($type == 'solid' && $icon == '' && $icon_pack == '') { ?>
			<span class="mkdf-btn-bottom-shadow"></span>
		<?php } ?>
	<?php } ?>
    <?php if ($icon !== '' && $icon_pack !== '') { ?>
		<span class="mkdf-btn-icon-holder" <?php cortex_mikado_inline_style($icon_styles);?>>
			<?php echo cortex_mikado_icon_collections()->renderIcon($icon, $icon_pack); ?>
		</span>
	<?php } ?>
</button>