<?php
namespace CortexMikado\Modules\Shortcodes\Button;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;


/**
 * Class Button that represents button shortcode
 * @package CortexMikado\Modules\Shortcodes\Button
 */
class Button implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	/**
	 * Sets base attribute and registers shortcode with Visual Composer
	 */
	public function __construct() {
		$this->base = 'mkdf_button';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base attribute
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Button', 'cortex'),
			'base'                      => $this->base,
			'category'                  => esc_html__('by MIKADO', 'cortex'),
			'icon'                      => 'icon-wpb-button extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array_merge(
				array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Size', 'cortex'),
						'param_name'  => 'size',
						'value'       => array(
							esc_html__('Default','cortex') => '',
							esc_html__('Small', 'cortex')  => 'small',
							esc_html__('Medium', 'cortex') => 'medium',
							esc_html__('Large', 'cortex')  => 'large',
							esc_html__('Extra Large', 'cortex') => 'huge',
							esc_html__('Full Width', 'cortex')  => 'huge-full-width'
						),
						'save_always' => true,
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Type', 'cortex'),
						'param_name'  => 'type',
						'value'       => array(
							esc_html__('Default', 'cortex') => '',
							esc_html__('Transparent', 'cortex') => 'transparent',
							esc_html__('Solid', 'cortex') => 'solid'
						),
						'save_always' => true,
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Text', 'cortex'),
						'param_name'  => 'text',
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Link', 'cortex'),
						'param_name'  => 'link',
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Link Target', 'cortex'),
						'param_name'  => 'target',
						'value'       => array(
							esc_html__('Self', 'cortex')  => '_self',
							esc_html__('Blank', 'cortex') => '_blank'
						),
						'save_always' => true,
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Custom CSS class', 'cortex'),
						'param_name'  => 'custom_class',
					)
				),
				cortex_mikado_icon_collections()->getVCParamsArray(array(), '', true),
				array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Hover Type', 'cortex'),
						'param_name'  => 'hover_type',
						'value'       => array(
							esc_html__('Default', 'cortex')  => '',
							esc_html__('Unveiling', 'cortex') => 'unveiling',
						),
						'dependency' => array('element' => 'type', 'value' => 'transparent'),
						'description' => esc_html__('This hover type will effect only buttons with icon set','cortex')
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Color', 'cortex'),
						'param_name'  => 'color',
						'group'       => esc_html__('Design Options','cortex'),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Hover Color', 'cortex'),
						'param_name'  => 'hover_color',
						'group'       => esc_html__('Design Options','cortex'),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Background Color', 'cortex'),
						'param_name'  => 'background_color',
						'group'       => esc_html__('Design Options','cortex'),
						'dependency'  => array('element' => 'type', 'value' => array('solid',''))
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Hover Background Color', 'cortex'),
						'param_name'  => 'hover_background_color',
						'group'       => esc_html__('Design Options','cortex'),
						'dependency'  => array('element' => 'type', 'value' => array('solid',''))
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Border Color', 'cortex'),
						'param_name'  => 'border_color',
						'group'       => esc_html__('Design Options','cortex'),
						'dependency'  => array('element' => 'type', 'value' => array('solid',''))
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Hover Border Color', 'cortex'),
						'param_name'  => 'hover_border_color',
						'group'       => esc_html__('Design Options','cortex'),
						'dependency'  => array('element' => 'type', 'value' => array('solid',''))
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Font Size (px)', 'cortex'),
						'param_name'  => 'font_size',
						'group'       => esc_html__('Design Options','cortex'),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Font Weight', 'cortex'),
						'param_name'  => 'font_weight',
						'value'       => array_flip(cortex_mikado_get_font_weight_array(true)),
						'group'       => esc_html__('Design Options','cortex'),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Margin', 'cortex'),
						'param_name'  => 'margin',
						'description' => esc_html__('Insert margin in format: 0px 0px 1px 0px', 'cortex'),
						'group'       => esc_html__('Design Options','cortex'),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Icon Color', 'cortex'),
						'param_name'  => 'icon_color',
						'group'       => esc_html__('Design Options','cortex'),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Icon Background Color', 'cortex'),
						'param_name'  => 'icon_background_color',
						'group'       => esc_html__('Design Options','cortex'),
						'dependency'  => array('element' => 'type', 'value' => array('solid',''))
					)
				)
			) //close array_merge
		));
	}

	/**
	 * Renders HTML for button shortcode
	 *
	 * @param array $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function render($atts, $content = null) {
		$default_atts = array(
			'size'                   => '',
			'type'                   => '',
			'text'                   => '',
			'link'                   => '',
			'target'                 => '',
			'color'                  => '',
			'background_color'       => '',
			'border_color'           => '',
			'hover_color'            => '',
			'hover_background_color' => '',
			'hover_border_color'     => '',
			'font_size'              => '',
			'font_weight'            => '',
			'icon_color'			 => '',
			'icon_background_color'	 => '',
			'margin'                 => '',
			'custom_class'           => '',
			'html_type'              => 'anchor',
			'input_name'             => '',
			'hover_type'			 => '',
			'custom_attrs'           => array()
		);

		$default_atts = array_merge($default_atts, cortex_mikado_icon_collections()->getShortcodeParams());
		$params       = shortcode_atts($default_atts, $atts);

        if($params['html_type'] !== 'input') {
        	$icon_attributes = array();

            $iconPackName   = cortex_mikado_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);
            $params['icon'] = $iconPackName ? $params[$iconPackName] : '';

			$icon_attributes['class'] = 'mkdf-btn-icon-holder';
        }

		$params['type'] = !empty($params['type']) ? $params['type'] : 'solid';


		if ($params['type'] == 'solid'){
			$params['size'] = !empty($params['size']) ? $params['size'] : 'large';
		}
		elseif ($params['type'] == 'transparent') {
			$params['size'] = !empty($params['size']) ? $params['size'] : 'medium';
		}


		$params['link']   = !empty($params['link']) ? $params['link'] : '#';
		$params['target'] = !empty($params['target']) ? $params['target'] : '_self';

		//prepare params for template
		$params['button_classes']      = $this->getButtonClasses($params);
		$params['button_custom_attrs'] = !empty($params['custom_attrs']) ? $params['custom_attrs'] : array();
		$params['button_styles']       = $this->getButtonStyles($params);
        $params['button_data']         = $this->getButtonDataAttr($params);
		$params['icon_styles']       = $this->getIconStyle($params);

		return cortex_mikado_get_shortcode_module_template_part('templates/'.$params['html_type'], 'button', '', $params);
	}

	/**
	 * Returns array of button styles
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getButtonStyles($params) {
		$styles = array();

		if(!empty($params['color'])) {
			$styles[] = 'color: '.$params['color'];
		}

		if(!empty($params['background_color']) && $params['type'] !== 'outline') {
			$styles[] = 'background-color: '.$params['background_color'];
		}

		if(!empty($params['border_color'])) {
			$styles[] = 'border: 1px solid '.$params['border_color'];
		}

		if(!empty($params['font_size'])) {
			$styles[] = 'font-size: '.cortex_mikado_filter_px($params['font_size']).'px';
		}

		if(!empty($params['font_weight'])) {
			$styles[] = 'font-weight: '.$params['font_weight'];
		}

		if(!empty($params['margin'])) {
			$styles[] = 'margin: '.$params['margin'];
		}

		return $styles;
	}

    /**
     *
     * Returns array of button data attr
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonDataAttr($params) {
        $data = array();

        if(!empty($params['hover_background_color'])) {
            $data['data-hover-bg-color'] = $params['hover_background_color'];
        }

        if(!empty($params['hover_color'])) {
            $data['data-hover-color'] = $params['hover_color'];
        }

        if(!empty($params['hover_border_color'])) {
            $data['data-hover-border-color'] = $params['hover_border_color'];
        }

        return $data;
    }

	/**
	 * Returns array of HTML classes for button
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getButtonClasses($params) {
		$buttonClasses = array(
			'mkdf-btn',
			'mkdf-btn-'.$params['size'],
			'mkdf-btn-'.$params['type']
		);

		if(!empty($params['icon'])) {
			$buttonClasses[] = 'mkdf-btn-icon';
		}

		if (empty($params['hover_background_color']) && empty($params['icon'])){
			$buttonClasses[] = 'mkdf-btn-bckg-hover';
		}

        if(!empty($params['hover_background_color'])) {
            $buttonClasses[] = 'mkdf-btn-custom-hover-bg';
        }

        if(!empty($params['hover_border_color'])) {
            $buttonClasses[] = 'mkdf-btn-custom-border-hover';
        }

        if(!empty($params['hover_color'])) {
            $buttonClasses[] = 'mkdf-btn-custom-hover-color';
        }

		if(!empty($params['custom_class'])) {
			$buttonClasses[] = $params['custom_class'];
		}

		if ($params['hover_type'] !== ''){
			$buttonClasses[] = 'mkdf-btn-hover-'.$params['hover_type'];
		}

		return $buttonClasses;
	}

	/**
	 * Returns icon style
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getIconStyle($params) {
		$style = '';

		if(!empty($params['icon_background_color'])) {
			$style .= 'background-color: '.$params['icon_background_color'].';';
		}

		if(!empty($params['icon_color'])) {
			$style .= 'color: '.$params['icon_color'];
		}

		return $style;
	}
}