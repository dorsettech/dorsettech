<?php

if(!function_exists('cortex_mikado_button_typography_styles')) {
    /**
     * Typography styles for all button types
     */
    function cortex_mikado_button_typography_styles() {
        $selector = '.mkdf-btn';
        $styles = array();

        $font_family = cortex_mikado_options()->getOptionValue('button_font_family');
        if(cortex_mikado_is_font_option_valid($font_family)) {
            $styles['font-family'] = cortex_mikado_get_font_option_val($font_family);
        }

        $text_transform = cortex_mikado_options()->getOptionValue('button_text_transform');
        if(!empty($text_transform)) {
            $styles['text-transform'] = $text_transform;
        }

        $font_style = cortex_mikado_options()->getOptionValue('button_font_style');
        if(!empty($font_style)) {
            $styles['font-style'] = $font_style;
        }

        $letter_spacing = cortex_mikado_options()->getOptionValue('button_letter_spacing');
        if($letter_spacing !== '') {
            $styles['letter-spacing'] = cortex_mikado_filter_px($letter_spacing).'px';
        }

        $font_weight = cortex_mikado_options()->getOptionValue('button_font_weight');
        if(!empty($font_weight)) {
            $styles['font-weight'] = $font_weight;
        }

        echo cortex_mikado_dynamic_css($selector, $styles);
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_button_typography_styles');
}

if(!function_exists('cortex_mikado_button_solid_styles')) {
    /**
     * Generate styles for solid type buttons
     */
    function cortex_mikado_button_solid_styles() {
        //solid styles
        $solid_selector = '.mkdf-btn.mkdf-btn-solid';
        $solid_icon_selector = '.mkdf-btn.mkdf-btn-solid .mkdf-btn-icon-holder';
        $solid_styles = array();
        $solid_icon_styles = array();

        if(cortex_mikado_options()->getOptionValue('btn_solid_text_color')) {
            $solid_styles['color'] = cortex_mikado_options()->getOptionValue('btn_solid_text_color');
        }

        if(cortex_mikado_options()->getOptionValue('btn_solid_border_color')) {
            $solid_styles['border-color'] = cortex_mikado_options()->getOptionValue('btn_solid_border_color');
            $solid_styles['border-width'] = '1px';
            $solid_styles['border-style'] = 'solid';
        }

        if(cortex_mikado_options()->getOptionValue('btn_solid_bg_color')) {
            $solid_styles['background-color'] = cortex_mikado_options()->getOptionValue('btn_solid_bg_color');
        }

        //solid hover styles
        if(cortex_mikado_options()->getOptionValue('btn_solid_hover_text_color')) {
            echo cortex_mikado_dynamic_css(
                '.mkdf-btn.mkdf-btn-solid:not(.mkdf-btn-custom-hover-color):hover',
                array('color' => cortex_mikado_options()->getOptionValue('btn_solid_hover_text_color').'!important')
            );
        }

        if(cortex_mikado_options()->getOptionValue('btn_solid_hover_bg_color')) {
            echo cortex_mikado_dynamic_css(
                '.mkdf-btn.mkdf-btn-solid.mkdf-btn-bckg-hover:hover',
                array('background-color' => cortex_mikado_options()->getOptionValue('btn_solid_hover_bg_color').'!important')
            );
        }

        if(cortex_mikado_options()->getOptionValue('btn_solid_hover_border_color')) {
            echo cortex_mikado_dynamic_css(
                '.mkdf-btn.mkdf-btn-solid:not(.mkdf-btn-custom-border-hover):hover',
                array('border-color' => cortex_mikado_options()->getOptionValue('btn_solid_hover_border_color').'!important')
            );

			if (!isset($solid_styles['border-width'])){
				$solid_styles['border-width'] = '1px';
				$solid_styles['border-style'] = 'solid';
				$solid_styles['border-color'] = 'transparent';
			}
        }

        if(cortex_mikado_options()->getOptionValue('btn_solid_icon_color')) {
            $solid_icon_styles['color'] = cortex_mikado_options()->getOptionValue('btn_solid_icon_color');
        }

        if(cortex_mikado_options()->getOptionValue('btn_solid_icon_bg_color')) {
            $solid_icon_styles['background-color'] = cortex_mikado_options()->getOptionValue('btn_solid_icon_bg_color');
        }

        echo cortex_mikado_dynamic_css($solid_selector, $solid_styles);
        echo cortex_mikado_dynamic_css($solid_icon_selector, $solid_icon_styles);
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_button_solid_styles');
}


if(!function_exists('cortex_mikado_button_transparent_styles')) {
    /**
     * Generate styles for transparent type buttons
     */
    function cortex_mikado_button_transparent_styles() {

        if(cortex_mikado_options()->getOptionValue('btn_transparent_text_color')) {
            echo cortex_mikado_dynamic_css(
                '.mkdf-btn.mkdf-btn-transparent',
                array('color' => cortex_mikado_options()->getOptionValue('btn_transparent_text_color'))
            );
        }

        //solid hover styles
        if(cortex_mikado_options()->getOptionValue('btn_transparent_hover_text_color')) {
            echo cortex_mikado_dynamic_css(
                '.mkdf-btn.mkdf-btn-transparent:not(.mkdf-btn-custom-hover-color):hover',
                array('color' => cortex_mikado_options()->getOptionValue('btn_transparent_hover_text_color').'!important')
            );
        }

        if(cortex_mikado_options()->getOptionValue('btn_transparent_icon_color')) {
            echo cortex_mikado_dynamic_css(
                '.mkdf-btn.mkdf-btn-transparent .mkdf-btn-icon-holder',
                array('color' => cortex_mikado_options()->getOptionValue('btn_transparent_icon_color'))
            );
        }
    }

    add_action('cortex_mikado_style_dynamic', 'cortex_mikado_button_transparent_styles');
}