<?php

if(!function_exists('cortex_mikado_button_map')) {
    function cortex_mikado_button_map() {
        $panel = cortex_mikado_add_admin_panel(array(
            'title' => esc_html__('Button', 'cortex'),
            'name'  => 'panel_button',
            'page'  => '_elements_page'
        ));

        //Typography options
        cortex_mikado_add_admin_section_title(array(
            'name' => 'typography_section_title',
            'title' => esc_html__('Typography', 'cortex'),
            'parent' => $panel
        ));

        $typography_group = cortex_mikado_add_admin_group(array(
            'name' => 'typography_group',
            'title' => esc_html__('Typography', 'cortex'),
            'description' => esc_html__('Setup typography for all button types', 'cortex'),
            'parent' => $panel
        ));

        $typography_row = cortex_mikado_add_admin_row(array(
            'name' => 'typography_row',
            'next' => true,
            'parent' => $typography_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'fontsimple',
            'name'          => 'button_font_family',
            'default_value' => '',
            'label'         => esc_html__('Font Family', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_text_transform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'cortex'),
            'options'       => cortex_mikado_get_text_transform_array()
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_font_style',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'cortex'),
            'options'       => cortex_mikado_get_font_style_array()
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'textsimple',
            'name'          => 'button_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'cortex'),
            'args'          => array(
                'suffix' => 'px'
            )
        ));

        $typography_row2 = cortex_mikado_add_admin_row(array(
            'name' => 'typography_row2',
            'next' => true,
            'parent' => $typography_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $typography_row2,
            'type'          => 'selectsimple',
            'name'          => 'button_font_weight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'cortex'),
            'options'       => cortex_mikado_get_font_weight_array()
        ));

        //Outline type options
        cortex_mikado_add_admin_section_title(array(
            'name' => 'type_section_title',
            'title' => esc_html__('Types', 'cortex'),
            'parent' => $panel
        ));

        //Solid type options
        $solid_group = cortex_mikado_add_admin_group(array(
            'name' => 'solid_group',
            'title' => esc_html__('Solid Type', 'cortex'),
            'description' => esc_html__('Setup solid button type', 'cortex'),
            'parent' => $panel
        ));

        $solid_row = cortex_mikado_add_admin_row(array(
            'name' => 'solid_row',
            'next' => true,
            'parent' => $solid_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color','cortex')
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Background Color', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Background Color','cortex')
        ));


        $solid_row2 = cortex_mikado_add_admin_row(array(
            'name' => 'solid_row2',
            'next' => true,
            'parent' => $solid_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color','cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_icon_color',
            'default_value' => '',
            'label'         => esc_html__('Icon Color', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_icon_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Icon Background Color', 'cortex'),
        ));

        //Transparent type options
        $transparent_group = cortex_mikado_add_admin_group(array(
            'name' => 'transparent_group',
            'title' => esc_html__('Transparent Type', 'cortex'),
            'description' => esc_html__('Setup transparent button type', 'cortex'),
            'parent' => $panel
        ));

        $transparent_row = cortex_mikado_add_admin_row(array(
            'name' => 'transparent_row',
            'next' => true,
            'parent' => $transparent_group
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $transparent_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_transparent_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $transparent_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_transparent_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color','cortex'),
        ));

        cortex_mikado_add_admin_field(array(
            'parent'        => $transparent_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_transparent_icon_color',
            'default_value' => '',
            'label'         => esc_html__('Icon Color', 'cortex'),
        ));

    }

    add_action('cortex_mikado_options_elements_map', 'cortex_mikado_button_map');
}