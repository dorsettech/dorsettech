<?php
/**
 * Parallax Call To Action shortcode template
 */
?>
<div class="mkdf-parallax-call-to-action">
	<div class="mkdf-pcta-content-holder" <?php cortex_mikado_inline_style($text_style); ?>>
		<div class="mkdf-pcta-title-holder">
			<h2 class="mkdf-pcta-title"><?php echo esc_attr($title); ?></h2>
		</div>
		<span class="mkdf-pcta-separator"></span>
		<div class="mkdf-pcta-text-holder">
			<p class="mkdf-pcta-text"><?php echo esc_attr($text); ?></p>
		</div>
		<div class="mkdf-pcta-button-holder"><?php echo cortex_mikado_get_button_html($button_params); ?></div>
	</div>
	<div class="mkdf-pcta-images-holder">
		<img class="mkdf-main-image" src="<?php echo wp_get_attachment_url($main_image); ?>" alt="<?php echo get_the_title($main_image) ?>"/>
		<img class="mkdf-additional-image" src="<?php echo wp_get_attachment_url($additional_image); ?>" alt="<?php echo get_the_title($additional_image) ?>"/>
	</div>
</div>
