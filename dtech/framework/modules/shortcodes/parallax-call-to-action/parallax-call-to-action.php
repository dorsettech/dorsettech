<?php
namespace CortexMikado\Modules\Shortcodes\ParallaxCallToAction;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class ParallaxCallToAction
 */
class ParallaxCallToAction implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'mkdf_parallax_call_to_action';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see mkd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		vc_map( array(
				'name' => esc_html__('Parallax Call To Action', 'cortex'),
				'base' => $this->getBase(),
				'category' => esc_html__('by MIKADO','cortex'),
				'icon' => 'icon-wpb-call-to-action extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array(
					array(
						"type" => "attach_image",
						"heading" => esc_html__("Main Image", 'cortex'),
						"param_name" => "main_image",
					),
					array(
						"type" => "attach_image",
						"heading" => esc_html__("Additional Image", 'cortex'),
						"param_name" => "additional_image",
						'dependency'  => array('element' => 'main_image', 'not_empty' => true)
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'cortex'),
						"param_name" => "title",
						'admin_label' => true,
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Text", 'cortex'),
						"param_name" => "text",
					),
					array(
						"type" => "colorpicker",
						"heading" => esc_html__("Text Color", 'cortex'),
						"param_name" => "text_color",
                        "group" => esc_html__("Design Options", 'cortex'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Button Text', 'cortex'),
						'param_name' => 'button_text',
						'description' => esc_html__('Default text is "Read More"', 'cortex'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Button Link', 'cortex'),
						'param_name' => 'button_link',
						'admin_label' => true,
						'dependency' => array('element' => 'button_text', 'not_empty' => true)
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Link Target', 'cortex'),
						'param_name' => 'link_target',
						'value' => array(
							esc_html__('Self', 'cortex') => '_self',
							esc_html__('Blank', 'cortex') => '_blank'
						),
						'dependency' => array('element' => 'button_text', 'not_empty' => true)
					),

				)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'main_image' => '',
			'additional_image' => '',
			'title' => '',
			'text' => '',
			'text_color' => '',
			'button_text' => 'Read More',
			'button_link' => '',
			'link_target' => '',

		);

		$params = shortcode_atts($args, $atts);

		$params['text_style'] = $this->getTextStyle($params);
		$params['button_params'] = $this->getButtonParameters($params);

		//Get HTML from template
		$html = cortex_mikado_get_shortcode_module_template_part('templates/parallax-call-to-action-template', 'parallax-call-to-action', '', $params);

		return $html;

	}

	private function getTextStyle($params) {
		$style = array();

		if ($params['text_color'] !== '') {
			$style[] = 'color: '.$params['text_color'];
		}

		return implode(';', $style);
	}

	private function getButtonParameters($params) {
		$button_params_array = array();
		
		if(!empty($params['button_link'])) {
			$button_params_array['link'] = $params['button_link'];
		}

		if(!empty($params['link_target'])) {
			$button_params_array['target'] = $params['link_target'];
		}
		
		if(!empty($params['button_text'])) {
			$button_params_array['text'] = $params['button_text'];
		}
		
		return $button_params_array;
	}

	
}