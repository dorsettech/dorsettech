<?php
namespace CortexMikado\Modules\Shortcodes\PricingTable;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTable implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'mkdf_pricing_table';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Pricing Table', 'cortex'),
			'base' => $this->base,
			'icon' => 'icon-wpb-pricing-table extended-custom-icon',
			'category' => esc_html__('by MIKADO', 'cortex'),
			'allowed_container_element' => 'vc_row',
			'as_child' => array('only' => 'mkdf_pricing_tables'),
			'params' => array(
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Title', 'cortex'),
					'param_name' => 'title',
					'value' => 'Basic Plan',
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Price', 'cortex'),
					'param_name' => 'price',
					'description' => esc_html__('Default value is 100', 'cortex'),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Currency', 'cortex'),
					'param_name' => 'currency',
					'description' => esc_html__('Default mark is $', 'cortex'),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Price Period', 'cortex'),
					'param_name' => 'price_period',
					'description' => esc_html__('Default label is "per month"', 'cortex')
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Show Button', 'cortex'),
					'param_name' => 'show_button',
					'value' => array(
						esc_html__('Default', 'cortex') => '',
						esc_html__('Yes', 'cortex') => 'yes',
						esc_html__('No', 'cortex') => 'no'
					),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Button Text', 'cortex'),
					'param_name' => 'button_text',
					'dependency' => array('element' => 'show_button',  'value' => 'yes') 
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Button Link', 'cortex'),
					'param_name' => 'link',
					'dependency' => array('element' => 'show_button',  'value' => 'yes')
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Active', 'cortex'),
					'param_name' => 'active',
					'value' => array(
						esc_html__('No', 'cortex') => 'no',
						esc_html__('Yes', 'cortex') => 'yes'
					),
					'save_always' => true,
				),
				array(
					'type' => 'textarea_html',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Content', 'cortex'),
					'param_name' => 'content',
					'value' => '<li>content content content</li><li>content content content</li><li>content content content</li>',
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'title'         			   => 'Basic Plan',
			'price'         			   => '100',
			'currency'      			   => '$',
			'price_period'  			   => 'Per Month',
			'active'        			   => 'no',
			'show_button'				   => 'yes',
			'link'          			   => '',
			'button_text'   			   => 'button'
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html						= '';
		$pricing_table_clasess		= 'mkdf-price-table';
		
		if($active == 'yes') {
			$pricing_table_clasess .= ' mkdf-active';
		}
		
		$params['pricing_table_classes'] = $pricing_table_clasess;
        $params['content'] = preg_replace('#^<\/p>|<p>$#', '', $content);
		
		$html .= cortex_mikado_get_shortcode_module_template_part('templates/pricing-table-template','pricing-table', '', $params);
		return $html;

	}

}
