<div <?php cortex_mikado_class_attribute($pricing_table_classes)?>>
	<div class="mkdf-price-table-inner">
		<ul>
			<li class="mkdf-table-title">
				<h5 class="mkdf-title-content"><?php echo esc_html($title) ?></h5>
			</li>
			<li class="mkdf-table-prices">
				<div class="mkdf-price-in-table">
					<span class="mkdf-price-holder">
						<sup class="mkdf-value"><?php echo esc_attr($currency) ?></sup>
						<span class="mkdf-price"><?php echo esc_attr($price)?></span>
					</span>
					<span class="mkdf-mark"><?php echo esc_attr($price_period)?></span>
				</div>	
			</li>
			<li class="mkdf-table-content">
				<?php
					echo do_shortcode($content);
				?>
			</li>
			<?php 
			if($show_button == "yes" && $button_text !== ''){ ?>
				<li class="mkdf-price-button">
					<?php echo cortex_mikado_get_button_html(array(
						'size' => 'small',
						'link' => $link,
						'text' => $button_text,
						'custom_class' => 'mkdf-btn-solid-no-shadow'
					)); ?>
				</li>				
			<?php } ?>
		</ul>
	</div>
</div>