<?php
namespace CortexMikado\Modules\Shortcodes\PricingTables;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTables implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'mkdf_pricing_tables';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {

		vc_map( array(
				'name' => esc_html__('Pricing Tables', 'cortex'),
				'base' => $this->base,
				'as_parent' => array('only' => 'mkdf_pricing_table'),
				'content_element' => true,
				'category' => esc_html__('by MIKADO', 'cortex'),
				'icon' => 'icon-wpb-pricing-tables extended-custom-icon',
				'show_settings_on_create' => true,
				'params' => array(
					array(
						'type' => 'dropdown',
						'admin_label' => true,
						'heading' => esc_html__('Columns', 'cortex'),
						'param_name' => 'columns',
						'value' => array(
							esc_html__('Two' , 'cortex') => 'mkdf-two-columns',
							esc_html__('Three', 'cortex') => 'mkdf-three-columns',
							esc_html__('Four','cortex') => 'mkdf-four-columns',
						),
						'save_always' => true,
					)
				),
				'js_view' => 'VcColumnView'
		) );
	}

	public function render($atts, $content = null) {
		$args = array(
			'columns'         => 'mkdf-two-columns'
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);
		
		$html = '<div class="mkdf-pricing-tables clearfix '.$columns.'">';
		$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;
	}
}