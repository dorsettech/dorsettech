<?php
/**
 * Counter shortcode template
 */
?>
<div class="mkdf-countdown" id="countdown<?php echo esc_html($id); ?>" <?php echo esc_attr($data_array);?>></div>