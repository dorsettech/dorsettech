<?php
namespace CortexMikado\Modules\Shortcodes\ElementsHolder;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class ElementsHolder implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'mkdf_elements_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elements Holder', 'cortex'),
			'base' => $this->base,
			'icon' => 'icon-wpb-elements-holder extended-custom-icon',
			'category' => esc_html__('by MIKADO', 'cortex'),
			'as_parent' => array('only' => 'mkdf_elements_holder_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'colorpicker',
					'class' => '',
					'heading' => esc_html__('Background Color', 'cortex'),
					'param_name' => 'background_color',
					'value' => '',
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Columns', 'cortex'),
					'admin_label' => true,
					'param_name' => 'number_of_columns',
					'value' => array(
						esc_html__('1 Column', 'cortex')   => 'one-column',
						esc_html__('2 Columns', 'cortex')  => 'two-columns',
						esc_html__('3 Columns', 'cortex')  => 'three-columns',
						esc_html__('4 Columns', 'cortex')  => 'four-columns',
						esc_html__('5 Columns', 'cortex')  => 'five-columns',
						esc_html__('6 Columns','cortex')   => 'six-columns'
					),
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__('Items Float Left', 'cortex'),
					'param_name' => 'items_float_left',
					'value' => array(esc_html__('Make Items Float Left?','cortex') => 'yes'),
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => esc_html__('Width & Responsiveness','cortex'),
					'heading' => esc_html__('Switch to One Column', 'cortex'),
					'param_name' => 'switch_to_one_column',
					'value' => array(
						esc_html__('Default','cortex')    		=> '',
						esc_html__('Below 1280px', 'cortex') 	=> '1280',
						esc_html__('Below 1024px', 'cortex')   => '1024',
						esc_html__('Below 768px', 'cortex')    => '768',
						esc_html__('Below 600px', 'cortex')   	=> '600',
						esc_html__('Below 480px', 'cortex')   	=> '480',
						esc_html__('Never', 'cortex')    		=> 'never'
					),
					'description' => esc_html__('Choose on which stage item will be in one column', 'cortex'),
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => esc_html__('Width & Responsiveness','cortex'),
					'heading' => esc_html__('Choose Alignment In Responsive Mode', 'cortex'),
					'param_name' => 'alignment_one_column',
					'value' => array(
						esc_html__('Default','cortex') => '',
						esc_html__('Left', 'cortex') 	=> 'left',
						esc_html__('Center','cortex')  => 'center',
						esc_html__('Right', 'cortex')  => 'right'
					),
					'description' => esc_html__('Alignment When Items are in One Column', 'cortex')
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'number_of_columns' 		=> '',
			'switch_to_one_column'		=> '',
			'alignment_one_column' 		=> '',
			'items_float_left' 			=> '',
			'background_color' 			=> ''
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html						= '';

		$elements_holder_classes = array();
		$elements_holder_classes[] = 'mkdf-elements-holder';
		$elements_holder_style = '';

		if($number_of_columns != ''){
			$elements_holder_classes[] .= 'mkdf-'.$number_of_columns ;
		}

		if($switch_to_one_column != ''){
			$elements_holder_classes[] = 'mkdf-responsive-mode-' . $switch_to_one_column ;
		} else {
			$elements_holder_classes[] = 'mkdf-responsive-mode-768' ;
		}

		if($alignment_one_column != ''){
			$elements_holder_classes[] = 'mkdf-one-column-alignment-' . $alignment_one_column ;
		}

		if($items_float_left !== ''){
			$elements_holder_classes[] = 'mkdf-elements-items-float';
		}

		if($background_color != ''){
			$elements_holder_style .= 'background-color:'. $background_color . ';';
		}

		$elements_holder_class = implode(' ', $elements_holder_classes);

		$html .= '<div ' . cortex_mikado_get_class_attribute($elements_holder_class) . ' ' . cortex_mikado_get_inline_attr($elements_holder_style, 'style'). '>';
			$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;

	}

}
