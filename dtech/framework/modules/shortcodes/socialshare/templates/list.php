<div class="mkdf-social-share-holder mkdf-list">
	<span class="mkdf-social-share-title"><?php esc_html_e('Share', 'cortex'); ?></span>
	<ul>
		<?php foreach ($networks as $net) {
			print $net;
		} ?>
	</ul>
</div>