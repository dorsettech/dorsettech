<div <?php cortex_mikado_class_attribute($progress_bar_classes);?>>
	<<?php echo esc_attr($title_tag);?> class="mkdf-progress-title-holder clearfix">
		<span class="mkdf-progress-title"><?php echo esc_attr($title)?></span>
		<span class="mkdf-progress-number-wrapper <?php echo esc_attr($percentage_classes)?> " >
			<span class="mkdf-progress-number">
				<span class="mkdf-percent">0</span>
			</span>
		</span>
	</<?php echo esc_attr($title_tag)?>>
	<div class="mkdf-progress-content-outer" <?php echo cortex_mikado_get_inline_style($inactive_bar_style); ?>>
		<div data-percentage=<?php echo esc_attr($percent)?> class="mkdf-progress-content" <?php echo cortex_mikado_get_inline_style($active_bar_style); ?> ></div>
	</div>
</div>	