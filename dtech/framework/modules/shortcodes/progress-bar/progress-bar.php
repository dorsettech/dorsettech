<?php
namespace CortexMikado\Modules\Shortcodes\ProgressBar;

use CortexMikado\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProgressBar implements ShortcodeInterface{
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_progress_bar';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Progress Bar', 'cortex'),
			'base' => $this->base,
			'icon' => 'icon-wpb-progress-bar extended-custom-icon',
			'category' => esc_html__('by MIKADO', 'cortex'),
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Title', 'cortex'),
					'param_name' => 'title',
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Skin','cortex'),
					'param_name' => 'skin',
					'value' => array(
						esc_html__('Dark','cortex') => 'dark',
						esc_html__('Light','cortex') => 'light',
					)
				),
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => esc_html__('Title Tag', 'cortex'),
					'param_name' => 'title_tag',
					'value' => array(
						''   => '',
						'h2' => 'h2',
						'h3' => 'h3',
						'h4' => 'h4',	
						'h5' => 'h5',	
						'h6' => 'h6',	
					),
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Percentage', 'cortex'),
					'param_name' => 'percent',
				),	
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Percentage Type', 'cortex'),
					'param_name' => 'percentage_type',
					'value' => array(
						esc_html__('Floating', 'cortex')  => 'floating',
						esc_html__('Static', 'cortex') => 'static'
					),
					'dependency' => Array('element' => 'percent', 'not_empty' => true)
				),
                array(
                    'type'        => 'colorpicker',
                    'admin_label' => true,
                    'heading'     => esc_html__('Active Bar Color', 'cortex'),
                    'param_name'  => 'active_bar_color',
                    'description' => ''
                ),
                array(
                    'type'        => 'colorpicker',
                    'admin_label' => true,
                    'heading'     => esc_html__('Inactive Bar Color', 'cortex'),
                    'param_name'  => 'inactive_bar_color',
                    'description' => ''
                ),
			)
		) );

	}

	public function render($atts, $content = null) {
		$args = array(
            'title' => '',
            'title_tag' => 'h6',
            'percent' => '100',
            'percentage_type' => 'floating',
            'skin' => 'dark',
            'active_bar_color' => '',
            'inactive_bar_color' => ''
        );
		$params = shortcode_atts($args, $atts);

		//Extract params for use in method
		extract($params);
		$headings_array = array('h2', 'h3', 'h4', 'h5', 'h6');

        //get correct heading value. If provided heading isn't valid get the default one
        $title_tag = (in_array($title_tag, $headings_array)) ? $title_tag : $args['title_tag'];

        $params['active_bar_style'] = $this->getActiveBarStyle($params);
        $params['inactive_bar_style'] = $this->getInactiveBarStyle($params);
		$params['percentage_classes'] = $this->getPercentageClasses($params);
		$params['progress_bar_classes'] = $this->getProgressBarClasses($params);

        //init variables
		$html = cortex_mikado_get_shortcode_module_template_part('templates/progress-bar-template', 'progress-bar', '', $params);
		
        return $html;
		
	}
	/**
    * Generates css classes for progress bar
    *
    * @param $params
    *
    * @return array
    */
	private function getPercentageClasses($params){
		
		$percentClassesArray = array();
		
		if(!empty($params['percentage_type']) !=''){
			
			if($params['percentage_type'] == 'floating'){
				
				$percentClassesArray[]= 'mkdf-floating';


			}
			elseif($params['percentage_type'] == 'static'){
				
				$percentClassesArray[] = 'mkdf-static';
				
			}
		}
		return implode(' ', $percentClassesArray);
	}

	/**
	 * Generates css classes for Progress Bar
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getProgressBarClasses($params){

		$progress_bar_classes = array();
		$progress_bar_classes[] = 'mkdf-progress-bar';


		if($params['skin'] !== ''){
			$progress_bar_classes[] = 'mkdf-progress-bar-'.$params['skin'];
		}

		return implode(' ', $progress_bar_classes);
	}

    private function getActiveBarStyle($params) {
        $style = array();

        if($params['active_bar_color'] !== '') {
            $style[] = 'background-color: '.$params['active_bar_color'];
        }

        return $style;
    }

    private function getInactiveBarStyle($params) {
        $style = array();

        if($params['inactive_bar_color'] !== '') {
            $style[] = 'background-color: '.$params['inactive_bar_color'];
        }

        return $style;
    }
}