<div class="mkdf-blog-carousel-item">
	<?php if($show_image == 'yes' && has_post_thumbnail()) { ?>
		<div class="mkdf-blog-slide-image">
			<a href="<?php the_permalink(); ?>">
				<?php
					if ($image_size != 'custom') {
						the_post_thumbnail($image_size);
					} else {
						print cortex_mikado_generate_thumbnail(get_post_thumbnail_id(get_the_ID()), null, $image_width, $image_height);
					}
                ?>
			</a>
		</div>
	<?php } ?>
	<div class="mkdf-blog-slide-info-holder clearfix">
		<div class="mkdf-item-info-section">
			<?php cortex_mikado_post_info(array(
				'category' => 'yes'
			)) ?>
			<?php cortex_mikado_post_info(array(
				'date' => 'yes',
			)) ?>
		</div>

		<h3 class="mkdf-blog-slide-title">
			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		</h3>

		<?php if ($text_length != '0') {
			$excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>
			<p class="mkdf-blog-slide-excerpt"><?php echo esc_html($excerpt)?>...</p>
		<?php } ?>

		<?php cortex_mikado_post_info(array(
			'author' => 'yes'
		)) ?>
	</div>
</div>