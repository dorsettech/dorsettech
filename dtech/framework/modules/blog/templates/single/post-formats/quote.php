<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="mkdf-post-content">
		<div class="mkdf-post-text">
			<div class="mkdf-post-text-inner clearfix">
				<div class="mkdf-post-mark mkdf-quote-mark">
					<span class="mkdf-quote-mark-inner"><span class="icon_quotations"></span></span>
				</div>
				<h4 class="mkdf-post-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo esc_html(get_post_meta(get_the_ID(), "mkdf_post_quote_text_meta", true)); ?></a>
				</h4>
				<span class="mkdf-quote-author"><?php the_title(); ?></span>
			</div>
		</div>

		<?php the_content(); ?>

		<?php cortex_mikado_get_module_template_part('templates/lists/parts/pages-navigation', 'blog');  ?>
		
		<div class="mkdf-post-info-bottom">
			<div class="mkdf-post-info-bottom-left">
				<?php cortex_mikado_post_info(array(
					'date' => 'yes',
					'author' => 'yes',
					'category' => 'yes',
				)) ?>
			</div>
			<div class="mkdf-post-info-bottom-right">
				<?php cortex_mikado_post_info(array(
					'comments' => 'yes',
					'like' => 'yes',
				), 'single') ?>
			</div>
		</div>
	</div>
</article>