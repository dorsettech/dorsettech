<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="mkdf-post-content">
		<div class="mkdf-post-text">
			<div class="mkdf-post-text-inner clearfix">
				<div class="mkdf-post-mark mkdf-link-mark">
					<span class="icon_link"></span>
				</div>
				<?php cortex_mikado_get_module_template_part('templates/single/parts/link', 'blog'); ?>
				<span class="mkdf-post-link-title">
					<a href="<?php echo esc_url(get_post_meta(get_the_ID(), "mkdf_post_link_link_meta", true)); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</span>
			</div>
		</div>

		<?php the_content(); ?>

		<?php cortex_mikado_get_module_template_part('templates/lists/parts/pages-navigation', 'blog');  ?>
		
		<div class="mkdf-post-info-bottom">
			<div class="mkdf-post-info-bottom-left">
				<?php cortex_mikado_post_info(array(
					'date' => 'yes',
					'author' => 'yes',
					'category' => 'yes',
				)) ?>
			</div>
			<div class="mkdf-post-info-bottom-right">
				<?php cortex_mikado_post_info(array(
					'comments' => 'yes',
					'like' => 'yes',
				), 'single') ?>
			</div>
		</div>
	</div>
</article>