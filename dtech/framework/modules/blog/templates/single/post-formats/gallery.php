<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="mkdf-post-content">
		<?php cortex_mikado_get_module_template_part('templates/single/parts/gallery', 'blog'); ?>
		<div class="mkdf-post-text">
			<div class="mkdf-post-text-inner clearfix">
				<?php cortex_mikado_get_module_template_part('templates/single/parts/title', 'blog'); ?>

				<?php the_content(); ?>

				<?php cortex_mikado_get_module_template_part('templates/lists/parts/pages-navigation', 'blog');  ?>
				
				<div class="mkdf-post-info-bottom">
					<div class="mkdf-post-info-bottom-left">
						<?php cortex_mikado_post_info(array(
							'date' => 'yes',
							'author' => 'yes',
							'category' => 'yes',
						)) ?>
					</div>
					<div class="mkdf-post-info-bottom-right">
						<?php cortex_mikado_post_info(array(
							'comments' => 'yes',
							'like' => 'yes',
						), 'single') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php do_action('cortex_mikado_before_blog_article_closed_tag'); ?>
</article>