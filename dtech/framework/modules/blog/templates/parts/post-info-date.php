<div class="mkdf-post-info-date">
	<?php if(!cortex_mikado_post_has_title()) { ?><a href="<?php the_permalink() ?>"><?php } ?>
		<?php the_time(get_option('date_format')); ?>
	<?php if(!cortex_mikado_post_has_title()) { ?></a><?php } ?>
</div>