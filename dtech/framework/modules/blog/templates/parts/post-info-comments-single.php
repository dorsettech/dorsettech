<div class="mkdf-post-info-comments-holder">
	<a class="mkdf-post-info-comments" href="<?php comments_link(); ?>" target="_self">
		<span class="icon_comment_alt"></span><?php comments_number(esc_html__('No Comments','cortex'), '1 '.esc_html__('Comment','cortex'), '% '.esc_html__('Comments','cortex')); ?></a></div>