<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="mkdf-post-content">
		<div class="mkdf-post-text">
			<div class="mkdf-post-text-inner">
				<div class="mkdf-post-mark mkdf-quote-mark">
					<span class="mkdf-quote-mark-inner"><span class="icon_quotations"></span></span>
				</div>
				<h6 class="mkdf-post-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo esc_html(get_post_meta(get_the_ID(), "mkdf_post_quote_text_meta", true)); ?></a>
				</h6>
				<span class="mkdf-quote-author"><?php the_title(); ?></span>
			</div>
		</div>
	</div>
</article>