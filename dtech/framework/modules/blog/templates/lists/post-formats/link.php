<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a class="mkdf-post-whole-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a>
	<div class="mkdf-post-content">
		<div class="mkdf-post-text">
			<div class="mkdf-post-text-inner">
				<div class="mkdf-post-mark mkdf-link-mark">
					<span class="icon_link"></span>
				</div>
				<?php cortex_mikado_get_module_template_part('templates/lists/parts/link', 'blog'); ?>
				<span class="mkdf-post-link-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</span>
			</div>
		</div>
	</div>
</article>