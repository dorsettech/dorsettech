<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="mkdf-post-content">
		<div class="mkdf-post-image">
			<?php cortex_mikado_get_module_template_part('templates/parts/video', 'blog'); ?>
		</div>
		<div class="mkdf-post-text">
			<div class="mkdf-post-text-inner">
				<?php cortex_mikado_get_module_template_part('templates/lists/parts/title', 'blog'); ?>

				<?php  if ($type == 'standard-whole-post') {
					the_content();
				}
				else{
					cortex_mikado_excerpt($excerpt_length);
				}
				?>
				<?php cortex_mikado_get_module_template_part('templates/lists/parts/pages-navigation', 'blog');  ?>

				<div class="mkdf-post-info-bottom">
					<div class="mkdf-post-info-bottom-left">
						<?php cortex_mikado_post_info(array(
							'date' => 'yes',
							'author' => $show_author,
							'category' => 'yes',
							'comments' => 'yes',
							'like' => 'yes'
						)) ?>
					</div>
					<div class="mkdf-post-info-bottom-right">
						<?php
						if (shortcode_exists('mkdf_button')) {
							echo cortex_mikado_get_button_html(array(
								'type' => 'transparent',
								'size' => 'large',
								'link' => get_the_permalink(),
								'text' => esc_html__('Read More', 'cortex'),
								'custom_class' => 'mkdf-blog-btn-read-more'
							));
						} else { ?>
							<a href="<?php echo get_the_permalink();?>" target="_self" class="mkdf-btn mkdf-btn-large mkdf-btn-transparent mkdf-btn-bckg-hover mkdf-blog-btn-read-more">
								<span class="mkdf-btn-text"><?php esc_html_e('Read More', 'cortex');?></span>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>