<?php

if ( ! function_exists('cortex_mikado_blog_options_map') ) {

	function cortex_mikado_blog_options_map() {

		cortex_mikado_add_admin_page(
			array(
				'slug' => '_blog_page',
				'title' => esc_html__('Blog','cortex'),
				'icon' => 'fa fa-files-o'
			)
		);

		/**
		 * Blog Lists
		 */

		$custom_sidebars = cortex_mikado_get_custom_sidebars();

		$panel_blog_lists = cortex_mikado_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_lists',
				'title' => esc_html__('Blog Lists','cortex')
			)
		);

		cortex_mikado_add_admin_field(array(
			'name'        => 'blog_list_type',
			'type'        => 'select',
			'label'       => esc_html__('Blog Layout for Archive Pages','cortex'),
			'description' => esc_html__('Choose a default blog layout','cortex'),
			'default_value' => 'standard',
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'standard'				=> esc_html__('Blog: Standard','cortex'),
				'masonry' 				=> esc_html__('Blog: Masonry','cortex'),
				'masonry-full-width' 	=> esc_html__('Blog: Masonry Full Width','cortex'),
				'standard-whole-post' 	=> esc_html__('Blog: Standard Whole Post','cortex')
			)
		));

		cortex_mikado_add_admin_field(array(
			'name'        => 'archive_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Archive and Category Sidebar','cortex'),
			'description' => esc_html__('Choose a sidebar layout for archived Blog Post Lists and Category Blog Lists','cortex'),
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'default'			=> esc_html__('No Sidebar','cortex'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right','cortex'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right','cortex'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left','cortex'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left','cortex')
			)
		));


		if(count($custom_sidebars) > 0) {
			cortex_mikado_add_admin_field(array(
				'name' => 'blog_custom_sidebar',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display','cortex'),
				'description' => esc_html__('Choose a sidebar to display on Blog Post Lists and Category Blog Lists. Default sidebar is "Sidebar Page"','cortex'),
				'parent' => $panel_blog_lists,
				'options' => cortex_mikado_get_custom_sidebars()
			));
		}

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'pagination',
				'default_value' => 'yes',
				'label' => esc_html__('Pagination','cortex'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display pagination links on bottom of Blog Post List','cortex'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_mkdf_pagination_container'
				)
			)
		);

		$pagination_container = cortex_mikado_add_admin_container(
			array(
				'name' => 'mkdf_pagination_container',
				'hidden_property' => 'pagination',
				'hidden_value' => 'no',
				'parent' => $panel_blog_lists,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'parent' => $pagination_container,
				'type' => 'text',
				'name' => 'blog_page_range',
				'default_value' => '',
				'label' => esc_html__('Pagination Range limit','cortex'),
				'description' => esc_html__('Enter a number that will limit pagination to a certain range of links','cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		cortex_mikado_add_admin_field(array(
			'name'        => 'masonry_pagination',
			'type'        => 'select',
			'label'       => esc_html__('Pagination on Masonry','cortex'),
			'description' => esc_html__('Choose a pagination style for Masonry Blog List','cortex'),
			'parent'      => $pagination_container,
			'options'     => array(
				'standard'			=> esc_html__('Standard','cortex'),
				'load-more'			=> esc_html__('Load More','cortex'),
				'infinite-scroll' 	=> esc_html__('Infinite Scroll','cortex'),
			),
			
		));
		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'enable_load_more_pag',
				'default_value' => 'no',
				'label' => esc_html__('Load More Pagination on Other Lists','cortex'),
				'parent' => $pagination_container,
				'description' => esc_html__('Enable Load More Pagination on other lists','cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'masonry_filter',
				'default_value' => 'no',
				'label' => esc_html__('Masonry Filter','cortex'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display category filter on Masonry and Masonry Full Width Templates','cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);		
		cortex_mikado_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Number of Words in Excerpt','cortex'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)','cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		cortex_mikado_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'standard_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Standard Type Number of Words in Excerpt','cortex'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)','cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		cortex_mikado_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'masonry_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Masonry Type Number of Words in Excerpt','cortex'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)','cortex'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		/**
		 * Blog Single
		 */
		$panel_blog_single = cortex_mikado_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_single',
				'title' => esc_html__('Blog Single','cortex'),
			)
		);


		cortex_mikado_add_admin_field(array(
			'name'        => 'blog_single_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Sidebar Layout','cortex'),
			'description' => esc_html__('Choose a sidebar layout for Blog Single pages','cortex'),
			'parent'      => $panel_blog_single,
			'options'     => array(
				'default'			=> esc_html__('No Sidebar','cortex'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right','cortex'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right','cortex'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left','cortex'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left','cortex')
			),
			'default_value'	=> 'default'
		));


		if(count($custom_sidebars) > 0) {
			cortex_mikado_add_admin_field(array(
				'name' => 'blog_single_custom_sidebar',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display','cortex'),
				'description' => esc_html__('Choose a sidebar to display on Blog Single pages. Default sidebar is "Sidebar"','cortex'),
				'parent' => $panel_blog_single,
				'options' => cortex_mikado_get_custom_sidebars()
			));
		}

		cortex_mikado_add_admin_field(array(
			'name'			=> 'blog_single_related_posts',
			'type'			=> 'yesno',
			'label'			=> esc_html__('Show Related Posts','cortex'),
			'description'   => esc_html__('Enabling this option will show related posts on your single post.','cortex'),
			'parent'        => $panel_blog_single,
			'default_value' => 'no'
		));

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_single_navigation',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Prev/Next Single Post Navigation Links','cortex'),
				'parent' => $panel_blog_single,
				'description' => esc_html__('Enable navigation links through the blog posts (left and right arrows will appear)','cortex'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_mkdf_blog_single_navigation_container'
				)
			)
		);

		$blog_single_navigation_container = cortex_mikado_add_admin_container(
			array(
				'name' => 'mkdf_blog_single_navigation_container',
				'hidden_property' => 'blog_single_navigation',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type'        => 'yesno',
				'name' => 'blog_navigation_through_same_category',
				'default_value' => 'no',
				'label'       => esc_html__('Enable Navigation Only in Current Category','cortex'),
				'description' => esc_html__('Limit your navigation only through current category','cortex'),
				'parent'      => $blog_single_navigation_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_author_info',
				'default_value' => 'no',
				'label' => esc_html__('Show Author Info Box','cortex'),
				'parent' => $panel_blog_single,
				'description' => esc_html__('Enabling this option will display author name and descriptions on Blog Single pages','cortex'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_mkdf_blog_single_author_info_container'
				)
			)
		);

		$blog_single_author_info_container = cortex_mikado_add_admin_container(
			array(
				'name' => 'mkdf_blog_single_author_info_container',
				'hidden_property' => 'blog_author_info',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		cortex_mikado_add_admin_field(
			array(
				'type'        => 'yesno',
				'name' => 'blog_author_info_email',
				'default_value' => 'no',
				'label'       => esc_html__('Show Author Email','cortex'),
				'description' => esc_html__('Enabling this option will show author email','cortex'),
				'parent'      => $blog_single_author_info_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

	}

	add_action( 'cortex_mikado_options_map', 'cortex_mikado_blog_options_map', 11);

}











