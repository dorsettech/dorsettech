<?php

class CortexMikadoFullScreenMenuOpener extends CortexMikadoWidget {
    public function __construct() {
        parent::__construct(
            'mkdf_full_screen_menu_opener', // Base ID
            esc_html__('Mikado Full Screen Menu Opener','cortex') // Name
        );

		$this->setParams();
    }

	protected function setParams() {

		$this->params = array(
			array(
				'name'			=> 'fullscreen_menu_opener_icon_color',
				'type'			=> 'textfield',
				'title'			=> esc_html__('Icon Color',  'cortex'),
				'description'	=> esc_html__('Define color for Side Area opener icon','cortex'),
			)
		);

	}

    public function widget($args, $instance) {

		$fullscreen_icon_styles = array();

		if ( !empty($instance['fullscreen_menu_opener_icon_color']) ) {
			$fullscreen_icon_styles[] = 'color: ' . $instance['fullscreen_menu_opener_icon_color'];
		}


		$icon_size = '';
		if (cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_size') !== '' ) {
			$icon_size = cortex_mikado_options()->getOptionValue('fullscreen_menu_icon_size');
		}
		?>

        <a href="javascript:void(0)" class="mkdf-fullscreen-menu-opener <?php echo esc_attr( $icon_size )?>">
            <span class="mkdf-fullscreen-menu-opener-inner">
				<span class="mkdf-fullscreen-icon icon_menu" <?php cortex_mikado_inline_style($fullscreen_icon_styles); ?>></span>
				<span class="mkdf-fullscreen-icon-close ion-close"></span>
            </span>
        </a>
    <?php }

}