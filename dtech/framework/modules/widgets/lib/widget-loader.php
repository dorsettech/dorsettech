<?php

if (!function_exists('cortex_mikado_register_widgets')) {

	function cortex_mikado_register_widgets() {

		$widgets = array(
			'CortexMikadoFullScreenMenuOpener',
			'CortexMikadoLatestPosts',
            'CortexMikadoRawHTMLWidget',
			'CortexMikadoSearchOpener',
			'CortexMikadoSideAreaOpener',
			'CortexMikadoStickySidebar',
			'CortexMikadoSocialIconWidget',
			'CortexMikadoSeparatorWidget'
		);

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'cortex_mikado_register_widgets');