<?php

/**
 * Widget that adds search icon that triggers opening of search form
 *
 * Class Mikado_Search_Opener
 */
class CortexMikadoSearchOpener extends CortexMikadoWidget {
    /**
     * Set basic widget options and call parent class construct
     */
    public function __construct() {
        parent::__construct(
            'mkd_search_opener', // Base ID
            esc_html__('Mikado Search Opener','cortex') // Name
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'name'        => 'search_icon_size',
                'type'        => 'textfield',
                'title'       => esc_html__('Search Icon Size (px)', 'cortex'),
                'description' => esc_html__('Define size for Search icon', 'cortex'),
            ),
            array(
                'name'        => 'search_icon_color',
                'type'        => 'textfield',
                'title'       => esc_html__('Search Icon Color', 'cortex'),
                'description' => esc_html__('Define color for Search icon', 'cortex'),
            ),
            array(
                'name'        => 'search_icon_hover_color',
                'type'        => 'textfield',
                'title'       => esc_html__('Search Icon Hover Color', 'cortex'),
                'description' => esc_html__('Define hover color for Search icon', 'cortex'),
            ),
            array(
                'name'        => 'show_label',
                'type'        => 'dropdown',
                'title'       => esc_html__('Enable Search Icon Text', 'cortex'),
                'description' => esc_html__('Enable this option to show \'Search\' text next to search icon in header', 'cortex'),
                'options'     => array(
                    ''    => '',
                    'yes' => esc_html__('Yes', 'cortex'),
                    'no'  => esc_html__('No', 'cortex'),
                )
            ),
			array(
				'name'			=> 'close_icon_position',
				'type'			=> 'dropdown',
				'title'			=> esc_html__('Close icon stays on opener place', 'cortex'),
				'description'	=> esc_html__('Enable this option to set close icon on same position like opener icon', 'cortex'),
				'options'		=> array(
					'yes'	=> esc_html__('Yes', 'cortex'),
					'no'	=> esc_html__('No', 'cortex'),
				)
			)
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        global $cortex_mikado_options, $cortex_mikado_IconCollections;

        $search_type_class    = 'mkdf-search-opener';
		$fullscreen_search_overlay = false;
        $search_opener_styles = array();
        $show_search_text     = $instance['show_label'] == 'yes' || $cortex_mikado_options['enable_search_icon_text'] == 'yes' ? true : false;
		$close_icon_on_same_position = $instance['close_icon_position'] == 'yes' ? true : false;
		$data = '';

		if (isset($cortex_mikado_options['search_type']) && $cortex_mikado_options['search_type'] == 'fullscreen-search') {
			if (isset($cortex_mikado_options['search_animation']) && $cortex_mikado_options['search_animation'] == 'search-from-circle') {
				$fullscreen_search_overlay = true;
			}
		}

        if(isset($cortex_mikado_options['search_type']) && $cortex_mikado_options['search_type'] == 'search_covers_header') {
            if(isset($cortex_mikado_options['search_cover_only_bottom_yesno']) && $cortex_mikado_options['search_cover_only_bottom_yesno'] == 'yes') {
                $search_type_class .= ' search_covers_only_bottom';
            }
        }

        if(!empty($instance['search_icon_size'])) {
            $search_opener_styles[] = 'font-size: '.$instance['search_icon_size'].'px';
        }
        
        if(!empty($instance['search_icon_color'])) {
            $search_opener_styles[] = 'color: '.$instance['search_icon_color'];
			$data .= 'data-color='.$instance['search_icon_color'];
        }


		if ($instance['search_icon_hover_color'] !== ''){
			$data .= ' data-hover-color='.$instance['search_icon_hover_color'];
		}

        ?>

        <a <?php echo esc_attr($data); ?>
			<?php if ( $close_icon_on_same_position ) {
				echo cortex_mikado_get_inline_attr('yes', 'data-icon-close-same-position');
			} ?>
            <?php cortex_mikado_inline_style($search_opener_styles); ?>
            <?php cortex_mikado_class_attribute($search_type_class); ?> href="javascript:void(0)">
            <?php if(isset($cortex_mikado_options['search_icon_pack'])) {
                $cortex_mikado_IconCollections->getSearchIcon($cortex_mikado_options['search_icon_pack'], false);
            } ?>
            <?php if($show_search_text) { ?>
                <span class="mkdf-search-icon-text"><?php esc_html_e('Search', 'cortex'); ?></span>
            <?php } ?>
        </a>
		<?php if($fullscreen_search_overlay) { ?>
			<div class="mkdf-fullscreen-search-overlay"></div>
		<?php } ?>
    <?php }
}