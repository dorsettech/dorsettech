<?php

class CortexMikadoSideAreaOpener extends CortexMikadoWidget {
    public function __construct() {
        parent::__construct(
            'mkdf_side_area_opener', // Base ID
            esc_html__('Mikado Side Area Opener','cortex') // Name
        );

        $this->setParams();
    }

    protected function setParams() {

		$this->params = array(
			array(
				'name'			=> 'side_area_opener_icon_color',
				'type'			=> 'textfield',
				'title'			=> esc_html__('Icon Color', 'cortex'),
				'description'	=> esc_html__('Define color for Side Area opener icon', 'cortex'),
			)
		);

    }


    public function widget($args, $instance) {
		
		$sidearea_icon_styles = array();

		if ( !empty($instance['side_area_opener_icon_color']) ) {
			$sidearea_icon_styles[] = 'color: ' . $instance['side_area_opener_icon_color'];
		}
		
		$icon_size = '';
		if ( cortex_mikado_options()->getOptionValue('side_area_predefined_icon_size') ) {
			$icon_size = cortex_mikado_options()->getOptionValue('side_area_predefined_icon_size');
		}
		?>
        <a class="mkdf-side-menu-button-opener <?php echo esc_attr( $icon_size ); ?>" <?php cortex_mikado_inline_style($sidearea_icon_styles) ?> href="javascript:void(0)">
            <?php// echo cortex_mikado_get_side_menu_icon_html(); ?>
            <img src="/wp-content/uploads/batphone4.gif" alt="Web Design Dorset" id="batphonhead"/>
        </a>

    <?php }

}