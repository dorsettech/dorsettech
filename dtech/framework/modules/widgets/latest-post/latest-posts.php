<?php

class CortexMikadoLatestPosts extends CortexMikadoWidget {
	protected $params;
	public function __construct() {
		parent::__construct(
			'mkdf_latest_posts_widget', // Base ID
			esc_html__('Mikado Latest Post','cortex'), // Name
			array( 'description' => esc_html__( 'Display posts from your blog', 'cortex' ), ) // Args
		);

		$this->setParams();
	}

	protected function setParams() {
		$this->params = array(
			array(
				'name' => 'number_of_posts',
				'type' => 'textfield',
				'title' => esc_html__('Number of posts', 'cortex'),
			),
			array(
				'name' => 'order_by',
				'type' => 'dropdown',
				'title' => esc_html__('Order By', 'cortex'),
				'options' => array(
					'title' => esc_html__('Title', 'cortex'),
					'date' => esc_html__('Date', 'cortex'),
				)
			),
			array(
				'name' => 'order',
				'type' => 'dropdown',
				'title' => 'Order',
				'options' => array(
					'ASC' => esc_html__('ASC', 'cortex'),
					'DESC' => esc_html__('DESC', 'cortex'),
				)
			),
			array(
				'name' => 'category',
				'type' => 'textfield',
				'title' => esc_html__('Category Slug', 'cortex'),
			),
			array(
				'name' => 'text_length',
				'type' => 'textfield',
				'title' => esc_html__('Number of characters', 'cortex'),
			),
			array(
				'name' => 'title_tag',
				'type' => 'dropdown',
				'title' => esc_html__('Title Tag','cortex'),
				'options' => array(
					""   => "",
					"h2" => "h2",
					"h3" => "h3",
					"h4" => "h4",
					"h5" => "h5",
					"h6" => "h6"
				)
			)			
		);
	}

	public function widget($args, $instance) {
		extract($args);

		//prepare variables
		$content        = '';
		$params         = array();
		$params['type'] = 'minimal';
		//is instance empty?
		if(is_array($instance) && count($instance)) {
			//generate shortcode params
			foreach($instance as $key => $value) {
				$params[$key] = $value;
			}
		}
		if(empty($params['title_tag'])){
			$params['title_tag'] = 'h6';
		}
		echo '<div class="widget mkdf-latest-posts-widget">';
		
		echo cortex_mikado_execute_shortcode('mkdf_blog_list', $params);

		echo '</div>'; //close mkdf-latest-posts-widget
	}
}
