<?php

/**
 * Widget that adds separator boxes type
 *
 * Class Separator_Widget
 */
class CortexMikadoSeparatorWidget extends CortexMikadoWidget {
    /**
     * Set basic widget options and call parent class construct
     */
    public function __construct() {
        parent::__construct(
            'mkd_separator_widget', // Base ID
            esc_html__('Mikado Separator Widget','cortex') // Name
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'type' => 'dropdown',
                'title' => esc_html__('Type', 'cortex'),
                'name' => 'type',
                'options' => array(
                    'normal' => esc_html__('Normal', 'cortex'),
                    'full-width' => esc_html__('Full Width', 'cortex'),
                )
            ),
            array(
                'type' => 'dropdown',
                'title' => esc_html__('Position', 'cortex'),
                'name' => 'position',
                'options' => array(
                    'center' => esc_html__('Center', 'cortex'),
                    'left' => esc_html__('Left', 'cortex'),
                    'right' => esc_html__('Right', 'cortex'),
                )
            ),
            array(
                'type' => 'dropdown',
                'title' => esc_html__('Style', 'cortex'),
                'name' => 'border_style',
                'options' => array(
                    'solid' => esc_html__('Solid', 'cortex'),
                    'dashed' => esc_html__('Dashed', 'cortex'),
                    'dotted' => esc_html__('Dotted','cortex'),
                )
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Color', 'cortex'),
                'name' => 'color'
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Width', 'cortex'),
                'name' => 'width',
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Thickness (px)', 'cortex'),
                'name' => 'thickness',
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Top Margin', 'cortex'),
                'name' => 'top_margin',
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Bottom Margin', 'cortex'),
                'name' => 'bottom_margin',
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {

        extract($args);

        //prepare variables
        $params = '';

        //is instance empty?
        if(is_array($instance) && count($instance)) {
            //generate shortcode params
            foreach($instance as $key => $value) {
                $params .= " $key='$value' ";
            }
        }

        echo '<div class="widget mkdf-separator-widget">';

        //finally call the shortcode
        echo do_shortcode("[mkdf_separator $params]"); // XSS OK

        echo '</div>'; //close div.mkdf-separator-widget
    }
}